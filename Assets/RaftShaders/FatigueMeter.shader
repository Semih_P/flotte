// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.25 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.25;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1873,x:33229,y:32719,varname:node_1873,prsc:2|emission-1749-OUT,alpha-603-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32127,y:32698,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1086,x:32388,y:32787,cmnt:RGB,varname:node_1086,prsc:2|A-4805-RGB,B-3039-OUT,C-5376-RGB;n:type:ShaderForge.SFN_Color,id:5983,x:31725,y:32739,ptovrint:False,ptlb:Color1,ptin:_Color1,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32127,y:33048,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1749,x:32601,y:32787,cmnt:Premultiply Alpha,varname:node_1749,prsc:2|A-1086-OUT,B-603-OUT;n:type:ShaderForge.SFN_Multiply,id:603,x:32388,y:32961,cmnt:A,varname:node_603,prsc:2|A-4805-A,B-5983-A,C-5376-A;n:type:ShaderForge.SFN_Lerp,id:3039,x:32069,y:32882,varname:node_3039,prsc:2|A-5983-RGB,B-1442-RGB,T-846-OUT;n:type:ShaderForge.SFN_Color,id:1442,x:31725,y:32920,ptovrint:False,ptlb:Color2,ptin:_Color2,varname:node_1442,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Time,id:3630,x:31445,y:33340,varname:node_3630,prsc:2;n:type:ShaderForge.SFN_Sin,id:3152,x:31833,y:33359,varname:node_3152,prsc:2|IN-976-OUT;n:type:ShaderForge.SFN_RemapRange,id:8445,x:32161,y:33388,varname:node_8445,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-3152-OUT;n:type:ShaderForge.SFN_Slider,id:5566,x:31069,y:33259,ptovrint:False,ptlb:FatigueValue,ptin:_FatigueValue,varname:node_5566,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8402407,max:1;n:type:ShaderForge.SFN_Lerp,id:846,x:31868,y:33062,varname:node_846,prsc:2|A-2324-OUT,B-8445-OUT,T-5566-OUT;n:type:ShaderForge.SFN_Vector1,id:2324,x:31521,y:33064,varname:node_2324,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:9255,x:31105,y:33539,varname:node_9255,prsc:2,v1:8;n:type:ShaderForge.SFN_Multiply,id:1799,x:31445,y:33524,varname:node_1799,prsc:2|A-5566-OUT,B-9255-OUT;n:type:ShaderForge.SFN_Multiply,id:976,x:31651,y:33439,varname:node_976,prsc:2|A-3630-T,B-1799-OUT;proporder:4805-5983-1442-5566;pass:END;sub:END;*/

Shader "Shader Forge/Fatique2" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        _Color1 ("Color1", Color) = (1,1,1,1)
        _Color2 ("Color2", Color) = (1,0,0,1)
        _FatigueValue ("FatigueValue", Range(0, 1)) = 0.8402407
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color1;
            uniform float4 _Color2;
            uniform float _FatigueValue;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_3630 = _Time + _TimeEditor;
                float node_603 = (_MainTex_var.a*_Color1.a*i.vertexColor.a); // A
                float3 emissive = ((_MainTex_var.rgb*lerp(_Color1.rgb,_Color2.rgb,lerp(0.0,(sin((node_3630.g*(_FatigueValue*8.0)))*0.5+0.5),_FatigueValue))*i.vertexColor.rgb)*node_603);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_603);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
