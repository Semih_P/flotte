// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:2,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-7241-RGB;n:type:ShaderForge.SFN_Color,id:7241,x:32471,y:32812,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_FragmentPosition,id:7829,x:29029,y:33211,varname:node_7829,prsc:2;n:type:ShaderForge.SFN_Normalize,id:111,x:29226,y:33211,varname:node_111,prsc:2|IN-7829-XYZ;n:type:ShaderForge.SFN_ComponentMask,id:5395,x:29741,y:33211,varname:node_5395,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-111-OUT;n:type:ShaderForge.SFN_Abs,id:3881,x:29938,y:33211,varname:node_3881,prsc:2|IN-5395-OUT;n:type:ShaderForge.SFN_OneMinus,id:8357,x:30134,y:33211,varname:node_8357,prsc:2|IN-3881-OUT;n:type:ShaderForge.SFN_Power,id:8645,x:30382,y:33211,varname:node_8645,prsc:2|VAL-8357-OUT,EXP-7713-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7713,x:30090,y:33420,ptovrint:False,ptlb:Horizon Sharpness,ptin:_HorizonSharpness,varname:node_7713,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:8;n:type:ShaderForge.SFN_ComponentMask,id:7273,x:30386,y:30987,varname:node_7273,prsc:2,cc1:0,cc2:2,cc3:-1,cc4:-1|IN-111-OUT;n:type:ShaderForge.SFN_ComponentMask,id:6258,x:30386,y:31176,varname:node_6258,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-111-OUT;n:type:ShaderForge.SFN_Clamp01,id:3088,x:30556,y:31176,varname:node_3088,prsc:2|IN-6258-OUT;n:type:ShaderForge.SFN_OneMinus,id:3474,x:30744,y:31176,varname:node_3474,prsc:2|IN-3088-OUT;n:type:ShaderForge.SFN_Power,id:4595,x:30817,y:31313,varname:node_4595,prsc:2|VAL-3088-OUT,EXP-7957-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7957,x:30604,y:31409,ptovrint:False,ptlb:StarPosition,ptin:_StarPosition,varname:node_7957,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Multiply,id:9044,x:30959,y:31070,varname:node_9044,prsc:2|A-7273-OUT,B-3474-OUT;n:type:ShaderForge.SFN_Add,id:5719,x:31118,y:31004,varname:node_5719,prsc:2|A-7273-OUT,B-9044-OUT;n:type:ShaderForge.SFN_Tex2d,id:5633,x:31361,y:30987,ptovrint:False,ptlb:StarsTexture,ptin:_StarsTexture,varname:node_5633,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5719-OUT;n:type:ShaderForge.SFN_Multiply,id:7843,x:31609,y:31055,varname:node_7843,prsc:2|A-5633-G,B-4595-OUT;n:type:ShaderForge.SFN_Color,id:753,x:30432,y:31819,ptovrint:False,ptlb:Night,ptin:_Night,varname:node_753,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Power,id:860,x:30655,y:31971,varname:node_860,prsc:2|VAL-753-RGB,EXP-753-A;n:type:ShaderForge.SFN_Lerp,id:5234,x:30910,y:31971,varname:node_5234,prsc:2|A-753-RGB,B-860-OUT,T-3297-OUT;n:type:ShaderForge.SFN_Power,id:7973,x:31122,y:31971,varname:node_7973,prsc:2|VAL-5234-OUT,EXP-6371-OUT;n:type:ShaderForge.SFN_Color,id:4895,x:30420,y:32164,ptovrint:False,ptlb:DayTop,ptin:_DayTop,varname:node_4895,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Power,id:5420,x:30643,y:32271,varname:node_5420,prsc:2|VAL-4895-RGB,EXP-4895-A;n:type:ShaderForge.SFN_Lerp,id:9488,x:30906,y:32251,varname:node_9488,prsc:2|A-4895-RGB,B-5420-OUT,T-3297-OUT;n:type:ShaderForge.SFN_Power,id:7122,x:31122,y:32251,varname:node_7122,prsc:2|VAL-9488-OUT,EXP-7920-OUT;n:type:ShaderForge.SFN_OneMinus,id:6371,x:30906,y:32120,varname:node_6371,prsc:2|IN-7920-OUT;n:type:ShaderForge.SFN_Slider,id:7920,x:30182,y:32484,ptovrint:False,ptlb:Cycle,ptin:_Cycle,varname:node_7920,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:9997,x:31841,y:32188,varname:node_9997,prsc:2|A-5387-OUT,B-7122-OUT,T-7920-OUT;n:type:ShaderForge.SFN_Multiply,id:637,x:31450,y:31847,varname:node_637,prsc:2|A-7843-OUT,B-6587-OUT,C-7973-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6587,x:31138,y:31694,ptovrint:False,ptlb:StarIntensity,ptin:_StarIntensity,varname:node_6587,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:5387,x:31603,y:31969,varname:node_5387,prsc:2|A-637-OUT,B-7973-OUT;n:type:ShaderForge.SFN_Relay,id:3297,x:30591,y:32526,varname:node_3297,prsc:2|IN-8357-OUT;n:type:ShaderForge.SFN_Add,id:1595,x:31249,y:32832,varname:node_1595,prsc:2|A-76-OUT,B-5395-OUT;n:type:ShaderForge.SFN_Vector1,id:8760,x:30790,y:32812,varname:node_8760,prsc:2,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:3548,x:30766,y:32878,ptovrint:False,ptlb:GroundSharpness,ptin:_GroundSharpness,varname:node_3548,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.005;n:type:ShaderForge.SFN_Multiply,id:76,x:30993,y:32812,varname:node_76,prsc:2|A-8760-OUT,B-3548-OUT;n:type:ShaderForge.SFN_Divide,id:2723,x:31428,y:32892,varname:node_2723,prsc:2|A-1595-OUT,B-3548-OUT;n:type:ShaderForge.SFN_Clamp01,id:8007,x:31608,y:32892,varname:node_8007,prsc:2|IN-2723-OUT;n:type:ShaderForge.SFN_OneMinus,id:884,x:31094,y:33305,varname:node_884,prsc:2|IN-8645-OUT;n:type:ShaderForge.SFN_Multiply,id:4583,x:31282,y:33305,varname:node_4583,prsc:2|A-884-OUT,B-164-OUT;n:type:ShaderForge.SFN_ValueProperty,id:164,x:31094,y:33472,ptovrint:False,ptlb:Light Eccentricity,ptin:_LightEccentricity,varname:node_164,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_LightColor,id:5366,x:31276,y:33121,varname:node_5366,prsc:2;n:type:ShaderForge.SFN_Power,id:3347,x:31513,y:33121,varname:node_3347,prsc:2|VAL-5366-RGB,EXP-4583-OUT;n:type:ShaderForge.SFN_LightVector,id:5194,x:29796,y:34258,varname:node_5194,prsc:2;n:type:ShaderForge.SFN_Dot,id:334,x:30025,y:34168,varname:node_334,prsc:2,dt:0|A-111-OUT,B-5194-OUT;n:type:ShaderForge.SFN_Clamp01,id:7392,x:30210,y:34178,varname:node_7392,prsc:2|IN-334-OUT;n:type:ShaderForge.SFN_Power,id:1760,x:30503,y:34188,varname:node_1760,prsc:2|VAL-7392-OUT,EXP-4518-OUT;n:type:ShaderForge.SFN_Exp,id:4518,x:30275,y:34404,varname:node_4518,prsc:2,et:0|IN-6335-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6335,x:30061,y:34421,ptovrint:False,ptlb:Sun Size,ptin:_SunSize,varname:node_6335,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:6.5;n:type:ShaderForge.SFN_Multiply,id:6912,x:30773,y:34196,varname:node_6912,prsc:2|A-1760-OUT,B-6566-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1137,x:30466,y:34472,ptovrint:False,ptlb:Sun Sharpness,ptin:_SunSharpness,varname:node_1137,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Exp,id:6566,x:30630,y:34472,varname:node_6566,prsc:2,et:0|IN-1137-OUT;n:type:ShaderForge.SFN_Clamp01,id:4486,x:30974,y:34196,varname:node_4486,prsc:2|IN-6912-OUT;n:type:ShaderForge.SFN_Add,id:5311,x:30513,y:33763,varname:node_5311,prsc:2|A-1089-OUT,B-334-OUT;n:type:ShaderForge.SFN_Vector1,id:1089,x:30324,y:33763,varname:node_1089,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:3230,x:30664,y:33727,varname:node_3230,prsc:2|A-9054-OUT,B-5311-OUT;n:type:ShaderForge.SFN_Vector1,id:9054,x:30474,y:33727,varname:node_9054,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Power,id:2837,x:30904,y:33661,varname:node_2837,prsc:2|VAL-3230-OUT,EXP-9857-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9857,x:30664,y:33859,ptovrint:False,ptlb:GroundHorizonRelations,ptin:_GroundHorizonRelations,varname:node_9857,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Add,id:5916,x:31061,y:33749,varname:node_5916,prsc:2|A-2837-OUT,B-3230-OUT;n:type:ShaderForge.SFN_OneMinus,id:1503,x:31569,y:33594,varname:node_1503,prsc:2|IN-4486-OUT;n:type:ShaderForge.SFN_Multiply,id:10,x:31762,y:33544,varname:node_10,prsc:2|A-164-OUT,B-1503-OUT;n:type:ShaderForge.SFN_Power,id:8977,x:31981,y:33475,varname:node_8977,prsc:2|VAL-5366-RGB,EXP-10-OUT;proporder:7241;pass:END;sub:END;*/

Shader "Shader Forge/Skybox" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
            "PreviewType"="Skybox"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float3 emissive = _Color.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
