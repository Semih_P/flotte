// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:1,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:33268,y:32536,varname:node_2865,prsc:2|diff-8014-OUT,spec-358-OUT,gloss-8959-OUT,normal-9833-OUT;n:type:ShaderForge.SFN_Color,id:6665,x:32547,y:31163,ptovrint:False,ptlb:RockColor1,ptin:_RockColor1,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5964,x:32103,y:33705,ptovrint:True,ptlb:Normal Map,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Slider,id:358,x:32619,y:32632,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:node_358,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:1813,x:31749,y:32896,ptovrint:False,ptlb:Roughness,ptin:_Roughness,varname:_Metallic_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8,max:1;n:type:ShaderForge.SFN_Tex2d,id:3693,x:32103,y:33525,ptovrint:False,ptlb:NormalDetail,ptin:_NormalDetail,varname:node_3693,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True|UVIN-3390-OUT;n:type:ShaderForge.SFN_NormalBlend,id:5355,x:32438,y:33669,varname:node_5355,prsc:2|BSE-5964-RGB,DTL-3693-RGB;n:type:ShaderForge.SFN_TexCoord,id:7040,x:30209,y:32851,varname:node_7040,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:3390,x:30401,y:32954,varname:node_3390,prsc:2|A-7040-UVOUT,B-9572-OUT;n:type:ShaderForge.SFN_Slider,id:8628,x:29782,y:33104,ptovrint:False,ptlb:NormalDetail tiling,ptin:_NormalDetailtiling,varname:node_8628,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:20;n:type:ShaderForge.SFN_ObjectScale,id:1463,x:29939,y:32915,varname:node_1463,prsc:2,rcp:False;n:type:ShaderForge.SFN_Multiply,id:9572,x:30209,y:33001,varname:node_9572,prsc:2|A-1463-X,B-8628-OUT;n:type:ShaderForge.SFN_Tex2d,id:3946,x:31075,y:32186,ptovrint:False,ptlb:SandMask,ptin:_SandMask,varname:node_3946,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-3390-OUT;n:type:ShaderForge.SFN_Lerp,id:8014,x:32657,y:32379,varname:node_8014,prsc:2|A-4983-RGB,B-7789-OUT,T-3493-OUT;n:type:ShaderForge.SFN_NormalVector,id:8683,x:30515,y:31864,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:8979,x:30760,y:31884,varname:node_8979,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-8683-OUT;n:type:ShaderForge.SFN_Lerp,id:1562,x:31409,y:32298,varname:node_1562,prsc:2|A-8874-OUT,B-3946-RGB,T-9624-OUT;n:type:ShaderForge.SFN_Vector3,id:8874,x:30975,y:32401,varname:node_8874,prsc:2,v1:1,v2:1,v3:1;n:type:ShaderForge.SFN_Power,id:9624,x:31342,y:31948,varname:node_9624,prsc:2|VAL-8979-OUT,EXP-1542-OUT;n:type:ShaderForge.SFN_Vector1,id:1542,x:30858,y:32066,varname:node_1542,prsc:2,v1:6;n:type:ShaderForge.SFN_Lerp,id:9833,x:32509,y:33357,varname:node_9833,prsc:2|A-4973-RGB,B-5355-OUT,T-2624-OUT;n:type:ShaderForge.SFN_Power,id:4525,x:31616,y:32298,varname:node_4525,prsc:2|VAL-1562-OUT,EXP-4600-OUT;n:type:ShaderForge.SFN_Vector1,id:4600,x:31356,y:32489,varname:node_4600,prsc:2,v1:10;n:type:ShaderForge.SFN_Tex2d,id:4983,x:31760,y:32619,ptovrint:False,ptlb:SandDiffuse,ptin:_SandDiffuse,varname:node_4983,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5383-OUT;n:type:ShaderForge.SFN_Multiply,id:5383,x:31509,y:32656,varname:node_5383,prsc:2|A-7255-OUT,B-5276-OUT;n:type:ShaderForge.SFN_Slider,id:5276,x:30999,y:32909,ptovrint:False,ptlb:SandTile,ptin:_SandTile,varname:node_5276,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:30,max:40;n:type:ShaderForge.SFN_Tex2d,id:4973,x:31565,y:33493,ptovrint:False,ptlb:SandNormal,ptin:_SandNormal,varname:node_4973,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True|UVIN-6217-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:8389,x:31034,y:33493,varname:node_8389,prsc:2;n:type:ShaderForge.SFN_ComponentMask,id:7255,x:31203,y:33493,varname:node_7255,prsc:2,cc1:0,cc2:2,cc3:-1,cc4:-1|IN-8389-XYZ;n:type:ShaderForge.SFN_Multiply,id:6217,x:31378,y:33493,varname:node_6217,prsc:2|A-7255-OUT,B-7366-OUT;n:type:ShaderForge.SFN_Slider,id:7366,x:31034,y:33660,ptovrint:False,ptlb:SandNormalTile,ptin:_SandNormalTile,varname:node_7366,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:40;n:type:ShaderForge.SFN_Clamp01,id:3493,x:32017,y:31995,varname:node_3493,prsc:2|IN-5711-OUT;n:type:ShaderForge.SFN_Lerp,id:4632,x:32279,y:33039,varname:node_4632,prsc:2|A-7942-OUT,B-1813-OUT,T-2624-OUT;n:type:ShaderForge.SFN_Vector1,id:7942,x:31857,y:33043,varname:node_7942,prsc:2,v1:1;n:type:ShaderForge.SFN_ComponentMask,id:8959,x:32496,y:33039,varname:node_8959,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-4632-OUT;n:type:ShaderForge.SFN_Set,id:7486,x:32378,y:31862,varname:SandMask,prsc:2|IN-3493-OUT;n:type:ShaderForge.SFN_Get,id:2624,x:31788,y:33267,varname:node_2624,prsc:2|IN-7486-OUT;n:type:ShaderForge.SFN_Lerp,id:7789,x:33280,y:31979,varname:node_7789,prsc:2|A-5278-RGB,B-3007-OUT,T-3946-RGB;n:type:ShaderForge.SFN_Color,id:5278,x:33011,y:32064,ptovrint:False,ptlb:Rockcolor2,ptin:_Rockcolor2,varname:node_5278,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_RgbToHsv,id:4060,x:33043,y:31310,varname:node_4060,prsc:2|IN-6665-RGB;n:type:ShaderForge.SFN_HsvToRgb,id:3007,x:33720,y:31431,varname:node_3007,prsc:2|H-4060-HOUT,S-4060-SOUT,V-4573-OUT;n:type:ShaderForge.SFN_Tex2d,id:3977,x:32791,y:31523,ptovrint:False,ptlb:SatClouds,ptin:_SatClouds,varname:node_3977,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-7896-OUT;n:type:ShaderForge.SFN_RemapRange,id:7687,x:33233,y:31560,varname:node_7687,prsc:2,frmn:0,frmx:1,tomn:0,tomx:0|IN-3933-OUT;n:type:ShaderForge.SFN_Multiply,id:7896,x:32584,y:31523,varname:node_7896,prsc:2|A-7040-UVOUT,B-5939-OUT;n:type:ShaderForge.SFN_Slider,id:5939,x:32162,y:31698,ptovrint:False,ptlb:SaturationTile,ptin:_SaturationTile,varname:node_5939,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:10;n:type:ShaderForge.SFN_Posterize,id:3933,x:33043,y:31560,varname:node_3933,prsc:2|IN-3977-R,STPS-336-OUT;n:type:ShaderForge.SFN_Vector1,id:336,x:32750,y:31740,varname:node_336,prsc:2,v1:5;n:type:ShaderForge.SFN_Add,id:4573,x:33490,y:31320,varname:node_4573,prsc:2|A-4060-VOUT,B-7687-OUT;n:type:ShaderForge.SFN_Subtract,id:5711,x:31838,y:31995,varname:node_5711,prsc:2|A-4525-OUT,B-9624-OUT;n:type:ShaderForge.SFN_OneMinus,id:2437,x:31863,y:33428,varname:node_2437,prsc:2|IN-4973-RGB;proporder:5964-6665-5278-358-1813-3693-8628-3946-4983-5276-4973-7366-3977-5939;pass:END;sub:END;*/

Shader "Shader Forge/RockShader" {
    Properties {
        _BumpMap ("Normal Map", 2D) = "bump" {}
        _RockColor1 ("RockColor1", Color) = (0.5019608,0.5019608,0.5019608,1)
        _Rockcolor2 ("Rockcolor2", Color) = (0.5,0.5,0.5,1)
        _Metallic ("Metallic", Range(0, 1)) = 0
        _Roughness ("Roughness", Range(0, 1)) = 0.8
        _NormalDetail ("NormalDetail", 2D) = "bump" {}
        _NormalDetailtiling ("NormalDetail tiling", Range(1, 20)) = 1
        _SandMask ("SandMask", 2D) = "white" {}
        _SandDiffuse ("SandDiffuse", 2D) = "white" {}
        _SandTile ("SandTile", Range(0, 40)) = 30
        _SandNormal ("SandNormal", 2D) = "bump" {}
        _SandNormalTile ("SandNormalTile", Range(0, 40)) = 0
        _SatClouds ("SatClouds", 2D) = "white" {}
        _SaturationTile ("SaturationTile", Range(0, 10)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "DEFERRED"
            Tags {
                "LightMode"="Deferred"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_DEFERRED
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile ___ UNITY_HDR_ON
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _RockColor1;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Metallic;
            uniform float _Roughness;
            uniform sampler2D _NormalDetail; uniform float4 _NormalDetail_ST;
            uniform float _NormalDetailtiling;
            uniform sampler2D _SandMask; uniform float4 _SandMask_ST;
            uniform sampler2D _SandDiffuse; uniform float4 _SandDiffuse_ST;
            uniform float _SandTile;
            uniform sampler2D _SandNormal; uniform float4 _SandNormal_ST;
            uniform float _SandNormalTile;
            uniform float4 _Rockcolor2;
            uniform sampler2D _SatClouds; uniform float4 _SatClouds_ST;
            uniform float _SaturationTile;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD7;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            void frag(
                VertexOutput i,
                out half4 outDiffuse : SV_Target0,
                out half4 outSpecSmoothness : SV_Target1,
                out half4 outNormal : SV_Target2,
                out half4 outEmission : SV_Target3 )
            {
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 node_7255 = i.posWorld.rgb.rb;
                float2 node_6217 = (node_7255*_SandNormalTile);
                float3 _SandNormal_var = UnpackNormal(tex2D(_SandNormal,TRANSFORM_TEX(node_6217, _SandNormal)));
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float2 node_3390 = (i.uv0*(objScale.r*_NormalDetailtiling));
                float3 _NormalDetail_var = UnpackNormal(tex2D(_NormalDetail,TRANSFORM_TEX(node_3390, _NormalDetail)));
                float3 node_5355_nrm_base = _BumpMap_var.rgb + float3(0,0,1);
                float3 node_5355_nrm_detail = _NormalDetail_var.rgb * float3(-1,-1,1);
                float3 node_5355_nrm_combined = node_5355_nrm_base*dot(node_5355_nrm_base, node_5355_nrm_detail)/node_5355_nrm_base.z - node_5355_nrm_detail;
                float3 node_5355 = node_5355_nrm_combined;
                float4 _SandMask_var = tex2D(_SandMask,TRANSFORM_TEX(node_3390, _SandMask));
                float node_9624 = pow(i.normalDir.g,6.0);
                float3 node_3493 = saturate((pow(lerp(float3(1,1,1),_SandMask_var.rgb,node_9624),10.0)-node_9624));
                float3 SandMask = node_3493;
                float3 node_2624 = SandMask;
                float3 normalLocal = lerp(_SandNormal_var.rgb,node_5355,node_2624);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float node_7942 = 1.0;
                float gloss = 1.0 - lerp(float3(node_7942,node_7942,node_7942),float3(_Roughness,_Roughness,_Roughness),node_2624).r; // Convert roughness to gloss
                float perceptualRoughness = lerp(float3(node_7942,node_7942,node_7942),float3(_Roughness,_Roughness,_Roughness),node_2624).r;
                float roughness = perceptualRoughness * perceptualRoughness;
/////// GI Data:
                UnityLight light; // Dummy light
                light.color = 0;
                light.dir = half3(0,1,0);
                light.ndotl = max(0,dot(normalDirection,light.dir));
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = 1;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
////// Specular:
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float2 node_5383 = (node_7255*_SandTile);
                float4 _SandDiffuse_var = tex2D(_SandDiffuse,TRANSFORM_TEX(node_5383, _SandDiffuse));
                float4 node_4060_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_4060_p = lerp(float4(float4(_RockColor1.rgb,0.0).zy, node_4060_k.wz), float4(float4(_RockColor1.rgb,0.0).yz, node_4060_k.xy), step(float4(_RockColor1.rgb,0.0).z, float4(_RockColor1.rgb,0.0).y));
                float4 node_4060_q = lerp(float4(node_4060_p.xyw, float4(_RockColor1.rgb,0.0).x), float4(float4(_RockColor1.rgb,0.0).x, node_4060_p.yzx), step(node_4060_p.x, float4(_RockColor1.rgb,0.0).x));
                float node_4060_d = node_4060_q.x - min(node_4060_q.w, node_4060_q.y);
                float node_4060_e = 1.0e-10;
                float3 node_4060 = float3(abs(node_4060_q.z + (node_4060_q.w - node_4060_q.y) / (6.0 * node_4060_d + node_4060_e)), node_4060_d / (node_4060_q.x + node_4060_e), node_4060_q.x);;
                float2 node_7896 = (i.uv0*_SaturationTile);
                float4 _SatClouds_var = tex2D(_SatClouds,TRANSFORM_TEX(node_7896, _SatClouds));
                float node_336 = 5.0;
                float3 diffuseColor = lerp(_SandDiffuse_var.rgb,lerp(_Rockcolor2.rgb,(lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac(node_4060.r+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_4060.g)*(node_4060.b+(floor(_SatClouds_var.r * node_336) / (node_336 - 1)*0.0+0.0))),_SandMask_var.rgb),node_3493); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
/////// Diffuse:
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
/// Final Color:
                outDiffuse = half4( diffuseColor, 1 );
                outSpecSmoothness = half4( specularColor, gloss );
                outNormal = half4( normalDirection * 0.5 + 0.5, 1 );
                outEmission = half4(0,0,0,1);
                outEmission.rgb += indirectSpecular * 1;
                outEmission.rgb += indirectDiffuse * diffuseColor;
                #ifndef UNITY_HDR_ON
                    outEmission.rgb = exp2(-outEmission.rgb);
                #endif
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _RockColor1;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Metallic;
            uniform float _Roughness;
            uniform sampler2D _NormalDetail; uniform float4 _NormalDetail_ST;
            uniform float _NormalDetailtiling;
            uniform sampler2D _SandMask; uniform float4 _SandMask_ST;
            uniform sampler2D _SandDiffuse; uniform float4 _SandDiffuse_ST;
            uniform float _SandTile;
            uniform sampler2D _SandNormal; uniform float4 _SandNormal_ST;
            uniform float _SandNormalTile;
            uniform float4 _Rockcolor2;
            uniform sampler2D _SatClouds; uniform float4 _SatClouds_ST;
            uniform float _SaturationTile;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 node_7255 = i.posWorld.rgb.rb;
                float2 node_6217 = (node_7255*_SandNormalTile);
                float3 _SandNormal_var = UnpackNormal(tex2D(_SandNormal,TRANSFORM_TEX(node_6217, _SandNormal)));
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float2 node_3390 = (i.uv0*(objScale.r*_NormalDetailtiling));
                float3 _NormalDetail_var = UnpackNormal(tex2D(_NormalDetail,TRANSFORM_TEX(node_3390, _NormalDetail)));
                float3 node_5355_nrm_base = _BumpMap_var.rgb + float3(0,0,1);
                float3 node_5355_nrm_detail = _NormalDetail_var.rgb * float3(-1,-1,1);
                float3 node_5355_nrm_combined = node_5355_nrm_base*dot(node_5355_nrm_base, node_5355_nrm_detail)/node_5355_nrm_base.z - node_5355_nrm_detail;
                float3 node_5355 = node_5355_nrm_combined;
                float4 _SandMask_var = tex2D(_SandMask,TRANSFORM_TEX(node_3390, _SandMask));
                float node_9624 = pow(i.normalDir.g,6.0);
                float3 node_3493 = saturate((pow(lerp(float3(1,1,1),_SandMask_var.rgb,node_9624),10.0)-node_9624));
                float3 SandMask = node_3493;
                float3 node_2624 = SandMask;
                float3 normalLocal = lerp(_SandNormal_var.rgb,node_5355,node_2624);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float node_7942 = 1.0;
                float gloss = 1.0 - lerp(float3(node_7942,node_7942,node_7942),float3(_Roughness,_Roughness,_Roughness),node_2624).r; // Convert roughness to gloss
                float perceptualRoughness = lerp(float3(node_7942,node_7942,node_7942),float3(_Roughness,_Roughness,_Roughness),node_2624).r;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float2 node_5383 = (node_7255*_SandTile);
                float4 _SandDiffuse_var = tex2D(_SandDiffuse,TRANSFORM_TEX(node_5383, _SandDiffuse));
                float4 node_4060_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_4060_p = lerp(float4(float4(_RockColor1.rgb,0.0).zy, node_4060_k.wz), float4(float4(_RockColor1.rgb,0.0).yz, node_4060_k.xy), step(float4(_RockColor1.rgb,0.0).z, float4(_RockColor1.rgb,0.0).y));
                float4 node_4060_q = lerp(float4(node_4060_p.xyw, float4(_RockColor1.rgb,0.0).x), float4(float4(_RockColor1.rgb,0.0).x, node_4060_p.yzx), step(node_4060_p.x, float4(_RockColor1.rgb,0.0).x));
                float node_4060_d = node_4060_q.x - min(node_4060_q.w, node_4060_q.y);
                float node_4060_e = 1.0e-10;
                float3 node_4060 = float3(abs(node_4060_q.z + (node_4060_q.w - node_4060_q.y) / (6.0 * node_4060_d + node_4060_e)), node_4060_d / (node_4060_q.x + node_4060_e), node_4060_q.x);;
                float2 node_7896 = (i.uv0*_SaturationTile);
                float4 _SatClouds_var = tex2D(_SatClouds,TRANSFORM_TEX(node_7896, _SatClouds));
                float node_336 = 5.0;
                float3 diffuseColor = lerp(_SandDiffuse_var.rgb,lerp(_Rockcolor2.rgb,(lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac(node_4060.r+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_4060.g)*(node_4060.b+(floor(_SatClouds_var.r * node_336) / (node_336 - 1)*0.0+0.0))),_SandMask_var.rgb),node_3493); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                half surfaceReduction;
                #ifdef UNITY_COLORSPACE_GAMMA
                    surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;
                #else
                    surfaceReduction = 1.0/(roughness*roughness + 1.0);
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                indirectSpecular *= surfaceReduction;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _RockColor1;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Metallic;
            uniform float _Roughness;
            uniform sampler2D _NormalDetail; uniform float4 _NormalDetail_ST;
            uniform float _NormalDetailtiling;
            uniform sampler2D _SandMask; uniform float4 _SandMask_ST;
            uniform sampler2D _SandDiffuse; uniform float4 _SandDiffuse_ST;
            uniform float _SandTile;
            uniform sampler2D _SandNormal; uniform float4 _SandNormal_ST;
            uniform float _SandNormalTile;
            uniform float4 _Rockcolor2;
            uniform sampler2D _SatClouds; uniform float4 _SatClouds_ST;
            uniform float _SaturationTile;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 node_7255 = i.posWorld.rgb.rb;
                float2 node_6217 = (node_7255*_SandNormalTile);
                float3 _SandNormal_var = UnpackNormal(tex2D(_SandNormal,TRANSFORM_TEX(node_6217, _SandNormal)));
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float2 node_3390 = (i.uv0*(objScale.r*_NormalDetailtiling));
                float3 _NormalDetail_var = UnpackNormal(tex2D(_NormalDetail,TRANSFORM_TEX(node_3390, _NormalDetail)));
                float3 node_5355_nrm_base = _BumpMap_var.rgb + float3(0,0,1);
                float3 node_5355_nrm_detail = _NormalDetail_var.rgb * float3(-1,-1,1);
                float3 node_5355_nrm_combined = node_5355_nrm_base*dot(node_5355_nrm_base, node_5355_nrm_detail)/node_5355_nrm_base.z - node_5355_nrm_detail;
                float3 node_5355 = node_5355_nrm_combined;
                float4 _SandMask_var = tex2D(_SandMask,TRANSFORM_TEX(node_3390, _SandMask));
                float node_9624 = pow(i.normalDir.g,6.0);
                float3 node_3493 = saturate((pow(lerp(float3(1,1,1),_SandMask_var.rgb,node_9624),10.0)-node_9624));
                float3 SandMask = node_3493;
                float3 node_2624 = SandMask;
                float3 normalLocal = lerp(_SandNormal_var.rgb,node_5355,node_2624);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float node_7942 = 1.0;
                float gloss = 1.0 - lerp(float3(node_7942,node_7942,node_7942),float3(_Roughness,_Roughness,_Roughness),node_2624).r; // Convert roughness to gloss
                float perceptualRoughness = lerp(float3(node_7942,node_7942,node_7942),float3(_Roughness,_Roughness,_Roughness),node_2624).r;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float2 node_5383 = (node_7255*_SandTile);
                float4 _SandDiffuse_var = tex2D(_SandDiffuse,TRANSFORM_TEX(node_5383, _SandDiffuse));
                float4 node_4060_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_4060_p = lerp(float4(float4(_RockColor1.rgb,0.0).zy, node_4060_k.wz), float4(float4(_RockColor1.rgb,0.0).yz, node_4060_k.xy), step(float4(_RockColor1.rgb,0.0).z, float4(_RockColor1.rgb,0.0).y));
                float4 node_4060_q = lerp(float4(node_4060_p.xyw, float4(_RockColor1.rgb,0.0).x), float4(float4(_RockColor1.rgb,0.0).x, node_4060_p.yzx), step(node_4060_p.x, float4(_RockColor1.rgb,0.0).x));
                float node_4060_d = node_4060_q.x - min(node_4060_q.w, node_4060_q.y);
                float node_4060_e = 1.0e-10;
                float3 node_4060 = float3(abs(node_4060_q.z + (node_4060_q.w - node_4060_q.y) / (6.0 * node_4060_d + node_4060_e)), node_4060_d / (node_4060_q.x + node_4060_e), node_4060_q.x);;
                float2 node_7896 = (i.uv0*_SaturationTile);
                float4 _SatClouds_var = tex2D(_SatClouds,TRANSFORM_TEX(node_7896, _SatClouds));
                float node_336 = 5.0;
                float3 diffuseColor = lerp(_SandDiffuse_var.rgb,lerp(_Rockcolor2.rgb,(lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac(node_4060.r+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_4060.g)*(node_4060.b+(floor(_SatClouds_var.r * node_336) / (node_336 - 1)*0.0+0.0))),_SandMask_var.rgb),node_3493); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _RockColor1;
            uniform float _Metallic;
            uniform float _Roughness;
            uniform float _NormalDetailtiling;
            uniform sampler2D _SandMask; uniform float4 _SandMask_ST;
            uniform sampler2D _SandDiffuse; uniform float4 _SandDiffuse_ST;
            uniform float _SandTile;
            uniform float4 _Rockcolor2;
            uniform sampler2D _SatClouds; uniform float4 _SatClouds_ST;
            uniform float _SaturationTile;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float2 node_7255 = i.posWorld.rgb.rb;
                float2 node_5383 = (node_7255*_SandTile);
                float4 _SandDiffuse_var = tex2D(_SandDiffuse,TRANSFORM_TEX(node_5383, _SandDiffuse));
                float4 node_4060_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_4060_p = lerp(float4(float4(_RockColor1.rgb,0.0).zy, node_4060_k.wz), float4(float4(_RockColor1.rgb,0.0).yz, node_4060_k.xy), step(float4(_RockColor1.rgb,0.0).z, float4(_RockColor1.rgb,0.0).y));
                float4 node_4060_q = lerp(float4(node_4060_p.xyw, float4(_RockColor1.rgb,0.0).x), float4(float4(_RockColor1.rgb,0.0).x, node_4060_p.yzx), step(node_4060_p.x, float4(_RockColor1.rgb,0.0).x));
                float node_4060_d = node_4060_q.x - min(node_4060_q.w, node_4060_q.y);
                float node_4060_e = 1.0e-10;
                float3 node_4060 = float3(abs(node_4060_q.z + (node_4060_q.w - node_4060_q.y) / (6.0 * node_4060_d + node_4060_e)), node_4060_d / (node_4060_q.x + node_4060_e), node_4060_q.x);;
                float2 node_7896 = (i.uv0*_SaturationTile);
                float4 _SatClouds_var = tex2D(_SatClouds,TRANSFORM_TEX(node_7896, _SatClouds));
                float node_336 = 5.0;
                float2 node_3390 = (i.uv0*(objScale.r*_NormalDetailtiling));
                float4 _SandMask_var = tex2D(_SandMask,TRANSFORM_TEX(node_3390, _SandMask));
                float node_9624 = pow(i.normalDir.g,6.0);
                float3 node_3493 = saturate((pow(lerp(float3(1,1,1),_SandMask_var.rgb,node_9624),10.0)-node_9624));
                float3 diffColor = lerp(_SandDiffuse_var.rgb,lerp(_Rockcolor2.rgb,(lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac(node_4060.r+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_4060.g)*(node_4060.b+(floor(_SatClouds_var.r * node_336) / (node_336 - 1)*0.0+0.0))),_SandMask_var.rgb),node_3493);
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, _Metallic, specColor, specularMonochrome );
                float node_7942 = 1.0;
                float3 SandMask = node_3493;
                float3 node_2624 = SandMask;
                float roughness = lerp(float3(node_7942,node_7942,node_7942),float3(_Roughness,_Roughness,_Roughness),node_2624).r;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
