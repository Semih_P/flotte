// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:0,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:-1,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:1,fgcg:1,fgcb:1,fgca:1,fgde:0.009,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:33627,y:32750,varname:node_2865,prsc:2|diff-5531-OUT,spec-358-OUT,gloss-1813-OUT,normal-836-OUT,alpha-7579-OUT,refract-4015-OUT,disp-7012-OUT,tess-1477-OUT;n:type:ShaderForge.SFN_Color,id:6665,x:31881,y:32733,ptovrint:False,ptlb:Water color,ptin:_Watercolor,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;n:type:ShaderForge.SFN_Slider,id:358,x:32921,y:32323,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:node_358,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:1813,x:32812,y:32453,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Metallic_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8,max:1;n:type:ShaderForge.SFN_DepthBlend,id:3344,x:31789,y:33259,varname:node_3344,prsc:2|DIST-6057-OUT;n:type:ShaderForge.SFN_Clamp01,id:7320,x:31984,y:33238,varname:node_7320,prsc:2|IN-3344-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6057,x:31581,y:33293,ptovrint:False,ptlb:Depth,ptin:_Depth,varname:node_6057,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_DepthBlend,id:356,x:31648,y:33031,varname:node_356,prsc:2|DIST-9145-OUT;n:type:ShaderForge.SFN_Clamp01,id:4577,x:32064,y:33066,varname:node_4577,prsc:2|IN-27-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9145,x:31648,y:32892,ptovrint:False,ptlb:Foam range,ptin:_Foamrange,varname:_Depth_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_OneMinus,id:9724,x:32238,y:33049,varname:node_9724,prsc:2|IN-4577-OUT;n:type:ShaderForge.SFN_Power,id:27,x:31814,y:33078,varname:node_27,prsc:2|VAL-356-OUT,EXP-6109-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6109,x:31581,y:33201,ptovrint:False,ptlb:Foam power,ptin:_Foampower,varname:node_6109,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:1991,x:32440,y:32771,varname:node_1991,prsc:2|A-6665-RGB,B-8085-OUT,T-9724-OUT;n:type:ShaderForge.SFN_Color,id:2468,x:31869,y:32907,ptovrint:False,ptlb:Foam color,ptin:_Foamcolor,varname:node_2468,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:7579,x:32479,y:33121,varname:node_7579,prsc:2|A-9724-OUT,B-7320-OUT;n:type:ShaderForge.SFN_Tex2d,id:1617,x:32048,y:32540,ptovrint:False,ptlb:Foam mask,ptin:_Foammask,varname:node_1617,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:36dd0b22da8874ed38075789055ca664,ntxv:0,isnm:False|UVIN-6527-UVOUT;n:type:ShaderForge.SFN_Panner,id:6527,x:31801,y:32507,varname:node_6527,prsc:2,spu:1,spv:1|UVIN-4047-UVOUT,DIST-712-OUT;n:type:ShaderForge.SFN_TexCoord,id:4047,x:29883,y:32076,varname:node_4047,prsc:2,uv:0;n:type:ShaderForge.SFN_Time,id:2469,x:30329,y:32411,varname:node_2469,prsc:2;n:type:ShaderForge.SFN_Multiply,id:712,x:31185,y:32526,varname:node_712,prsc:2|A-1911-OUT,B-7071-OUT;n:type:ShaderForge.SFN_Slider,id:1911,x:30644,y:32273,ptovrint:False,ptlb:Water speed,ptin:_Waterspeed,varname:node_1911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1416042,max:1;n:type:ShaderForge.SFN_Lerp,id:8085,x:32251,y:32771,varname:node_8085,prsc:2|A-6665-RGB,B-2468-RGB,T-1617-RGB;n:type:ShaderForge.SFN_Panner,id:8759,x:30229,y:31976,varname:node_8759,prsc:2,spu:0,spv:0.7|UVIN-4047-UVOUT,DIST-2675-OUT;n:type:ShaderForge.SFN_Panner,id:1258,x:30229,y:32122,varname:node_1258,prsc:2,spu:1,spv:0.7|UVIN-4047-UVOUT,DIST-2675-OUT;n:type:ShaderForge.SFN_Sin,id:2953,x:30968,y:32676,varname:node_2953,prsc:2|IN-2469-T;n:type:ShaderForge.SFN_RemapRange,id:1169,x:31699,y:32662,varname:node_1169,prsc:2,frmn:-1,frmx:1,tomn:0.5,tomx:0.5|IN-2953-OUT;n:type:ShaderForge.SFN_Lerp,id:3105,x:32404,y:32320,varname:node_3105,prsc:2|A-7414-OUT,B-2343-OUT,T-1169-OUT;n:type:ShaderForge.SFN_Multiply,id:7071,x:30989,y:32549,varname:node_7071,prsc:2|A-2469-TSL,B-6903-OUT;n:type:ShaderForge.SFN_Vector1,id:6903,x:30715,y:32526,varname:node_6903,prsc:2,v1:0.01;n:type:ShaderForge.SFN_TexCoord,id:2290,x:31150,y:34166,varname:node_2290,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:8599,x:32232,y:33542,varname:node_8599,prsc:2,v1:100;n:type:ShaderForge.SFN_Append,id:7012,x:32687,y:34265,varname:node_7012,prsc:2|A-4549-OUT,B-5055-OUT,C-4549-OUT;n:type:ShaderForge.SFN_Vector1,id:4549,x:32491,y:34216,varname:node_4549,prsc:2,v1:0;n:type:ShaderForge.SFN_Slider,id:7615,x:32062,y:34151,ptovrint:False,ptlb:Wave height,ptin:_Waveheight,varname:node_7615,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.1;n:type:ShaderForge.SFN_Multiply,id:5055,x:32460,y:34334,varname:node_5055,prsc:2|A-7615-OUT,B-4848-OUT;n:type:ShaderForge.SFN_Slider,id:8191,x:31115,y:34445,ptovrint:False,ptlb:Wave speed,ptin:_Wavespeed,varname:node_8191,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:20;n:type:ShaderForge.SFN_Fresnel,id:2254,x:32760,y:31608,varname:node_2254,prsc:2|NRM-5156-OUT,EXP-2018-OUT;n:type:ShaderForge.SFN_NormalVector,id:5156,x:32524,y:31640,prsc:2,pt:False;n:type:ShaderForge.SFN_Color,id:3014,x:32502,y:31813,ptovrint:False,ptlb:Water color 2,ptin:_Watercolor2,varname:node_3014,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:5531,x:32964,y:31726,varname:node_5531,prsc:2|A-1991-OUT,B-3014-RGB,T-2254-OUT;n:type:ShaderForge.SFN_Slider,id:2018,x:32385,y:31576,ptovrint:False,ptlb:Fresnel power,ptin:_Fresnelpower,varname:node_2018,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:10;n:type:ShaderForge.SFN_Multiply,id:1974,x:32423,y:33587,varname:node_1974,prsc:2|A-8599-OUT,B-6566-OUT;n:type:ShaderForge.SFN_Tex2d,id:4447,x:31702,y:33663,ptovrint:False,ptlb:TesselationGradient,ptin:_TesselationGradient,varname:node_4447,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:d5c9e7bb4ddf71e44bb098db89a61282,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:6566,x:32100,y:33654,varname:node_6566,prsc:2|A-4447-R,B-9379-OUT;n:type:ShaderForge.SFN_Vector1,id:9379,x:31897,y:33498,varname:node_9379,prsc:2,v1:0.01;n:type:ShaderForge.SFN_ComponentMask,id:7659,x:34786,y:33700,varname:node_7659,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-3315-RGB;n:type:ShaderForge.SFN_Slider,id:2932,x:34591,y:33870,ptovrint:False,ptlb:RefractionStrength,ptin:_RefractionStrength,varname:node_2932,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:4015,x:35054,y:33885,varname:node_4015,prsc:2|A-7659-OUT,B-2932-OUT;n:type:ShaderForge.SFN_Time,id:1142,x:33708,y:33822,varname:node_1142,prsc:2;n:type:ShaderForge.SFN_Vector1,id:4148,x:33856,y:33856,varname:node_4148,prsc:2,v1:0.01;n:type:ShaderForge.SFN_Multiply,id:7161,x:34029,y:33846,varname:node_7161,prsc:2|A-1142-TSL,B-4148-OUT;n:type:ShaderForge.SFN_Multiply,id:2525,x:34207,y:33828,varname:node_2525,prsc:2|A-2467-OUT,B-7161-OUT;n:type:ShaderForge.SFN_Panner,id:8250,x:34402,y:33700,varname:node_8250,prsc:2,spu:1,spv:0.7|UVIN-3153-UVOUT,DIST-2525-OUT;n:type:ShaderForge.SFN_TexCoord,id:3153,x:34207,y:33670,varname:node_3153,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:2467,x:33856,y:33760,ptovrint:False,ptlb:Refractionspeed,ptin:_Refractionspeed,varname:_Waterspeed_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1416042,max:1;n:type:ShaderForge.SFN_Tex2d,id:3315,x:34602,y:33700,ptovrint:False,ptlb:RefractionNormal,ptin:_RefractionNormal,varname:node_3315,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a53cf5449d11a15d1100a04b44295342,ntxv:3,isnm:True|UVIN-8250-UVOUT;n:type:ShaderForge.SFN_FragmentPosition,id:6703,x:30463,y:31326,varname:node_6703,prsc:2;n:type:ShaderForge.SFN_ViewPosition,id:3303,x:30463,y:31479,varname:node_3303,prsc:2;n:type:ShaderForge.SFN_Distance,id:1681,x:30681,y:31417,varname:node_1681,prsc:2|A-6703-XYZ,B-3303-XYZ;n:type:ShaderForge.SFN_Divide,id:7794,x:30870,y:31451,varname:node_7794,prsc:2|A-1681-OUT,B-4298-OUT;n:type:ShaderForge.SFN_Vector1,id:4298,x:30652,y:31558,varname:node_4298,prsc:2,v1:20;n:type:ShaderForge.SFN_Power,id:1736,x:31041,y:31461,varname:node_1736,prsc:2|VAL-7794-OUT,EXP-5192-OUT;n:type:ShaderForge.SFN_Vector1,id:5192,x:30811,y:31595,varname:node_5192,prsc:2,v1:1;n:type:ShaderForge.SFN_Clamp01,id:4399,x:31210,y:31492,varname:node_4399,prsc:2|IN-1736-OUT;n:type:ShaderForge.SFN_Lerp,id:7414,x:32175,y:31950,varname:node_7414,prsc:2|A-5552-RGB,B-6112-RGB,T-4399-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:2564,x:31266,y:31726,ptovrint:False,ptlb:WaveNormalSource,ptin:_WaveNormalSource,varname:node_2564,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Lerp,id:2343,x:32192,y:32229,varname:node_2343,prsc:2|A-5608-RGB,B-6983-RGB,T-4399-OUT;n:type:ShaderForge.SFN_Tex2d,id:5552,x:31989,y:31827,varname:node_5552,prsc:2,ntxv:0,isnm:False|UVIN-5770-OUT,TEX-2564-TEX;n:type:ShaderForge.SFN_Tex2d,id:6112,x:31989,y:31977,varname:node_6112,prsc:2,ntxv:0,isnm:False|UVIN-8036-OUT,TEX-2564-TEX;n:type:ShaderForge.SFN_Tex2d,id:5608,x:31989,y:32107,varname:node_5608,prsc:2,ntxv:0,isnm:False|UVIN-85-OUT,TEX-2564-TEX;n:type:ShaderForge.SFN_Tex2d,id:6983,x:31968,y:32259,varname:node_6983,prsc:2,ntxv:0,isnm:False|UVIN-2819-OUT,TEX-2564-TEX;n:type:ShaderForge.SFN_Multiply,id:8036,x:31537,y:31940,varname:node_8036,prsc:2|A-5770-OUT,B-5917-OUT;n:type:ShaderForge.SFN_Vector1,id:5917,x:31343,y:32133,varname:node_5917,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:2819,x:31656,y:32266,varname:node_2819,prsc:2|A-85-OUT,B-5917-OUT;n:type:ShaderForge.SFN_Tex2d,id:8453,x:31993,y:34260,varname:node_8453,prsc:2,ntxv:0,isnm:False|UVIN-3956-UVOUT,TEX-3038-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:3038,x:31700,y:34548,ptovrint:False,ptlb:WaveForms,ptin:_WaveForms,varname:node_3038,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:False;n:type:ShaderForge.SFN_Panner,id:3956,x:31700,y:34222,varname:node_3956,prsc:2,spu:1,spv:1|UVIN-2290-UVOUT,DIST-863-OUT;n:type:ShaderForge.SFN_Set,id:5419,x:31175,y:32789,varname:Time,prsc:2|IN-7071-OUT;n:type:ShaderForge.SFN_Get,id:6374,x:31212,y:34564,varname:node_6374,prsc:2|IN-5419-OUT;n:type:ShaderForge.SFN_Multiply,id:863,x:31497,y:34524,varname:node_863,prsc:2|A-8191-OUT,B-6374-OUT;n:type:ShaderForge.SFN_Tex2d,id:3434,x:31993,y:34451,varname:node_3434,prsc:2,ntxv:0,isnm:False|UVIN-1343-UVOUT,TEX-3038-TEX;n:type:ShaderForge.SFN_Lerp,id:4848,x:32219,y:34369,varname:node_4848,prsc:2|A-8453-R,B-3434-R,T-5442-OUT;n:type:ShaderForge.SFN_Panner,id:1343,x:31700,y:34365,varname:node_1343,prsc:2,spu:0.8,spv:1.5|UVIN-2290-UVOUT,DIST-863-OUT;n:type:ShaderForge.SFN_Vector1,id:5442,x:31982,y:34592,varname:node_5442,prsc:2,v1:0.5;n:type:ShaderForge.SFN_FragmentPosition,id:2724,x:33756,y:32165,varname:node_2724,prsc:2;n:type:ShaderForge.SFN_ComponentMask,id:7601,x:33949,y:32165,varname:node_7601,prsc:2,cc1:0,cc2:2,cc3:-1,cc4:-1|IN-2724-XYZ;n:type:ShaderForge.SFN_Set,id:7764,x:34371,y:32167,varname:UV,prsc:2|IN-3483-OUT;n:type:ShaderForge.SFN_Get,id:7284,x:31086,y:32092,varname:node_7284,prsc:2|IN-7764-OUT;n:type:ShaderForge.SFN_Add,id:85,x:31435,y:32266,varname:node_85,prsc:2|A-7284-OUT,B-2675-OUT;n:type:ShaderForge.SFN_Add,id:5770,x:31343,y:31940,varname:node_5770,prsc:2|A-7284-OUT,B-5373-OUT;n:type:ShaderForge.SFN_Multiply,id:2675,x:30644,y:32024,varname:node_2675,prsc:2|A-2469-TSL,B-1911-OUT;n:type:ShaderForge.SFN_Multiply,id:5373,x:30720,y:31730,varname:node_5373,prsc:2|A-2675-OUT,B-9415-OUT;n:type:ShaderForge.SFN_Vector1,id:9415,x:30186,y:31711,varname:node_9415,prsc:2,v1:1.2;n:type:ShaderForge.SFN_Multiply,id:1477,x:32698,y:33544,varname:node_1477,prsc:2|A-1974-OUT,B-2203-OUT;n:type:ShaderForge.SFN_RemapRange,id:2203,x:32484,y:33821,varname:node_2203,prsc:2,frmn:0,frmx:1,tomn:0.2,tomx:1|IN-4848-OUT;n:type:ShaderForge.SFN_Multiply,id:3483,x:34171,y:32167,varname:node_3483,prsc:2|A-7601-OUT,B-1836-OUT;n:type:ShaderForge.SFN_Vector1,id:1836,x:33949,y:32368,varname:node_1836,prsc:2,v1:0.001;n:type:ShaderForge.SFN_Tex2d,id:3213,x:32705,y:32660,ptovrint:False,ptlb:NormalBase,ptin:_NormalBase,varname:node_3213,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Lerp,id:836,x:32921,y:32632,varname:node_836,prsc:2|A-3105-OUT,B-3213-RGB,T-5942-OUT;n:type:ShaderForge.SFN_Vector1,id:5942,x:32730,y:32823,varname:node_5942,prsc:2,v1:0;proporder:6665-3014-358-1813-6057-9145-6109-2468-1617-1911-7615-8191-2018-4447-2932-2467-3315-2564-3038-3213;pass:END;sub:END;*/

Shader "Shader Forge/DepthTest" {
    Properties {
        _Watercolor ("Water color", Color) = (0.5019608,0.5019608,0.5019608,1)
        _Watercolor2 ("Water color 2", Color) = (0.5,0.5,0.5,1)
        _Metallic ("Metallic", Range(0, 1)) = 0
        _Gloss ("Gloss", Range(0, 1)) = 0.8
        _Depth ("Depth", Float ) = 0
        _Foamrange ("Foam range", Float ) = 0
        _Foampower ("Foam power", Float ) = 0
        _Foamcolor ("Foam color", Color) = (0.5,0.5,0.5,1)
        _Foammask ("Foam mask", 2D) = "white" {}
        _Waterspeed ("Water speed", Range(0, 1)) = 0.1416042
        _Waveheight ("Wave height", Range(0, 0.1)) = 0
        _Wavespeed ("Wave speed", Range(0, 20)) = 0
        _Fresnelpower ("Fresnel power", Range(0, 10)) = 0
        _TesselationGradient ("TesselationGradient", 2D) = "white" {}
        _RefractionStrength ("RefractionStrength", Range(0, 1)) = 0
        _Refractionspeed ("Refractionspeed", Range(0, 1)) = 0.1416042
        _RefractionNormal ("RefractionNormal", 2D) = "bump" {}
        _WaveNormalSource ("WaveNormalSource", 2D) = "bump" {}
        _WaveForms ("WaveForms", 2D) = "bump" {}
        _NormalBase ("NormalBase", 2D) = "bump" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent-1"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 5.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _Watercolor;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float _Depth;
            uniform float _Foamrange;
            uniform float _Foampower;
            uniform float4 _Foamcolor;
            uniform sampler2D _Foammask; uniform float4 _Foammask_ST;
            uniform float _Waterspeed;
            uniform float _Waveheight;
            uniform float _Wavespeed;
            uniform float4 _Watercolor2;
            uniform float _Fresnelpower;
            uniform sampler2D _TesselationGradient; uniform float4 _TesselationGradient_ST;
            uniform float _RefractionStrength;
            uniform float _Refractionspeed;
            uniform sampler2D _RefractionNormal; uniform float4 _RefractionNormal_ST;
            uniform sampler2D _WaveNormalSource; uniform float4 _WaveNormalSource_ST;
            uniform sampler2D _WaveForms; uniform float4 _WaveForms_ST;
            uniform sampler2D _NormalBase; uniform float4 _NormalBase_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                float4 projPos : TEXCOORD8;
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                    float2 texcoord1 : TEXCOORD1;
                    float2 texcoord2 : TEXCOORD2;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    o.texcoord1 = v.texcoord1;
                    o.texcoord2 = v.texcoord2;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float node_4549 = 0.0;
                    float4 node_2469 = _Time + _TimeEditor;
                    float node_7071 = (node_2469.r*0.01);
                    float Time = node_7071;
                    float node_863 = (_Wavespeed*Time);
                    float2 node_3956 = (v.texcoord0+node_863*float2(1,1));
                    float4 node_8453 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_3956, _WaveForms),0.0,0));
                    float2 node_1343 = (v.texcoord0+node_863*float2(0.8,1.5));
                    float4 node_3434 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_1343, _WaveForms),0.0,0));
                    float node_4848 = lerp(node_8453.r,node_3434.r,0.5);
                    v.vertex.xyz += float3(node_4549,(_Waveheight*node_4848),node_4549);
                }
                float Tessellation(TessVertex v){
                    float4 _TesselationGradient_var = tex2Dlod(_TesselationGradient,float4(TRANSFORM_TEX(v.texcoord0, _TesselationGradient),0.0,0));
                    float4 node_2469 = _Time + _TimeEditor;
                    float node_7071 = (node_2469.r*0.01);
                    float Time = node_7071;
                    float node_863 = (_Wavespeed*Time);
                    float2 node_3956 = (v.texcoord0+node_863*float2(1,1));
                    float4 node_8453 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_3956, _WaveForms),0.0,0));
                    float2 node_1343 = (v.texcoord0+node_863*float2(0.8,1.5));
                    float4 node_3434 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_1343, _WaveForms),0.0,0));
                    float node_4848 = lerp(node_8453.r,node_3434.r,0.5);
                    return ((100.0*(_TesselationGradient_var.r+0.01))*(node_4848*0.8+0.2));
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    v.texcoord1 = vi[0].texcoord1*bary.x + vi[1].texcoord1*bary.y + vi[2].texcoord1*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 UV = (i.posWorld.rgb.rb*0.001);
                float2 node_7284 = UV;
                float4 node_2469 = _Time + _TimeEditor;
                float node_2675 = (node_2469.r*_Waterspeed);
                float2 node_5770 = (node_7284+(node_2675*1.2));
                float3 node_5552 = UnpackNormal(tex2D(_WaveNormalSource,TRANSFORM_TEX(node_5770, _WaveNormalSource)));
                float node_5917 = 0.5;
                float2 node_8036 = (node_5770*node_5917);
                float3 node_6112 = UnpackNormal(tex2D(_WaveNormalSource,TRANSFORM_TEX(node_8036, _WaveNormalSource)));
                float node_4399 = saturate(pow((distance(i.posWorld.rgb,_WorldSpaceCameraPos)/20.0),1.0));
                float2 node_85 = (node_7284+node_2675);
                float3 node_5608 = UnpackNormal(tex2D(_WaveNormalSource,TRANSFORM_TEX(node_85, _WaveNormalSource)));
                float2 node_2819 = (node_85*node_5917);
                float3 node_6983 = UnpackNormal(tex2D(_WaveNormalSource,TRANSFORM_TEX(node_2819, _WaveNormalSource)));
                float3 _NormalBase_var = UnpackNormal(tex2D(_NormalBase,TRANSFORM_TEX(i.uv0, _NormalBase)));
                float3 normalLocal = lerp(lerp(lerp(node_5552.rgb,node_6112.rgb,node_4399),lerp(node_5608.rgb,node_6983.rgb,node_4399),(sin(node_2469.g)*0.0+0.5)),_NormalBase_var.rgb,0.0);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 node_1142 = _Time + _TimeEditor;
                float2 node_8250 = (i.uv0+(_Refractionspeed*(node_1142.r*0.01))*float2(1,0.7));
                float3 _RefractionNormal_var = UnpackNormal(tex2D(_RefractionNormal,TRANSFORM_TEX(node_8250, _RefractionNormal)));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (_RefractionNormal_var.rgb.rg*_RefractionStrength);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float3 specularColor = float3(_Metallic,_Metallic,_Metallic);
                float specularMonochrome;
                float node_7071 = (node_2469.r*0.01);
                float2 node_6527 = (i.uv0+(_Waterspeed*node_7071)*float2(1,1));
                float4 _Foammask_var = tex2D(_Foammask,TRANSFORM_TEX(node_6527, _Foammask));
                float node_9724 = (1.0 - saturate(pow(saturate((sceneZ-partZ)/_Foamrange),_Foampower)));
                float3 diffuseColor = lerp(lerp(_Watercolor.rgb,lerp(_Watercolor.rgb,_Foamcolor.rgb,_Foammask_var.rgb),node_9724),_Watercolor2.rgb,pow(1.0-max(0,dot(i.normalDir, viewDirection)),_Fresnelpower)); // Need this for specular when using metallic
                diffuseColor = EnergyConservationBetweenDiffuseAndSpecular(diffuseColor, specularColor, specularMonochrome);
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, GGXTerm(NdotH, 1.0-gloss));
                float specularPBL = (NdotL*visTerm*normTerm) * (UNITY_PI / 4);
                if (IsGammaSpace())
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                specularPBL = max(0, specularPBL * NdotL);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz)*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,(node_9724+saturate(saturate((sceneZ-partZ)/_Depth)))),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 5.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _Watercolor;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float _Depth;
            uniform float _Foamrange;
            uniform float _Foampower;
            uniform float4 _Foamcolor;
            uniform sampler2D _Foammask; uniform float4 _Foammask_ST;
            uniform float _Waterspeed;
            uniform float _Waveheight;
            uniform float _Wavespeed;
            uniform float4 _Watercolor2;
            uniform float _Fresnelpower;
            uniform sampler2D _TesselationGradient; uniform float4 _TesselationGradient_ST;
            uniform float _RefractionStrength;
            uniform float _Refractionspeed;
            uniform sampler2D _RefractionNormal; uniform float4 _RefractionNormal_ST;
            uniform sampler2D _WaveNormalSource; uniform float4 _WaveNormalSource_ST;
            uniform sampler2D _WaveForms; uniform float4 _WaveForms_ST;
            uniform sampler2D _NormalBase; uniform float4 _NormalBase_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                float4 projPos : TEXCOORD8;
                LIGHTING_COORDS(9,10)
                UNITY_FOG_COORDS(11)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                    float2 texcoord1 : TEXCOORD1;
                    float2 texcoord2 : TEXCOORD2;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    o.texcoord1 = v.texcoord1;
                    o.texcoord2 = v.texcoord2;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float node_4549 = 0.0;
                    float4 node_2469 = _Time + _TimeEditor;
                    float node_7071 = (node_2469.r*0.01);
                    float Time = node_7071;
                    float node_863 = (_Wavespeed*Time);
                    float2 node_3956 = (v.texcoord0+node_863*float2(1,1));
                    float4 node_8453 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_3956, _WaveForms),0.0,0));
                    float2 node_1343 = (v.texcoord0+node_863*float2(0.8,1.5));
                    float4 node_3434 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_1343, _WaveForms),0.0,0));
                    float node_4848 = lerp(node_8453.r,node_3434.r,0.5);
                    v.vertex.xyz += float3(node_4549,(_Waveheight*node_4848),node_4549);
                }
                float Tessellation(TessVertex v){
                    float4 _TesselationGradient_var = tex2Dlod(_TesselationGradient,float4(TRANSFORM_TEX(v.texcoord0, _TesselationGradient),0.0,0));
                    float4 node_2469 = _Time + _TimeEditor;
                    float node_7071 = (node_2469.r*0.01);
                    float Time = node_7071;
                    float node_863 = (_Wavespeed*Time);
                    float2 node_3956 = (v.texcoord0+node_863*float2(1,1));
                    float4 node_8453 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_3956, _WaveForms),0.0,0));
                    float2 node_1343 = (v.texcoord0+node_863*float2(0.8,1.5));
                    float4 node_3434 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_1343, _WaveForms),0.0,0));
                    float node_4848 = lerp(node_8453.r,node_3434.r,0.5);
                    return ((100.0*(_TesselationGradient_var.r+0.01))*(node_4848*0.8+0.2));
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    v.texcoord1 = vi[0].texcoord1*bary.x + vi[1].texcoord1*bary.y + vi[2].texcoord1*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 UV = (i.posWorld.rgb.rb*0.001);
                float2 node_7284 = UV;
                float4 node_2469 = _Time + _TimeEditor;
                float node_2675 = (node_2469.r*_Waterspeed);
                float2 node_5770 = (node_7284+(node_2675*1.2));
                float3 node_5552 = UnpackNormal(tex2D(_WaveNormalSource,TRANSFORM_TEX(node_5770, _WaveNormalSource)));
                float node_5917 = 0.5;
                float2 node_8036 = (node_5770*node_5917);
                float3 node_6112 = UnpackNormal(tex2D(_WaveNormalSource,TRANSFORM_TEX(node_8036, _WaveNormalSource)));
                float node_4399 = saturate(pow((distance(i.posWorld.rgb,_WorldSpaceCameraPos)/20.0),1.0));
                float2 node_85 = (node_7284+node_2675);
                float3 node_5608 = UnpackNormal(tex2D(_WaveNormalSource,TRANSFORM_TEX(node_85, _WaveNormalSource)));
                float2 node_2819 = (node_85*node_5917);
                float3 node_6983 = UnpackNormal(tex2D(_WaveNormalSource,TRANSFORM_TEX(node_2819, _WaveNormalSource)));
                float3 _NormalBase_var = UnpackNormal(tex2D(_NormalBase,TRANSFORM_TEX(i.uv0, _NormalBase)));
                float3 normalLocal = lerp(lerp(lerp(node_5552.rgb,node_6112.rgb,node_4399),lerp(node_5608.rgb,node_6983.rgb,node_4399),(sin(node_2469.g)*0.0+0.5)),_NormalBase_var.rgb,0.0);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 node_1142 = _Time + _TimeEditor;
                float2 node_8250 = (i.uv0+(_Refractionspeed*(node_1142.r*0.01))*float2(1,0.7));
                float3 _RefractionNormal_var = UnpackNormal(tex2D(_RefractionNormal,TRANSFORM_TEX(node_8250, _RefractionNormal)));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (_RefractionNormal_var.rgb.rg*_RefractionStrength);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float3 specularColor = float3(_Metallic,_Metallic,_Metallic);
                float specularMonochrome;
                float node_7071 = (node_2469.r*0.01);
                float2 node_6527 = (i.uv0+(_Waterspeed*node_7071)*float2(1,1));
                float4 _Foammask_var = tex2D(_Foammask,TRANSFORM_TEX(node_6527, _Foammask));
                float node_9724 = (1.0 - saturate(pow(saturate((sceneZ-partZ)/_Foamrange),_Foampower)));
                float3 diffuseColor = lerp(lerp(_Watercolor.rgb,lerp(_Watercolor.rgb,_Foamcolor.rgb,_Foammask_var.rgb),node_9724),_Watercolor2.rgb,pow(1.0-max(0,dot(i.normalDir, viewDirection)),_Fresnelpower)); // Need this for specular when using metallic
                diffuseColor = EnergyConservationBetweenDiffuseAndSpecular(diffuseColor, specularColor, specularMonochrome);
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, GGXTerm(NdotH, 1.0-gloss));
                float specularPBL = (NdotL*visTerm*normTerm) * (UNITY_PI / 4);
                if (IsGammaSpace())
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                specularPBL = max(0, specularPBL * NdotL);
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * (node_9724+saturate(saturate((sceneZ-partZ)/_Depth))),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 5.0
            uniform float4 _TimeEditor;
            uniform float _Waveheight;
            uniform float _Wavespeed;
            uniform sampler2D _TesselationGradient; uniform float4 _TesselationGradient_ST;
            uniform sampler2D _WaveForms; uniform float4 _WaveForms_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float2 uv1 : TEXCOORD2;
                float2 uv2 : TEXCOORD3;
                float4 posWorld : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                    float2 texcoord1 : TEXCOORD1;
                    float2 texcoord2 : TEXCOORD2;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    o.texcoord1 = v.texcoord1;
                    o.texcoord2 = v.texcoord2;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float node_4549 = 0.0;
                    float4 node_2469 = _Time + _TimeEditor;
                    float node_7071 = (node_2469.r*0.01);
                    float Time = node_7071;
                    float node_863 = (_Wavespeed*Time);
                    float2 node_3956 = (v.texcoord0+node_863*float2(1,1));
                    float4 node_8453 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_3956, _WaveForms),0.0,0));
                    float2 node_1343 = (v.texcoord0+node_863*float2(0.8,1.5));
                    float4 node_3434 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_1343, _WaveForms),0.0,0));
                    float node_4848 = lerp(node_8453.r,node_3434.r,0.5);
                    v.vertex.xyz += float3(node_4549,(_Waveheight*node_4848),node_4549);
                }
                float Tessellation(TessVertex v){
                    float4 _TesselationGradient_var = tex2Dlod(_TesselationGradient,float4(TRANSFORM_TEX(v.texcoord0, _TesselationGradient),0.0,0));
                    float4 node_2469 = _Time + _TimeEditor;
                    float node_7071 = (node_2469.r*0.01);
                    float Time = node_7071;
                    float node_863 = (_Wavespeed*Time);
                    float2 node_3956 = (v.texcoord0+node_863*float2(1,1));
                    float4 node_8453 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_3956, _WaveForms),0.0,0));
                    float2 node_1343 = (v.texcoord0+node_863*float2(0.8,1.5));
                    float4 node_3434 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_1343, _WaveForms),0.0,0));
                    float node_4848 = lerp(node_8453.r,node_3434.r,0.5);
                    return ((100.0*(_TesselationGradient_var.r+0.01))*(node_4848*0.8+0.2));
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    v.texcoord1 = vi[0].texcoord1*bary.x + vi[1].texcoord1*bary.y + vi[2].texcoord1*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 5.0
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _Watercolor;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float _Foamrange;
            uniform float _Foampower;
            uniform float4 _Foamcolor;
            uniform sampler2D _Foammask; uniform float4 _Foammask_ST;
            uniform float _Waterspeed;
            uniform float _Waveheight;
            uniform float _Wavespeed;
            uniform float4 _Watercolor2;
            uniform float _Fresnelpower;
            uniform sampler2D _TesselationGradient; uniform float4 _TesselationGradient_ST;
            uniform sampler2D _WaveForms; uniform float4 _WaveForms_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float4 projPos : TEXCOORD5;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                    float2 texcoord1 : TEXCOORD1;
                    float2 texcoord2 : TEXCOORD2;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    o.texcoord1 = v.texcoord1;
                    o.texcoord2 = v.texcoord2;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float node_4549 = 0.0;
                    float4 node_2469 = _Time + _TimeEditor;
                    float node_7071 = (node_2469.r*0.01);
                    float Time = node_7071;
                    float node_863 = (_Wavespeed*Time);
                    float2 node_3956 = (v.texcoord0+node_863*float2(1,1));
                    float4 node_8453 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_3956, _WaveForms),0.0,0));
                    float2 node_1343 = (v.texcoord0+node_863*float2(0.8,1.5));
                    float4 node_3434 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_1343, _WaveForms),0.0,0));
                    float node_4848 = lerp(node_8453.r,node_3434.r,0.5);
                    v.vertex.xyz += float3(node_4549,(_Waveheight*node_4848),node_4549);
                }
                float Tessellation(TessVertex v){
                    float4 _TesselationGradient_var = tex2Dlod(_TesselationGradient,float4(TRANSFORM_TEX(v.texcoord0, _TesselationGradient),0.0,0));
                    float4 node_2469 = _Time + _TimeEditor;
                    float node_7071 = (node_2469.r*0.01);
                    float Time = node_7071;
                    float node_863 = (_Wavespeed*Time);
                    float2 node_3956 = (v.texcoord0+node_863*float2(1,1));
                    float4 node_8453 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_3956, _WaveForms),0.0,0));
                    float2 node_1343 = (v.texcoord0+node_863*float2(0.8,1.5));
                    float4 node_3434 = tex2Dlod(_WaveForms,float4(TRANSFORM_TEX(node_1343, _WaveForms),0.0,0));
                    float node_4848 = lerp(node_8453.r,node_3434.r,0.5);
                    return ((100.0*(_TesselationGradient_var.r+0.01))*(node_4848*0.8+0.2));
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    v.texcoord1 = vi[0].texcoord1*bary.x + vi[1].texcoord1*bary.y + vi[2].texcoord1*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i, float facing : VFACE) : SV_Target {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float4 node_2469 = _Time + _TimeEditor;
                float node_7071 = (node_2469.r*0.01);
                float2 node_6527 = (i.uv0+(_Waterspeed*node_7071)*float2(1,1));
                float4 _Foammask_var = tex2D(_Foammask,TRANSFORM_TEX(node_6527, _Foammask));
                float node_9724 = (1.0 - saturate(pow(saturate((sceneZ-partZ)/_Foamrange),_Foampower)));
                float3 diffColor = lerp(lerp(_Watercolor.rgb,lerp(_Watercolor.rgb,_Foamcolor.rgb,_Foammask_var.rgb),node_9724),_Watercolor2.rgb,pow(1.0-max(0,dot(i.normalDir, viewDirection)),_Fresnelpower));
                float3 specColor = float3(_Metallic,_Metallic,_Metallic);
                float specularMonochrome = max(max(specColor.r, specColor.g),specColor.b);
                diffColor *= (1.0-specularMonochrome);
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
