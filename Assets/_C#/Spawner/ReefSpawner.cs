﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ObjectSpawner))]
public class ReefSpawner : MonoBehaviour 
{
	//public variables

	//private variables
    private ObjectSpawner spawner;

	//Unity methods
	void Start () 
	{
        spawner = GetComponent<ObjectSpawner>();
        spawner.spawnCallStack += OnSpawn;
	}

	//public methods

	//private methods
    private void OnSpawn(GameObject obj)
    {
        if (obj != null)
        {
            Reef reef = obj.GetComponent<Reef>();
            if (reef != null)
            {
                reef.ResetPickups();
            }
        }
    }
}
