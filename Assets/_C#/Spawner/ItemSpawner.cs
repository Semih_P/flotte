﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ObjectSpawner))]
public class ItemSpawner : MonoBehaviour 
{
	//public variables

	//private variables
    private ObjectSpawner spawner;
    private Raft raft;

	//Unity methods
	void Start () 
	{
        spawner = GetComponent<ObjectSpawner>();
        raft = ComponentManager<Raft>.Get();
        spawner.SpawnNewItemsHost();
	}
	void Update () 
	{
        spawner.enabled = !raft.IsAnchored;
	}

	//public methods

	//private methods
}
