﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectSpawnerManager : MonoBehaviour 
{
	//public variables
    [HideInInspector]public List<ObjectSpawner> spawners = new List<ObjectSpawner>();

	//private variables


	//Unity methods
    void Awake()
    {
        spawners = GetComponentsInChildren<ObjectSpawner>().ToList();
        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].UniqueSpawnerIndex = (uint)i;
        }
    }

	//public methods

	//private methods
}
