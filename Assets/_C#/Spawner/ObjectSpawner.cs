﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SpawnableFloatingObject
{
    public uint uniqueIndex; 
    public ObjectSpawnType typeToSpawn = ObjectSpawnType.Prefab;
    public string poolName = "DefaultPool";
    public GameObject prefab;
    public Vector3 minRotation = new Vector3(-10, 0, -10);
    public Vector3 maxRotation = new Vector3(10, 360, 10);
    public float metersUnderSurface = 0;
    public float spawnChance = 100;
}

public enum ObjectSpawnType { Pool, Prefab }
public class ObjectSpawner : MonoBehaviour
{
    public delegate void OnSpawnedObject(GameObject obj);
    public OnSpawnedObject spawnCallStack;

    //Properties
    public uint UniqueSpawnerIndex { get { return uniqueSpawnerIndex; } set { uniqueSpawnerIndex = value; } }
    private uint uniqueSpawnerIndex;

 	//public variables
    [Header("Spawn settings")]
    [Tooltip("How many objects can the spawner have in total. (-1 means infinite)")]
    public int maxSpawnedObjects = -1;
    public int spawnMinAmount;
    public int spawnMaxAmount;
    public float spawnIntervalSec;
    public float spawnDistanceFromRaft = 15f;
    public float horizontalSpawnOffset = 15f;

    [HideInInspector]
    [SerializeField]
    public List<SpawnableFloatingObject> spawnableObjects;
    
    [HideInInspector]
    public List<Network_FloatingObject> spawnedObjects = new List<Network_FloatingObject>();

 	//private variables
    private Semih_Network network;
    private PoolManager poolManager;
    private float spawnTimer = 0;

 	//Unity methods
    void Start()
    {
        poolManager = ComponentManager<PoolManager>.Get();
        network = ComponentManager<Semih_Network>.Get();
    }
	void Update () 
	{
        if (!network.IsHost)
            return;

        //Spawn additional objects
        bool canSpawn = (maxSpawnedObjects == -1) ? true : (spawnedObjects.Count < maxSpawnedObjects);
        if (canSpawn)
        {
            //Spawn timer
            spawnTimer += Time.deltaTime;
            if (spawnTimer >= spawnIntervalSec)
            {
                spawnTimer -= spawnIntervalSec;
                SpawnNewItemsHost();
            }
        }

        RemoveItemsOutsideRange();
	}
    void OnDrawGizmosSelected()
    {


        Vector3 originPoint = transform.position + Raft.direction.normalized * spawnDistanceFromRaft;
        Vector3 crossDirection = Vector3.Cross(Raft.direction, Vector3.up);
        Vector3 leftSide = originPoint - crossDirection * horizontalSpawnOffset;
        Vector3 rightSide = originPoint + crossDirection * horizontalSpawnOffset;

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, leftSide);
        Gizmos.DrawLine(transform.position, rightSide);

        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(originPoint, new Vector3(horizontalSpawnOffset, 1f, 5f) * 2);

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position - Raft.direction.normalized * (spawnDistanceFromRaft * 1.3f) - Raft.direction.normalized * 3, new Vector3(horizontalSpawnOffset * 4, 1f, 6f));
    }

	//public methods
    public void SpawnNewItemsHost()
    {
        if (network == null)
            network = ComponentManager<Semih_Network>.Get();

        if (!network.IsHost)
            return;

        Vector3 originPoint = transform.position + Raft.direction.normalized * spawnDistanceFromRaft;

        int numberOfItemsToSpawn = Random.Range(spawnMinAmount, spawnMaxAmount + 1);
        for (int i = 0; i < numberOfItemsToSpawn; i++)
        {
            SpawnableFloatingObject spawnableObject = GetObjectToSpawn();
            if (spawnableObject == null)
            {
                i--;
                continue;
            }

            GameObject newItem = null;
            if (spawnableObject.typeToSpawn == ObjectSpawnType.Pool)
                newItem = poolManager.GetObject(spawnableObject.poolName);
            else
                newItem = Instantiate(spawnableObject.prefab, originPoint, Quaternion.identity);
            if (newItem != null)
            {
                Vector3 crossDirection = Vector3.Cross(Raft.direction, Vector3.up);
                Vector3 spawnPosition = originPoint + crossDirection.normalized * Random.Range(-horizontalSpawnOffset, horizontalSpawnOffset) + Raft.direction.normalized * Random.Range(-5, 5f);
                spawnPosition.y = -spawnableObject.metersUnderSurface;

                Network_FloatingObject networkObject = newItem.GetComponent<Network_FloatingObject>();
                networkObject.spawnType = spawnableObject.typeToSpawn;
                networkObject.spawner = this;
                networkObject.spawnerIndex = UniqueSpawnerIndex;
                networkObject.floatingObjectIndex = spawnableObject.uniqueIndex;
                NetworkUpdateManager.SetIndexForBehaviour(networkObject);
                NetworkUpdateManager.AddBehaviour(networkObject);
                networkObject.SetDirty();

                newItem.transform.position = spawnPosition;
                newItem.transform.rotation = Quaternion.Euler(
                    Random.Range(spawnableObject.minRotation.x, spawnableObject.maxRotation.x),
                    Random.Range(spawnableObject.minRotation.y, spawnableObject.maxRotation.y),
                    Random.Range(spawnableObject.minRotation.z, spawnableObject.maxRotation.z));
                spawnedObjects.Add(networkObject);

                if (spawnCallStack != null)
                    spawnCallStack(newItem);
            }
        }
    }
    public void SpawnNewItemsClient(Message_FloatingObject_Create msg)
    {
        if (network.IsHost)
            return;

        SpawnableFloatingObject spawnableObject = spawnableObjects[(int)msg.floatingObjectIndex];

        //Get item and set position
        GameObject newItem = null;
        if (spawnableObject.typeToSpawn == ObjectSpawnType.Pool)
        {
            newItem = poolManager.GetObject(spawnableObject.poolName);
            newItem.transform.position = new Vector3(msg.xPos, msg.yPos, msg.zPos);
        }
        else
            newItem = Instantiate(spawnableObject.prefab, new Vector3(msg.xPos, msg.yPos, msg.zPos), Quaternion.identity);

        //Apply rotation
        newItem.transform.eulerAngles = new Vector3(msg.xRot, msg.yRot, msg.zRot);
        
        Network_FloatingObject netFloat = newItem.GetComponent<Network_FloatingObject>();
        netFloat.spawnType = spawnableObject.typeToSpawn;
        netFloat.spawner = this;
        netFloat.ObjectIndex = msg.objectIndex;
        netFloat.BehaviourIndex = msg.behaviourIndex;
        NetworkUpdateManager.AddBehaviour(netFloat);
        spawnedObjects.Add(netFloat);

        if (spawnCallStack != null)
            spawnCallStack(newItem);


        if (msg is Message_FloatingObjectReef_Create)
        {
            Message_FloatingObjectReef_Create msgReefCreate = msg as Message_FloatingObjectReef_Create;
            Reef reef = newItem.GetComponent<Reef>();
            for (int i = 0; i < msgReefCreate.activeReefIndexes.Length; i++)
            {
                reef.pickups[i].gameObject.SetActive(msgReefCreate.activeReefIndexes[i]);
            }
        }
    }

	//private methods
    private void RemoveItemsOutsideRange()
    {
        if (!network.IsHost)
            return;

        for (int i = 0; i < spawnedObjects.Count; i++)
        {
            //Remove outside radius
            Network_FloatingObject obj = spawnedObjects[i];
            Vector3 objPos = obj.transform.position;
            Vector3 raftPos = transform.position;
            objPos.y = raftPos.y = 0;

            float distanceToRaft = Vector3.Distance(objPos, raftPos);
            float extendedCircleRadius = spawnDistanceFromRaft * 1.3f;
            if (distanceToRaft >= extendedCircleRadius)
            {
                NetworkUpdateManager.SetBehaviourDead(obj);
                spawnedObjects.RemoveAt(i);
                i--;
            }
        }
    }
    private SpawnableFloatingObject GetObjectToSpawn()
    {
        float totalRange = 0;
        foreach (SpawnableFloatingObject obj in spawnableObjects)
            totalRange += obj.spawnChance;

        float randomValue = Random.Range(0, totalRange);
        float accumulative = 0;

        foreach (SpawnableFloatingObject obj in spawnableObjects)
        {
            accumulative += obj.spawnChance;

            if (randomValue < accumulative)
            {
                return obj;
            }
        }

        return null;
    }
}
