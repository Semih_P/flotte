﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public class RGD_Game
{
    [OptionalField(VersionAdded = 2)]// 1.04
    public float globalRaftTimeOffset;
    [OptionalField(VersionAdded = 2)]// 1.04
    public SerializableTransformF globalRaftSerializableTransform;

    [OptionalField(VersionAdded = 2)]// 1.04
    public List<RGD_Block> blocks = new List<RGD_Block>();
    [OptionalField(VersionAdded = 2)]// 1.04
    public List<RGD_CookingStand> cookingStands = new List<RGD_CookingStand>();
    [OptionalField(VersionAdded = 2)]// 1.04
    public List<RGD_Cropplot> cropplots = new List<RGD_Cropplot>();
    [OptionalField(VersionAdded = 2)]// 1.04
    public List<RGD_Inventory> inventories = new List<RGD_Inventory>();

    [OptionalField(VersionAdded = 3)]// 1.05
    public List<RGD_ItemNet> itemNets = new List<RGD_ItemNet>();
    [OptionalField(VersionAdded = 3)]// 1.05
    public RGD_Sky sky;

    [OptionalField(VersionAdded = 2)]// 1.04
    public RGD_Player player = new RGD_Player();
}
