﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviour_ID : MonoBehaviour 
{
    //properties
    public uint ObjectIndex { get { return objectIndex; } set { objectIndex = value; } }
    private uint objectIndex;

	//public variables
    public virtual RGD Serialize_Save()
    {
        return null;
    }

	//private variables

    //Unity methods
}
