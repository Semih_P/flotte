﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

[System.Serializable]
public class RGD_Inventory
{
    public List<RGD_Slot> slots = new List<RGD_Slot>();
    public int hotslotIndex;
    public uint storageObjectIndex;

    public RGD_Inventory(uint storageObjectIndex, Inventory inventory)
    {
        this.storageObjectIndex = storageObjectIndex;
        //Check if it is the players inventory
        bool isPlayerInventory = inventory is PlayerInventory;
        if (!isPlayerInventory)
        {
            hotslotIndex = -1;
        }
        else
        {
            hotslotIndex = ComponentManager<PlayerInventory>.Get().hotbar.GetSelectedSlotIndex();
        }

        //Save slots
        int slotCount = inventory.GetSlotCount();
        for (int i = 0; i < slotCount; i++)
        {
            Slot slot = inventory.GetSlot(i);
            if (slot == null)
                continue;

            RGD_Slot rgdSlot = new RGD_Slot(slot);
            slots.Add(rgdSlot);
        }
    }
    public void RestoreInventory(Inventory inventory)
    {
        if (inventory == null)
            return;

        //Reset each slot to save
        for (int i = 0; i < slots.Count; i++)
        {
            if (i < inventory.GetSlotCount())
            {
                Slot slot = inventory.GetSlot(i);
                RGD_Slot rgdSlot = slots[i];

                if (rgdSlot.uniqueItemIndex >= 0)
                {
                    Item_Base itemInSavedSlot = ItemManager.GetItemByIndex(rgdSlot.uniqueItemIndex);
                    slot.SetItem(itemInSavedSlot, rgdSlot.amount);
                }
                else
                {
                    slot.SetItem(null);
                }
            }
        }
    }
}
[System.Serializable]
public class RGD_Slot
{
    public int uniqueItemIndex;
    public int amount;

    public RGD_Slot(Slot slot)
    {
        if (slot.IsEmpty())
        {
            uniqueItemIndex = -1;
            amount = 0;
        }
        else
        {
            uniqueItemIndex = slot.itemInstance.UniqueIndex;
            amount = slot.itemInstance.amount;
        }
    }
}
