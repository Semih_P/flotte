﻿    using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveAndLoad : MonoBehaviour
{
    public static SaveAndLoad Singleton;

    //public variables
    [HideInInspector] public string Path;
    [HideInInspector] public string[] SavedGameNames;
    [HideInInspector] public Scene startScene;

    //private variables
    private static uint globalObjectIndex;

    void Awake()
    {
        if (Singleton != null)
        {
            Destroy(gameObject);
            return;
        }
        Singleton = this;

        startScene = SceneManager.GetActiveScene(); 
        Path = Application.persistentDataPath + "/SavedGames/";

        try
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
                Debug.Log("Created new directory at: " + Path);
            }
            else
            {
                //Debug.Log("Successfully found directory at: " + Path);
            }
        }
        catch(Exception e)
        {
            Debug.LogWarning(e.ToString() + " _|_ " + e.Message);
        }

        DontDestroyOnLoad(gameObject);
    }

    public static uint GetUniqueObjectIndex()
    {
        return ++globalObjectIndex;
    }

    public void Save()
    {
        RGD_Game game = CreateRGDGame();

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = File.Create(Path + GameManager.CurrentGameFileName);

        formatter.Serialize(file, game);

        file.Close();
        Debug.Log("Saved Map: Succes");
    }
    public void Load()
    {
        if (File.Exists(Path + GameManager.CurrentGameFileName))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream file = File.Open(Path + GameManager.CurrentGameFileName, FileMode.Open);

            RGD_Game game = null;
            try
            {
                game = formatter.Deserialize(file) as RGD_Game;
            }
            catch(Exception e)
            {
                Debug.Log(e);
                file.Close();

                CorruptFile(Path + GameManager.CurrentGameFileName, "OldVersion-");
                Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
                SceneManager.LoadScene("MainMenuScene");
                return;
            }

            if (game != null)
            {
                RestoreRGDGame(game);
                Debug.Log("Load Map: Success");
            }

            file.Close();
        }
    }
    public void CorruptFile(string path, string textOnFile)
    {
        if (File.Exists(path))
        {
            File.Copy(path, Path + textOnFile + GameManager.CurrentGameFileName, true);
            File.Delete(path);
            Debug.Log("Corrupted file: " + path + "\nWith message: " + textOnFile);
        }
    }
    private RGD_Game CreateRGDGame()
    {
        RGD_Game game = new RGD_Game();
        game.globalRaftTimeOffset = GameManager.Singleton.globalBuoyancyParent.GetComponent<BobTransform>().timeOffset;
        game.globalRaftSerializableTransform = new SerializableTransformF(GameManager.Singleton.globalBuoyancyParent);

        //Save time of day
        game.sky = new RGD_Sky(GameManager.Singleton.skyController);

        //Save blocks
        List<Block> blocks = BlockCreator.GetPlacedBlocks();
        for (int i = 0; i < blocks.Count; i++)
        {
            RGD_Block block = new RGD_Block();
            block.serializableTransform = new SerializableTransformF(blocks[i].transform);
            block.uniqueBlockIndex = blocks[i].buildableItem.UniqueIndex;
            block.health = blocks[i].health;
            block.blockIndex = blocks[i].blockIndex;

            game.blocks.Add(block);
        }

        //Save player
        PlayerStats playerStats = GameObject.FindObjectOfType<PlayerStats>();
        RGD_Player player = game.player;
        player.serializableTransform = new SerializableTransformF(playerStats.transform);
        player.hunger = playerStats.GetHunger();
        player.thirst = playerStats.GetThirst();
        player.health = playerStats.GetHealth();
        player.oxygen = playerStats.stat_oxygen.Value;
        //RGD_Inventory rgdPlayerInventory = new RGD_Inventory(-1, ComponentManager<PlayerInventory>.Get());
        //game.inventories.Insert(0, rgdPlayerInventory);

        //Save inventory
        //Storage_Small[] chests = GameObject.FindObjectsOfType<Storage_Small>();
        //if (chests.Length > 0)
        //{
        //    for (int i = 0; i < chests.Length; i++)
        //    {
        //        Inventory inventory = chests[i].GetInventoryReference();
        //        Block block = chests[i].GetComponent<Block>();
        //        if (inventory == null || block == null)
        //            continue;
        //        RGD_Inventory rgdInventory = new RGD_Inventory(block.blockIndex, inventory);
        //        game.inventories.Add(rgdInventory);
        //    }
        //}

        //Save cookingstands
        CookingStand[] cookingStands = GameObject.FindObjectsOfType<CookingStand>();
        if (cookingStands.Length > 0)
        {
            for (int i = 0; i < cookingStands.Length; i++)
            {
                CookingStand stand = cookingStands[i];
                RGD_CookingStand rgdStand = new RGD_CookingStand(stand);

                game.cookingStands.Add(rgdStand);
            }
        }

        //Save cropplots
        Cropplot[] cropplots = GameObject.FindObjectsOfType<Cropplot>();
        if (cropplots.Length > 0)
        {
            for (int i = 0; i < cropplots.Length; i++)
            {
                Cropplot plot = cropplots[i];
                RGD_Cropplot rgdPlot = new RGD_Cropplot(plot);

                game.cropplots.Add(rgdPlot);
            }
        }

        //Save itemnets
        //Net[] nets = GameObject.FindObjectsOfType<Net>();
        //if (nets.Length > 0)
        //{
        //    for (int i = 0; i < nets.Length; i++)
        //    {
        //        Net net = nets[i];
        //        RGD_ItemNet rgdNet = new RGD_ItemNet(net);

        //        game.itemNets.Add(rgdNet);
        //    }
        //}

        return game;
    }
    private void RestoreRGDGame(RGD_Game game)
    {
        //Restore time of day
        if (game.sky != null)
            game.sky.RestoreSky(GameManager.Singleton.skyController);

        //Restore player
        PlayerStats playerStats = GameObject.FindObjectOfType<PlayerStats>();
        if (playerStats != null)
        {
            RGD_Player rgdPlayer = game.player;
            rgdPlayer.RestorePlayer(playerStats);
        }

        //Restore player inventory
        game.inventories[0].RestoreInventory(ComponentManager<PlayerInventory>.Get());
        ComponentManager<PlayerInventory>.Get().hotbar.SelectHotslot(ComponentManager<PlayerInventory>.Get().GetSlot(game.inventories[0].hotslotIndex));
        ComponentManager<PlayerInventory>.Get().hotbar.SetSelectedSlotIndex(game.inventories[0].hotslotIndex);

        //Restore blocks
        GameManager.Singleton.globalBuoyancyParent.GetComponent<BobTransform>().timeOffset = game.globalRaftTimeOffset;
        game.globalRaftSerializableTransform.DeserializeTransform(GameManager.Singleton.globalBuoyancyParent);

        //Create all blocks and save special blocks
        Block.GlobalIndex = 0;
        List<Block> specialSavedBlocks = new List<Block>();
        for (int i = 0; i < game.blocks.Count; i++)
        {
            RGD_Block rgdBlock = game.blocks[i];
            Block block = ItemManager.GetItemByIndex(rgdBlock.uniqueBlockIndex).settings_buildable.GetBlockPrefab();
            Transform newBlockTransform = (Instantiate(block.prefab, Vector3.zero, block.prefab.transform.rotation) as GameObject).transform;
            newBlockTransform.transform.SetParent(GameManager.Singleton.globalBuoyancyParent);
            rgdBlock.serializableTransform.DeserializeTransform(newBlockTransform);

            Block newBlock = newBlockTransform.GetComponent<Block>();
            newBlock.health = rgdBlock.health;
            newBlock.blockIndex = rgdBlock.blockIndex;
            if (newBlock.blockIndex > Block.GlobalIndex)
            {
                Block.GlobalIndex = newBlock.blockIndex;
            }

            newBlock.RefreshOverlapps();
            BlockCreator.GetPlacedBlocks().Add(newBlock);

            if (newBlock.specialySaved)
            {
                specialSavedBlocks.Add(newBlock);
            }
        }


        //Restore special blocks
        for (int i = 0; i < specialSavedBlocks.Count; i++)
        {
            Block specialBlock = specialSavedBlocks[i];

            //Restore cookingstand
            if (RestoreCookingStand(game, specialBlock))
                continue;

            //Restore cropplots
            if (RestoreCropplot(game, specialBlock))
                continue;

            //Restore itemnets
            if (RestoreItemNet(game, specialBlock))
                continue;

            //Restore other inventories
            if (RestoreChest(game, specialBlock))
                continue;

        }
    }

    private bool RestoreCookingStand(RGD_Game game, Block specialBlock)
    {
        //if (game == null || game.cookingStands == null)
        //    return false;

        //for (int i = 0; i < game.cookingStands.Count; i++)
        //{
        //    RGD_CookingStand rgdStand = game.cookingStands[i];
        //    if (rgdStand.blockIndex == specialBlock.blockIndex)
        //    {
        //        CookingStand stand = specialBlock.GetComponent<CookingStand>();
        //        if (stand != null)
        //        {
        //            rgdStand.RestoreStand(stand);
        //            return true;
        //        }
        //        else
        //        {
        //            Debug.Log("Could not find cookingstand component on " + specialBlock.transform.name + "... May be wrong indexing on blocks");
        //        }
        //    }
        //}

        return false;
    }
    private bool RestoreCropplot(RGD_Game game, Block specialBlock)
    {
        if (game == null || game.cropplots == null)
            return false;

        for (int i = 0; i < game.cropplots.Count; i++)
        {
            //RGD_Cropplot rgdPlot = game.cropplots[i];
            //if (rgdPlot.blockIndex == specialBlock.blockIndex)
            //{
            //    Cropplot plot = specialBlock.GetComponent<Cropplot>();
            //    if (plot != null)
            //    {
            //        rgdPlot.RestorePlot(plot);
            //        return true;
            //    }
            //    else
            //    {
            //        Debug.Log("Could not find cropplot component on " + specialBlock.transform.name + "... May be wrong indexing on blocks");
            //    }
            //}
        }

        return false;
    }
    private bool RestoreChest(RGD_Game game, Block specialBlock)
    {
        //if (game == null || game.inventories == null)
        //    return false;

        //for (int i = 1; i < game.inventories.Count; i++)
        //{
        //    RGD_Inventory rgdInventory = game.inventories[i];
        //    if (rgdInventory.blockIndex == specialBlock.blockIndex)
        //    {
        //        Storage_Small chest = specialBlock.GetComponent<Storage_Small>();
        //        if (chest != null)
        //        {
        //            Inventory inventory = chest.GetInventoryReference();
        //            if (inventory != null)
        //            {
        //                rgdInventory.RestoreInventory(inventory);
        //                return true;
        //            }
        //        }
        //        else
        //        {
        //            Debug.Log("Could not find chest component on " + specialBlock.transform.name + "... May be wrong indexing on blocks");
        //        }
        //    }
        //}

        return false;
    }
    private bool RestoreItemNet(RGD_Game game, Block specialBlock)
    {
        if (game == null || game.itemNets == null)
            return false;

        //for (int i = 0; i < game.itemNets.Count; i++)
        //{
        //    RGD_ItemNet rgdNet = game.itemNets[i];
        //    if (rgdNet.blockIndex == specialBlock.blockIndex)
        //    {
        //        Net net = specialBlock.GetComponent<Net>();
        //        if (net != null)
        //        {
        //            rgdNet.RestoreItemNet(net);
        //            return true;
        //        }
        //        else
        //        {
        //            Debug.Log("Could not find Net component on " + specialBlock.transform.name + "... May be wrong indexing on blocks");
        //        }
        //    }
        //}

        return false;
    }
}
