﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RGD_ItemNet 
{
	//public variables
    public int blockIndex;
    public List<RGD_PickupItem> itemsInNet = new List<RGD_PickupItem>();

 	//private variables

    //CTOR
    //public RGD_ItemNet(Net net)
    //{
    //    blockIndex = net.transform.GetComponent<Block>().blockIndex;

    //    List<PickupItem> items = net.collector.pickedUpItems;
    //    for (int i = 0; i < items.Count; i++)
    //    {
    //        PickupItem item = items[i];
    //        if (item == null)
    //            continue;

    //        RGD_PickupItem rgdItem = new RGD_PickupItem(item);
    //        itemsInNet.Add(rgdItem);
    //    }
    //}

    ////public methods
    //public void RestoreItemNet(Net net)
    //{
    //    if (net == null || itemsInNet.Count == 0)
    //        return;

    //    for (int i = 0; i < itemsInNet.Count; i++)
    //    {
    //        RGD_PickupItem rgdItem = itemsInNet[i];
    //        if (rgdItem == null)
    //            continue;

    //        GameObject obj = PoolManager.Singleton.GetObject(rgdItem.uniqueItemIndex.ToString());
    //        PickupItem item = obj.GetComponent<PickupItem>();

    //        if (item != null)
    //        {
    //            rgdItem.RestorePickupItem(item);
    //        }
    //    }
    //}
    ////private methods

}

[System.Serializable]
public class RGD_PickupItem
{
    //public variables

    public RGD_ItemInstance rgdInstance;
    public Vector3 DropPosition { get { return new Vector3(xPos, yPos, zPos); } set { xPos = value.x; yPos = value.y; zPos = value.z; } }
    public Vector3 DropRotation { get { return new Vector3(xRot, yRot, zRot); } set { xRot = value.x; yRot = value.y; zRot = value.z; } }
    private float xPos, yPos, zPos;
    private float xRot, yRot, zRot;
    public int amount;

    //CTOR
    public RGD_PickupItem(PickupItem pickupItem)
    {
        if (pickupItem == null)
            return;

        rgdInstance = new RGD_ItemInstance(pickupItem.itemInstance);
        amount = pickupItem.amount;
    }

    //public methods
    public void RestorePickupItem(PickupItem pickupItem)
    {
        pickupItem.transform.position = DropPosition;
        pickupItem.transform.eulerAngles = DropRotation;
        pickupItem.itemInstance = rgdInstance.GetRealInstance();
        pickupItem.amount = amount;
    }
}
