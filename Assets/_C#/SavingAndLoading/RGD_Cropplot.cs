﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RGD_Cropplot
{
    public uint cropplotObjectIndex;
    public List<RGD_PlantationSlot> plantationSlots = new List<RGD_PlantationSlot>();

    public RGD_Cropplot(Cropplot cropplot)
    {
        cropplotObjectIndex = cropplot.ObjectIndex;

        for (int i = 0; i < cropplot.plantationSlots.Count; i++)
        {
            RGD_PlantationSlot rgdSlot = new RGD_PlantationSlot(cropplot.plantationSlots[i].plant);
            rgdSlot.plantationSlot = i;
            plantationSlots.Add(rgdSlot);
        }
    }

    public void RestoreCropplot(Cropplot plot, PlantManager plantManager)
    {
        foreach (RGD_PlantationSlot rgdSlot in plantationSlots)
        {
            Plant plant = plantManager.GetPlantByIndex(rgdSlot.uniquePlantIndex);
            if (plant != null)
            {
                Plant newPlant = plot.Plant(plant, rgdSlot.plantationSlot);

                newPlant.SetGrowTimer(rgdSlot.growTimer);
                newPlant.SetYield(rgdSlot.GetYield());
            }
        }
    }
}

[System.Serializable]
public class RGD_PlantationSlot
{
    public int uniquePlantIndex;
    public int plantationSlot;
    public float growTimer;
    public List<RGD_Yield> yields = new List<RGD_Yield>();

    public RGD_PlantationSlot(Plant plant)
    {
        if (plant != null)
        {
            uniquePlantIndex = plant.item.UniqueIndex;
            growTimer = plant.GetGrowTimer();


            foreach (Cost yield in plant.yieldItems)
            {
                RGD_Yield rgdYield = new RGD_Yield(yield);
                yields.Add(rgdYield);
            }
        }
        else
        {
            uniquePlantIndex = -1;
        }
    }

    public List<Cost> GetYield()
    {
        List<Cost> yield = new List<Cost>();
        foreach(RGD_Yield y in yields)
        {
            Cost cost = new Cost();
            cost.amount = y.amount;
            cost.item = ItemManager.GetItemByIndex(y.uniqueItemIndex) as Item_Base;
            yield.Add(cost);
        }
        return yield;
    }
}

[System.Serializable]
public class RGD_Yield
{
    public int uniqueItemIndex;
    public int amount;

    public RGD_Yield(Cost cost)
    {
        if (cost == null)
            return;

        uniqueItemIndex = cost.item.UniqueIndex;
        amount = cost.amount;
    }
}
