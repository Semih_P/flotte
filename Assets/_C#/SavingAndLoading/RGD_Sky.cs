﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public class RGD_Sky
{
    //public variables
    [OptionalField(VersionAdded = 3)]// 1.05
    public float timeOfDay;

 	//private variables

    //CTOR
    public RGD_Sky(AzureSky_Controller skyController)
    {
        timeOfDay = skyController.TIME_of_DAY;
    }

	//public methods
    public void RestoreSky(AzureSky_Controller skyController)
    {
        skyController.SetTime(timeOfDay, skyController.DAY_CYCLE);
    }
	//private methods

}
