﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RGD_Player
{
    //public variables
    public SerializableTransformF serializableTransform;

    public float hunger;
    public float thirst;
    public float health;
    public float oxygen;

    //public methods
    public void RestorePlayer(PlayerStats stats)
    {
        serializableTransform.DeserializeTransform(stats.transform);
        stats.ForceSetHunger(hunger);
        stats.ForceSetThirst(thirst);
        stats.ForceSetHealth(health);
        stats.stat_oxygen.Value = oxygen;
    }
}
