﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RGD_ItemInstance 
{
	//public variables
    public int itemIndex;
    public int amount;
    public int consumable_uses;

	//private variables

    //Ctor
    public RGD_ItemInstance(ItemInstance instance)
    {
        itemIndex = instance.UniqueIndex;
        amount = instance.amount;

        //Save consumable settings
        consumable_uses = instance.settings_consumeable.GetUses();
    }

	//public methods
    public ItemInstance GetRealInstance()
    {
        ItemInstance instance = new ItemInstance(ItemManager.GetItemByIndex(itemIndex), amount);

        //Restore consumable settings
        instance.settings_consumeable.SetUses(consumable_uses);


        return instance;
    }

	//private methods
}
