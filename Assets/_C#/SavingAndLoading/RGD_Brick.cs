﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RGD_Brick 
{
	//public variables
    public uint brickObjectIndex;
    public float dryTimer;

	//private variables

    //CTOR
    public RGD_Brick(Brick_Wet brick)
    {
        this.brickObjectIndex = brick.ObjectIndex;
        this.dryTimer = brick.Timer;
    }

	//public methods
    public void Restore(Brick_Wet brick)
    {
        brick.Timer = this.dryTimer;
    }
	//private methods
}
