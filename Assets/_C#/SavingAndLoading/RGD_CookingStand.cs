﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RGD_CookingStand
{
    public uint cookingStandObjectIndex;
    public List<RGD_CookingSlot> cookingSlots = new List<RGD_CookingSlot>();
    public float fuelTimer;
    public int fuelCount;

    public RGD_CookingStand(CookingStand stand)
    {
        this.cookingStandObjectIndex = stand.ObjectIndex;

        for (int i = 0; i < stand.cookingSlots.Length; i++)
        {
            CookingSlot cookingSlot = stand.cookingSlots[i];
            RGD_CookingSlot rgdCookingSlot = new RGD_CookingSlot(cookingSlot);
            cookingSlots.Add(rgdCookingSlot);
        }
        this.fuelTimer = stand.fuel.GetTimer();
        this.fuelCount = stand.fuel.GetFuelCount();
    }

    public void RestoreStand(CookingStand stand)
    {
        stand.fuel.ForceSetTimer(fuelTimer);
        stand.fuel.AddFuel(fuelCount);

        for (int i = 0; i < cookingSlots.Count; i++)
        {
            RGD_CookingSlot rgdCookingSlot = cookingSlots[i];
            CookingSlot cookingSlot = stand.cookingSlots[i];

            rgdCookingSlot.RestoreSlot(cookingSlot);
        }
    }
}
[System.Serializable]
public class RGD_CookingSlot
{
    public bool busy;
    public bool isComplete;
    public int cookableUniqueItemIndex;
    public float cookTimer;

    public RGD_CookingSlot(CookingSlot cookingSlot)
    {
        this.busy = cookingSlot.IsBusy;
        this.isComplete = cookingSlot.IsComplete;
        this.cookTimer = cookingSlot.CookTimer;
        this.cookableUniqueItemIndex = cookingSlot.GetCookingItem().UniqueIndex;
    }

    public void RestoreSlot(CookingSlot cookingSlot)
    {
        if (busy && !isComplete)
        {
            cookingSlot.StartCooking(ItemManager.GetItemByIndex(cookableUniqueItemIndex));
            cookingSlot.ForceSetTimer(cookTimer);
            return;
        }

        if (isComplete)
        {
            cookingSlot.StartCooking(ItemManager.GetItemByIndex(cookableUniqueItemIndex));
            cookingSlot.ForceSetTimer(float.MaxValue);
        }
    }
}
