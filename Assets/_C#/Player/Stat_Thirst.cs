﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stat_Thirst : Stat 
{
	//public variables
    [Header("Thirst settings")]
    [SerializeField] private Stat stat_targetThirst = null;
    [SerializeField] private float thirstLostPerSecond = 1f;
    [SerializeField] private float drinkTime = 20f;
    private float drinkTimer = 0f;
    private float startThirstValue = 0f;
    private bool drinking;

    [SerializeField] private YieldManipulator saltWaterManipulator = null;
    [SerializeField] private YieldManipulator waterManipulator = null;
	//private variables


	//Unity methods
    void Update()
    {
        if (drinking)
        {
            drinkTimer += Time.deltaTime / drinkTime;
            Value = Mathf.Lerp(startThirstValue, stat_targetThirst.Value, drinkTimer);

            if (Value == stat_targetThirst.Value)
            {
                drinking = false;
            }
        }
        else
        {
            Value -= Time.deltaTime * thirstLostPerSecond;
            stat_targetThirst.Value = Value;
        }
    }
    void OnValidate()
    {
        saltWaterManipulator.finalYield = saltWaterManipulator.baseYield * saltWaterManipulator.GetValue(saltWaterManipulator.cravePercent);
        waterManipulator.finalYield = waterManipulator.baseYield * waterManipulator.GetValue(waterManipulator.cravePercent);
    }

	//public methods
    public void Drink(Item_Base edibleItem)
    {
        drinking = true;
        startThirstValue = Value;
        drinkTimer = 0;
        float curveValue = edibleItem.settings_consumeable.IsRaw() ? saltWaterManipulator.GetValue(stat_targetThirst.NormalValue) : waterManipulator.GetValue(stat_targetThirst.NormalValue);
        stat_targetThirst.Value += edibleItem.settings_consumeable.GetThirstYield() * curveValue;
        ComponentManager<SoundManager>.Get().PlaySound("Drink");
    }
    public float GetTargetThirstNormal()
    {
        return stat_targetThirst.NormalValue;
    }

	//private methods
}
