﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stat_Hunger : Stat 
{
	//public variables

	//private variables
    [Header("Hunger settings")]
    [SerializeField] private Stat stat_targetHunger = null;
    [SerializeField] private float hungerLostPerSecond = 1f;
    [SerializeField] private float digestTime = 20f;
    private float digestTimer = 0f;
    private float startDigestValue = 0f;
    private bool digesting;

    [SerializeField] private YieldManipulator rawFoodManipulator = null;
    [SerializeField] private YieldManipulator cookedFoodManipulator = null;

	//Unity methods
    protected override void Awake()
    {
        base.Awake();
        stat_targetHunger.SetMaxValue(Max);
        stat_targetHunger.Value = Value;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            Value -= 20;
            stat_targetHunger.Value = Value;
        }
        if (Input.GetKeyDown(KeyCode.U))
            Eat(ItemManager.GetItemByName("Cooked_Potato"));

        if (digesting)
        {
            digestTimer += Time.deltaTime / digestTime;
            Value = Mathf.Lerp(startDigestValue, stat_targetHunger.Value, digestTimer);
            if (Value == stat_targetHunger.Value)
            {
                digesting = false;
            }
        }
        else
        {
            Value -= Time.deltaTime * hungerLostPerSecond;
            stat_targetHunger.Value = Value;
        }
    }
	void OnValidate()
    {
        rawFoodManipulator.finalYield = rawFoodManipulator.baseYield * rawFoodManipulator.GetValue(rawFoodManipulator.cravePercent);
        cookedFoodManipulator.finalYield = cookedFoodManipulator.baseYield * cookedFoodManipulator.GetValue(cookedFoodManipulator.cravePercent);
    }

	//public methods
    public float GetTargetHungerNormal()
    {
        return stat_targetHunger.NormalValue;
    }
    public void Eat(Item_Base edibleItem)
    {
        digesting = true;
        startDigestValue = Value;
        digestTimer = 0;

        float curveValue = edibleItem.settings_consumeable.IsRaw() ? rawFoodManipulator.GetValue(stat_targetHunger.NormalValue) : cookedFoodManipulator.GetValue(stat_targetHunger.NormalValue);
        stat_targetHunger.Value += edibleItem.settings_consumeable.GetHungerYield() * curveValue;
        ComponentManager<SoundManager>.Get().PlaySound("Chew");
    }

    //protected methods

	//private methods
}
