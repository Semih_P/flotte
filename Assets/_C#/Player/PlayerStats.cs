﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

[System.Serializable]
public class YieldManipulator
{
    public float baseYield;
    [Tooltip("How hungry/thirsty are you?, 0 = Could eat a horse, 1 = I'm full")]
    [Range(0, 1f)]public float cravePercent;
    [Space(20)]
    public float finalYield;

    [SerializeField] private AnimationCurve curve;

    public float GetValue(float craveNormal)
    {
        craveNormal = Mathf.Clamp(craveNormal, 0f, 1f);
        return Mathf.Clamp(curve.Evaluate(craveNormal), 0f, 1f);
    }
}
public class PlayerStats : Network_Entity
 {
 	//public variables
    [Header("Components")]
    [SerializeField] private PersonController personController = null;
    [SerializeField] private ParticleSystem bloodParticles = null;

    [Header("Stats")]
    [SerializeField] private Stat_Hunger stat_hunger = null;
    [SerializeField] private Stat_Thirst stat_thirst = null;
    [SerializeField] private Stat_WellBeing stat_wellBeing = null;
    public Stat_Oxygen stat_oxygen;

    [Header("Settings")]
    [SerializeField] private float healthLostPerSecond = 1f;


 	//private variables
    private CanvasHelper canvas;
    private Network_Player playerNetwork;


 	//Unity methods
    void Start()
    {
        canvas = CanvasHelper.Singleton;
        playerNetwork = GetComponentInParent<Network_Player>();
    }
	void Update () 
	{
        if (!playerNetwork.IsLocalPlayer)
            return;

        if (Input.GetKeyDown(KeyCode.K))
        {
            if (playerNetwork.Network.IsHost)
                Damage(20f);
            else
                playerNetwork.SendP2P(new Message_DamageEntity(Messages.DamageEntity, this, 20), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
        //Oxygen
        SetOxygenUI();
        
        //Decrease health if oxygen is zero
        if (stat_oxygen.Value <= 0 || stat_hunger.Value <= 0 || stat_thirst.Value <= 0)
        {
            stat_health.Value -= healthLostPerSecond * Time.deltaTime;
        }
        //Regenerate health if we feel ok
        else if (Stat_WellBeing.Factor > 1)
        {
            stat_health.Regenerate();
        }

        //Update UI for stats
        canvas.healthSlider.value = stat_health.NormalValue;
        canvas.hungerSlider.value = stat_hunger.NormalValue;
        canvas.hungerTargetSlider.value = stat_hunger.GetTargetHungerNormal();
        canvas.thirstSlider.value = stat_thirst.NormalValue;
        canvas.thirstTargetSlider.value = stat_thirst.GetTargetThirstNormal();
        canvas.wellBeingSlider.value = stat_wellBeing.NormalValue;
	}

	//public methods
    public void Consume(Item_Base edibleItem)
    {
        if (edibleItem.settings_consumeable.GetHungerYield() > 0)
        {
            stat_hunger.Eat(edibleItem);
        }
        if (edibleItem.settings_consumeable.GetThirstYield() > 0)
        {
            stat_thirst.Drink(edibleItem);
        }
    }

    public float GetHealth()
    {
        return stat_health.Value;
    }
    public float GetHunger()
    {
        return stat_hunger.Value;
    }
    public float GetThirst()
    {
        return stat_hunger.Value;
    }
    public void ForceSetHealth(float newValue)
    {
        stat_health.Value = newValue;
    }
    public void ForceSetHunger(float newValue)
    {
        stat_hunger.Value = newValue;
    }
    public void ForceSetThirst(float newValue)
    {
        stat_thirst.Value = newValue;
    }

    //private methods
    private void SetOxygenUI()
    {
        //Set UI for oxygen
        float fatigueNormal = 1 - stat_oxygen.NormalValue;
        canvas.SetFatigueSlider(fatigueNormal);
        canvas.SetFatigueSlider(fatigueNormal > 0);
    }

    //Ovveride methods
    public override void Damage(float damage)
    {
        if (playerNetwork.Network.IsHost)
            playerNetwork.Network.RPC(new Message_DamageEntity(Messages.DamageEntity, this, 20), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        base.Damage(damage);
        playerNetwork.playerNameTextMesh.text = playerNetwork.playerName + stat_health.Value;
        bloodParticles.Play();

        if (IsDead)
        {
            GetComponent<Player>().Kill();

            if (playerNetwork.IsLocalPlayer)
                GameManager.Singleton.GameOver();
        }
    }

    //Network methods
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        if (msg.type == Messages.DamageEntity)
        {
            Message_DamageEntity msgDamage = msg as Message_DamageEntity;
            Damage(msgDamage.damage);
        }
    }
}
