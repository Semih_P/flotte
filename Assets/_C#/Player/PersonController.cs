﻿using UnityEngine;
using System.Collections;
using PlayWay.Water;

public enum ControllerType { Ground, Water}
[RequireComponent(typeof(CharacterController))]
public class PersonController : MonoBehaviour
 {
    //public variables
    [Header("Movement settings")]
    public float normalSpeed = 3.0f;
    public float sprintSpeed = 6f;
    public float swimSpeed = 2.0f;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public float stopGravitySpeed = 5f;
    [SerializeField] private float jumpCooldown = 0.3f;

    [Space(10)]
    public float footstepSoundInterval = 1f;

    [Space(10)]
    [Header("Flags")]
    [HideInInspector]
    public ControllerType controllerType = ControllerType.Ground;
    public bool moving;
    public bool sprinting;
    public bool headAboveWater = true;

    [Header("Components")]
    public WaterCamera waterCamera;
    public JumpOutOfWaterChecker jumpOutOfWaterChecker;
    [SerializeField] private BoxCollider groundChecker;

    [Header("Network settings")]
    public float network_movementLerpSpeed = 8f;
    public float network_rotationLerpSpeed = 10f;

 	//private variables
    private CharacterController controller;
    private Vector3 moveDirection = Vector3.zero;
    private float yVelocity;
    private bool canJump = true;

    private float groundedTimer = 0f;
    private float footStepTimer;
    private Vector3 velocity;
    private Transform camTransform;
    private WaterPointGetter waterPointGetter;

    private Vector3 networkPosition;
    private float networkRotationY, networkRotationX;
    private Network_Player playerNetwork;
    private PlayerAnimator playerAnimator;

 	//Unity methods
    void Awake()
    {
        playerNetwork = GetComponent<Network_Player>();
        waterPointGetter = GetComponent<WaterPointGetter>();
        controller = GetComponent<CharacterController>();
        headAboveWater = true;
        transform.SetParent(GameManager.Singleton.globalBuoyancyParent);
        Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);
    }
    void Start()
    {
        playerAnimator = playerNetwork.Animator;
        camTransform = playerNetwork.CameraTransform;
    }
    void Update()
    {
        if (playerNetwork.IsLocalPlayer)
        {
            if (controllerType == ControllerType.Ground)
                GroundControll();
            else if (controllerType == ControllerType.Water)
                WaterControll();

            //Move with character controller
            controller.Move(moveDirection * Time.deltaTime);
        }
        else
        {
            bool ground = IsTouchingGround();
            playerNetwork.Animator.anim.SetBool("InAir", !ground);

            //Lerp rotation Y axis
            float newY = Mathf.LerpAngle(transform.eulerAngles.y, networkRotationY, Time.deltaTime * network_rotationLerpSpeed);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, newY, transform.eulerAngles.z);

            //Lerp rotation X axis
            if (controllerType == ControllerType.Water)
            {
                Vector3 euler = playerNetwork.playerPivot.eulerAngles;
                euler.x = Mathf.LerpAngle(euler.x, networkRotationX, Time.deltaTime * network_rotationLerpSpeed);
                playerNetwork.playerPivot.eulerAngles = euler;
            }

            //Lerp movement
            if (controllerType == ControllerType.Ground)
                transform.localPosition = Vector3.Lerp(transform.localPosition, networkPosition, Time.deltaTime * network_movementLerpSpeed);
            else if (controllerType == ControllerType.Water)
                transform.position = Vector3.Lerp(transform.position, networkPosition, Time.deltaTime * network_movementLerpSpeed);
        }

        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }

    //public methods
    public void SubmersionChanged()
    {
        switch(waterCamera.SubmersionState)
        {
            case SubmersionState.Full:
                SwitchControllerType(ControllerType.Water);
                headAboveWater = false;
                break;

            case SubmersionState.None:
                headAboveWater = true;
                break;
            case SubmersionState.Partial:
                break;
        }
    }
    public void SetNetworkProperties(Message_Player_Update msg)
    {
        if (playerAnimator != null)
        {
            if (msg.animVelocityX != 0f || msg.animVelocityZ != 0f)
                playerAnimator.anim.SetFloat("MoveSpeed", 1f);
            else
                playerAnimator.anim.SetFloat("MoveSpeed", 0f);
        }

        this.networkPosition = new Vector3(msg.posX, msg.posY, msg.posZ);
        this.networkRotationX = msg.rotX;
        this.networkRotationY = msg.rotY;

        if (this.controllerType != msg.controllerType)
            SwitchControllerType(msg.controllerType);
    }

	//private methods
    private void GroundControll()
    {
        bool ground = IsTouchingGround();
        if (ground)
        {
            groundedTimer += Time.deltaTime;
            if (groundedTimer > 0.25f)
            {
                groundedTimer = 0f;
                if (!canJump)
                {
                    CancelInvoke("ResetJump");
                    ResetJump();
                }
            }
        }
        else
        {
            groundedTimer = 0f;
        }

        //Flag if player is moving
        moving = controller.velocity.magnitude > 0.8f;

        //Flag if player is sprinting
        sprinting = Input.GetButton("Sprint");

        //Aquire input
        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        //Set direction to move in
        moveDirection = transform.right * input.x + transform.forward * input.z;

        //Jump
        if (ground)
        {
            yVelocity = 0;
            if (Input.GetButton("Jump") && canJump)
            {
                canJump = false;
                Invoke("ResetJump", jumpCooldown);

                yVelocity = jumpSpeed;
            }
        }
        //Fall with gravity
        else
        {
            yVelocity -= gravity * Time.deltaTime;
        }

        //Set animator settings
        playerAnimator.anim.SetBool("InAir", !ground);
        float camXRotation = camTransform.eulerAngles.x > 180 ? -(360 - camTransform.eulerAngles.x) : camTransform.eulerAngles.x;
        Vector3 animMoveVelocity = new Vector3((input.z < 0) ? -input.x : input.x, yVelocity, input.z);
        if (Stat_WellBeing.Factor < 2)
            animMoveVelocity *= 0.5f;
        playerAnimator.SetAnimVelocity(new Vector4(animMoveVelocity.x, animMoveVelocity.y, animMoveVelocity.z, Helper.MapValue(-75f, 70f, camXRotation, -1f, 1f)));
        playerAnimator.anim.SetFloat("MoveSpeed", (input.x != 0f || input.z != 0f) ? 1f : 0f);

        //Play movement sounds
        PlayFootSteps();


        //Multiply direction by speed
        float speed = (sprinting) ? sprintSpeed : normalSpeed;
        if (Stat_WellBeing.Factor < 2)
        {
            speed = normalSpeed * Stat_WellBeing.groundSpeedMultiplier;
        }
        if (!ground)
            speed *= 0.6f;

        moveDirection.Normalize();
        moveDirection *= speed;

        //Add gravity to movedirection
        moveDirection.y = yVelocity;
    }
    private void WaterControll()
    {
        //Flag if player is moving
        moving = controller.velocity.magnitude > 0.8f;

        //Aquire input
        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        //Set direction to move in
        moveDirection = (camTransform.forward.normalized * input.z) + (camTransform.right * input.x);

        //Move towards 0 y velocity because of just hitting water
        if (yVelocity != 0)
        {
            yVelocity = Mathf.Lerp(yVelocity, 0, Time.deltaTime * stopGravitySpeed);
            if (Mathf.Abs(yVelocity) <= 0.1f)
            {
                yVelocity = 0;
            }
        }
        else
        {
            if (Input.GetButton("Jump") && !headAboveWater)
            {
                moveDirection += Vector3.up;
            }
        }

        //Is the player at surface?
        if (headAboveWater)
        {
            //Lock player at surface if the the player is moving upwards
            if (moveDirection.y >= 0)
            {
                float waterPoint = waterPointGetter.GetWaterPoint(transform.position);
                Vector3 pos = transform.position;
                pos.y = Mathf.Lerp(pos.y, waterPoint, Time.deltaTime * 4);
                if (pos.y > waterPoint)
                    pos.y = waterPoint;
                transform.position = pos;
            }

            //Is player close to a block?
            if (jumpOutOfWaterChecker.isTouchingBlock)
            {
                //Jump out of water
                if (Input.GetButton("Jump"))
                {
                    SwitchControllerType(ControllerType.Ground);
                    yVelocity = jumpSpeed * 1.25f;
                }
            }

            //Keep player at surface if space is held down
            if (Input.GetButton("Jump"))
            {
                if (moveDirection.y < 0)
                    moveDirection.y = 0;
            }
        }

        //Set animator settings
        playerAnimator.SetAnimVelocity(new Vector3(input.x, 0f, input.z));

        //Multiply direction by speed
        float speed = swimSpeed;
        if (Stat_WellBeing.Factor < 2)
        {
            speed = swimSpeed * Stat_WellBeing.swimSpeedMultiplier;
        }
        moveDirection.Normalize();
        moveDirection *= speed;

        if (yVelocity != 0)
        {
            //Apply Y velocity to movement
            moveDirection.y = yVelocity;
        }
    }

    private void PlayFootSteps()
    {
        if (moving)
        {
            footStepTimer += Time.deltaTime;
            float limit = sprinting ? footstepSoundInterval * 0.75f : footstepSoundInterval;
            if (footStepTimer >= limit)
            {
                footStepTimer = 0;
                PlayFootStepSound();
            }
        }
    }
    private void PlayFootStepSound()
    {
        ComponentManager<SoundManager>.Get().PlaySound("FootstepWood");
    }
    private void ResetJump()
    {
        canJump = true;
    }
    private bool IsTouchingGround()
    {
        return Helper.IsColliding(groundChecker, LayerMasks.MASK_block);
    }

    private void SwitchControllerType(ControllerType newType)
    {
        if (newType == controllerType)
            return;

        if (playerAnimator == null)
            playerAnimator = playerNetwork.Animator;

        switch(newType)
        {
            case ControllerType.Ground:
                transform.SetParent(GameManager.Singleton.globalBuoyancyParent);
                playerAnimator.anim.SetBool("InWater", false);

                if (!playerNetwork.IsLocalPlayer)
                {
                    Vector3 euler = playerNetwork.playerPivot.eulerAngles;
                    euler.x = 0;
                    playerNetwork.playerPivot.eulerAngles = euler;
                }
                break;

            case ControllerType.Water:
                transform.SetParent(null);
                playerAnimator.anim.SetBool("InWater", true);
                playerAnimator.anim.SetBool("InAir", false);
                break;
        }

        controllerType = newType;
    }
}
