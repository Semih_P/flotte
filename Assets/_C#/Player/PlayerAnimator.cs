﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PlayerAnimation
{
    Index_0_Idle,
    Index_1_Point,
    Trigger_PointPlace,
    Index_2_Hook,
    Trigger_HookReset,
    Index_3_Fishing,
    Trigger_FishingRetract,
    Index_4_HammerAxe,
    Index_5_HoldItem,
    Index_6_Spear,
    Trigger_SpearHit,
    Trigger_GrabItem,
    Trigger_Plant,
    Index_8_HoldSeeds
}
[System.Serializable]
public class AnimationConnection
{
    [HideInInspector]
    public string name;

    public string layerName;
    public string stateName;

    public string functionName;
    public GameObject objectReciever;

    public float timeOfEvent;
    [HideInInspector]
    public bool calledEvent;
}
public class PlayerAnimator : MonoBehaviour
 {
 	//public variables
    [HideInInspector] public List<string> networkAnimTriggers = new List<string>();
    public List<AnimationConnection> connections = new List<AnimationConnection>();
    public Animator anim;
    [SerializeField]
    [Range(0, 15)]
    private float networkVelocityLerpSpeed = 9f;

 	//private variables
    private int selectedIndex = 0;
    private Vector4 animVelocity, animTargetVelocity;

 	//Unity methods
    void Update()
    {
        animVelocity = Vector4.Lerp(animVelocity, animTargetVelocity, Time.deltaTime * networkVelocityLerpSpeed);
        anim.SetFloat("VelocityX", animVelocity.x);
        anim.SetFloat("VelocityY", animVelocity.y);
        anim.SetFloat("VelocityZ", animVelocity.z);
        anim.SetFloat("VelocityW", animVelocity.w);


        foreach (AnimationConnection con in connections)
        {
            int layerIndex = anim.GetLayerIndex(con.layerName);
            AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(layerIndex);
            if (info.IsName(con.stateName))
            {
                //Debug.Log(info.normalizedTime);
                if (!con.calledEvent && info.normalizedTime >= con.timeOfEvent)
                {
                    con.objectReciever.SendMessage(con.functionName, SendMessageOptions.DontRequireReceiver);
                    con.calledEvent = true;
                }
            }

            if (con.calledEvent && info.normalizedTime < con.timeOfEvent)
                con.calledEvent = false;
        }
    }
    void OnValidate()
    {
        foreach(AnimationConnection con in connections)
        {
            if (string.IsNullOrEmpty(con.stateName))
                con.name = "Set a state name";
            else
                con.name = con.stateName;
        }
    }

    //Animaton methods
    public void SetAnimation(PlayerAnimation animation, bool triggering = false) 
    {
        string name = animation.ToString();
        string[] parts = name.Split('_');
        if (parts.Length == 0)
            return;

        string type = parts[0];
        if (type == "Index" && parts.Length == 3)
        {
            int index = int.Parse(parts[1]);

            SetAnimationIndex(index, false);
        }
        else if (type == "Trigger" && parts.Length == 2)
        {
            string parameterName = parts[1];

            anim.SetTrigger(parameterName);
            if (!networkAnimTriggers.Contains(parameterName))
                networkAnimTriggers.Add(parameterName);
            
            if (triggering)
                anim.SetBool("Triggering", triggering);
        }
    }
    public void SetAnimation(PlayerAnimation animation, bool overrideIndex, bool triggering)
    {
        string name = animation.ToString();
        string[] parts = name.Split('_');
        if (parts.Length == 0)
            return;

        string type = parts[0];
        if (type == "Index" && parts.Length == 3)
        {
            int index = int.Parse(parts[1]);

            SetAnimationIndex(index, overrideIndex);
        }
        else if (type == "Trigger" && parts.Length == 2)
        {
            string parameterName = parts[1];

            anim.SetTrigger(parameterName);
            if (!networkAnimTriggers.Contains(parameterName))
                networkAnimTriggers.Add(parameterName);

            if (triggering)
                anim.SetBool("Triggering", triggering);
        }
    }
    private void SetAnimationIndex(int index, bool overrideIndex)
    {
        if (!overrideIndex)
        {
            if (selectedIndex == index)
                return;
        }

        selectedIndex = index;
        anim.SetInteger("ItemID", selectedIndex);
        anim.SetTrigger("Switch");
    }

    public void SetNetworkProperties(Message_Player_Update msg)
    {
        SetAnimVelocity(new Vector4(msg.animVelocityX, msg.animVelocityY, msg.animVelocityZ, msg.animVelocityW));
        anim.SetBool("HammerAxeHit", msg.animHammerAxeHit);
        anim.SetBool("HookThrow", msg.animHookThrow);

        foreach (string trigger in msg.anim_triggers)
            anim.SetTrigger(trigger);
    }
    public void SetAnimVelocity(Vector4 velocity)
    {
        animTargetVelocity = velocity;
    }

    //Event method
    public void StopTriggering()
    {
        anim.SetBool("Triggering", false);
    }
}
