﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stat_WellBeing : Stat 
{
	//public variables
    public static int Factor = 1;
    public static float groundSpeedMultiplier = 0.6f;
    public static float swimSpeedMultiplier = 0.7f;
    public static float oxygenLostMultiplier = 3f;

	//private variables
    [SerializeField] private Stat_Hunger stat_hunger;
    [SerializeField] private Stat_Thirst stat_thirst;

	//Unity methods
    void Update()
    {
        if (stat_hunger == null || stat_thirst == null)
            return;

        float lowestNormal = stat_thirst.NormalValue < stat_hunger.NormalValue ? stat_thirst.NormalValue : stat_hunger.NormalValue;

        float third = 1f / 3f;
        if (lowestNormal < third)
            Factor = 1;
        else if (lowestNormal < third * 2)
            Factor = 2;
        else
            Factor = 3;

        Value = Max * (third * Factor);
    }

	//public methods

	//private methods
}
