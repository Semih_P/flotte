﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBob : MonoBehaviour
{
    //public variables
    public PersonController controller;
    public float bobbingSpeed = 0.18f;
    public float bobbingSpeedSprint = 0.18f;
    public float bobbingAmount = 0.2f;
    public float midpoint = 2.0f;

    //private variables
    private float timer;

    //Unity methods
    void Update()
    {
        if (controller.controllerType == ControllerType.Water || PauseMenu.IsOpen || GameManager.IsInMenu)
            return;

        float waveslice = 0.0f;
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float currentBobSpeed = controller.sprinting ? bobbingSpeedSprint : bobbingSpeed;
        Vector3 cSharpConversion = transform.localPosition;

        if (Mathf.Abs(horizontal) == 0 && Mathf.Abs(vertical) == 0)
        {
            timer = 0.0f;
        }
        else
        {
            waveslice = Mathf.Sin(timer);
            timer = timer + currentBobSpeed;
            if (timer > Mathf.PI * 2)
            {
                timer = timer - (Mathf.PI * 2);
            }
        }
        if (waveslice != 0)
        {
            float translateChange = waveslice * bobbingAmount;
            float totalAxes = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
            totalAxes = Mathf.Clamp(totalAxes, 0.0f, 1.0f);
            translateChange = totalAxes * translateChange;
            cSharpConversion.y = midpoint + translateChange;
        }
        else
        {
            cSharpConversion.y = midpoint;
        }

        transform.localPosition = cSharpConversion;
    }


    //public methods


    //private methods

}
