﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stat_Oxygen : Stat
{
	//public variables

	//private variables
    [SerializeField] private float oxygenLostPerSecond = 1f;
    [SerializeField] private PersonController personController = null;

	//Unity methods
	void Update () 
	{
        if (personController == null)
            return;

        //Regain oxygen
        if (personController.headAboveWater)
        {
            if (Value != Max)
            {
                Value = Mathf.Lerp(Value, Max, Time.deltaTime);
                if (NormalValue > 0.99f)
                {
                    Value = Max;
                }
            }
        }
        //Lose oxygen
        else
        {
            float lostPerSecond = Stat_WellBeing.Factor < 2 ? oxygenLostPerSecond * Stat_WellBeing.oxygenLostMultiplier : oxygenLostPerSecond;
            Value -= lostPerSecond * Time.deltaTime;
        }
	}

	//public methods
    public void SetOxygenLostPerSecond(float perSecond)
    {
        this.oxygenLostPerSecond = perSecond;
    }
    public float GetOxygenLostPerSecond()
    {
        return this.oxygenLostPerSecond;
    }

	//private methods
}
