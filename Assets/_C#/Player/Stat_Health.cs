﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stat_Health : Stat 
{
	//public variables

	//private variables
    [SerializeField] private float regenPerSecond;
    [SerializeField] private string uniqueSoundName;
    private AudioSource hurtSound;

	//Unity methods

	//public methods
    public void Regenerate()
    {
        Value += regenPerSecond * Time.deltaTime;
    }
    public bool IsDead()
    {
        return Value <= 0;
    }
    //protected methods
    protected override void ChangeValue(float diff, float newValue)
    {
        base.ChangeValue(diff, newValue);

        if (uniqueSoundName == string.Empty)
            return;

        if (diff < 0)
        {
            if (hurtSound == null)
                hurtSound = ComponentManager<SoundManager>.Get().PlaySound(uniqueSoundName);
            else if (!hurtSound.isPlaying)
                hurtSound.Play();
        }
    }

	//private methods
}
