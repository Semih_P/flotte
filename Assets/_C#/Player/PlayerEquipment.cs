﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipment : MonoBehaviour 
{
	//public variables
    [Header("Flipper settings")]
    public float swimSpeedMultiplier = 2f;

    [Header("AirBottle settings")]
    public float oxygenLostMultiplier = 0.6f;

	//private variables
    private PlayerStats playerStats;
    private PersonController controller;


	//Unity methods
	void Awake () 
	{
        controller = GetComponent<PersonController>();
        playerStats = GetComponent<PlayerStats>();
	}

	//public methods
    public void Equip(Item_Base item)
    {
        Debug.Log("Equip: " + item.UniqueName);
        switch(item.UniqueName)
        {
            case "Flipper":
                controller.swimSpeed *= swimSpeedMultiplier;
                break;
            case "AirBottle":
                float lostPer = playerStats.stat_oxygen.GetOxygenLostPerSecond();
                playerStats.stat_oxygen.SetOxygenLostPerSecond(lostPer * oxygenLostMultiplier);
                break;
        }
    }
    public void UnEquip(Item_Base item)
    {
        Debug.Log("UnEquip: " + item.UniqueName);
        switch (item.UniqueName)
        {
            case "Flipper":
                controller.swimSpeed /= swimSpeedMultiplier;
                break;
            case "AirBottle":
                float lostPer = playerStats.stat_oxygen.GetOxygenLostPerSecond();
                playerStats.stat_oxygen.SetOxygenLostPerSecond(lostPer / oxygenLostMultiplier);
                break;
        }
    }

	//private methods
}
