﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stat : MonoBehaviour
{
	//public variables
    public float Max { get { return max; } }
    private float max;

    public float Value 
    {
        get { return this.value; }
        set 
        {
            float diff = value - this.value;
            ChangeValue(diff, value);
        }
    }
    private float value;

    public float NormalValue { get { return Value / Max; } }

	//private variables 
    [SerializeField] private float startValue = 100f;

    //Unity methods
    protected virtual void Awake()
    {
        SetMaxValue(startValue);
        Value = startValue;
    }

	//public methods
    public void SetMaxValue(float newMax)
    {
        max = newMax;
    }

    //protected methods
    protected virtual void ChangeValue(float diff, float newValue)
    {
        value = Mathf.Clamp(newValue, 0, Max);
    }
	//private methods
}
