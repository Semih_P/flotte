﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpOutOfWaterChecker : MonoBehaviour 
{
	//Public variables
    public bool isTouchingBlock;

	//Private variables


	//Unity methods
    void OnTriggerStay(Collider other)
    {
        if (((1 << other.transform.gameObject.layer) & LayerMasks.MASK_block) != 0)
        {
            isTouchingBlock = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (((1 << other.transform.gameObject.layer) & LayerMasks.MASK_block) != 0)
        {
            isTouchingBlock = false;
        }
    }

	//Public methods


	//Private methods

}
