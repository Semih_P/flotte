﻿using UnityEngine;
using System.Collections;


public class Player : MonoBehaviour
 {
 	//public variables
    public static float UseDistance = 2.5f;
    public static bool IsDead;
    [SerializeField] private MouseLook mouseLookXScript;
    [SerializeField] private MouseLook mouseLookYScript;
    [SerializeField] private PlayerInventory playerInventoryPrefab;
    [SerializeField] private Hotbar playerHotbarPrefab;

 	//private variables
    private PlayerInventory playerInventory;
    private Network_Player playerNetwork;

 	//Unity methods
    void Awake()
    {
        Semih_Network.PlayerInitialized += PlayerInitialized;
    }
    void Start()
    {
        playerNetwork = GetComponent<Network_Player>();
    }
    void PlayerInitialized(Network_Player playerNetwork)
    {
        if (playerNetwork.IsLocalPlayer)
        {
            this.playerNetwork = playerNetwork;
            ComponentManager<Settings>.Get().LoadSettings(playerNetwork.Camera);
            ComponentManager<PlayerEquipment>.Set(GetComponent<PlayerEquipment>());

            //Setup inventory
            InitializeInventory();
        }
    }
    void Update()
    {
        if (playerNetwork == null || !playerNetwork.IsLocalPlayer)
            return;

        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F1))
        {
            playerInventory.AddItem("Scrap", 5);
            playerInventory.AddItem("Rope", 5);
            playerInventory.AddItem("Thatch", 5);
            playerInventory.AddItem("SeaVine", 5);
            playerInventory.AddItem("Stone", 5);
            playerInventory.AddItem("Plastic", 5);
            playerInventory.AddItem("Plank", 10);
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            playerInventory.AddItem("Raw_Beet", 1);
            playerInventory.AddItem("Raw_Mackerel", 1);
            playerInventory.AddItem("Raw_Shark", 1);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            Time.timeScale += 10;
        }
        else if (Input.GetKeyDown(KeyCode.K))
        {
            float newTimeScale = Time.timeScale - 10;
            if (newTimeScale < 0)
                newTimeScale = 1;

            Time.timeScale = newTimeScale;
        }
        #endif
    }

	//public methods
    public void Kill()
    {
        if (playerNetwork.IsLocalPlayer)
        {
            //Flag as dead
            IsDead = true;

            //Disable movement
            playerNetwork.Controller.enabled = false;

            //Disable mouse
            SetMouseLookScripts(false);

            //Close menus
            CanvasHelper.Singleton.CloseMenus();
        }


        //Mark as dead inside animator
        playerNetwork.Animator.anim.SetBool("Dead", true);

        //Deselect item in hand
        playerNetwork.ItemManager.SelectUsable(null);

    }
    public bool MouseLookIsActive()
    {
        return mouseLookXScript.enabled && mouseLookYScript.enabled;
    }
    public void SetMouseLookScripts(bool canLook)
    {
        if (mouseLookXScript != null && mouseLookYScript != null)
            mouseLookXScript.enabled = mouseLookYScript.enabled = canLook;
    }

	//private methods
    private void InitializeInventory()
    {
        //Create player inventory
        playerInventory = Instantiate(playerInventoryPrefab, Inventory.Parent) as PlayerInventory;
        playerInventory.transform.localScale = playerInventoryPrefab.transform.localScale;
        playerInventory.transform.localPosition = playerInventoryPrefab.transform.localPosition;

        ComponentManager<PlayerInventory>.Set(playerInventory);
        CanvasHelper.Singleton.inventoryMenus.Add(playerInventory.gameObject);

        //Create a hotbar
        Hotbar hotbar = Instantiate(playerHotbarPrefab, GameObject.FindWithTag("HotbarParent").transform) as Hotbar;
        hotbar.transform.localScale = playerHotbarPrefab.transform.localScale;

        hotbar.Initialize(playerNetwork.ItemManager, playerNetwork);
        playerInventory.Initialize(hotbar);
        
    }
}
