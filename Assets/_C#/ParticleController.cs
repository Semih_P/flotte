﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleController : MonoBehaviour
 {
 	//public variables
    public bool startEnabled = false;
    public List<ParticleSystem> particleSystems = new List<ParticleSystem>();
    public Transform particleParent;
    public bool IsPlaying { get { return isPlaying; } private set { isPlaying = value; } }

    private bool isPlaying;
 	//private variables


 	//Unity methods
	void Awake () 
	{
        if (startEnabled)
            PlayParticles();
        else
            StopParticles();
	}


	//public methods
    public void PlayParticles()
    {
        IsPlaying = true;
        foreach(ParticleSystem system in particleSystems)
        {
            system.Play();
        }
    }
    public void StopParticles()
    {
        IsPlaying = false;
        foreach (ParticleSystem system in particleSystems)
        {
            system.Stop();
        }
    }

    public void SetPosition(Vector3 pos)
    {
        if (particleParent != null)
            particleParent.position = pos;
    }
    public void SetLookRotation(Vector3 dir)
    {
        if (particleParent != null)
            particleParent.rotation = Quaternion.LookRotation(dir);
    }

	//private methods
}
