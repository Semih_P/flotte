﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlantComponent : MonoBehaviour
 {
 	//public variables
    [SerializeField] private Plant plantPrefab;
    [SerializeField] private PlantManager plantManager;

 	//private variables
    private Cropplot lastCropplot;
    private Network_Player playerNetwork;
    private PlayerAnimator playerAnimator;

 	//Unity methods
    void Start()
    {
        playerNetwork = GetComponentInParent<Network_Player>();
    }
    void Update()
    {
        if (GameManager.IsInMenu || !playerNetwork.IsLocalPlayer)
            return;

        Cropplot cropplot = GetCropplot();
        if (cropplot != null)
        {
            lastCropplot = cropplot;
            if (cropplot.AcceptsPlantType(plantPrefab.item))
            {
                cropplot.IsAimedAtWithPlantable = true;
            }
            else
                return;

            if (cropplot.IsFull)
                return;

            if (Input.GetButtonDown("UseButton"))
            {
                if (playerNetwork.Network.IsHost)
                    plantManager.Plant(cropplot, plantPrefab, true);
                else
                    playerNetwork.SendP2P(new Message_PlantSeed(Messages.PlantSeed, plantManager, cropplot, plantPrefab), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
            }
        }
        else if (lastCropplot != null)
        {
            lastCropplot.IsAimedAtWithPlantable = false;
            lastCropplot = null;
        }
    }

    //public methods
    public void OnSelect()
    {
        plantManager.OnSelectPlantComponent(plantPrefab.holdPlantAnimation);
    }
    public void OnDeSelect()
    {
        if (lastCropplot != null)
        {
            lastCropplot.IsAimedAtWithPlantable = false;
        }
    }

    //private methods
    private Cropplot GetCropplot()
    {
        RaycastHit hit = Helper.HitAtCursor(Player.UseDistance, LayerMasks.MASK_item);

        if (hit.transform != null && hit.transform.tag == "Cropplot")
        {
            Cropplot cropplot = hit.transform.gameObject.GetComponent<Cropplot>();

            return cropplot;
        }

        return null;
    }
}
