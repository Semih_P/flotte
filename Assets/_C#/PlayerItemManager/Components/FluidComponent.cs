﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluidComponent : MonoBehaviour 
{
	//public variables

	//private variables
    [SerializeField] private Transform fluidTransform;
    [SerializeField] private float yPosStartLocal;
    [SerializeField] private float stepInterval;
    private float targetY = 0;

	//Unity methods
	protected virtual void Start () 
	{
        HideFluid(true);
	}
    void Update()
    {
        if (fluidTransform.gameObject.activeInHierarchy)
        {
            Vector3 pos = fluidTransform.localPosition;
            if (pos.y != targetY)
            {
                pos.y = Mathf.Lerp(pos.y, targetY, Time.deltaTime * 2);
                fluidTransform.localPosition = pos;

                if (targetY == yPosStartLocal)
                {
                    float diff = Mathf.Abs(pos.y - targetY);
                    if (diff < 0.01f)
                    {
                        fluidTransform.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

	//public methods
    public void SetStep(int stepNumber)
    {
        if (!fluidTransform.gameObject.activeInHierarchy)
            ShowFluid();

        targetY = yPosStartLocal + stepInterval * stepNumber;
    }
    public void HideFluid(bool instantly = false)
    {
        if (instantly)
        {
            fluidTransform.gameObject.SetActive(false);
        }
        else
            targetY = yPosStartLocal;
    }

	//private methods
    private void ShowFluid()
    {
        fluidTransform.gameObject.SetActive(true);
    }
}
