﻿using UnityEngine;
using System.Collections;

public class BuildComponent : MonoBehaviour
 {
 	//public variables
    [SerializeField] private BlockCreator blockCreator;

 	//private variables
    private Item_Base previousBlockType;
    private Network_Player playerNetwork;

 	//Unity methods
    void Start()
    {
        playerNetwork = GetComponentInParent<Network_Player>();
    }

	//public methods
    public void OnSelect()
    {
        if (playerNetwork == null)
            playerNetwork = GetComponentInParent<Network_Player>();
        if (blockCreator == null)
            blockCreator = playerNetwork.BlockCreator;

        if (!playerNetwork.IsLocalPlayer)
            return;


        blockCreator.gameObject.SetActive(true);
        previousBlockType = blockCreator.GetCurrentBlockType();

        Item_Base itemInHand = playerNetwork.ItemManager.useItemController.GetCurrentItemInHand();
        blockCreator.SetBlockTypeToBuild(itemInHand.UniqueName);
    }
    public void OnDeSelect()
    {
        if (!playerNetwork.IsLocalPlayer)
            return;

        blockCreator.SetGhostBlockVisibility(false);
        if (previousBlockType != null)
            blockCreator.SetBlockTypeToBuild(previousBlockType.UniqueName);
        blockCreator.gameObject.SetActive(false);
    }

	//private methods
}
