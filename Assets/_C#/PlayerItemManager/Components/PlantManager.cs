﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantManager : MonoBehaviour_Network 
{
    public static List<Cropplot> allCropplots = new List<Cropplot>();

	//public variables
    [SerializeField]
    private List<Plant> allPlants = new List<Plant>();

	//private variables
    private Network_Player playerNetwork;
    private PlayerInventory playerInventory;
    private PlayerAnimator playerAnimator;
    private SoundManager soundManager;
    private CanvasHelper canvas;

	//Unity methods
    void Start()
    {
        playerNetwork = GetComponentInParent<Network_Player>();
        playerAnimator = playerNetwork.Animator;
        playerInventory = ComponentManager<PlayerInventory>.Get();
        soundManager = ComponentManager<SoundManager>.Get();
        canvas = CanvasHelper.Singleton;
    }
    void Update()
    {
        HarvestPlants();
    }
    private void HarvestPlants()
    {
        if (GameManager.IsInMenu || !playerNetwork.IsLocalPlayer)
            return;

        RaycastHit hit = Helper.HitAtCursor(Player.UseDistance, LayerMasks.MASK_item);
        if (hit.transform != null && hit.transform.tag == "Plant")
        {
            Plant plant = hit.transform.GetComponent<Plant>();

            if (plant.FullyGrown() && plant.harvestable)
            {
                canvas.SetDisplayText("Harvest", true);

                if (Input.GetButtonDown("UseButton"))
                {
                    if (playerNetwork.Network.IsHost)
                        Harvest(plant, true);
                    else
                        playerNetwork.SendP2P(new Message_HarvestPlant(Messages.HarvestPlant, this, plant.cropplot, plant), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                }
            }
        }
    }

	//public methods
    public Plant GetPlantByIndex(int uniqueItemIndex)
    {
        foreach (Plant plant in allPlants)
        {
            if (plant.item.UniqueIndex == uniqueItemIndex)
            {
                return plant;
            }
        }
        return null;
    }
    public void Plant(Cropplot cropplot, Plant plantPrefab, bool useNetwork)
    {
        NetworkUI.Singleton.Console("Planting: " + cropplot.ObjectIndex);
        cropplot.Plant(plantPrefab);

        if (playerNetwork.IsLocalPlayer)
        {
            soundManager.PlaySound("PlantSeed");
            playerAnimator.SetAnimation(PlayerAnimation.Trigger_Plant, true);
            playerInventory.RemoveSelectedHotSlotItem(1);
        }

        if (useNetwork)
        {
            playerNetwork.Network.RPC(new Message_PlantSeed(Messages.PlantSeed, this, cropplot, plantPrefab), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }
    public void Harvest(Plant plant, bool useNetwork)
    {
        NetworkUI.Singleton.Console("Harvesting: " + plant.cropplot.ObjectIndex + "," + plant.plantationSlotIndex);
        if (useNetwork)
        {
            playerNetwork.Network.RPC(new Message_HarvestPlant(Messages.HarvestPlant, this, plant.cropplot, plant), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }

        Cost yieldObj = plant.GetYieldItem(); 
        if (playerNetwork.IsLocalPlayer)
        {
            playerInventory.AddItem(yieldObj.item.UniqueName, yieldObj.amount);
        }
        if (plant.YieldLeft() == 0)
            plant.Pickup();
    }
    public void OnSelectPlantComponent(PlayerAnimation holdAnimation)
    {
        if (playerAnimator == null)
            playerAnimator = GetComponentInParent<Network_Player>().Animator;

        playerAnimator.SetAnimation(holdAnimation);
    }

	//private methods
    private static Cropplot GetCropplotByIndex(uint objectIndex)
    {
        foreach(Cropplot cropplot in allCropplots)
        {
            if (cropplot.ObjectIndex == objectIndex)
            {
                return cropplot;
            }
        }
        return null;
    }   

    //network methods
    public override void Serialize_Create()
    {
        AddMessage(new Message_PlantManagerCreate(Messages.PlantManagerCreate, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.PlantManagerCreate:
                Message_PlantManagerCreate msgCreate = msg as Message_PlantManagerCreate;
                for (int i = 0; i < msgCreate.rgdCropplots.Count; i++)
                {
                    RGD_Cropplot rgdCropplot = msgCreate.rgdCropplots[i];
                    Cropplot cropplotRestore = GetCropplotByIndex(rgdCropplot.cropplotObjectIndex);

                    rgdCropplot.RestoreCropplot(cropplotRestore, this);
                }
                break;

            case Messages.PlantSeed:
                Message_PlantSeed msgPlant = msg as Message_PlantSeed;
                Plant plant = GetPlantByIndex(msgPlant.plantUniqueIndex);
                Cropplot cropplot = GetCropplotByIndex(msgPlant.cropplotObjectIndex);

                Plant(cropplot, plant, playerNetwork.Network.IsHost);

                break;

            case Messages.HarvestPlant:
                Message_HarvestPlant msgHarvest = msg as Message_HarvestPlant;
                cropplot = GetCropplotByIndex(msgHarvest.cropplotObjectIndex);

                Harvest(cropplot.plantationSlots[msgHarvest.plantationSlot].plant, playerNetwork.Network.IsHost);
                break;
        }
    }
}

[System.Serializable]
public class Message_PlantManagerCreate : Message_NetworkBehaviour
{
    public List<RGD_Cropplot> rgdCropplots = new List<RGD_Cropplot>();

    public Message_PlantManagerCreate(Messages type, PlantManager plantManager)
        :base(type, plantManager)
    {
        for(int i = 0; i < PlantManager.allCropplots.Count; i++)
        {
            Cropplot cropplot = PlantManager.allCropplots[i];
            RGD_Cropplot rgdCropplot = new RGD_Cropplot(cropplot);
            rgdCropplots.Add(rgdCropplot);
        }
    }
}
[System.Serializable]
public class Message_PlantSeed : Message_NetworkBehaviour
{
    public uint cropplotObjectIndex;
    public int plantUniqueIndex;

    public Message_PlantSeed(Messages type, MonoBehaviour_Network behaviour, Cropplot cropplot, Plant plant)
        :base(type, behaviour)
    {
        plantUniqueIndex = plant.item.UniqueIndex;
        cropplotObjectIndex = cropplot.ObjectIndex;
    }
}
[System.Serializable]
public class Message_HarvestPlant : Message_NetworkBehaviour
{
    public uint cropplotObjectIndex;
    public int plantationSlot;

    public Message_HarvestPlant(Messages type, MonoBehaviour_Network behaviour, Cropplot cropplot, Plant plant)
        :base(type, behaviour)
    {
        plantationSlot = plant.plantationSlotIndex;
        cropplotObjectIndex = cropplot.ObjectIndex;
    }
}
