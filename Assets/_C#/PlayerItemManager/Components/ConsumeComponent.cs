﻿using UnityEngine;
using System.Collections;

public class ConsumeComponent : MonoBehaviour
 {
 	//public variables
 	//private variables
    private bool canConsume;
    private PlayerStats playerStats;
    private PlayerInventory inventory;

 	//Unity methods
	void Start () 
	{
        canConsume = true;
        playerStats = GetComponentInParent<PlayerStats>();
        inventory = ComponentManager<PlayerInventory>.Get();
	}

	//public methods
    public void OnUse()
    {
        if (GameManager.IsInMenu)
            return;

        Consume();
    }

	//private methods
    private void Consume()
    {
        ItemInstance itemInstance = inventory.GetSelectedHotbarItem();
        if (itemInstance.settings_consumeable.GetUses() <= 0)
            return;

        if (!canConsume)
            return;
        canConsume = false;


        //Set eating on cooldown
        Invoke("ResetCanConsume", itemInstance.settings_usable.GetUseButtonCooldown());

        //Increase stats
        playerStats.Consume(itemInstance.baseItem);

        //Aquire currently selected hotslot
        Slot selectedSlot = inventory.GetSelectedHotbarSlot();

        //Remove 1 use from the item
        itemInstance.settings_consumeable.IncrementUses(-1);

        //If there is no uses left, remove and add possible after use item
        if (itemInstance.settings_consumeable.GetUses() <= 0)
        {
            //Add possible after use item
            Cost itemAfterUse = itemInstance.settings_consumeable.GetItemAfterUse();
            if (itemAfterUse.item != null)
            {
                selectedSlot.SetItem(itemAfterUse.item, itemAfterUse.amount);
                inventory.hotbar.ReselectCurrentSlot();
            }
            else
            {
                //Remove one stack size
                itemInstance.amount -= 1;
                selectedSlot.RefreshComponents();

                //If there is stacks left, refill uses and reselect
                if (itemInstance.amount >= 1)
                {
                    itemInstance.settings_consumeable.SetUses(itemInstance.settings_consumeable.GetMaxUses());
                    inventory.hotbar.ReselectCurrentSlot();
                }
                //Remove from hotbar
                else
                {
                    inventory.RemoveSelectedHotSlotItem(1);
                }
            }
        }
        else
        {
            //Fake drink animation by pulling hand down and up
            inventory.hotbar.ReselectCurrentSlot();
        }
    }

    private void ResetCanConsume()
    {
        canConsume = true;
    }
}
