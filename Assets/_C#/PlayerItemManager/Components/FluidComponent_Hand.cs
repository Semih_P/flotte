﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluidComponent_Hand : FluidComponent 
{
	//public variables

	//private variables
    private PlayerInventory inventory;

	//Unity methods
    protected override void Start()
    {
        base.Start();
        inventory = ComponentManager<PlayerInventory>.Get();
    }

	//public methods
    public void OnSelect()
    {
        if (inventory == null)
            inventory = ComponentManager<PlayerInventory>.Get();

        if (!inventory.hotbar.playerNetwork.IsLocalPlayer)
            return;

        ItemInstance handItem = inventory.GetSelectedHotbarItem();
        int uses = handItem.settings_consumeable.GetUses();
        if (uses > 0)
            SetStep(handItem.settings_consumeable.GetUses());
        else
            HideFluid(true);
    }
	//private methods
}
