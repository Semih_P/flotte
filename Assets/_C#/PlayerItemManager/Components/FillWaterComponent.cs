﻿using UnityEngine;
using System.Collections;

public class FillWaterComponent : MonoBehaviour
 {
 	//public variables

 	//private variables
    [SerializeField] private Item_Base saltWaterItem;
    private CanvasHelper canvasHelper;
    private PlayerInventory inventory;
    private Network_Player playerNetwork;

 	//Unity methods
    void Start()
    {
        playerNetwork = GetComponentInParent<Network_Player>();
        canvasHelper = CanvasHelper.Singleton;
        inventory = ComponentManager<PlayerInventory>.Get();
    }
	void Update () 
	{
        if (!playerNetwork.IsLocalPlayer)
            return;

        ItemInstance instance = inventory.GetSelectedHotbarItem();
        if (instance == null)
            return;

        if (AimingAtTarget())
        {
            if (instance.GetType() == ItemType.Drinkable)
            {
                //Fill empty with water
                if (instance.settings_consumeable.FluidType == Fluid.None)
                {
                    canvasHelper.SetDisplayText("Fill " + instance.settings_Inventory.GetDisplayName() + " with saltwater", true);
                    if (Input.GetButtonDown("UseButton"))
                    {
                        canvasHelper.SetDisplayText(false);
                        Slot slot = inventory.GetSelectedHotbarSlot();
                        slot.SetItem(saltWaterItem, 1);
                        instance.settings_consumeable.SetUses(instance.settings_consumeable.GetMaxUses());
                        inventory.hotbar.ReselectCurrentSlot();
                    }
                }
                //Refill if they have the same fluid type
                else if (instance.settings_consumeable.FluidType == saltWaterItem.settings_consumeable.FluidType)
                {
                    if (!instance.settings_consumeable.HasMaxUses())
                    {
                        canvasHelper.SetDisplayText("Fill " + instance.settings_Inventory.GetDisplayName() + " with saltwater", true);
                        if (Input.GetButtonDown("UseButton"))
                        {
                            canvasHelper.SetDisplayText(false);
                            instance.settings_consumeable.SetUses(instance.settings_consumeable.GetMaxUses());
                            inventory.hotbar.ReselectCurrentSlot();
                        }
                    }
                }
            }
        }
        else if (canvasHelper.displayText.text.Contains("saltwater"))
        {
            canvasHelper.SetDisplayText(false);
        }
	}


	//public methods

	//private methods
    private bool AimingAtTarget()
    {
        RaycastHit hit = Helper.HitAtCursor(Player.UseDistance * 3f);

        return hit.transform != null && hit.transform.tag == "Water";
    }
}
