﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ItemConnection
{
    [HideInInspector]
    public string name = "ItemConnection";
    public Item_Base inventoryItem;
    public GameObject obj;
}

public class UseItemController : MonoBehaviour
 {
 	//public variables
    public List<ItemConnection> allConnections = new List<ItemConnection>();

 	//private variables
    private Item_Base usableItem;
    private Dictionary<string, GameObject> connectionDictionary = new Dictionary<string, GameObject>();
    private GameObject activeObject;
    private Network_Player playerNetwork;
    private PlayerAnimator playerAnimator;
    private bool canUse = true;

 	//Unity methods
    void Awake()
    {
        foreach (ItemConnection con in allConnections)
        {
            connectionDictionary.Add(con.inventoryItem.UniqueName, con.obj);
            con.obj.gameObject.SetActive(false);
        }
    }
    void Start()
    {
        playerNetwork = GetComponentInParent<Network_Player>();
        playerAnimator = playerNetwork.Animator;
    }
	void Update () 
	{
        if (!playerNetwork.IsLocalPlayer)
            return;

        if (!usableItem || !canUse || GameManager.IsInMenu)
            return;

        string buttonName = usableItem.settings_usable.GetUseButtonName();
        if (usableItem.settings_usable.AllowHoldButton())
        {
            if (Input.GetButton(buttonName))
                Use();
        }
        else
        {
            if (Input.GetButtonDown(buttonName))
                Use();
        }
	}
    void OnValidate()
    {
        foreach(ItemConnection con in allConnections)
        {
            if (con.inventoryItem != null)
            {
                con.name = con.inventoryItem.UniqueName;
                if (!con.name.Contains("Placeable") && !con.name.Contains("PlasticCup") && !con.name.Contains("PlasticBottle"))
                {
                    if (con.obj != null && con.obj.transform.name != con.name)
                    {
                        con.obj.transform.name = con.name;
                    }
                }
            }
        }
    }

	//public methods
    public void StartUsing(Item_Base item)
    {
        if (connectionDictionary.ContainsKey(item.UniqueName))
        {
            this.usableItem = item;

            if (item.settings_Inventory.SetAnimationToGrip())
                playerAnimator.SetAnimation(PlayerAnimation.Index_5_HoldItem, true, false);

            activeObject = connectionDictionary[item.UniqueName];
            activeObject.SetActive(true);
            activeObject.SendMessage("OnSelect", SendMessageOptions.DontRequireReceiver);
        }
    }
    public void Deselect()
    {
        if (usableItem != null)
        {
            if (connectionDictionary.ContainsKey(usableItem.UniqueName))
            {
                if (activeObject != null)
                {
                    activeObject.SendMessage("OnDeSelect", SendMessageOptions.DontRequireReceiver);
                    activeObject = null;
                }
                connectionDictionary[usableItem.UniqueName].SetActive(false);
                usableItem = null;
            }
        }
    }
    public GameObject GetObjectByName(string uniqueName)
    {
        return connectionDictionary[uniqueName];
    }
    public Item_Base GetCurrentItemInHand()
    {
        return usableItem;
    }

	//private methods
    private void Use()
    {
        canUse = false;
        Invoke("ResetUse", usableItem.settings_usable.GetUseButtonCooldown());

        if (activeObject != null)
            activeObject.SendMessage("OnUse", SendMessageOptions.DontRequireReceiver);
    }
    private void ResetUse()
    {
        canUse = true;
        if (activeObject != null)
            activeObject.SendMessage("OnResetUse", SendMessageOptions.DontRequireReceiver);
    }
}
