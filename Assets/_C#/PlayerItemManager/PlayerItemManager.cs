﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UseItemController))]
public class PlayerItemManager : MonoBehaviour_Network
 {
    private enum PlayerState { Nothing, Using }

 	//public variables
    public static bool IsBusy;
    [SerializeField] private Network_Player playerNetwork;

 	//private variables
    private PlayerState state;
    [HideInInspector] public UseItemController useItemController;
    private PlayerAnimator playerAnimator;

 	//Unity methods
    void Awake()
    {
        useItemController = GetComponent<UseItemController>();
    }
    void Start()
    {
        IsFresh = false;        
        playerAnimator = playerNetwork.Animator;
    }

	//public methods
    public bool CanSwitch()
    {
        return !IsBusy;
    }
    public void SelectUsable(Item_Base item)
    {
        if (playerNetwork.IsLocalPlayer)
        {
            if (item != null)
            {
                SwitchState(PlayerState.Using);
                useItemController.StartUsing(item);
            }
            else
            {
                SwitchState(PlayerState.Nothing);
            }
            SetDirty();
        }
    }

	//private methods
    private void SwitchState(PlayerState newState)
    {
        //Out
        SwitchOut(state);

        //In
        SwitchIn(newState);
    }
    private void SwitchOut(PlayerState stateOut)
    {
        switch(stateOut)
        {
            case PlayerState.Nothing:
                break;
            case PlayerState.Using:
                useItemController.Deselect();
                break;
        }
    }
    private void SwitchIn(PlayerState stateIn)
    {
        IsBusy = false;
        switch (stateIn)
        {
            case PlayerState.Nothing:
                if (playerAnimator != null)
                    playerAnimator.SetAnimation(PlayerAnimation.Index_0_Idle);
                break;
            case PlayerState.Using:
                break;
        }

        state = stateIn;
    }

    //Network methods
    public override void Serialize_Create()
    {
        Serialize_Update();
    }
    public override void Serialize_Update()
    {
        if (useItemController != null)
        {
            Item_Base itemInHand = useItemController.GetCurrentItemInHand();
            int index = itemInHand == null ? -1 : itemInHand.UniqueIndex;
            AddMessage(new Message_SelectItem(Messages.SelectItem, this, index));
        }
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        Message_SelectItem msgSelectItem = msg as Message_SelectItem;
        Item_Base item = msgSelectItem.itemIndex == -1 ? null : ItemManager.GetItemByIndex(msgSelectItem.itemIndex);

        if (item != null)
        {
            SwitchState(PlayerState.Using);
            useItemController.StartUsing(item);
        }
        else
        {
            SwitchState(PlayerState.Nothing);
        }
    }
}

[System.Serializable]
public class Message_SelectItem : Message_NetworkBehaviour
{
    public int itemIndex;
    public Message_SelectItem(Messages type, MonoBehaviour_Network behaviour, int itemIndex) : base(type, behaviour)
    {
        this.itemIndex = itemIndex;
    }
}
