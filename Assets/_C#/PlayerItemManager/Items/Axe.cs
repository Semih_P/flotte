﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AxeMode { None, Chopping, RemovingBlock}
public class Axe : MonoBehaviour
 {
 	//public variables
    public AxeMode mode;
    public LayerMask hitmask;
    [SerializeField] private ParticleController woodParticles;
    [SerializeField] private PlantManager plantManager;

 	//private variables
    private PlayerAnimator playerAnimator;
    private Network_Player playerNetwork;
    private PlayerInventory playerInventory;
    private CanvasHelper canvas;
    private float chopTimer;
    public float chopBlockTime = 2f;
    private Block currentBlockToRemove;
    private Plant currentPlantToChop;
    private RaycastHit chopHitPoint;

 	//Unity methods
	void Start () 
	{
        canvas = CanvasHelper.Singleton;
        playerNetwork = GetComponentInParent<Network_Player>();
        playerAnimator = playerNetwork.Animator;

        woodParticles.particleParent.parent = null;
	}
	void Update () 
	{
        if (GameManager.IsInMenu || !playerNetwork.IsLocalPlayer)
            return;

        if (Input.GetButton("LeftClick"))
        {
            playerAnimator.anim.SetBool("HammerAxeHit", true);
            RaycastHit hit = Helper.HitAtCursor(5f, hitmask);
            if (hit.transform != null)
            {
                chopHitPoint = hit;

                if (hit.transform.tag == "Plant")
                {
                    Plant plant = hit.transform.GetComponentInParent<Plant>();
                    if (plant != null && !plant.harvestable && plant.FullyGrown())
                    {
                        mode = AxeMode.Chopping;

                        currentPlantToChop = plant;
                    }
                }
                else
                {
                    Block block = hit.transform.GetComponentInParent<Block>();
                    if (block != null && block.removeableWithAxe)
                    {
                        mode = AxeMode.RemovingBlock;
                        chopTimer += Time.deltaTime;

                        //Started on new block
                        if (block != currentBlockToRemove || currentBlockToRemove == null)
                        {
                            chopTimer = 0f;
                            currentBlockToRemove = block;
                        }

                        if (chopTimer >= chopBlockTime)
                        {
                            chopTimer -= chopBlockTime;
                            DestroyBlock(currentBlockToRemove);
                        }

                        canvas.SetLoadCircle(chopTimer > 0);
                        canvas.SetLoadCircle(chopTimer / chopBlockTime);
                    }
                    else
                    {
                        chopTimer = 0f;
                        currentBlockToRemove = null;
                    }
                }
            }
        }
        else
        {
            playerAnimator.anim.SetBool("HammerAxeHit", false);
            canvas.SetLoadCircle(false);
        }
	}


	//public methods
    public void OnAxeHit()
    {
        if (!playerNetwork.IsLocalPlayer)
            return;
        Debug.Log("Axe hit");

        if (mode == AxeMode.Chopping)
        {
            if (currentPlantToChop != null)
            {
                bool dropCoconuts = currentPlantToChop.YieldLeft() == 1;

                if (playerNetwork.Network.IsHost)
                    plantManager.Harvest(currentPlantToChop, true);
                else
                    playerNetwork.SendP2P(new Message_HarvestPlant(Messages.HarvestPlant, plantManager, currentPlantToChop.cropplot, currentPlantToChop), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

                if (Random.Range(0, 3) == 0)
                {
                    Plant_PalmTree palmTree = currentPlantToChop as Plant_PalmTree;
                    if (palmTree.coconuts.Count > 0)
                    {
                        Coconut coconut = palmTree.coconuts[0];
                        coconut.transform.SetParent(GameManager.Singleton.globalBuoyancyParent);
                        coconut.DropFromTree();
                        palmTree.coconuts.Remove(coconut);
                    }
                }
            }
        }

        currentPlantToChop = null;
        currentBlockToRemove = null;

        if (mode != AxeMode.None)
        {
            mode = AxeMode.None;
            ComponentManager<SoundManager>.Get().PlaySound("WoodHit");
            woodParticles.SetPosition(chopHitPoint.point);
            woodParticles.SetLookRotation(chopHitPoint.normal);
            woodParticles.PlayParticles();
        }
    }
    public void OnSelect()
    {
        if (playerAnimator == null)
            playerAnimator = GetComponentInParent<Network_Player>().Animator;

        playerAnimator.SetAnimation(PlayerAnimation.Index_4_HammerAxe);
    }
    public void OnDeSelect()
    {
        playerAnimator.anim.SetBool("HammerAxeHit", false);
        canvas.SetLoadCircle(false);
    }

	//private methods
    private void DestroyBlock(Block block)
    {
        if (block == null)
            return;
        if (playerInventory == null)
            playerInventory = ComponentManager<PlayerInventory>.Get();

        //Return original object
        if (block.itemToReturnOnDestroy != null)
        {
            playerInventory.AddItem(block.itemToReturnOnDestroy.UniqueName, 1);
            switch (block.buildableItem.UniqueIndex)
            {
                case 42: //Chest
                    Storage_Small chest = block.gameObject.GetComponent<Storage_Small>();
                    if (chest != null)
                    {
                        Inventory inv = chest.GetInventoryReference();
                        for (int i = 0; i < inv.GetSlotCount(); i++)
                        {
                            Slot slot = inv.GetSlot(i);
                            if (slot.IsEmpty())
                                continue;

                            playerInventory.AddItem(slot.itemInstance.UniqueName, slot.itemInstance.amount);
                        }
                    }
                    break;

                case 43: //Food one
                case 77: //Food two
                case 71: //Smelter
                case 78: //Purifier two
                    CookingStand stand = block.gameObject.GetComponent<CookingStand>();
                    if (stand != null)
                    {
                        foreach(CookingSlot slot in stand.cookingSlots)
                        {
                            if (slot.IsBusy && slot.IsComplete)
                            {
                                playerInventory.AddItem(slot.GetCookingItem().UniqueName, 1);
                            }
                        }
                    }
                    break;

                case 8: //Itemnet
                    ItemCollector netItemCollector = block.gameObject.GetComponentInChildren<ItemCollector>();
                    foreach (PickupItem pickup in netItemCollector.pickedUpItems)
                    {
                        for (int i = 0; i < pickup.amount; i++)
                        {
                            Item_Base itemToPickup = pickup.GetItem();
                            playerInventory.AddItem(itemToPickup.UniqueName, 1);
                        }
                    }
                    break;

                case 45: //Cropplot small
                case 46: //Cropplot tree
                    Cropplot cropplot = block.gameObject.GetComponent<Cropplot>();
                    if (cropplot != null)
                    {
                        List<PlantationSlot> slots = cropplot.GetSlots();
                        for (int i = 0; i < slots.Count; i++)
                        {
                            if (!slots[i].busy)
                                continue;

                            Plant plant = slots[i].plant;
                            if (plant.FullyGrown())
                            {
                                for (int j = 0; j < plant.yieldItems.Count; j++)
                                {
                                    Cost yield = plant.yieldItems[j];
                                    playerInventory.AddItem(yield.item.UniqueName, yield.amount);
                                }
                            }
                        }
                    }
                    break;
            }
        }
        //Return materials
        else
        {
            for (int i = 0; i < block.buildCost.Count; i++)
            {
                Cost cost = block.buildCost[i];
                int amount = Mathf.CeilToInt(((float)cost.amount * 0.5f));
                playerInventory.AddItem(cost.item.UniqueName, amount);
            }
        }

        NetworkUpdateManager.SetBehaviourDead(block);
        currentBlockToRemove = null;
    }
}
