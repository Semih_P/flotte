﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedCollision : MonoBehaviour 
{
	//public variables

    //private variables
    [HideInInspector] public BoxCollider[] colliders;


 	//private variables

 	//Unity methods
    void Awake()
    {
        colliders = GetComponents<BoxCollider>();
    }

	//public methods
    public bool IsOverlapping(BoxCollider[] advCollisions)
    {
        foreach (BoxCollider box in colliders)
        {
            if (Helper.IsColliding(box, LayerMasks.MASK_block, advCollisions))
                return true;
        }
        return false;
    }

	//private methods
}
