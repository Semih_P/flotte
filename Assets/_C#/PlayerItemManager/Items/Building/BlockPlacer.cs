﻿//using UnityEngine;
//using System.Collections;
//using UnityEngine.UI;
//using System.Collections.Generic;
//using System.Linq;

//public class BlockPlacer : SingletonGeneric<BlockPlacer>
// {
//    public delegate void OnBlockChange();
//    private OnBlockChange placeBlockCallstack;

//    //public variables
//    [Header("Build settings")]
//    public Material ghostMaterial;
//    public List<Block> blockPrefabs = new List<Block>();

//    [HideInInspector]
//    public List<Block> allPlacedBlocks = new List<Block>();

//    //private variables
//    private CanvasHelper canvasHelper;
//    private BlockType selectedBlockType = BlockType.Foundation;
//    private BlockQuad selectedQuad;
//    private Vector3 selectedQuadHitPoint;
//    private Block selectedBlock;
//    private Transform ghostBlock;
//    private const float rotateDegreeSnap = 90f;
//    private const float rotateDegreeSmooth = 5f;
//    private float currentRotationY;
//    private float rotationKeyAccumulation;
//    private bool aimingAtQuad;

//    //Unity methods
//    void Awake()
//    {
//        canvasHelper = CanvasHelper.Singleton;
//        canvasHelper.HideBlockPlacerCostBoxes();

//        if (GameManager.IsInNewGame)
//        {
//            CreateNewGameLayout();
//        }
//    }
//    void Update () 
//    {
//        Block block = GetBlockFromType(selectedBlockType);
//        if (block == null)
//            return;

//        //Select current block
//        SelectBlock(block);

//        //Get current quad at cursor;
//        selectedQuad = AquireQuadAtCursor();

//        aimingAtQuad = selectedQuad != null;

//        //Deactivate/active ghost block gameobject
//        bool canPlaceBlock = selectedQuad != null && selectedQuad.AcceptsBlock(selectedBlock);
//        SetGhostBlockState(canPlaceBlock);

//        //Rotate current block
//        if (selectedBlock.isRotateable)
//        {
//            if (Input.GetButtonDown("Rotate"))
//            {
//                if (selectedBlock.canRotateFreely)
//                    GameObject.FindObjectOfType<Player>().SetMouseLookScripts(false);
//                else
//                    RotateBlock(selectedBlock, rotateDegreeSnap, true);
//            }

//            if (selectedBlock.canRotateFreely)
//            {
//                float accumulationLimit = 0.2f;
//                if (Input.GetButton("Rotate"))
//                {
//                    rotationKeyAccumulation += Time.deltaTime;
//                    if (rotationKeyAccumulation >= accumulationLimit)
//                    {
//                        float degree = Input.GetAxis("Mouse X");
//                        RotateBlock(selectedBlock, degree * rotateDegreeSmooth);
//                    }
//                }
//                if (Input.GetButtonUp("Rotate"))
//                {
//                    if (rotationKeyAccumulation < accumulationLimit)
//                    {
//                        RotateBlock(selectedBlock, rotateDegreeSnap, true);
//                    }
//                    rotationKeyAccumulation = 0;
//                    GameObject.FindObjectOfType<Player>().SetMouseLookScripts(true);
//                }
//            }
//        }

//        SetGhostBlockPositionAndRotation();
//        ShowCostOfSelectedBlock();

//        if (!canPlaceBlock)
//            return;

//        bool canBuildBlock = true;
//        bool posOccupied = PositionOccupied(selectedBlock);
//        bool isStable = selectedBlock.IsStable();
//        bool hasEnoughResources = HasEnoughResourcesToBuild(selectedBlock);
//        if (posOccupied ||!isStable || !hasEnoughResources)
//            canBuildBlock = false;


//        if (canPlaceBlock && canBuildBlock)
//            ghostMaterial.color = Color.green;
//        else
//            ghostMaterial.color = Color.red;

//        if (Input.GetButtonDown("LeftClick") && canBuildBlock && !GameManager.IsInMenu)
//        {
//            PlaceBlock();
//        } 
//    }

//    //public methods
//    public void SubsribeToOnPlace(OnBlockChange method)
//    {
//        placeBlockCallstack += method;
//    }
//    public void OnHammerDeSelect()
//    {
//        canvasHelper.HideBlockPlacerCostBoxes();
//        canvasHelper.SetDisplayText(false);
//        SetGhostBlockState(false);
//        this.enabled = false;
//    }
//    public void SetBlockTypeToBuild(BlockType type)
//    {
//        selectedBlockType = type;

//        if (type == BlockType.Repair)
//        {
//            SetGhostBlockState(false);
//        }
//    }
//    public void RemoveBlock(Block block)
//    {
//        DestroyBlock(block);

//        for (int i = 0; i < allPlacedBlocks.Count; i++)
//        {
//            if (allPlacedBlocks[i] == null)
//                allPlacedBlocks.RemoveAt(i--);
//        }

//        foreach(Block b in allPlacedBlocks)
//        {
//            b.RefreshOverlapps();
//        }
//    }
//    public void SetGhostBlockState(bool state)
//    {
//        if (ghostBlock == null)
//            return;

//        ghostBlock.gameObject.SetActive(state);
//    }
//    public void Disable()
//    {
//        if (this != null)
//        {
//            this.enabled = false;
//            SetGhostBlockState(false);
//        }
//    }
//    public BlockType GetCurrentBlockType()
//    {
//        return selectedBlockType;
//    }
//    public Block GetBlockFromType(BlockType type)
//    {
//        foreach(Block b in blockPrefabs)
//        {
//            if (b.uniqueBlockName == type)
//            {
//                return b;
//            }
//        }
//        return null;
//    }

//    //private methods
//    private void SelectBlock(Block block)
//    {
//        if (selectedBlock != null && block != null && selectedBlock.uniqueBlockName == block.uniqueBlockName)
//            return;

//        //Destroy Previous
//        DestroyGhost();

//        //Select new ghost
//        ghostBlock = ((GameObject)Instantiate(block.prefab, Vector3.zero, block.prefab.transform.rotation, GameManager.Singleton.globalRaftParent)).transform;
//        ghostBlock.eulerAngles = new Vector3(ghostBlock.parent.eulerAngles.x, block.prefab.transform.eulerAngles.y, ghostBlock.parent.eulerAngles.z);
//        selectedBlock = ghostBlock.GetComponent<Block>();
        
//        //Rotate block to saved rotation
//        currentRotationY = Mathf.RoundToInt(currentRotationY / 90) * 90;
//        if (block.isRotateable)
//        {
//            ghostBlock.Rotate(Vector3.up, currentRotationY, Space.Self);
//        }


//        //Call event
//        selectedBlock.OnStartingPlacement();

//        //Start looking for overlaps & change material
//        if (selectedBlock.overlappingComponent != null)
//        {
//            selectedBlock.overlappingComponent.SetNewMaterial(ghostMaterial);
//        }


//        SetGhostBlockState(false);
//    }
//    private void DestroyBlock(Block block)
//    {
//        if (block != null)
//        {
//            DestroyImmediate(block.gameObject);
//        }

//        foreach(Block b in allPlacedBlocks)
//        {
//            if (b == block)
//                continue;

//            if (b != null)
//            {
//                if (!b.IsStable())
//                {
//                    DestroyBlock(b);
//                }
//            }
//        }
//    }
//    private void RotateBlock(Block block, float degrees, bool snap = false)
//    {
//        if (block == null)
//            return;

//        currentRotationY += degrees;
//        if (snap)
//        {
//            currentRotationY = Mathf.RoundToInt(currentRotationY / 90) * 90;
//        }
//        if (currentRotationY > 360)
//            currentRotationY -= 360;


//        Transform t = block.transform;
//        //t.Rotate(Vector3.up, degrees, Space.Self);
//        t.eulerAngles = new Vector3(t.eulerAngles.x, currentRotationY, t.eulerAngles.z);
//    }
//    private void PlaceBlock()
//    {
//        PlayerAnimator.SetAnimation(PlayerAnimation.Trigger_HammerHit);
//        CanvasHelper.Singleton.SetDisplayText(false);
//        ComponentManager<SoundManager>.Get().PlaySound("PlaceObject");
//        GameObject.FindObjectOfType<Player>().SetMouseLookScripts(true);

//        foreach(Cost cost in selectedBlock.buildCost)
//        {
//            PlayerInventory.Singleton.RemoveItem(cost.item, cost.amount);
//        }



//        ghostBlock = null;
//        selectedQuad = null;

//        allPlacedBlocks.Add(selectedBlock);

//        //Restore material and stop looking for overlapps
//        if (selectedBlock.overlappingComponent != null)
//        {
//            selectedBlock.overlappingComponent.RestoreToDefaultMaterial();
//        }

//        if (placeBlockCallstack != null)
//            placeBlockCallstack();


//        selectedBlock.OnFinishedPlacement();
//        selectedBlock = null;

//        foreach (Block b in allPlacedBlocks)
//        {
//            b.RefreshOverlapps();
//        }
//    }

//    private void SetGhostBlockPositionAndRotation()
//    {
//        if (ghostBlock == null || selectedQuad == null)
//            return;

//        //Set position
//        if (selectedBlock.snapsToQuads)
//        {
//            ghostBlock.position = selectedQuad.transform.position + selectedBlock.buildOffset;
//        }
//        else
//        {
//            ghostBlock.position = selectedQuadHitPoint + selectedBlock.buildOffset;
//        }

//        //Set rotation
//        //ghostBlock.rotation = selectedBlock.prefab.transform.rotation;
//        ghostBlock.localEulerAngles = new Vector3(0, ghostBlock.localEulerAngles.y, 0);
//    }
//    private void DestroyGhost()
//    {
//        if (ghostBlock != null)
//        {
//            Destroy(ghostBlock.gameObject);
//        }
//    }

//    private void ShowCostOfSelectedBlock()
//    {
//        if (selectedBlock == null || GameManager.IsInMenu)
//        {
//            canvasHelper.HideBlockPlacerCostBoxes();
//            canvasHelper.SetDisplayText(false);
//        }
//        else
//        {
//            canvasHelper.HideBlockPlacerCostBoxes();

//            for (int i = 0; i < selectedBlock.buildCost.Count; i++)
//            {
//                Cost cost = selectedBlock.buildCost[i];
//                canvasHelper.SetBlockPlacerCostBox(i, cost);
//            }

//            if (selectedBlock.uniqueBlockName != BlockType.Repair)
//            {
//                string text = selectedBlock.displayName;
//                if (selectedBlock.isRotateable)
//                {
//                    text += "\n'R' to rotate";
//                    if (selectedBlock.canRotateFreely)
//                    {
//                        text += ", hold for smooth rotation";
//                    }
//                }
//                canvasHelper.SetDisplayText(text);
//            }
//        }
//    }
//    private BlockQuad AquireQuadAtCursor()
//    {
//        RaycastHit hit;
//        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));

//        if (Physics.Raycast(ray, out hit, Player.UseDistance * 2, LayerMasks.MASK_buildQuad))
//        {
//            BlockQuad quad = hit.transform.GetComponent<BlockQuad>();

//            if (selectedBlock.snapsToTopCollider)
//            {
//                Collider col = hit.transform.GetComponent<Collider>();

//                if (col != null)
//                {
//                    Vector3 pos = hit.point;
//                    pos.y = col.bounds.max.y;
//                    selectedQuadHitPoint = pos;
//                }
//            }
//            else
//                selectedQuadHitPoint = hit.point;

//            return quad;
//        }

//        return null;
//    }
//    private bool PositionOccupied(Block block)
//    {
//        if (block.uniqueBlockName == BlockType.Wall_Window || block.uniqueBlockName == BlockType.Wall)
//        {
//            bool hitOtherThanWall = false;
//            foreach (AdvancedCollision advancedCol in block.overlappingComponent.advancedCollisions)
//            {
//                foreach(Collider col in advancedCol.overlappingColliders)
//                {
//                    if (col != null)
//                    {
//                        Block hitBlock = col.GetComponentInParent<Block>();
//                        if (hitBlock != null)
//                        {
//                            if (hitBlock.uniqueBlockName != BlockType.Wall && hitBlock.uniqueBlockName != BlockType.Wall_Window)
//                            {
//                                hitOtherThanWall = true;
//                            }
//                            if (hitBlock.transform.position == block.transform.position && Mathf.RoundToInt(hitBlock.transform.localEulerAngles.y) == Mathf.RoundToInt(block.transform.localEulerAngles.y))
//                            {
//                                hitOtherThanWall = true;
//                            }
//                        }
//                    }
//                }
//            }

//            return hitOtherThanWall;
//        }
//        else
//            return block.overlappingComponent.IsCollidingWithOtherBlocks();
//    }
//    private bool HasEnoughResourcesToBuild(Block block)
//    {
//        if (block == null)
//            return false;

//        foreach(Cost cost in block.buildCost)
//        {
//            int invCount = PlayerInventory.Singleton.GetItemCount(cost.item.UniqueName);
//            if (invCount < cost.amount)
//                return false;
//        }

//        return true;
//    }

//    private void CreateNewGameLayout()
//    {
//        //Find prefab to spawn
//        Block blockPrefab = GetBlockFromType(BlockType.Foundation);

//        //Create layout
//        Vector3[] positions = new Vector3[4];
//        positions[0] = new Vector3(-0.75f, 0, -0.75f);
//        positions[1]= new Vector3(-0.75f, 0, 0.75f);
//        positions[2] = new Vector3(0.75f, 0, 0.75f);
//        positions[3] = new Vector3(0.75f, 0, -0.75f);

//        for (int i = 0; i < 4; i++)
//        {
//            Transform blockTransform = Instantiate(blockPrefab.transform, Vector3.zero, blockPrefab.transform.rotation).transform;
//            blockTransform.SetParent(GameManager.Singleton.globalRaftParent);
//            blockTransform.position = positions[i];
//            blockTransform.eulerAngles = new Vector3(blockTransform.parent.eulerAngles.x, blockPrefab.transform.eulerAngles.y, blockTransform.parent.eulerAngles.z);


//            //Aquire block
//            Block block = blockTransform.GetComponent<Block>();
//            block.OnFinishedPlacement();
//            allPlacedBlocks.Add(block);
//        }
//    }
//}
