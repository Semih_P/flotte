﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BlockQuadType", menuName = "Semih/BlockQuadType", order = 1)]
public class BlockQuadType : ScriptableObject 
{
	//public variables
    [Header("What type of blocks can be built?")]
    [SerializeField] private List<Item_Base> acceptableBlockTypes = new List<Item_Base>();
    
    [Header("Extension settings")]
    [SerializeField] private Item_Base key;
    [SerializeField] private Item_Base extension;

    public bool HasExtension { get { return key != null && extension != null; } }
    public Item_Base Key { get { return key; } }
    public Item_Base Extension { get { return extension; } }


	//private variables

	//Unity methods

	//public methods
    public bool AcceptsBlock(Item_Base block)
    {
        foreach (Item_Base blockType in acceptableBlockTypes)
        {
            if (blockType.UniqueIndex == block.UniqueIndex)
                return true;
        }
        return false;
    }

	//private methods
}
