﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class OccupyingComponent : MonoBehaviour
 {
 	//public variables
    [HideInInspector]
    public MeshRenderer[] renderers;
    [HideInInspector]
    public AdvancedCollision[] advancedCollisions;
    [HideInInspector]
    public List<BoxCollider> allAdvBoxColliders = new List<BoxCollider>();

 	//private variables
    private Material[] originalMaterials;

 	//Unity methods
    void Awake()
    {
        renderers = transform.GetComponentsInChildren<MeshRenderer>();
        advancedCollisions = transform.GetComponentsInChildren<AdvancedCollision>();

        if (renderers.Length > 0)
        {
            originalMaterials = new Material[renderers.Length];
        }
        for (int i = 0; i < renderers.Length; i++)
        {
            originalMaterials[i] = renderers[i].material;
        }

    }
    void Start()
    {
        foreach(AdvancedCollision advCol in advancedCollisions)
        {
            allAdvBoxColliders.AddRange(advCol.colliders);
        }
    }

	//public methods
    public void RestoreToDefaultMaterial()
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material = originalMaterials[i];
        }
    }
    public void SetNewMaterial(Material material)
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material = material;
        }
    }

    //New methods
    public bool IsCollidingWithOtherBlocks()
    {
        foreach(AdvancedCollision advCol in advancedCollisions)
        {
            if (advCol.IsOverlapping(allAdvBoxColliders.ToArray()))
                return true;
        }
        return false;
    }

	//private methods
}
