﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(OccupyingComponent))]
[RequireComponent(typeof(StableComponent))]
public class Block : MonoBehaviour_Network
 {
    public static int GlobalIndex;
    public int blockIndex;

 	//public variables
    [Header("Settings")]
    public Item_Base buildableItem;
    public GameObject prefab;
    public bool specialySaved;
    public bool showHealthOnMouseOver;
    public int health = 100;
    public int maxHealth = 100;

    [Header("Build settings")]
    public bool hasItemNet;
    public bool isRotateable;
    public bool canRotateFreely;
    public bool forceRotationToQuadDirection;
    public bool removeableWithAxe;
    public bool snapsToQuads = true;
    public bool snapsToTopCollider = false;
    public Vector3 buildOffset;
    public List<Cost> buildCost = new List<Cost>();

    [Header("Destroy setting")]
    [Tooltip("Unique item name of item to return when the block is destroyed, leave empty if no item should be returned")]
    public Item_Base itemToReturnOnDestroy;

    [Header("On/Off colliders... Off when placing")]
    public Collider[] onoffColliders;

    [Space(20)]
    [HideInInspector]
    public OccupyingComponent occupyingComponent;
    private StableComponent stabilityComponent;

    [Header("Overlapping check")]
    public List<BoxCollider> overlappChecks;

    [Header("Stability")]
    public bool requireAll = false;
    public int requireHitCount = 1;

 	//private variables
    private CanvasHelper canvas;

 	//Unity methods
    void Awake()
    {
        occupyingComponent = GetComponent<OccupyingComponent>();
        stabilityComponent = GetComponent<StableComponent>();
    }
    void Start()
    {
        canvas = CanvasHelper.Singleton;
    }
    void OnDestroy()
    {
        if (canvas != null)
            canvas.SetDisplayText(false);
    }
    void OnValidate()
    {
        if (stabilityComponent == null)
        {
            stabilityComponent = GetComponent<StableComponent>();
            return;
        }

        if (requireAll)
            requireHitCount = stabilityComponent.requiredColliders.Length;

        if (requireHitCount > stabilityComponent.requiredColliders.Length)
            requireHitCount = stabilityComponent.requiredColliders.Length;

        if (requireHitCount < 1)
            requireHitCount = 1;
    }

	//public methods
    public void OnStartingPlacement()
    {
        foreach(Collider col in onoffColliders)
        {
            if (col != null)
                col.enabled = false;
        }
        foreach(BoxCollider box in overlappChecks)
        {
            if (box != null)
                box.gameObject.SetActive(false);
        }
    }
    public virtual void OnFinishedPlacement()
    {
        RefreshOverlapps();

        foreach (Collider col in onoffColliders)
        {
            col.enabled = true;
        }

        if (hasItemNet)
        {
            GetComponentInChildren<ItemCollector>().SetIndexes(this);
        }
        SendMessage("OnBlockPlaced", SendMessageOptions.DontRequireReceiver);

        GlobalIndex++;
        blockIndex = GlobalIndex;
    }
    public void Damage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            NetworkUpdateManager.SetBehaviourDead(this);
        }
    }
    public bool IsStable()
    {
        if (stabilityComponent == null)
            return true;

        int overlapCount = stabilityComponent.GetOverlapCount();
        if (requireAll)
        {
            return overlapCount == stabilityComponent.requiredColliders.Length;
        }
        else
        {
            return overlapCount >= requireHitCount;
        }
    }
    public bool IsOverlapping()
    {
        return occupyingComponent.IsCollidingWithOtherBlocks();
    }
    public void Repair(int amount)
    {
        health += amount;
        if (health > maxHealth)
            health = maxHealth;
    }
    public bool CanBeRepaired()
    {
        return health < maxHealth;
    }
    public void RefreshOverlapps()
    {
        for (int i = 0; i < overlappChecks.Count; i++)
        {
            BoxCollider box = overlappChecks[i];

            if (Helper.IsColliding(box, LayerMasks.MASK_buildQuad, 0.9f))
            {
                box.gameObject.SetActive(false);
            }
            else
            {
                box.gameObject.SetActive(true);
            }
        }
    }

	//private methods

    //Network methods
    public override void Serialize_Create()
    {
        if (this != null && buildableItem != null)
            AddMessage(new Message_BuildBlock(Messages.CreateBlock, this, buildableItem.UniqueIndex, transform.localPosition, transform.localEulerAngles.y));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.RemoveNetworkBehaviour:
                BlockCreator.RemoveBlock(this);
                break;
        }
    }
}
