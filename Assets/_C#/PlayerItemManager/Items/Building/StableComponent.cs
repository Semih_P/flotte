﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StableComponent : MonoBehaviour 
{
	//public variables
    public Collider[] requiredColliders;

 	//private variables


 	//Unity methods

	//public methods
    public int GetOverlapCount()
    {
        int overlapCount = 0;
        foreach (Collider col in requiredColliders)
        {
            if (Helper.IsColliding(col as BoxCollider, LayerMasks.MASK_block, 0.99f))
            {
                overlapCount++;
            }
        }
        return overlapCount;
    }


	//private methods

}
