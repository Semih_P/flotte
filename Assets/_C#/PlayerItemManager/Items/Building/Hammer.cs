﻿using UnityEngine;
using System.Collections;

public class Hammer : MonoBehaviour
 {
 	//public variables
    public int blockRepairAmount;
    public ParticleController particles;
    [SerializeField] private BlockCreator blockCreator;

 	//private variables
    private PlayerInventory playerInventory;
    private PlayerAnimator playerAnimator;
    private Block blockToRepair;
    private CanvasHelper canvas;
    private bool canHit;

 	//Unity methods
	void Awake()
	{
        particles.particleParent.SetParent(null);
	}
    void Start()
    {
        canvas = CanvasHelper.Singleton;
        playerInventory = ComponentManager<PlayerInventory>.Get();
        playerAnimator = GetComponentInParent<Network_Player>().Animator;
    }
	void Update () 
	{
        HandleRepairs();
	}


	//public methods
    public void OnSelect()
    {
        if (playerAnimator == null)
            playerAnimator = GetComponentInParent<Network_Player>().Animator;

        playerAnimator.SetAnimation(PlayerAnimation.Index_4_HammerAxe);
        canHit = true;
        blockCreator.gameObject.SetActive(true);
    }
    public void OnDeSelect()
    {
        blockCreator.SetGhostBlockVisibility(false);
        blockCreator.gameObject.SetActive(false);
    }
    public void OnHammerHit()
    {
        canHit = true;

        if (blockToRepair == null)
            return;

        ComponentManager<SoundManager>.Get().PlaySound("WoodHit");
        particles.PlayParticles();
        blockToRepair.Repair(blockRepairAmount);
        playerInventory.RemoveItem("Plank", 1);
    }

	//private methods
    private void HandleRepairs()
    {
        Item_Base currentBuildableItem = blockCreator.GetCurrentBlockType();
        if (currentBuildableItem != null && currentBuildableItem.UniqueName == "Repair")
        {
            if (Input.GetButton("LeftClick") && canHit)
            {
                canHit = false;
                playerAnimator.anim.SetBool("HammerAxeHit", true);
            }
            else if (Input.GetButtonUp("LeftClick"))
                playerAnimator.anim.SetBool("HammerAxeHit", false);

            if (playerInventory.GetItemCount("Plank") >= 1)
            {
                RaycastHit hit = Helper.HitAtCursor(Player.UseDistance, LayerMasks.MASK_block);
                if (hit.transform != null)
                {
                    particles.SetPosition(hit.point);
                    particles.SetLookRotation(hit.normal);
                    Block block = hit.transform.GetComponentInParent<Block>();
                    if (block != null && block.CanBeRepaired())
                    {
                        blockToRepair = block;
                        if (blockToRepair.showHealthOnMouseOver)
                        {
                            canvas.SetDisplayText(blockToRepair.buildableItem.settings_Inventory.GetDisplayName() + "\nHealth: " + blockToRepair.health + "/" + blockToRepair.maxHealth);
                        }
                    }
                    else
                    {
                        blockToRepair = null;
                    }
                }
                else
                {
                    blockToRepair = null;
                }
            }
            else
            {
                blockToRepair = null;
            }
        }
    }
}
