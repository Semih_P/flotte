﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public delegate void OnBlockPlaced(Block block);
public class BlockCreator : MonoBehaviour_Network
{
    public static event OnBlockPlaced PlaceBlockCallStack;

	//public variables
    [SerializeField] private Material ghostMaterial = null;

	//private variables
    private static List<Block> placedBlocks = new List<Block>();
    private static List<Item_Base> buildableItems = new List<Item_Base>();
    private Network_Player playerNetwork;
    [HideInInspector] public Block selectedBlock;
    private Item_Base selectedBuildableItem;
    private CanvasHelper canvas;
    private PlayerInventory playerInventory;
    private PlayerAnimator playerAnimator;
    private Player player;
    private Transform ghostBlock;
    private BlockQuad quadAtCursor;
    private Vector3 quadHitPoint;
    private const float rotateDegreeSnap = 90f;
    private const float rotateDegreeSmooth = 5f;
    private float currentRotationY;
    private float rotationKeyAccumulation;

    private Messages messageType = Messages.NOTHING;

	//Unity methods
    void Start()
    {
        if (buildableItems == null || buildableItems.Count == 0)
            buildableItems = ItemManager.GetBuildableItems();

        playerNetwork = GetComponentInParent<Network_Player>();
        playerAnimator = playerNetwork.Animator;

        if (playerNetwork.IsLocalPlayer)
        {
            ComponentManager<BuildMenu>.Get().Initialize(this, GetComponentInParent<Player>());
        }

        player = GameObject.FindObjectOfType<Player>();
        playerInventory = ComponentManager<PlayerInventory>.Get();
        canvas = CanvasHelper.Singleton;
        canvas.HideBlockPlacerCostBoxes();

        RefreshOverlaps();
        SetBlockTypeToBuild("Block_Foundation");


        gameObject.SetActive(false);
    }
	void Update () 
	{
        if (playerAnimator.anim.GetBool("HammerAxeHit"))
            playerAnimator.anim.SetBool("HammerAxeHit", false);
        if (selectedBlock == null)
            return;

        if (!playerNetwork.IsLocalPlayer)
        {
            return;
        }

        //Are we aiming at a quad?
        quadAtCursor = GetQuadAtCursor();

        //Set ghost block visibility
        SetGhostBlockVisibility(quadAtCursor != null);

        //ABORT, if we are not aiming at a quad
        if (quadAtCursor == null)
            return;

        //Set rotation of selected block
        HandleRotationOfSelectedBlock();

        //Set position and rotation of ghost block
        SetGhostBlockPositionAndRotation();

        //Is the selected block a buildable, (it could have  been a placeable)
        if (selectedBuildableItem.settings_buildable.GetBlockPrefab().buildCost.Count != 0)
        {
            //ABORT, if the aimed at quad does not accept the current block
            BlockQuadType quad = quadAtCursor.quadType;
            if (quad.HasExtension && quad.Key == selectedBuildableItem)
            {
                SetBlockTypeToBuild(quad.Extension.UniqueName);
                return;
            }
            //else if (quadAtCursor.Stability <= selectedBuildableItem.settings_buildable.RequiredStabilityCount)
            //{
            //    SetGhostBlockVisibility(false);
            //    return;
            //}
        }

        //ABORT, if we can't create the block because of error below
        string error = string.Empty;
        if (!CanBuildBlock(selectedBlock, ref error))
        {
            //Red
            ghostMaterial.color = Color.red;
            return;
        }
        else
        {
            //Green
            ghostMaterial.color = Color.green;
        }


        //We have passed all the tests, lets create the block!
        if (Input.GetButtonDown("LeftClick"))
        {
            if (playerNetwork.Network.IsHost)
                CreateBlock();
            else
            {
                playerNetwork.SendP2P(new Message_BuildBlock(Messages.CreateBlock, this, selectedBlock.buildableItem.UniqueIndex, selectedBlock.transform.localPosition, selectedBlock.transform.localEulerAngles.y), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

                //Remove cost from inventory
                if (playerInventory != null)
                {
                    if (selectedBlock.buildCost.Count != 0)
                    {
                        foreach (Cost cost in selectedBlock.buildCost)
                        {
                            playerInventory.RemoveItem(cost.item.UniqueName, cost.amount);
                        }
                        playerAnimator.anim.SetBool("HammerAxeHit", true);
                    }
                    else
                    {
                        playerInventory.RemoveSelectedHotSlotItem(1);
                        playerAnimator.SetAnimation(PlayerAnimation.Trigger_PointPlace, true);
                    }
                }
            }
        }
	}
    void OnDestroy()
    {
        placedBlocks.Clear();
        Debug.Log("Clear");
    }

	//public methods
    public static List<Block> GetPlacedBlocks()
    {
        return placedBlocks;
    }
    public static void RemoveBlock(Block block)
    {
        DestroyBlock(block);

        for (int i = 0; i < placedBlocks.Count; i++)
        {
            if (placedBlocks[i] == null)
                placedBlocks.RemoveAt(i--);
        }

        foreach (Block b in placedBlocks)
        {
            b.RefreshOverlapps();
        }
    }
    private static void DestroyBlock(Block block)
    {
        if (block != null)
        {
            DestroyImmediate(block.gameObject);
        }

        foreach (Block b in placedBlocks)
        {
            if (b == block)
                continue;

            if (b != null)
            {
                if (!b.IsStable())
                {
                    DestroyBlock(b);
                }
            }
        }
    }

    public Item_Base GetCurrentBlockType()
    {
        return selectedBuildableItem;
    }
    public void SetBlockTypeToBuild(string uniqueBlockName, bool useNetwork = true)
    {
        //Find new block type
        selectedBuildableItem = ItemManager.GetItemByName(uniqueBlockName);

        //Is placeable?
        if (selectedBuildableItem.settings_buildable.GetBlockPrefab().buildCost.Count == 0)
        {
            playerAnimator.SetAnimation(PlayerAnimation.Index_1_Point);
        }

        //Delete previous ghost
        if (ghostBlock != null)
            Destroy(ghostBlock.gameObject);

        //Create a new ghost
        Block blockPrefab = selectedBuildableItem.settings_buildable.GetBlockPrefab();
        Block newBlock = Instantiate(blockPrefab, Vector3.zero, blockPrefab.transform.rotation, GameManager.Singleton.globalBuoyancyParent) as Block;
        selectedBlock = newBlock;
        ghostBlock = newBlock.transform;

        //Rotate ghost block to correct rotation
        ghostBlock.eulerAngles = new Vector3(ghostBlock.parent.eulerAngles.x, blockPrefab.transform.eulerAngles.y, ghostBlock.parent.eulerAngles.z);
        currentRotationY = Mathf.RoundToInt(currentRotationY / 90) * 90;
        if (blockPrefab.isRotateable)
        {
            ghostBlock.eulerAngles = new Vector3(ghostBlock.eulerAngles.x, currentRotationY, ghostBlock.eulerAngles.z);
        }

        //Call event
        newBlock.OnStartingPlacement();

        //Change material
        if (newBlock.occupyingComponent != null)
        {
            if (playerNetwork.IsLocalPlayer)
                newBlock.occupyingComponent.SetNewMaterial(ghostMaterial);
        }

        SetGhostBlockVisibility(false);

        if (useNetwork && playerNetwork != null && playerNetwork.IsLocalPlayer)
        {
            messageType = Messages.SelectBlock;
            SetDirty();
        }
    }
    public void SetGhostBlockVisibility(bool visible)
    {
        if (ghostBlock == null)
            return;

        ghostBlock.gameObject.SetActive(visible);
    }
    public void CreateNewGameLayout()
    {
        //Find prefab to spawn
        Block blockPrefab = ItemManager.GetItemByName(("Block_Foundation")).settings_buildable.GetBlockPrefab();

        //Create layout
        Vector3[] positions = new Vector3[4];
        positions[0] = new Vector3(-0.75f, 0, -0.75f);
        positions[1] = new Vector3(-0.75f, 0, 0.75f);
        positions[2] = new Vector3(0.75f, 0, 0.75f);
        positions[3] = new Vector3(0.75f, 0, -0.75f);

        for (int i = 0; i < 4; i++)
        {
            Transform blockTransform = Instantiate(blockPrefab.transform, Vector3.zero, blockPrefab.transform.rotation).transform;
            blockTransform.SetParent(GameManager.Singleton.globalBuoyancyParent);
            blockTransform.position = positions[i];
            blockTransform.eulerAngles = new Vector3(blockTransform.parent.eulerAngles.x, blockPrefab.transform.eulerAngles.y, blockTransform.parent.eulerAngles.z);


            //Aquire block
            Block block = blockTransform.GetComponent<Block>();
            block.OnFinishedPlacement();
            NetworkUpdateManager.SetIndexForBehaviour(block);
            NetworkUpdateManager.AddBehaviour(block);
            placedBlocks.Add(block);
        }
    }
    public void CreateBlock()
    {
        bool isPlaceable = selectedBuildableItem.settings_buildable.GetBlockPrefab().buildCost.Count == 0;

        //Play placed sound
        ComponentManager<SoundManager>.Get().PlaySound("PlaceObject");

        //Remove ui and release player camera
        canvas.SetDisplayText(false);
        player.SetMouseLookScripts(true);

        //Remove cost from inventory
        if (!isPlaceable)
        {
            foreach (Cost cost in selectedBlock.buildCost)
            {
                playerInventory.RemoveItem(cost.item.UniqueName, cost.amount);
            }
            playerAnimator.anim.SetBool("HammerAxeHit", true);
        }
        else
        {
            playerAnimator.SetAnimation(PlayerAnimation.Trigger_PointPlace, true);
        }

        //Save reference to placed block
        placedBlocks.Add(selectedBlock);

        //Restore material
        if (selectedBlock.occupyingComponent != null)
        {
            selectedBlock.occupyingComponent.RestoreToDefaultMaterial();
        }

        //Call event
        NetworkUpdateManager.SetIndexForBehaviour(selectedBlock);
        NetworkUpdateManager.AddBehaviour(selectedBlock);
        selectedBlock.OnFinishedPlacement();

        //Refresh overlaps
        RefreshOverlaps();

        //Set block to dirty for networking
        if (playerNetwork.Network.IsHost)
            selectedBlock.SetDirty();


        //Remove references
        string placedBlockName = selectedBuildableItem.UniqueName;
        selectedBuildableItem = null;
        ghostBlock = null;

        //Call stack
        if (PlaceBlockCallStack != null)
            PlaceBlockCallStack(selectedBlock);

        if (!isPlaceable)
            selectedBlock = null;

        //Reselect item
        if (!isPlaceable)
            SetBlockTypeToBuild(placedBlockName, true);
        else if (playerNetwork.IsLocalPlayer)
            playerInventory.RemoveSelectedHotSlotItem(1);
    }
    public void CreateBlock(Message_BuildBlock msg)
    {
        Item_Base blockItem = ItemManager.GetItemByIndex(msg.blockIndex);
        bool isPlaceable = blockItem.settings_buildable.GetBlockPrefab().buildCost.Count == 0;

        //Play placed sound
        ComponentManager<SoundManager>.Get().PlaySound("PlaceObject");

        //Delete previous ghost
        if (ghostBlock != null)
            Destroy(ghostBlock.gameObject);

        //Create a new ghost
        Block blockPrefab = blockItem.settings_buildable.GetBlockPrefab();
        Block newBlock = Instantiate(blockPrefab, msg.GetPosition(), blockPrefab.transform.rotation, GameManager.Singleton.globalBuoyancyParent) as Block;
        newBlock.transform.localPosition = msg.GetPosition();
        newBlock.transform.localEulerAngles = new Vector3(0, msg.rotY, 0);

        //Call event
        newBlock.OnStartingPlacement();

        //Save reference to placed block
        placedBlocks.Add(newBlock);

        if (playerNetwork == null)
            playerNetwork = GetComponentInParent<Network_Player>();
        if (playerAnimator == null)
            playerAnimator = playerNetwork.Animator;

        //You have recieved build message from host
        if (!playerNetwork.Network.IsHost)
        {   
            newBlock.ObjectIndex = msg.objectIndex;
            newBlock.BehaviourIndex = msg.behaviourIndex;
            NetworkUpdateManager.AddBehaviour(newBlock);
        }
        //Host is building for someone else
        else
        {
            NetworkUpdateManager.SetIndexForBehaviour(newBlock);
            NetworkUpdateManager.AddBehaviour(newBlock);
            newBlock.SetDirty();
        }

        //Call event
        newBlock.OnFinishedPlacement();

        //Refresh overlaps
        RefreshOverlaps();

        //Call stack
        if (PlaceBlockCallStack != null)
            PlaceBlockCallStack(newBlock);

        //Remove references
        string placedBlockName = blockItem.UniqueName;
        selectedBuildableItem = null;
        selectedBlock = null;
        ghostBlock = null;

        //Reselect item
        if (!isPlaceable && !playerNetwork.Network.IsHost)
            SetBlockTypeToBuild(placedBlockName, true);
    }
    public Network_Player GetPlayerNetwork()
    {
        return playerNetwork;
    }

	//private methods
    private bool CanBuildBlock(Block block, ref string errorMessage)
    {
        if (PositionOccupied(block))
        {
            errorMessage = "Position was occupied";
            return false;
        }
        else if (!selectedBlock.IsStable())
        {
            errorMessage = "Block was not stable";
            return false;
        }
        else if (!HasEnoughResourcesToBuild(block))
        {
            errorMessage = "Not enough resources";
            return false;
        }
        else if (GameManager.IsInMenu)
        {
            errorMessage = "Menu is active";
            return false;
        }
        else
        {
            return true;
        }
    }
    private bool HasEnoughResourcesToBuild(Block block)
    {
        if (block == null)
            return false;

        foreach (Cost cost in block.buildCost)
        {
            int invCount = playerInventory.GetItemCount(cost.item.UniqueName);
            if (invCount < cost.amount)
                return false;
        }

        return true;
    }
    private bool PositionOccupied(Block block)
    {
        string blockName = block.buildableItem.UniqueName.Split('_')[1];
        if (blockName.Contains("Wall"))
        {
            bool hitOtherThanWall = false;
            foreach (BoxCollider collider in block.occupyingComponent.allAdvBoxColliders)
            {
                if (collider == null)
                    continue;

                Collider[] colliders = Helper.GetCollisions(collider, LayerMasks.MASK_block);
                foreach (Collider hitCollider in colliders)
                {
                    if (hitCollider == null)
                        continue;

                    Block hitBlock = hitCollider.GetComponentInParent<Block>();
                    if (hitBlock == null)
                        continue;

                    if (!hitBlock.buildableItem.UniqueName.Contains(blockName))
                    {
                        hitOtherThanWall = true;
                    }
                    if (hitBlock.transform.position == block.transform.position && Mathf.RoundToInt(hitBlock.transform.localEulerAngles.y) == Mathf.RoundToInt(block.transform.localEulerAngles.y))
                    {
                        hitOtherThanWall = true;
                    }
                }
            }

            return hitOtherThanWall;
        }
        else
            return block.IsOverlapping();
    }
    private void HandleRotationOfSelectedBlock()
    {
        if (selectedBlock.forceRotationToQuadDirection)
        {
            selectedBlock.transform.rotation = Quaternion.LookRotation(quadAtCursor.transform.forward);
        }
        else if (selectedBlock.isRotateable)
        {
            if (Input.GetButtonDown("Rotate"))
            {
                if (selectedBlock.canRotateFreely)
                    player.SetMouseLookScripts(false);
                else
                    RotateBlock(selectedBlock, rotateDegreeSnap, true, true);
            }

            if (selectedBlock.canRotateFreely)
            {
                float accumulationLimit = 0.2f;
                if (Input.GetButton("Rotate"))
                {
                    rotationKeyAccumulation += Time.deltaTime;
                    if (rotationKeyAccumulation >= accumulationLimit)
                    {
                        float degree = Input.GetAxis("Mouse X");
                        RotateBlock(selectedBlock, degree * rotateDegreeSmooth, false);
                    }
                }
                if (Input.GetButtonUp("Rotate"))
                {
                    if (rotationKeyAccumulation < accumulationLimit)
                    {
                        RotateBlock(selectedBlock, rotateDegreeSnap, true, true);
                    }
                    else
                    {
                        Debug.Log("Rawr");
                    }
                    rotationKeyAccumulation = 0;
                    player.SetMouseLookScripts(true);
                }
            }
        }
    }
    private void RotateBlock(Block block, float degrees, bool sendNetwork, bool snap = false)
    {
        if (block == null)
            return;

        currentRotationY += degrees;
        if (snap)
        {
            currentRotationY = Mathf.RoundToInt(currentRotationY / 90) * 90;
        }
        if (currentRotationY > 360)
            currentRotationY -= 360;

        Transform t = block.transform;
        t.eulerAngles = new Vector3(t.eulerAngles.x, currentRotationY, t.eulerAngles.z);

        if (sendNetwork && playerNetwork.IsLocalPlayer)
        {
            messageType = Messages.RotateBlock;
            SetDirty();
        }
    }
    private void SetGhostBlockPositionAndRotation()
    {
        if (ghostBlock == null || quadAtCursor == null)
            return;

        //Set position
        if (selectedBlock.snapsToQuads)
        {
            ghostBlock.position = quadAtCursor.transform.position + selectedBlock.buildOffset;
        }
        else
        {
            ghostBlock.position = quadHitPoint + selectedBlock.buildOffset;
        }

        //Set rotation
        ghostBlock.localEulerAngles = new Vector3(0, ghostBlock.localEulerAngles.y, 0);
    }
    private BlockQuad GetQuadAtCursor()
    {
        RaycastHit[] hits = Physics.RaycastAll(playerNetwork.CameraTransform.position, playerNetwork.CameraTransform.forward, Player.UseDistance * 2f, LayerMasks.MASK_buildQuad);

        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit = hits[i];
                if (hit.transform == null)
                    continue;

                BlockQuad quad = hit.collider.transform.GetComponent<BlockQuad>();
                if (quad != null && quad.quadType.AcceptsBlock(selectedBuildableItem))
                {
                    if (selectedBlock.snapsToTopCollider)
                    {
                        Collider col = hit.transform.GetComponent<Collider>();

                        if (col != null)
                        {
                            Vector3 pos = hit.point;
                            pos.y = col.bounds.max.y;
                            quadHitPoint = pos;
                        }
                    }
                    else
                        quadHitPoint = hit.point;

                    return quad;
                }
            }

            
        }

        return null;
    }
    private void RefreshOverlaps()
    {
        foreach (Block b in placedBlocks)
        {
            b.RefreshOverlapps();
        }
    }

    //Network methods
    public override void Serialize_Update()
    {
        if (messageType == Messages.SelectBlock)
            AddMessage(new Message_SelectBlock(messageType, this, selectedBlock));
        else if (messageType == Messages.RotateBlock)
            AddMessage(new Message_BuildBlock(messageType, this, selectedBlock.buildableItem.UniqueIndex, selectedBlock.transform.position, selectedBlock.transform.eulerAngles.y));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        if (msg.type == Messages.SelectBlock)
        {
            Message_SelectBlock msgSelectBlock = msg as Message_SelectBlock;
            SetBlockTypeToBuild(ItemManager.GetItemByIndex(msgSelectBlock.blockIndex).UniqueName, false);
        }
        else if (msg.type == Messages.RotateBlock)
        {
            Message_BuildBlock msgRotateBlock = msg as Message_BuildBlock;
            if (selectedBlock != null)
            {
                Vector3 euler = selectedBlock.transform.localEulerAngles;
                euler.y = msgRotateBlock.rotY;
                selectedBlock.transform.localEulerAngles = euler;
            }
        }
    }
}

[System.Serializable]
public class Message_SelectBlock : Message_NetworkBehaviour
{
    public int blockIndex;

    public Message_SelectBlock(Messages type, MonoBehaviour_Network behaviour, Block block) : base(type, behaviour)
    {
        this.type = type;
        this.blockIndex = block.buildableItem.UniqueIndex;
    }
}
[System.Serializable]
public class Message_BuildBlock : Message_NetworkBehaviour
{
    public int blockIndex;
    public float posX, posY, posZ;
    public float rotY;

    public Message_BuildBlock(Messages type, MonoBehaviour_Network behaviour, int blockIndex, Vector3 position, float yRotation) : base(type, behaviour)
    {
        this.blockIndex = blockIndex;
        this.posX = position.x;
        this.posY = position.y;
        this.posZ = position.z;
        this.rotY = yRotation;
    }

    public Vector3 GetPosition()
    {
        return new Vector3(posX, posY, posZ);
    }
}
