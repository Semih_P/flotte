﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WaterPointGetter))]
public class SharkBait : Block 
{
	//public variables

	//private variables
    private WaterPointGetter waterPointGetter;

	//Unity methods
    void Awake()
    {
        waterPointGetter = GetComponent<WaterPointGetter>();
        this.enabled = false;
    }
	void Update () 
	{
        Vector3 pos = transform.position;
        pos.y = waterPointGetter.GetWaterPoint(transform.position);
        transform.position = pos;

        if (health <= 0)
        {
            NetworkUpdateManager.SetBehaviourDead(this);
            this.enabled = false;
        }
	}

	//public methods
    public void OnHitWater()
    {
        Shark.sharkBaitInWorld.Add(this);

        this.enabled = true;
        transform.parent = null;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        Destroy(GetComponent<Rigidbody>());
    }

	//private methods
}
