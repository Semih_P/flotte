﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ChargeMeter))]
public class Throwable : MonoBehaviour_NetworkTick
 {
    public delegate void ThrowableDelegate();

    private ThrowableDelegate waterHitCallstack;
    private ThrowableDelegate throwCallStack;

 	//public variables
    public Transform throwableObject;
    public Rigidbody throwableObjectBody;
    public Vector3 throwForceMultiplier;
    public WaterPointGetter waterPointGetter;
    public bool setThrowableToSurface = true;

 	//private variables
    private Network_Player playerNetwork;
    private CanvasHelper canvasHelper;
    private GameManager gameManager;
    private SoundManager soundManager;

    private Transform raftTransform;
    private Transform throwableObjectParent;
    private ChargeMeter chargeMeter;

    private Vector3 throwableStartPosition;
    private Vector3 throwableStartRotation;
    private Vector3 throwableStartLocalScale;

    public bool InHand { get { return inHand; } set { inHand = value; } }
    private bool inHand = true;

    public bool InWater { get { return inWater; } set { inWater = value; } }
    private bool inWater = false;

    public bool CanThrow { get { return canThrow; } set { canThrow = value; } }
    private bool canThrow = true;

    public bool IsLocked;

    [HideInInspector]public Vector3 networkPosition;
    public float networkLerpSpeed = 6f;

 	//Unity methods
	void Awake()
	{
        IsFresh = false;
        chargeMeter = GetComponent<ChargeMeter>();
	}
    void Start()
    {
        raftTransform = GameObject.FindObjectOfType<Raft>().transform;

        //Save rotation and position of throwable object
        throwableObjectParent = throwableObject.parent;
        throwableStartPosition = throwableObject.localPosition;
        throwableStartRotation = throwableObject.localEulerAngles;
        throwableStartLocalScale = throwableObject.localScale;

        //Start body settings
        throwableObjectBody.isKinematic = true;

        //Aquires references
        playerNetwork = GetComponentInParent<Network_Player>();
        canvasHelper = CanvasHelper.Singleton;
        gameManager = GameManager.Singleton;
        soundManager = ComponentManager<SoundManager>.Get();
    }
	void Update () 
	{
        if (!playerNetwork.IsLocalPlayer)
        {
            if (InHand)
                return;

            if (setThrowableToSurface)
            {
                if (InWater && !throwableObjectBody.isKinematic)
                    throwableObjectBody.isKinematic = true;

                if (!InHand && InWater)
                    throwableObject.position = Vector3.Lerp(throwableObject.position, networkPosition, networkLerpSpeed * Time.deltaTime);
                else
                    networkPosition = throwableObject.position;
            }
        }
        else
        {
            if (IsLocked)
            {
                ResetCanThrow();
                return;
            }
            if (GameManager.IsInMenu)
                return;

            if (InWater || !InHand || !CanThrow || chargeMeter.ChargeNormal > 0)
                PlayerItemManager.IsBusy = true;
            else
                PlayerItemManager.IsBusy = false;

            if (CanThrow)
            {
                //Charge slider
                if (Input.GetButton("LeftClick"))
                {
                    chargeMeter.Charge();
                }

                //Release charge
                if (Input.GetButtonUp("LeftClick") && chargeMeter.ChargeNormal > 0)
                {
                    Vector3 force =
                        Camera.main.transform.forward * (chargeMeter.ChargeNormal * throwForceMultiplier.z) +
                        Camera.main.transform.right * (chargeMeter.ChargeNormal * throwForceMultiplier.x) +
                        Vector3.up * (chargeMeter.ChargeNormal * throwForceMultiplier.y);

                    if (playerNetwork.Network.IsHost)
                        Throw(force, true);
                    else
                        playerNetwork.SendP2P(new Message_Throw(Messages.ThrowableThrow, this, force), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                }
            }

            //Check if throwable is in water
            float waterPointLevel = waterPointGetter.GetWaterPoint(throwableObject.transform.position);
            if (!InHand)
            {
                if (throwableObject.position.y <= waterPointLevel)
                {
                    //Set flag
                    if (!InWater)
                    {
                        //Control body
                        InWater = true;

                        if (setThrowableToSurface)
                            throwableObjectBody.isKinematic = true;

                        if (waterHitCallstack != null)
                        {
                            waterHitCallstack();

                            //Play water hit sound
                            AudioSource source = soundManager.PlaySoundCopy("WaterPlump", throwableObject.position, true);
                            source.minDistance = 4f;
                        }

                    }

                }
            }

            if (InWater)
            {
                if (setThrowableToSurface)
                {
                    Vector3 pos = throwableObject.position;
                    pos.y = Mathf.Lerp(pos.y, waterPointLevel, Time.deltaTime * 2);
                    throwableObject.position = pos;
                }
            }

            canvasHelper.SetLoadCircle(CanThrow && chargeMeter.ChargeNormal > 0);
            canvasHelper.SetLoadCircle(GetChargeNormal());

            if (!InHand && InWater)
                UpdateNetworkTick();
        }
	}

	//public methods
    public void ResetCanThrow()
    {
        CanThrow = true;
        PlayerItemManager.IsBusy = false;
        chargeMeter.Reset();
        canvasHelper.SetLoadCircle(false);
    }
    public void PickupThrowable()
    {
        throwableObject.parent = throwableObjectParent;
        throwableObject.localPosition = throwableStartPosition;
        throwableObject.localEulerAngles = throwableStartRotation;
        throwableObject.localScale = throwableStartLocalScale;

        throwableObjectBody.isKinematic = true;

        InHand = true;
        InWater = false;

        if (playerNetwork.IsLocalPlayer)
            SetDirty();
    }
    public float GetChargeNormal()
    {
        return chargeMeter.ChargeNormal;
    }

    public void SetPosition(Vector3 position)
    {
        throwableObject.position = position;
    }
    public Vector3 GetPositon()
    {
        return throwableObject.position;
    }

    public void SubscribeToWaterHitEvent(ThrowableDelegate method)
    {
        waterHitCallstack += method;
    }
    public void SubscribeToThrowEvent(ThrowableDelegate method)
    {
        throwCallStack += method;
    }

    public void Throw(Vector3 force, bool useNetwork)
    {
        //Play sound
        soundManager.PlaySound("Throw");

        //Set flags
        InHand = false;
        CanThrow = false;

        throwableObject.parent = raftTransform;
        throwableObjectBody.isKinematic = false;
        throwableObjectBody.AddForce(force);

        //Throw hook
        if (throwCallStack != null)
            throwCallStack();

        chargeMeter.Reset();

        if (useNetwork)
        {
            playerNetwork.Network.RPC(new Message_Throw(Messages.ThrowableThrow, this, force), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }

	//private methods

    //Network methods
    public override void Serialize_Update()
    {
        AddMessage(new Message_ThrowableUpdate(Messages.Update, this, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.Update:
                Message_ThrowableUpdate msgUpdate = msg as Message_ThrowableUpdate;
                networkPosition = msgUpdate.GetPosition();

                InWater = msgUpdate.inWater;
                if (msgUpdate.inHand && !InHand)
                {
                    PickupThrowable();
                }
                else if (!msgUpdate.inHand && InHand)
                {
                    InHand = false;
                    throwableObject.SetParent(raftTransform);
                }
                break;
            case Messages.ThrowableThrow:
                Message_Throw msgThrow = msg as Message_Throw;
                Throw(msgThrow.Force, playerNetwork.Network.IsHost);
                break;
        }
    }
}
[System.Serializable]
public class Message_ThrowableUpdate : Message_NetworkBehaviour
{
    public bool inHand;
    public bool inWater;
    public float xPos, yPos, zPos;
    public Message_ThrowableUpdate(Messages type, MonoBehaviour_Network behaviour, Throwable throwable)
        : base(type, behaviour)
    {
        xPos = throwable.throwableObject.position.x;
        yPos = throwable.throwableObject.position.y;
        zPos = throwable.throwableObject.position.z;
        inHand = throwable.InHand;
        inWater = throwable.InWater;
    }

    public Vector3 GetPosition()
    {
        return new Vector3(xPos, yPos, zPos);
    }
}
[System.Serializable]
public class Message_Throw : Message_NetworkBehaviour
{
    public Vector3 Force { get { return new Vector3(xForce, yForce, zForce); } private set { xForce = value.x; yForce = value.y; zForce = value.z; } }
    private float xForce, yForce, zForce;
    public Message_Throw(Messages type, MonoBehaviour_Network behaviour, Vector3 force)
        :base(type, behaviour)
    {
        Force = force;
    }
}
