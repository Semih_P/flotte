﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Throwable))]
public class SharkBaitThrower : MonoBehaviour 
{
	//public variables

	//private variables
    [SerializeField] private SharkBait sharkBaitPrefab;
    [SerializeField] private ParticleSystem waterSplash;
    private Throwable throwable;
    private PlayerInventory inventory;
    private SharkBait currentSharkBait;
    private PlayerAnimator playerAnimator;

	//Unity methods
	void Awake () 
	{
        throwable = GetComponent<Throwable>();
        throwable.SubscribeToThrowEvent(OnThrow);
        throwable.SubscribeToWaterHitEvent(OnHitWater);
        currentSharkBait = throwable.throwableObject.GetComponent<SharkBait>();
	}
    void Start()
    {
        playerAnimator = GetComponentInParent<Network_Player>().Animator;
        inventory = ComponentManager<PlayerInventory>.Get();
    }

	//public methods
    public void OnUse()
    {
        if (throwable.CanThrow && !GameManager.IsInMenu)
        {
            playerAnimator.anim.SetBool("HookThrow", true);
        }
    }
    public void OnSelect()
    {
        if (playerAnimator == null)
            playerAnimator = GetComponentInParent<Network_Player>().Animator;
        playerAnimator.SetAnimation(PlayerAnimation.Index_2_Hook);
    }
    private void OnThrow()
    {
        playerAnimator.anim.SetBool("HookThrow", false);
    }
    private void OnHitWater()
    {
        currentSharkBait.OnHitWater();

        SharkBait newSharkBait = Instantiate(sharkBaitPrefab, transform.position, Quaternion.identity, transform) as SharkBait;
        throwable.throwableObject = newSharkBait.transform;
        throwable.throwableObjectBody = newSharkBait.transform.GetComponent<Rigidbody>();
        throwable.InWater = true;
        throwable.InHand = true;
        throwable.CanThrow = true;
        throwable.throwableObjectBody.isKinematic = true;
        throwable.PickupThrowable();

        waterSplash.transform.eulerAngles = Vector3.zero;
        waterSplash.transform.position = throwable.throwableObject.transform.position;
        waterSplash.Play();

        currentSharkBait = newSharkBait;

        inventory.RemoveSelectedHotSlotItem(1);
    }
	//private methods

}
