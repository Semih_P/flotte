﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemCollector : MonoBehaviour_Network
 {
 	//public variables
    public bool playerControlled = true;

 	//private variables
    public List<PickupItem> pickedUpItems = new List<PickupItem>();
    private Semih_Network network;

 	//Unity methods
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
    }
    void OnTriggerEnter(Collider other)
    {
        if (((1 << other.transform.gameObject.layer) & LayerMasks.MASK_item) != 0)
        {
            PickupItem item = other.transform.GetComponent<PickupItem>();
            if (item != null && !item.requiresHands)
            {
                if (playerControlled)
                {
                    Network_FloatingObject netObj = other.transform.GetComponent<Network_FloatingObject>();
                    if (netObj != null)
                    {
                        if (network.IsHost)
                        {
                            CollectItem(item, true);
                        }
                        else
                        {
                            network.SendP2P(network.HostID, new Message_ItemCollectorCollect(Messages.ItemCollectorCollect, this, netObj), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                        }
                    }
                }
                else if (network.IsHost)
                {
                    CollectItem(item, true);
                }
            }
        }
    }

	//public methods
    public int GetItemCount()
    {
        return pickedUpItems.Count;
    }
    public void AddCollectedItemsToInventory(PlayerInventory playerInventory)
    {
        if (GetItemCount() == 0)
            return;

        foreach (PickupItem pickup in pickedUpItems)
        {
            for (int i = 0; i < pickup.amount; i++)
            {
                Item_Base itemToPickup = pickup.GetItem();
                playerInventory.AddItem(itemToPickup.UniqueName, 1);
            }
        }

        if (network.IsHost)
        {
            ClearCollectedItems(true);
        }
        else
        {
            network.SendP2P(network.HostID, new Message_NetworkBehaviour(Messages.ItemCollectorClear, this), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }
    public void SetIndexes(Block block)
    {
        if (playerControlled)
            return;

        if (network == null)
            network = ComponentManager<Semih_Network>.Get();

        if (network.IsHost)
        {
            NetworkUpdateManager.SetIndexForBehaviour(this);
        }
        else
        {
            ObjectIndex = block.ObjectIndex + 1;
            BehaviourIndex = block.BehaviourIndex + 1;
        }
        NetworkUpdateManager.AddBehaviour(this);
    }

	//private methods
    private void CollectItem(PickupItem item, bool useNetwork)
    {
        Collider col = item.GetComponent<Collider>();
        if (col != null)
            col.enabled = false;

        item.transform.SetParent(transform);
        pickedUpItems.Add(item);

        ComponentManager<SoundManager>.Get().PlaySoundCopy("ItemCollector", transform.position, true);
        
        if (useNetwork)
        {
            network.RPC(new Message_ItemCollectorCollect(Messages.ItemCollectorCollect, this, item.GetComponent<Network_FloatingObject>()), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }
    private void ClearCollectedItems(bool useNetwork)
    {
        foreach(PickupItem item in pickedUpItems)
        {
            Collider col = item.GetComponent<Collider>();
            if (col != null)
                col.enabled = true;

            //Returns it to the pool
            if (item.networkBehaviour != null)
                NetworkUpdateManager.SetBehaviourDead(item.networkBehaviour);
            else
                PoolManager.ReturnObject(item.gameObject);
        }
        pickedUpItems.Clear();

        if (useNetwork)
        {
            network.RPC(new Message_NetworkBehaviour(Messages.ItemCollectorClear, this), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }

    //Network methods
    public override void Serialize_Create()
    {
        if (!playerControlled)
        {
            AddMessage(new Message_ItemCollectorCreate(Messages.ItemCollectorCreate, this));
        }
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.ItemCollectorCreate:
                Message_ItemCollectorCreate msgCreate = msg as Message_ItemCollectorCreate;
                foreach(uint itemIndex in msgCreate.collectedItems)
                {
                    MonoBehaviour_Network collectedItemBehaviour = NetworkUpdateManager.GetBehaviourFromIndex(itemIndex);
                    PickupItem pickupItem = collectedItemBehaviour.transform.GetComponent<PickupItem>();
                    pickupItem.transform.SetParent(transform);
                    pickupItem.transform.position = transform.position;
                    pickedUpItems.Add(pickupItem);
                }
                break;

            case Messages.ItemCollectorCollect:
                Message_ItemCollectorCollect msgCollect = msg as Message_ItemCollectorCollect;
                MonoBehaviour_Network itemBehaviour = NetworkUpdateManager.GetBehaviourFromIndex(msgCollect.itemBehaviourIndex);
                if (itemBehaviour != null)
                {
                    PickupItem pickupItem = itemBehaviour.GetComponent<PickupItem>();
                    if (pickupItem != null)
                        CollectItem(pickupItem, network.IsHost);
                }
                break;
                
            case Messages.ItemCollectorClear:
                ClearCollectedItems(network.IsHost);
                break;
        }
    }
}

[System.Serializable]
public class Message_ItemCollectorCreate : Message_NetworkBehaviour
{
    public uint[] collectedItems;

    public Message_ItemCollectorCreate(Messages type, ItemCollector itemCollector)
        : base(type, itemCollector)
    {
        List<uint> itemsInCollector = new List<uint>();
        foreach(PickupItem item in itemCollector.pickedUpItems)
        {
            if (item.networkBehaviour != null)
            {
                itemsInCollector.Add(item.networkBehaviour.BehaviourIndex);
            }
        }
        collectedItems = itemsInCollector.ToArray();
    }
}
[System.Serializable]
public class Message_ItemCollectorCollect : Message_NetworkBehaviour
{
    public uint itemBehaviourIndex;
    public Message_ItemCollectorCollect(Messages type, MonoBehaviour_Network behaviour, Network_FloatingObject item)
        :base(type, behaviour)
    {
        this.itemBehaviourIndex = item.BehaviourIndex;
    }
}
