﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook_Scrap : Hook 
{
	//public variables
    [Header("Scrap hook settings")]
    public float gatherTime = 5f;

	//private variables
    private float gatherTimer = 0f;
    private PlayerInventory inventory;
    private CanvasHelper canvas;
    private PickupItem currentOre;

	//Unity methods
    protected override void Start()
    {
        base.Start();
        inventory = ComponentManager<PlayerInventory>.Get();
        canvas = CanvasHelper.Singleton;
    }
    protected override void Update()
    {
        base.Update();

        if (!personController.headAboveWater)
        {
            if (Input.GetButton("LeftClick"))
            {
                PickupItem ore = GetOreAtCursor();
                if (ore != currentOre)
                {
                    currentOre = ore;
                    ResetGatherTimer();
                }

                if (ore != null)
                {
                    gatherTimer += Time.deltaTime;
                    float gatherNormal = gatherTimer / gatherTime;
                    if (gatherNormal >= 1)
                    {
                        inventory.AddItem(ore.item.UniqueName, ore.amount);
                        ore.gameObject.SetActive(false);
                    }

                    canvas.SetLoadCircle(gatherNormal);
                }
                else
                {
                    ResetGatherTimer();
                }
            }
        }


        //Reset gather timer
        if (Input.GetButtonUp("LeftClick"))
        {
            ResetGatherTimer();
        }
    }

	//public methods
    public override void OnSelect()
    {
        base.OnSelect();
        gatherTimer = 0f;
    }

	//private methods
    private PickupItem GetOreAtCursor()
    {
        RaycastHit hit = Helper.HitAtCursor(Player.UseDistance);
        if (hit.transform != null && hit.transform.tag == "MetalOre")
        {
            return hit.transform.GetComponent<PickupItem>();
        }
        return null;
    }
    private void ResetGatherTimer()
    {
        gatherTimer = 0f;
        canvas.SetLoadCircle(false);
    }
}
