﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class FishingItem
{
    public Item_Base item = null;
    [Space]
    [Range(0, 100)]
    public float minSpawnChance = 0;
    [Range(0, 100)]
    public float maxSpawnChance = 100;
}
public class Bobber : MonoBehaviour
 {
 	//public variables
    [Range(0, 100)]
    public float minWaitTimeSeconds = 5f;
    [Range(0, 100)]
    public float maxWaitTimeSeconds = 10f;

    [Range(0, 10)]
    public float fishOnHookTime = 5f;

    [Space]
    public List<FishingItem> fishingItems = new List<FishingItem>();

 	//private variables
    private Throwable rodThrowable;
    private bool active;
    private bool fishIsOnHook;
    private bool escaped;
    private Vector3 startPos;
    private GameObject reelsource;

 	//Unity methods
    void Awake()
    {
        rodThrowable = transform.GetComponentInParent<Throwable>();
    }
    void Update()
    {
        if (active)
        {
            if (fishIsOnHook)
                transform.position -= Vector3.up * Time.deltaTime;
        }
    }
    void OnValidate()
    {
        if (minWaitTimeSeconds >= maxWaitTimeSeconds - 1)
            minWaitTimeSeconds = maxWaitTimeSeconds - 1;
        if (maxWaitTimeSeconds <= minWaitTimeSeconds + 1)
            maxWaitTimeSeconds = minWaitTimeSeconds + 1;

        foreach (FishingItem item in fishingItems)
        {
            if (item.maxSpawnChance <= item.minSpawnChance + 1)
                item.maxSpawnChance = item.minSpawnChance + 1;

            if (item.minSpawnChance >= item.maxSpawnChance - 1)
                item.minSpawnChance = item.maxSpawnChance - 1;
        }
    }

	//public methods
    public Item_Base TryToGetFish()
    {
        if (!active || !fishIsOnHook)
            return null;

        float spawnChance = Random.Range(0f, 100f);
        if (reelsource != null)
            Destroy(reelsource);
        return GetItemFromSea(spawnChance);
    }
    public void StartBobbing()
    {
        if (active)
            return;

        rodThrowable.throwableObjectBody.isKinematic = true;
        rodThrowable.setThrowableToSurface = true;
        active = true;
        startPos = transform.position;

        float waitTime = Random.Range(minWaitTimeSeconds, maxWaitTimeSeconds);
        Invoke("FishOnHook", waitTime);
    }
    public void StopBobbing()
    {
        active = false;
        fishIsOnHook = false;
        escaped = false;
        rodThrowable.setThrowableToSurface = false;

        CancelInvoke();
    }
    public bool Escaped()
    {
        return escaped;
    }
    public bool FishIsOnHook()
    {
        return fishIsOnHook;
    }

	//private methods
    private void FishOnHook()
    {
        rodThrowable.setThrowableToSurface = false;
        fishIsOnHook = true;
        transform.localScale *= 1.25f;

        ComponentManager<SoundManager>.Get().PlaySoundCopy("FishOnHook", transform.position, true);
        reelsource = ComponentManager<SoundManager>.Get().PlaySoundCopy("Reel", transform.position, false).gameObject;
        Invoke("FishEscaped", fishOnHookTime);
    }
    private void FishEscaped()
    {
        active = false;
        fishIsOnHook = false;
        escaped = true;

        if (reelsource != null)
            Destroy(reelsource);
    }
    private Item_Base GetItemFromSea(float spawnChance)
    {
        List<FishingItem> availableItems = new List<FishingItem>();

        foreach(FishingItem item in fishingItems)
        {
            if (spawnChance >= item.minSpawnChance && spawnChance <= item.maxSpawnChance)
            {
                availableItems.Add(item);
            }
        }

        if (availableItems.Count > 0)
        {
            return availableItems[Random.Range(0, availableItems.Count)].item;
        }
        else
        {
            return null;
        }
    }
}
