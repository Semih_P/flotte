﻿using UnityEngine;
using System.Collections;

public class ChargeMeter : MonoBehaviour
 {
 	//public variables
    public float chargeSpeed = 100f;
    [Range(0f, 1f)]
    public float minimumChargeRate = 0.3f;
    [Range(1, 10)]
    public int difficultyLevel = 2;

    public float ChargeNormal { get { return Mathf.Clamp(currentCharge / maxCharge, minCharge, maxCharge) ; } }

 	//private variables
    private float maxCharge = 100f;
    private float minCharge = 0f;
    private float currentCharge = 0f;
    private int chargeDirection = 1;

 	//Unity methods

	//public methods
    public void Charge()
    {
        //Set charge value
        currentCharge += chargeDirection * chargeSpeed * Time.deltaTime * difficultyLevel;

        //Clamp charge value
        currentCharge = Mathf.Clamp(currentCharge, minCharge, maxCharge);
    }
    public void Reset()
    {
        currentCharge = 0f;
        chargeDirection = 1;
    }

	//private methods
}
