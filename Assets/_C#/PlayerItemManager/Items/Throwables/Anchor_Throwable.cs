﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anchor_Throwable : MonoBehaviour 
{
	//public variables
    public Anchor_Throwable_Stand anchor_stand;

	//private variables
    [SerializeField] private Anchor anchorPrefab = null;
    public Transform anchorParent = null;
    public Throwable throwable;
    private PlayerAnimator playerAnimator;
    private Network_Player playerNetwork;
    private CanvasHelper canvas;

    //Unity methods
	void Awake () 
	{
        throwable.setThrowableToSurface = false;
        throwable.SubscribeToThrowEvent(OnThrow);
	}
    void Start()
    {
        canvas = CanvasHelper.Singleton;
        playerNetwork = GetComponentInParent<Network_Player>();
        playerAnimator = playerNetwork.Animator;
    }
    void Update()
    {
        if (!playerNetwork.IsLocalPlayer)
            return;

        if (this.anchor_stand != null)
            PlayerItemManager.IsBusy = true;
        else
        {
            RaycastHit hit = Helper.HitAtCursor(Player.UseDistance, LayerMasks.MASK_item);
            if (hit.transform != null && hit.transform.tag == "ThrowableAnchor")
            {
                Anchor_Throwable_Stand stand = hit.transform.GetComponent<Anchor_Throwable_Stand>();
                if (stand.IsFullyAnchored())
                {
                    canvas.SetDisplayText("Remove", true);
                    if (Input.GetButtonDown("UseButton"))
                    {
                        stand.DestroyStand();
                    }
                }
                else if (anchor_stand == null && !stand.IsBusy() && !stand.HasThrownAnchor())
                {
                    canvas.SetDisplayText("Pickup", true);
                    if (Input.GetButtonDown("UseButton"))
                    {
                        canvas.SetDisplayText(false);
                        if (playerNetwork.Network.IsHost)
                        {
                            stand.ConnectStandWithThrowable(true, playerNetwork);
                        }
                        else
                        {
                            playerNetwork.SendP2P(new Message_NetworkBehaviour_SteamID(Messages.ConnectThrowableAnchorStand, stand, playerNetwork.Network.GetIDFromPlayer(playerNetwork)), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                        }
                    }
                }
                else
                {
                    canvas.SetDisplayText(false);
                }
            }
        }
    }

	//public methods

	//private methods
    private void OnThrow()
    {
        playerAnimator.anim.SetBool("HookThrow", false);

        anchor_stand.OnThrowableThrow(throwable.throwableObject.GetComponent<Anchor>());
        anchor_stand = null;

        Anchor anchor = Instantiate(anchorPrefab, anchorParent.position, Quaternion.identity, anchorParent) as Anchor;
        throwable.throwableObject = anchor.transform;
        throwable.throwableObjectBody = anchor.transform.GetComponent<Rigidbody>();
        throwable.CanThrow = true;
        throwable.throwableObjectBody.isKinematic = true;
        throwable.PickupThrowable();
        throwable.ResetCanThrow();
    }
}
