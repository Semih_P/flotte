﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Throwable))]
public class Hook : MonoBehaviour
 {
 	//public variables
    [Header("Basic hook settings")]
    public Collider hookObjectCollider;
    public ItemCollector hookObject;
    public float pullSpeed = 3f;

 	//private variables
    [HideInInspector]public Throwable throwable;
    protected PersonController personController;
    [SerializeField] private Rope rope = null;
    private PlayerAnimator playerAnimator;
    private Network_Player playerNetwork;
    private PlayerInventory playerInventory;
    private ParticleSystem watersplash_hook_pulling;

 	//Unity methods
    void Awake()
    {
        throwable = GetComponent<Throwable>();
        personController = GetComponentInParent<PersonController>();

        throwable.SubscribeToThrowEvent(OnTrow);
        throwable.SubscribeToWaterHitEvent(OnHitWater);

        hookObjectCollider.enabled = false;
    }
    protected virtual void Start()
    {
        playerNetwork = GetComponentInParent<Network_Player>();
        playerAnimator = playerNetwork.Animator;
        playerInventory = ComponentManager<PlayerInventory>.Get();
    }
	protected virtual void Update () 
	{
        if (!playerNetwork.IsLocalPlayer)
        {
            if (!throwable.InHand)
            {
                RotateHookTowardsPlayer();
            }
        }
        else
        {
            //Reset hook to hand if we are diving
            throwable.IsLocked = !personController.headAboveWater;
            if (!personController.headAboveWater)
            {
                if (!throwable.InHand)
                {
                    hookObject.AddCollectedItemsToInventory(playerInventory);
                    throwable.ResetCanThrow();
                    ResetHookToPlayer();
                }
                else if (throwable.GetChargeNormal() > 0f)
                {
                    hookObject.AddCollectedItemsToInventory(playerInventory);
                    throwable.ResetCanThrow();
                    ResetHookToPlayer();
                }
                return;
            }

            if (GameManager.IsInMenu)
                return;

            //Pull hook  towards player
            if (throwable.InWater)
            {
                RotateHookTowardsPlayer();

                if (Input.GetButton("LeftClick"))
                {
                    if (watersplash_hook_pulling == null)
                        watersplash_hook_pulling = ParticleManager.PlaySystem("WaterSplash_Hook_Pulling", hookObject.transform.position).GetComponent<ParticleSystem>();

                    if (!watersplash_hook_pulling.isPlaying)
                        watersplash_hook_pulling.Play();


                    //Move hook towards player
                    Vector3 hookPosition = throwable.GetPositon();
                    Vector3 dir = hookPosition - transform.position;
                    dir.y = 0;
                    dir.Normalize();

                    hookPosition -= dir * Time.deltaTime * pullSpeed;
                    throwable.SetPosition(hookPosition);

                    //Play splash effect while moving towards player
                    watersplash_hook_pulling.transform.position = hookObject.transform.position + Vector3.up * 0.2f + dir * -1.0f;

                    //Pickup hook if its close
                    Vector3 playerPos = transform.position;
                    playerPos.y = hookPosition.y;
                    float distance = Vector3.Distance(hookPosition, playerPos);
                    if (distance <= 1)
                    {
                        ResetHookToPlayer();

                        hookObject.AddCollectedItemsToInventory(playerInventory);
                    }
                }
                else if (Input.GetButtonUp("LeftClick"))
                {
                    if (watersplash_hook_pulling != null)
                    {
                        watersplash_hook_pulling.Stop();
                    }
                }
                else if (Input.GetButtonDown("RightClick") && hookObject.GetItemCount() <= 0)
                {
                    throwable.ResetCanThrow();
                    ResetHookToPlayer();
                }
            }

            if (throwable.InHand)
            {
                if (throwable.CanThrow)
                {
                    if (Input.GetButtonDown("LeftClick"))
                    {
                        playerAnimator.anim.SetBool("HookThrow", true);
                    }
                }
                if (Input.GetButtonUp("LeftClick"))
                {
                    throwable.ResetCanThrow();
                }
            }
        }

        rope.gameObject.SetActive(!throwable.InHand);
        if (rope.gameObject.activeInHierarchy)
        {
            rope.SetPosition(0, transform.position);
            rope.SetPosition(1, throwable.GetPositon());
        }
	}

	//public method
    public virtual void OnSelect()
    {
        if (playerAnimator == null)
            playerAnimator = GetComponentInParent<Network_Player>().Animator;
        playerAnimator.SetAnimation(PlayerAnimation.Index_2_Hook);
    }
    public virtual void OnDeSelect()
    {

    }

	//private methods
    private void OnTrow()
    {
        if (playerNetwork.IsLocalPlayer)
        {
            playerAnimator.anim.SetBool("HookThrow", false);
        }
    }
    private void OnHitWater()
    {
        ParticleManager.PlaySystem("WaterSplash_Hook", hookObject.transform.position + Vector3.up * 0.2f);

        hookObjectCollider.enabled = true;
    }
    protected void ResetHookToPlayer()
    {
        if (watersplash_hook_pulling != null)
        {
            PoolManager.ReturnObject(watersplash_hook_pulling.gameObject);
            watersplash_hook_pulling = null;
        }
        rope.gameObject.SetActive(false);

        throwable.PickupThrowable();
        hookObjectCollider.enabled = false;
        playerAnimator.SetAnimation(PlayerAnimation.Trigger_HookReset);
    }
    private void RotateHookTowardsPlayer()
    {
        Vector3 playerPosition = transform.position;
        Vector3 hookPos = hookObject.transform.position;
        hookPos.y = playerPosition.y;

        Vector3 dirAwayFromPlayer = hookPos - playerPosition; dirAwayFromPlayer.Normalize();
        hookObject.transform.up = dirAwayFromPlayer;
        hookObject.transform.eulerAngles = new Vector3(-270, hookObject.transform.eulerAngles.y, hookObject.transform.eulerAngles.z);
    }
}
