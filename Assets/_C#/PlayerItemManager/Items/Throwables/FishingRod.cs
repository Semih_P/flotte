﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Throwable))]
public class FishingRod : MonoBehaviour
 {
 	//public variables
    [Header("Components")]
    public ParticleSystem waterRingSystem;

    [Space]
    public Bobber bobber;
    public Transform fishingLineStart;

 	//private variables
    private Throwable throwable;
    private PersonController personController;
    [SerializeField] private Rope rope;
    private PlayerAnimator playerAnimator;

 	//Unity methods
	void Awake () 
	{
        personController = GetComponentInParent<PersonController>();
        throwable = GetComponent<Throwable>();
        throwable.SubscribeToWaterHitEvent(OnWaterHit);
        throwable.SubscribeToThrowEvent(OnThrow);

        waterRingSystem.transform.parent = null;
	}
    void Start()
    {
        playerAnimator = GetComponentInParent<Network_Player>().Animator;
    }
	void Update () 
	{
        throwable.IsLocked = !personController.headAboveWater;
        if (!personController.headAboveWater)
        {
            if (!throwable.InHand)
            {
                throwable.ResetCanThrow();
                PullItemFromSea();
            }
            else if (throwable.GetChargeNormal() > 0f)
            {
                throwable.ResetCanThrow();
                PullItemFromSea();
            }
            return;
        }

        if (GameManager.IsInMenu)
            return;

        if (throwable.InWater && !throwable.CanThrow)
        {
            if (Input.GetButtonDown("LeftClick"))
            {
                PullItemFromSea();
            }
        }

        if (throwable.InHand)
        {
            if (throwable.CanThrow)
            {
                if (Input.GetButtonDown("LeftClick"))
                {
                    playerAnimator.anim.SetBool("HookThrow", true);
                }
            }
            if (Input.GetButtonUp("LeftClick"))
            {
                throwable.ResetCanThrow();
            }
        }

        if (bobber.Escaped())
        {
            PullItemFromSea();
            throwable.ResetCanThrow();
        }
        if (bobber.FishIsOnHook())
        {
            waterRingSystem.Stop();
        }

        rope.SetPosition(0, fishingLineStart.position);
        rope.SetPosition(1, bobber.transform.position);
	}


	//public methods
    public void OnSelect()
    {
        if (playerAnimator == null)
            playerAnimator = GetComponentInParent<Network_Player>().Animator;
        playerAnimator.SetAnimation(PlayerAnimation.Index_3_Fishing);
    }
    public void OnDeSelect()
    {
        playerAnimator.anim.SetBool("HookThrow", false);
        rope.gameObject.SetActive(false);
    }


	//private methods
    private void PullItemFromSea()
    {
        Item_Base item = bobber.TryToGetFish();
        if (item != null)
        {
            playerAnimator.SetAnimation(PlayerAnimation.Trigger_FishingRetract);
            ComponentManager<PlayerInventory>.Get().AddItem(item.UniqueName, 1);
        }
        else
        {
            playerAnimator.SetAnimation(PlayerAnimation.Index_3_Fishing, true, false);
        }

        ParticleManager.PlaySystem("WaterSplash_Hook", throwable.throwableObject.position + Vector3.up * 0.1f);
        bobber.StopBobbing();
        throwable.PickupThrowable();
        throwable.throwableObjectBody.isKinematic = true;
        waterRingSystem.Stop();
    }
    private void OnThrow()
    {
        playerAnimator.anim.SetBool("HookThrow", false);
    }
    private void OnWaterHit()
    {
        bobber.StartBobbing();

        waterRingSystem.transform.position = bobber.transform.position;
        waterRingSystem.Play();

        ParticleManager.PlaySystem("WaterSplash_Hook", bobber.transform.position + Vector3.up * 0.1f);

        bobber.transform.rotation = Quaternion.LookRotation(Vector3.right, Vector3.up);
    }
}
