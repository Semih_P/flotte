﻿using UnityEngine;
using System.Collections;

public class Tool : MonoBehaviour
 {
 	//public variables
    public Item_Base ActiveTool { get { return tool; } }
    [SerializeField] private Item_Base tool;

 	//private variables
    protected bool canUse = true;
    protected Network_Player playerNetwork;
    protected PlayerAnimator playerAnimator;


 	//Unity methods
    protected virtual void Start()
    {
        playerNetwork = GetComponentInParent<Network_Player>();
        playerAnimator = playerNetwork.Animator;
    }

	//public methods
    public void OnSelect()
    {
        if (playerNetwork == null)
            playerNetwork = GetComponentInParent<Network_Player>();
        if (playerAnimator == null)
            playerAnimator = playerNetwork.Animator;

        OnToolSelect();
    }
    public void OnDeSelect()
    {
        OnToolDeSelect();
    }
    public void OnUse()
    {
        OnToolUse();
    }
    public void OnResetUse()
    {
        OnToolResetUse();
    }


    //protected methods
    protected virtual void OnToolSelect()
    {

    }
    protected virtual void OnToolDeSelect()
    {

    }
    protected virtual void OnToolUse()
    {

    }
    protected virtual void OnToolResetUse()
    {

    }

	//private methods
}
