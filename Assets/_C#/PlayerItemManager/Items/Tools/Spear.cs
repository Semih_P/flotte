﻿using UnityEngine;
using System.Collections;

public class Spear : Tool
 {
 	//public variables
    [Header("Spear settings")]
    public LayerMask attackMask;
    [SerializeField] private float attackRange = 3f;
    [SerializeField] private int damage = 5;

 	//private variables

 	//Unity methods
    protected override void Start()
    {
        base.Start();
    }

	//public methods

    //Tool methods
    protected override void OnToolSelect()
    {
        playerAnimator.SetAnimation(PlayerAnimation.Index_6_Spear);
    }
    protected override void OnToolUse()
    {
        if (PlayerItemManager.IsBusy)
            return;
        PlayerItemManager.IsBusy = true;

        playerAnimator.SetAnimation(PlayerAnimation.Trigger_SpearHit);
    }
    protected override void OnToolResetUse()
    {
        PlayerItemManager.IsBusy = false;
    }

	//private methods

    //Animation methods
    public void OnSpearHit()
    {
        RaycastHit hit = Helper.HitAtCursor(attackRange, attackMask);
        if (hit.transform != null)
        {
            Network_Entity entity = hit.collider.transform.GetComponentInParent<Network_Entity>();
            if (entity != null)
            {
                if (playerNetwork.Network.IsHost)
                    entity.Damage(damage);
                else
                    playerNetwork.SendP2P(new Message_DamageEntity(Messages.DamageEntity, entity, damage), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

                ParticleManager.PlaySystem("Blood_Spear", hit.point, Quaternion.LookRotation(hit.normal)); 

                ComponentManager<SoundManager>.Get().PlaySoundCopy("SpearHit", hit.point, true);
            }
        }
    }
}
