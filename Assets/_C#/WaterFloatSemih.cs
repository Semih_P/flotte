﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WaterPointGetter))]
public class WaterFloatSemih : MonoBehaviour 
{
	//public variables

	//private variables
    private WaterPointGetter waterPointGetter;

	//Unity methods
	void Awake () 
	{
        waterPointGetter = GetComponent<WaterPointGetter>();
	}	
	void Update () 
	{
        transform.position = new Vector3(transform.position.x, waterPointGetter.GetWaterPoint(transform.position), transform.position.z);
	}

	//public methods

	//private methods
}
