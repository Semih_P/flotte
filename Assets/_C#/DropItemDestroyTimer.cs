﻿using UnityEngine;
using System.Collections;

public class DropItemDestroyTimer : MonoBehaviour
 {
 	//public variables
    public float destroyTime = 20f;

 	//private variables
    private float timer;

 	//Unity methods
	void Start () 
	{
        ResetDestroyTimer();
	}
	void Update () 
	{
        timer -= Time.deltaTime;
        if (timer <= 0 )
        {
            PickupObjectManager.trackedPickupItems.Remove(GetComponent<PickupItem>());
            ResetDestroyTimer();
            gameObject.SetActive(false);
        }
	}
	//public methods
    public void ResetDestroyTimer()
    {
        timer = destroyTime;
    }

	//private methods
}
