﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightLightController : MonoBehaviour 
{
    //public variables
    public Light[] lights;
    public float nightIntensity;
    public float dayIntensity;

    //private variables
    private const float nightTimeStart = 0.8f;
    private const float nightTimeEnd = 0.1f;
    private GameManager gameManager;

    //Untiy methods
    void Awake()
    {
        gameManager = GameManager.Singleton;
    }
    void Update()
    {
        if (gameManager == null || gameManager.skyController == null)
            return;

        float normalDayValue = gameManager.skyController.TIME_of_DAY / 24f;
        bool isNight = normalDayValue >= nightTimeStart || normalDayValue <= nightTimeEnd;

        if (isNight)
        {
            SetLightIntensity(nightIntensity);
        }
        else
        {
            SetLightIntensity(dayIntensity);
        }
    }

    //private methods
    private void SetLightIntensity(float targetValue)
    {
        foreach(Light l in lights)
        {
            l.intensity = Mathf.MoveTowards(l.intensity, targetValue, Time.deltaTime * 0.05f);
        }
    }
}
