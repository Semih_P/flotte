﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class BuoyancyPoint
{
    public Transform transform;
    [HideInInspector]
    public Vector3 finalPosition;
    private WaterPointGetter waterPointGetter;

    public void Update()
    {
        transform.position = Vector3.Lerp(transform.position, finalPosition, Time.deltaTime * Buoyancy.LerpSpeed);
    }
    public void SetPosition(float xPosition, float zPosition)
    {
        if (waterPointGetter == null)
            waterPointGetter = transform.GetComponent<WaterPointGetter>();

        finalPosition.y = waterPointGetter.GetWaterPoint(transform.position);
        finalPosition.x = xPosition;
        finalPosition.z = zPosition;
    }
}
[System.Serializable]
public class BuoyancyBounds
{
    public BuoyancyPoint center;
    public BuoyancyPoint left;
    public BuoyancyPoint right;
    public BuoyancyPoint top;
    public BuoyancyPoint bottom;

    public void Update()
    {
        left.Update();
        right.Update();
        top.Update();
        bottom.Update();
    }
    public void SetCenterPosition(float x, float z)
    {
        center.transform.position = new Vector3(
            x,
            (left.transform.position.y + right.transform.position.y + top.transform.position.y + bottom.transform.position.y) / 4f,
            z);
    }
}

public class Buoyancy : MonoBehaviour 
{
    //public variables
    public static float LerpSpeed = 50f;
    public float lerpSpeed;
    public BuoyancyBounds bounds;
    public Transform buoyancyHolder;
    public float gizmoSize;
    public float edgeOffset = 1f;
    public List<Transform> foundations = new List<Transform>();
    
    //private variables

    void Start()
    {
        List<Block> placedBlocks = BlockCreator.GetPlacedBlocks();
            foreach(Block block in placedBlocks)
                if (block.buildableItem.UniqueName.Contains("Foundation"))
                    foundations.Add(block.transform);

        Invoke("LateStart", 1f);
        BlockCreator.PlaceBlockCallStack += OnBlockPlaced;
    }
    void LateStart()
    {
        OnBlockPlaced(null);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
            RefreshChildToHolderPosition();
        LerpSpeed = lerpSpeed;

        if (foundations == null || foundations.Count == 0)
            return;

        SetBuoyancyBounds();
        RefreshHolderRotation();
        buoyancyHolder.transform.position = new Vector3(buoyancyHolder.transform.position.x, bounds.center.transform.position.y, buoyancyHolder.transform.position.z);

        bounds.Update();
    }
    void OnDrawGizmosSelected()
    {
        if (bounds == null || bounds.left == null || bounds.right == null || bounds.top == null || bounds.bottom == null || bounds.center == null)
            return;

        Gizmos.color = Color.red * 0.5f;
        Gizmos.DrawSphere(bounds.left.transform.position, gizmoSize);

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(bounds.right.transform.position, gizmoSize);

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(bounds.top.transform.position, gizmoSize);

        Gizmos.color = Color.blue * 0.5f;
        Gizmos.DrawSphere(bounds.bottom.transform.position, gizmoSize);

        Gizmos.color = Color.green;
        Gizmos.DrawSphere(bounds.center.transform.position, gizmoSize);

        Gizmos.DrawLine(bounds.center.transform.position, bounds.right.transform.position);
        Gizmos.DrawLine(bounds.center.transform.position, bounds.left.transform.position);
        Gizmos.DrawLine(bounds.center.transform.position, bounds.top.transform.position);
        Gizmos.DrawLine(bounds.center.transform.position, bounds.bottom.transform.position);
    }
    
    //public methods
    public void SetBuoyancyBounds()
    {
        
        Vector3 accumilation = Vector3.zero;
        int blockCounter = 0;

        float xMin = float.MaxValue;
        float xMax = float.MinValue;
        float zMin = float.MaxValue;
        float zMax = float.MinValue;
        foreach(Transform blockTransform in foundations)
        {
            if (blockTransform == null)
                continue;

            if (blockTransform.position.x < xMin)
                xMin = blockTransform.position.x;
            if (blockTransform.position.x > xMax)
                xMax = blockTransform.position.x;

            if (blockTransform.position.z < zMin)
                zMin = blockTransform.position.z;
            if (blockTransform.position.z > zMax)
                zMax = blockTransform.position.z;

            accumilation += blockTransform.position;
            blockCounter++;
        }

        xMin -= edgeOffset;
        xMax += edgeOffset;
        zMin -= edgeOffset;
        zMax += edgeOffset;

        Vector3 center = accumilation / blockCounter;
        bounds.left.SetPosition(xMin, center.z);
        bounds.right.SetPosition(xMax, center.z);
        bounds.bottom.SetPosition(center.x, zMin);
        bounds.top.SetPosition(center.x, zMax);

        bounds.SetCenterPosition(center.x, center.z);
    }

    //private methods
    private void RefreshChildToHolderPosition()
    {
        if (buoyancyHolder != null)
        {
            while (buoyancyHolder.childCount > 0)
            {
                buoyancyHolder.GetChild(0).SetParent(transform);
            }
            buoyancyHolder.position = bounds.center.transform.position;
            while (transform.childCount > 0)
            {
                transform.GetChild(0).SetParent(buoyancyHolder);
            }
        }
    }
    private void RefreshHolderRotation()
    {
        Vector3 forward = bounds.top.transform.position - bounds.bottom.transform.position;
        Vector3 right = bounds.right.transform.position - bounds.left.transform.position;

        Vector3 cross = Vector3.Cross(forward, right);
        if (forward != Vector3.zero && cross != Vector3.zero)
            buoyancyHolder.rotation = Quaternion.LookRotation(forward, cross);
    }
    private void OnBlockPlaced(Block placedBlock)
    {
        if (placedBlock != null)
        {
            if (placedBlock.buildableItem.UniqueName.Contains("Foundation"))
            {
                foundations.Add(placedBlock.transform);
                RefreshChildToHolderPosition();
            }
        }
    }
}
