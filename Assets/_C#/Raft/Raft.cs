﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raft : MonoBehaviour
{
    //properties
    public bool IsAnchored { get { return anchored; } }
    public bool IsFullyAnchored { get { return fullyAnchored; } }

	//public variables
    public static Vector3 direction = new Vector3(0, 0, 1);
    public float maxSpeed = 5f;
    public float accelerationSpeed = 2f;
    public float deAccelerationSpeed = 1f;
    [SerializeField] private float networkLerpSpeed = 5f;

	//private variables
    private bool anchored = false;
    private bool fullyAnchored = false;
    private float speed = 0f;
    private int anchorCount = 0;
    private Semih_Network network;
    private Vector3 networkPosition;

	//Unity methods
    void Awake()
    {
        ComponentManager<Raft>.Set(this);
    }
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
    }
	void Update () 
	{
        if (!network.IsHost)
        {
            transform.position = Vector3.Lerp(transform.position, networkPosition, Time.deltaTime * networkLerpSpeed);
        }
        else
        {
            ControlSpeed();
            Move();
        }
	}

	//public methods
    public void AddAnchor()
    {
        if (network == null)
            network = ComponentManager<Semih_Network>.Get();

        if (network.IsHost)
        {
            anchorCount++;

            if (anchorCount == 1)
                SetAnchor(true);
        }
    }
    public void RemoveAnchor()
    {
        if (network == null)
            network = ComponentManager<Semih_Network>.Get();

        if (network.IsHost)
        {
            anchorCount--;

            if (anchorCount <= 0)
            {
                anchorCount = 0;
                SetAnchor(false);
            }
        }
        else
        {
            network.SendP2P(network.HostID, new Message_AnchorChange(Messages.AnchorChange, network.hostNetwork, -1), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }

	//private methods
    private void SetAnchor(bool anchorState)
    {
        anchored = anchorState;
    }
    private void ControlSpeed()
    {
        if (anchored)
        {
            speed = Mathf.MoveTowards(speed, 0f, Time.deltaTime * deAccelerationSpeed);
        }
        else
        {
            speed = Mathf.MoveTowards(speed, maxSpeed, Time.deltaTime * accelerationSpeed);
        }

        //Check if the raft is fully anchored
        fullyAnchored = speed <= 0.05f;
    }
    private void Move()
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    //Network methods
    public void SetNetworkProperties(Message_Raft msg)
    {
        networkPosition = new Vector3(msg.xPos, transform.position.y, msg.zPos);
    }
}

[System.Serializable]
public class Message_AnchorChange : Message_NetworkBehaviour
{
    public int anchorChange;

    public Message_AnchorChange(Messages type, MonoBehaviour_Network behaviour, int change)
        :base(type, behaviour)
    {
        this.anchorChange = change;
    }
}
