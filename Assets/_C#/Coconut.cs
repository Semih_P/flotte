﻿using UnityEngine;
using System.Collections;

public class Coconut : MonoBehaviour
 {
 	//public variables


 	//private variables
    private Rigidbody body;
    private PickupItem pickup;

 	//Unity methods
	void Start () 
	{
        body = GetComponent<Rigidbody>();
        pickup = GetComponent<PickupItem>();
        body.isKinematic = true;
        transform.localPosition = new Vector3(transform.localPosition.x, 2.7f, transform.localPosition.z);
	}


	//public methods
    public void DropFromTree()
    {
        body.isKinematic = false;
        pickup.canBePickedUp = true;
    }


	//private methods
}
