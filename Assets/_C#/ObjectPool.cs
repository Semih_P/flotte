﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPool
{
	//Public variables
    public string name;

	//Private variables
    [SerializeField]
    private GameObject objectPrefab = null;
    [SerializeField]
    private int initialPoolSize = 50;
    [SerializeField]
    private bool canGrow = true;

    private List<GameObject> pool = new List<GameObject>();


	//Public methods
    public void Initialize()
    {
        for (int i = 0; i < initialPoolSize; i++)
        {
            AddObjectToPool();
        }
    }
    public GameObject GetObject(bool returnObjectActive)
    {
        //Return first inactive object
        for (int i = 0; i < pool.Count; i++)
        {
            GameObject obj = pool[i];

            if (obj.activeSelf == false)
            {
                if (returnObjectActive)
                    obj.SetActive(true);
                return obj;
            }
        }

        //We did not find any object if we came here
        //If we can grow, create a new object and return that one
        if (canGrow)
        {
            GameObject obj = AddObjectToPool();
            if (returnObjectActive)
                obj.SetActive(true);
            return obj;
        }


        return null;
    }

	//Private methods
    private GameObject AddObjectToPool()
    {
        GameObject obj = GameObject.Instantiate(objectPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        pool.Add(obj);

        obj.transform.SetParent(PoolManager.Transform);
        obj.SetActive(false);
        return obj;
    }

}
