﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPlacementCollection
{
    public GameObject prefab = null;
    public bool alignToNormal = true;
    public float offsetFromSurface = 0f;

    public bool manipulateRotation = false;
    public Vector3 baseRotation = Vector3.zero;
    public Vector3 minRotationOffset = Vector3.zero;
    public Vector3 maxRotationOffset = Vector3.zero;

    public bool manipulateScale = false;
    public bool uniformScale = true;
    public Vector3 minScale = Vector3.one;
    public Vector3 maxScale = Vector3.one * 2;
}
public class ObjectPlacementScript : MonoBehaviour
{
    //public variables
    [HideInInspector]
    public List<ObjectPlacementCollection> prefabCollections;
    [HideInInspector]
    public Transform parentTransform;
    [HideInInspector]
    public ObjectPlacementCollection selectedCollection;
    [HideInInspector]
    public List<GameObject> lastCreatedObjects;
    [HideInInspector]
    public bool active;
}
