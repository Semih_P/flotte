﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class ObjectPlacementEditor : EditorWindow 
{
	//public variables

	//private variables
    private static ObjectPlacementScript Script
    {
        get
        {
            if (script != null)
                return script;
            else
            {
                script = GameObject.FindObjectOfType<ObjectPlacementScript>();
                if (script == null)
                {
                    GameObject obj = new GameObject("ObjectPlacementScript");
                    script = obj.AddComponent<ObjectPlacementScript>();
                }
                return script;
            }
        }
    }
    private static ObjectPlacementScript script;

    public static ObjectPlacementEditor Window 
    { 
        get 
        { 
            if (window == null)
            {
                window = (ObjectPlacementEditor)EditorWindow.GetWindow(typeof(ObjectPlacementEditor));
                return window;
            }
            return window;
        }
    }
    private static ObjectPlacementEditor window;

    private static Event currentEvent;
    private static RaycastHit hit;
    private static Vector2 scrollPosition;

    private static int buttonWidth = 75;
    private static int windowWidth = 300;

	//Unity methods
    [MenuItem("Semih/ObjectPlacement/Window")]
    static void Init()
    {
        window = (ObjectPlacementEditor)EditorWindow.GetWindow(typeof(ObjectPlacementEditor));
        window.Show();
    }
    void OnFocus()
    {
        SceneView.onSceneGUIDelegate -= OnEvent;
        SceneView.onSceneGUIDelegate += OnEvent;
    }
    void OnGUI()
    {
        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, "Box", GUILayout.MinWidth(windowWidth + 100));
        if (GUILayout.Button(Script.active ? "Deactivate" : "Activate"))
        {
            Script.active = !Script.active;
        }
        if (!Script.active)
        {
            EditorGUILayout.EndScrollView();
            return;
        }

        EditorGUILayout.BeginVertical("Box");
        //Select which parent to create under
        Script.parentTransform = (Transform)EditorGUILayout.ObjectField("Parent transform: ", Script.parentTransform, typeof(Transform), true);
        EditorGUILayout.EndVertical();

        GUILayout.Space(30);



        EditorGUILayout.BeginVertical("Box", new GUILayoutOption[] { GUILayout.MaxWidth(windowWidth), GUILayout.MinWidth(windowWidth) });
        //Create new prefab collection
        if (GUILayout.Button("Create new prefab"))
        {
            AddNewCollection();
        }
        GUILayout.Space(15);
        if (Script.prefabCollections != null && Script.prefabCollections.Count >= 0)
        {
            //Draw the prefab list
            for (int i = 0; i < Script.prefabCollections.Count; i++)
            {
                ObjectPlacementCollection collection = Script.prefabCollections[i];
                if (collection == Script.selectedCollection)
                    GUI.color = new Color(0.6f, 1f, 0.6f);
                else
                    GUI.color = new Color(1, 0.6f, 0.6f);

                //Display collection properties
                EditorGUILayout.BeginVertical("Box", new GUILayoutOption[] { GUILayout.MaxWidth(windowWidth), GUILayout.MinWidth(windowWidth) });
                if (collection == Script.selectedCollection)
                {
                    GUILayout.Label(collection.prefab == null ? "Attach a prefab" : collection.prefab.name.ToString(), EditorStyles.boldLabel);
                    GUI.color = Color.white;
                    GUILayout.Space(10);
                    collection.prefab = (GameObject)EditorGUILayout.ObjectField("Prefab: ", collection.prefab, typeof(GameObject), true);
                    collection.offsetFromSurface = EditorGUILayout.FloatField("Surface offset: ", collection.offsetFromSurface);
                    collection.alignToNormal = EditorGUILayout.Toggle("Align to normal: ", collection.alignToNormal);

                    collection.manipulateRotation = EditorGUILayout.BeginToggleGroup("Manipulate rotation", collection.manipulateRotation);
                    if (collection.manipulateRotation)
                    {
                        collection.baseRotation = EditorGUILayout.Vector3Field("Base rotation: ", collection.baseRotation);
                        collection.minRotationOffset = EditorGUILayout.Vector3Field("Min rotation offset: ", collection.minRotationOffset);
                        collection.maxRotationOffset = EditorGUILayout.Vector3Field("Max rotation offset: ", collection.maxRotationOffset);
                    }
                    EditorGUILayout.EndToggleGroup();

                    collection.manipulateScale = EditorGUILayout.BeginToggleGroup("Manipulate scale", collection.manipulateScale);
                    if (collection.manipulateScale)
                    {
                        collection.uniformScale = EditorGUILayout.Toggle("Uniform scale: ", collection.uniformScale);
                        if (collection.uniformScale)
                        {
                            collection.minScale.x = EditorGUILayout.FloatField("Min scale: ", collection.minScale.x);
                            collection.maxScale.x = EditorGUILayout.FloatField("Max scale: ", collection.maxScale.x);
                        }
                        else
                        {
                            collection.minScale = EditorGUILayout.Vector3Field("Min scale: ", collection.minScale);
                            collection.maxScale = EditorGUILayout.Vector3Field("Max scale: ", collection.maxScale);
                        }
                    }
                    EditorGUILayout.EndToggleGroup();
                }

                //Select, deselect and remove collection
                EditorGUILayout.BeginHorizontal();
                if (collection != Script.selectedCollection)
                {
                    GUILayout.Label(collection.prefab == null ? "Attach a prefab" : collection.prefab.name.ToString(), EditorStyles.boldLabel);
                    GUI.color = Color.white;
                    GUILayout.Space(20);
                    if (GUILayout.Button("Select", GUILayout.Width(buttonWidth)))
                    {
                        Script.selectedCollection = collection;
                    }
                }
                else
                {
                    GUILayout.Space(windowWidth - buttonWidth * 2);
                    if (GUILayout.Button("Deselect", GUILayout.Width(buttonWidth)))
                    {
                        Script.selectedCollection = null;
                    }
                }
                if (GUILayout.Button("Remove", GUILayout.Width(buttonWidth)))
                {
                    Script.prefabCollections.RemoveAt(i);
                    i--;
                }
                if (Script.prefabCollections.Count > 1)
                {
                    if (i != 0)
                    {
                        if (GUILayout.Button("^", GUILayout.Width(buttonWidth / 2)))
                        {
                            ObjectPlacementCollection previousCollection = Script.prefabCollections[i - 1];
                            ObjectPlacementCollection tmpCollection = collection;
                            script.prefabCollections[i] = previousCollection;
                            script.prefabCollections[i - 1] = tmpCollection;
                        }
                    }
                    if (i != Script.prefabCollections.Count - 1)
                    {
                        if (GUILayout.Button("v", GUILayout.Width(buttonWidth / 2)))
                        {
                            ObjectPlacementCollection nextCollection = Script.prefabCollections[i + 1];
                            ObjectPlacementCollection tmpCollection = collection;
                            script.prefabCollections[i] = nextCollection;
                            script.prefabCollections[i + 1] = tmpCollection;
                        }
                    }
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
            }
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.EndScrollView();
    }
    static void OnEvent(SceneView view)
    {
        if (!Script.active)
            return;

        currentEvent = Event.current;

        Ray ray = HandleUtility.GUIPointToWorldRay(currentEvent.mousePosition);
        Physics.Raycast(ray, out hit);

        switch (currentEvent.type)
        {
            case EventType.keyDown:
                if (currentEvent.modifiers == EventModifiers.Shift)
                {
                    if (currentEvent.keyCode == KeyCode.W)
                    {
                        CreateObject();
                    }
                    else if (currentEvent.keyCode == KeyCode.S)
                    {
                        UndoLastObject();
                    }
                }
                break;
        }
    }

    //[MenuItem("Semih/ObjectPlacement/Place object #f")]
    static void CreateObject()
    {
        if (!Script.active)
            return;

        ObjectPlacementCollection collection = Script.selectedCollection;
        if (collection == null || collection.prefab == null)
            return;


        GameObject obj = Instantiate(collection.prefab, hit.point, Quaternion.identity, Script.parentTransform) as GameObject;

        //Align to normal of surface
        if (collection.alignToNormal)
        {
            obj.transform.rotation = Quaternion.LookRotation(hit.normal);
            obj.transform.eulerAngles += new Vector3(90, 0, 0);
        }

        //Set surface offset
        obj.transform.position += hit.normal * collection.offsetFromSurface;

        //Manipulate offset
        if (collection.manipulateRotation)
        {
            Vector3 rotationOffset = new Vector3(
            collection.baseRotation.x + Random.Range(collection.minRotationOffset.x, collection.maxRotationOffset.x),
            collection.baseRotation.y + Random.Range(collection.minRotationOffset.y, collection.maxRotationOffset.y),
            collection.baseRotation.z + Random.Range(collection.minRotationOffset.z, collection.maxRotationOffset.z)
            );

            obj.transform.Rotate(obj.transform.right, rotationOffset.x, Space.World);
            obj.transform.Rotate(obj.transform.up, rotationOffset.y, Space.World);
            obj.transform.Rotate(obj.transform.forward, rotationOffset.z, Space.World);
        }

        //Manipulate scale
        if (collection.manipulateScale)
        {
            Vector3 newScale = new Vector3(
                Random.Range(collection.minScale.x, collection.maxScale.x),
                Random.Range(collection.minScale.y, collection.maxScale.y),
                Random.Range(collection.minScale.z, collection.maxScale.z)
                );

            if (collection.uniformScale)
            {
                obj.transform.localScale = new Vector3(newScale.x, newScale.x, newScale.x);
            }
            else
            {
                obj.transform.localScale = newScale;
            }
        }

        //Save created objects
        if (Script.lastCreatedObjects == null)
            Script.lastCreatedObjects = new List<GameObject>();
        Script.lastCreatedObjects.Add(obj);

        //Dont save too many
        if (Script.lastCreatedObjects.Count > 100)
        {
            Script.lastCreatedObjects.RemoveAt(0);
        }
        Window.Repaint();
    }
    //[MenuItem("Semih/ObjectPlacement/Undo Placement #z")]
    static void UndoLastObject()
    {
        if (!Script.active)
            return;

        if (Script != null)
        {
            if (Script.lastCreatedObjects != null && Script.lastCreatedObjects.Count > 0)
            {
                RemoveNullLastCreatedObjects();

                int index = Script.lastCreatedObjects.Count - 1;
                DestroyImmediate(Script.lastCreatedObjects[index]);
                Script.lastCreatedObjects.RemoveAt(index);
                Window.Repaint();
            }
        }
    }
    static void AddNewCollection()
    {
        if (Script.prefabCollections == null)
            Script.prefabCollections = new List<ObjectPlacementCollection>();

        Script.prefabCollections.Add(new ObjectPlacementCollection());

        if (Script.prefabCollections.Count == 1)
            Script.selectedCollection = Script.prefabCollections[0];

        Window.Repaint();
    }
	//public methods

	//private methods
    static void RemoveNullLastCreatedObjects()
    {
        for (int i = 0; i < Script.lastCreatedObjects.Count; i++)
        {
            if (Script.lastCreatedObjects[i] == null)
            {
                Script.lastCreatedObjects.RemoveAt(i);
                i--;
            }
        }
    }
}
#endif