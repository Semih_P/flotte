﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ObjectSpawner))]
public class ObjectSpawnerEditor : Editor
{
    //Public variables
    ObjectSpawner objectSpawner;

    //Private variables


    //Unity methods
    void OnEnable()
    {
        objectSpawner = target as ObjectSpawner;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();



        GUILayout.Space(20);
        GUILayout.BeginVertical("Box");

        if (GUILayout.Button("Add spawnable object"))
        {
            AddSpawnable();
        }

        if (objectSpawner.spawnableObjects == null)
            return;

        GUILayout.Space(15);

        for (int i = 0; i < objectSpawner.spawnableObjects.Count; i++)
        {
            SpawnableFloatingObject obj = objectSpawner.spawnableObjects[i];

            GUILayout.BeginHorizontal("Box");

            GUILayout.BeginVertical();
            obj.uniqueIndex = (uint)i;
            EditorGUILayout.LabelField("Unique index: ", obj.uniqueIndex.ToString());
            obj.typeToSpawn = (ObjectSpawnType)EditorGUILayout.EnumPopup("Type of spawnable: ", obj.typeToSpawn);
            if (obj.typeToSpawn == ObjectSpawnType.Pool)
            {
                obj.poolName = EditorGUILayout.TextField("Pool name: ", obj.poolName);
            }
            else
            {
                obj.prefab = (GameObject)EditorGUILayout.ObjectField("Prefab: ", obj.prefab, typeof(GameObject), true);
            }
            GUILayout.Space(10);

            obj.minRotation = EditorGUILayout.Vector3Field("Min rotation: ", obj.minRotation);
            obj.maxRotation = EditorGUILayout.Vector3Field("Max rotation: ", obj.maxRotation);
            obj.spawnChance = EditorGUILayout.Slider("Spawn chance: ", obj.spawnChance, 0f, 100f);
            obj.metersUnderSurface = EditorGUILayout.Slider("Meters under surface: ", obj.metersUnderSurface, 0, 100f);


            if (GUILayout.Button("X", GUILayout.Width(50f)))
            {
                objectSpawner.spawnableObjects.RemoveAt(i);
                return;
            }

            GUILayout.EndVertical();

            GUILayout.EndHorizontal();
            GUILayout.Space(10);
        }

        GUILayout.EndVertical();
    }

    //Public methods


    //Private methods
    private void AddSpawnable()
    {
        if (objectSpawner.spawnableObjects == null)
            objectSpawner.spawnableObjects = new List<SpawnableFloatingObject>();

        objectSpawner.spawnableObjects.Add(new SpawnableFloatingObject());
    }

}
#endif