﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;

[CustomEditor(typeof(Reef))]
public class ReefEditor : Editor 
{
	//public variables

	//private variables
    private Reef reef;

	//Unity methods
    void OnEnable()
    {
        reef = target as Reef;
    }

	//public methods
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Space(20);
        GUILayout.BeginVertical("Box");

        PickupItem[] pickups = reef.transform.GetComponentsInChildren<PickupItem>();
        Dictionary<string, int> itemCount = new Dictionary<string, int>();
        reef.pickups = pickups.ToList();
        for (int i = 0; i < pickups.Length; i++)
        {
            PickupItem item = pickups[i];
            if (item.item != null)
            {
                if (itemCount.ContainsKey(item.item.UniqueName))
                {
                    itemCount[item.item.UniqueName]++;
                }
                else
                    itemCount.Add(item.item.UniqueName, 1);
            }
        }

        GUILayout.Label("Total number of items in reef: " + reef.pickups.Count);
        foreach(KeyValuePair<string, int> pair in itemCount)
        {
            GUILayout.BeginVertical("Box");
            GUILayout.Label(pair.Key + ": " + pair.Value);
            GUILayout.EndVertical();
        }

        GUILayout.EndVertical();
    }

	//private methods
}
#endif