﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class ItemManagerEditor : EditorWindow
{
    private string itemName;
    private ItemType itemType;
    private int selectedDirectoryIndex;
    private static List<Item_Base> allAvailableItems = new List<Item_Base>();

    //Inventory settings
    private bool showInventorySettings;
    private Sprite itemSprite;
    private string itemDisplayName;
    private string itemDescription;
    private int itemMaxStack = 1;
    private bool setAnimationGrip = false;

    //Equipment settings
    private bool showEquipmentSettings;
    private EquipSlotType equipSlotType;

    //Buildable settings
    private bool showBuildableSettings;
    private Block buildablePrefab;
    private int requiredStabilityCount;

    //Consume settings
    private bool showConsumeSettings;
    private int consumeUses;
    private float hungerYield;
    private float thirstYield;
    private bool isRaw;
    private Item_Base itemAfterUse;
    private int itemAfterUseCount;
    private Fluid fluidType;

    //Cookable settings
    private bool showCookableSettings;
    private float cookTime;
    private Item_Base cookResult;

    //Usable settings
    private bool showUsableSettings;
    private bool allowUseButtonHold = false;
    private bool isUsable = false;
    private string useButtonName = "LeftClick";
    private float useCooldown = 0.2f;


    private Vector2 scrollPosition;
    // Add menu named "My Window" to the Window menu
    [MenuItem("Semih/ItemManager/Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        ItemManagerEditor window = (ItemManagerEditor)EditorWindow.GetWindow(typeof(ItemManagerEditor));
        window.Show();
    }

    void OnGUI()
    {
        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, "Box");
        allAvailableItems = Resources.LoadAll<Item_Base>("IItems").ToList();
        int currentIndex = LoadIndex();

        //Check item name
        itemName = EditorGUILayout.TextField("Item name", itemName);
        if (ItemNameExist(itemName))
        {
            EditorGUILayout.LabelField((itemName == string.Empty) ? "You must enter a name for the item" : "Item name already exists");
            EditorGUILayout.EndScrollView();
            return;
        }

        //Check item type
        itemType = (ItemType)EditorGUILayout.EnumPopup("Item type", itemType);

        //Folder to put asset in
        string[] directories = Directory.GetDirectories("Assets/Resources/IItems/");
        selectedDirectoryIndex = EditorGUILayout.Popup("Directory of item: ", selectedDirectoryIndex, directories);

        //Inventory settings
        EditorGUILayout.BeginVertical("Box");
        GUILayout.Label("Inventory settings", EditorStyles.boldLabel);

        itemSprite = (Sprite)EditorGUILayout.ObjectField("Item sprite", itemSprite, typeof(Sprite), true);
        itemDisplayName = EditorGUILayout.TextField("Display name", itemDisplayName);
        itemDescription = EditorGUILayout.TextArea(itemDescription, GUILayout.Height(50));
        itemMaxStack = EditorGUILayout.IntSlider("Stack size", itemMaxStack, 0, 100);
        setAnimationGrip = EditorGUILayout.Toggle("Set animation to grip on equip", setAnimationGrip);

        EditorGUILayout.EndVertical();
        GUILayout.Space(20);

        //Usable settings
        EditorGUILayout.BeginVertical("Box");
        GUILayout.Label("Usable settings", EditorStyles.boldLabel);

        isUsable = EditorGUILayout.Toggle("Is a usable", isUsable);
        allowUseButtonHold = EditorGUILayout.Toggle("Allow hold use button", allowUseButtonHold);
        useButtonName = EditorGUILayout.TextField("Use button name", useButtonName);
        useCooldown = EditorGUILayout.FloatField("Use cooldown", useCooldown);

        EditorGUILayout.EndVertical();
        GUILayout.Space(20);

        //Consumeable settings
        EditorGUILayout.BeginVertical("Box");
        GUILayout.Label("Consumeable settings", EditorStyles.boldLabel);

        consumeUses = EditorGUILayout.IntField("Uses", consumeUses);
        hungerYield = EditorGUILayout.FloatField("Hunger yield", hungerYield);
        thirstYield = EditorGUILayout.FloatField("Thirst yield", thirstYield);
        isRaw = EditorGUILayout.Toggle("Is raw?", isRaw);
        itemAfterUse = (Item_Base)EditorGUILayout.ObjectField("Item after use", itemAfterUse, typeof(Item_Base), true);
        itemAfterUseCount = EditorGUILayout.IntField("After use count", itemAfterUseCount);
        fluidType = (Fluid)EditorGUILayout.EnumPopup("Fluid type", fluidType);

        EditorGUILayout.EndVertical();
        GUILayout.Space(20);

        //Cookable settings
        EditorGUILayout.BeginVertical("Box");
        GUILayout.Label("Cookable settings", EditorStyles.boldLabel);

        cookTime = EditorGUILayout.FloatField("Cook time", cookTime);
        cookResult = (Item_Base)EditorGUILayout.ObjectField("Cook result", cookResult, typeof(Item_Base), true);

        EditorGUILayout.EndVertical();
        GUILayout.Space(20);

        //Buildable settings
        EditorGUILayout.BeginVertical("Box");
        GUILayout.Label("Buildable settings", EditorStyles.boldLabel);

        buildablePrefab = EditorGUILayout.ObjectField("Buildable Prefab: ", buildablePrefab, typeof(Block), true) as Block;
        requiredStabilityCount = EditorGUILayout.IntField("Required stability count: ", requiredStabilityCount);

        EditorGUILayout.EndVertical();
        GUILayout.Space(20);

        //Equipable items
        EditorGUILayout.BeginVertical("Box");
        GUILayout.Label("Equipable settings", EditorStyles.boldLabel);

        equipSlotType = (EquipSlotType)EditorGUILayout.EnumPopup("Slot type", equipSlotType);

        EditorGUILayout.EndVertical();
        GUILayout.Space(20);


        //Create item
        if (GUILayout.Button("Create new item"))
        {
            Item_Base item = null;
            currentIndex++;
            SaveIndex(currentIndex);

            item = ScriptableObject.CreateInstance<Item_Base>();
            item.Initialize(currentIndex, itemName, itemType);

            item.settings_Inventory = new ItemInstance_Inventory(itemSprite, itemDisplayName, itemDescription, itemMaxStack, setAnimationGrip);
            item.settings_cookable = new ItemInstance_Cookable(cookTime, cookResult);
            item.settings_consumeable = new ItemInstance_Consumeable(consumeUses, hungerYield, thirstYield, isRaw, new Cost(itemAfterUse, itemAfterUseCount));
            item.settings_usable = new ItemInstance_Usable(useButtonName, useCooldown, isUsable, allowUseButtonHold);
            item.settings_buildable = new ItemInstance_Buildable(buildablePrefab, requiredStabilityCount);
            item.settings_equipment = new ItemInstance_Equipment(equipSlotType);

            string dir = "Assets/Resources/IItems/";
            if (selectedDirectoryIndex < directories.Length)
                dir = directories[selectedDirectoryIndex] + "/";
            string path = dir + itemName + ".asset";
            AssetDatabase.CreateAsset(item, path);
            AssetDatabase.SaveAssets();
        }
        EditorGUILayout.EndScrollView();
    }

    private bool ItemNameExist(string itemName)
    {
        foreach(Item_Base item in allAvailableItems)
        {
            if (item.UniqueName == itemName)
                return true;
        }
        return false;
    }

    private static void SaveIndex(int newIndex)
    {
        allAvailableItems.Sort((a, b) => a.UniqueIndex.CompareTo(b.UniqueIndex));

        string path = "ItemManager.txt";
        StreamWriter stream = new StreamWriter(path);
        for (int i = 0; i < allAvailableItems.Count; i++)
        {
            stream.WriteLine(allAvailableItems[i].UniqueName + "," + allAvailableItems[i].UniqueIndex);
        }
        stream.WriteLine("CurrentIndex," + newIndex.ToString());
        stream.Close();
    }
    private static int LoadIndex()
    {
        StreamReader stream = new StreamReader("ItemManager.txt");
        string line = string.Empty;
        while(!line.Contains("CurrentIndex"))
        {
            line = stream.ReadLine();
        }
        int index = int.Parse(line.Split(',').Last());
        stream.Close();

        return index;
    }
}
#endif