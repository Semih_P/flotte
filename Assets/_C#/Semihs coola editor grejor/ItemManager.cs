﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemManager 
{
    //private variables
    private static List<Item_Base> allAvailableItems;

    //CTOR 
    public static void Initialize()
    {
        LoadAllIItems();
        InitializeEquipItems();
    }

    //public methods
    public static int UniqueNameToIndex(string uniqueItemName)
    {
        foreach(Item_Base item in allAvailableItems)
        {
            if (item.UniqueName == uniqueItemName)
            {
                return item.UniqueIndex;
            }
        }
        return -1;
    }
    public static string UniqueIndexToName(int uniqueIndex)
    {
        foreach (Item_Base item in allAvailableItems)
        {
            if (item.UniqueIndex == uniqueIndex)
            {
                return item.UniqueName;
            }
        }
        return string.Empty;
    }
    public static Item_Base GetItemByIndex(int uniqueIndex)
    {
        return allAvailableItems.Single(i => i.UniqueIndex == uniqueIndex);
    }
    public static Item_Base GetItemByName(string uniqueName)
    {
        return allAvailableItems.Single(i => i.UniqueName == uniqueName);
    }
    public static List<Item_Base> GetBuildableItems()
    {
        List<Item_Base> buildableItems = new List<Item_Base>();
        for (int i = 0; i < allAvailableItems.Count; i++)
        {
            if (allAvailableItems[i].GetType() == ItemType.Buildable)
            {
                buildableItems.Add(allAvailableItems[i]);
            }
        }
        return buildableItems;
    }
    //private methods
    private static void LoadAllIItems()
    {
        allAvailableItems = Resources.LoadAll<Item_Base>("IItems").ToList();
    }
    private static void InitializeEquipItems()
    {
        //Item_Equipable_Flipper flipper = GetItemByName("Flipper") as Item_Equipable_Flipper;
        //flipper.Initialize(GameObject.FindObjectOfType<PersonController>());

        //Item_Equipable_AirBottle airBottle = GetItemByName("AirBottle") as Item_Equipable_AirBottle;
        //airBottle.Initialize(GameObject.FindObjectOfType<PlayerStats>());
    }
}
