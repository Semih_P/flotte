﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemNet : PickupItem 
{
	//public variables
    [HideInInspector] public ItemCollector itemCollector;

	//private variables
    private CanvasHelper canvas;

	//Unity methods
    void Awake()
    {
        itemCollector = GetComponentInChildren<ItemCollector>();
    }
    void Start()
    {
        canvas = CanvasHelper.Singleton;
    }
    void OnMouseOver()
    {
        if (Helper.LocalPlayerIsWithinDistance(transform.position, Player.UseDistance) && itemCollector.GetItemCount() > 0)
        {
            canvas.SetDisplayText("Pickup " + itemCollector.GetItemCount() + " items", true);
        }
        else
            canvas.SetDisplayText(false);
    }
	//public methods

	//private methods
}
