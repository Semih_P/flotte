﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlantationSlot
{
    public Transform transform;
    [HideInInspector]
    public bool busy;
    public Plant plant;
}
public class Cropplot : MonoBehaviour_ID
 {
 	//public variables
    public bool IsAimedAtWithPlantable;
    public bool IsFull { get { return GetBusySlotCount() == plantationSlots.Count; } }

    [Space(10)]
    [Tooltip("Which items are available to plant in this cropplot, unique item names")]
    public List<Item_Base> acceptableItemTypes = new List<Item_Base>();

    [Space(10)]
    public List<PlantationSlot> plantationSlots = new List<PlantationSlot>();

 	//private variables
    private CanvasHelper canvas;

 	//Unity methods
	void Start () 
	{
        canvas = CanvasHelper.Singleton;
	}

    //Called from Block.cs via sendmessages
    void OnBlockPlaced()
    {
        ObjectIndex = SaveAndLoad.GetUniqueObjectIndex();
        ObjectIndex = GetComponent<Block>().ObjectIndex + 1;
        PlantManager.allCropplots.Add(this);
    }

    void OnMouseOver()
    {
        if (IsAimedAtWithPlantable && !IsFull)
        {
            canvas.SetDisplayText("Plant seed", true);
        }
        else
        {
            canvas.SetDisplayText(false);
        }
    }
    void OnMouseExit()
    {
        canvas.SetDisplayText(false);
    }

	//public methods
    public Plant Plant(Plant plantPrefab, int slotIndex = -1)
    {
        if (IsFull)
            return null;

        PlantationSlot slot = slotIndex == -1 ? GetEmptyPlantationSlot() : plantationSlots[slotIndex];
        if (slot == null)
            return null;

        GameObject plantObj = Instantiate(plantPrefab.gameObject, slot.transform.position, plantPrefab.transform.rotation, transform) as GameObject;
        Plant plant = plantObj.GetComponent<Plant>();
        slot.plant = plant;
        slot.busy = true;

        if (plant != null)
        {
            plant.cropplot = this;
            plant.plantationSlotIndex = plantationSlots.IndexOf(slot);
            plant.SubscribeToOnPlantRemove(PlantRemoved);
            plant.SetGrowTimer(0);
        }

        return plant;
    }
    public void PlantRemoved(int slotIndex)
    {
        for (int i = 0; i < plantationSlots.Count; i++)
        {
            if (i == slotIndex)
            {
                plantationSlots[i].busy = false;
                break;
            }
        }
    }
    public bool AcceptsPlantType(Item_Base item)
    {
        foreach (Item_Base acceptableItem in acceptableItemTypes)
        {
            if (acceptableItem == item)
                return true;
        }
        return false;
    }
    public List<PlantationSlot> GetSlots()
    {
        return plantationSlots;
    }

	//private methods
    private PlantationSlot GetEmptyPlantationSlot()
    {
        for (int i = 0; i < plantationSlots.Count; i++)
        {
            if (!plantationSlots[i].busy)
            {
                return plantationSlots[i];
            }
        }
        return null;
    }
    private int GetBusySlotCount()
    {
        int count = 0;
        for (int i = 0; i < plantationSlots.Count; i++)
        {
            if (plantationSlots[i].busy)
                count++;
        }
        return count;
    }
}
