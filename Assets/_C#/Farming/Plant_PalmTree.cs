﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Plant_PalmTree : Plant 
{
	//public variables
    [HideInInspector] public List<Coconut> coconuts = new List<Coconut>();

	//private variables

	//Unity methods
    public override void Awake()
    {
        base.Awake();
        coconuts = transform.GetComponentsInChildren<Coconut>(true).ToList();
    }
	//public methods
    public override void Pickup()
    {
        for (int i = 0; i < coconuts.Count; i++)
        {
            Coconut coconut = coconuts[i];

            coconut.transform.SetParent(GameManager.Singleton.globalBuoyancyParent);
            coconut.DropFromTree();
        }
        base.Pickup();
    }

	//private methods
}
