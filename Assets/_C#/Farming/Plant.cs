﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Plant : MonoBehaviour
 {
    public delegate void OnPlantRemoved(int slotIndex);
    private OnPlantRemoved onRemovedCallstack;

 	//public variables
    [Tooltip("The name of the plant item")]
    public Item_Base item;

    [Header("Grow settings")]
    [Tooltip("In seconds")]
    public float growTime = 1f;
    public Vector3 minScale = new Vector3(0.1f, 0.1f, 0.1f);
    public Vector3 maxScale = Vector3.one;

    [Header("Harvest settings")]
    public PlayerAnimation holdPlantAnimation = PlayerAnimation.Index_5_HoldItem;
    public bool harvestable = true;
    public List<Cost> yieldItems = new List<Cost>();

    [HideInInspector]
    public int plantationSlotIndex;
    [HideInInspector]
    public Cropplot cropplot;

 	//private variables
    private float growTimer;
    private bool fullyGrown;
    private CanvasHelper canvas;
    private PlayerInventory playerInventory;

 	//Unity methods
    public virtual void Awake()
    {
        ResetStats();
    }
	void Start () 
	{
        canvas = CanvasHelper.Singleton;
        playerInventory = ComponentManager<PlayerInventory>.Get();
	}
	void Update () 
	{
        float normal = Mathf.Clamp(growTimer / growTime, 0, 1f);
        Vector3 targetScale = minScale + ((maxScale - minScale) * normal);
        transform.localScale = Vector3.MoveTowards(transform.localScale, targetScale, Time.deltaTime * 20);

        if (fullyGrown)
            return;

        growTimer += Time.deltaTime;


        if (growTimer >= growTime)
        {
            Complete();
        }
	}
    void OnMouseExit()
    {
        canvas.SetDisplayText(false);
    }


	//public methods
    public void SubscribeToOnPlantRemove(OnPlantRemoved method)
    {
        onRemovedCallstack += method;
    }
    public bool FullyGrown()
    {
        return fullyGrown;
    }
    public virtual void Pickup()
    {
        if (onRemovedCallstack != null)
            onRemovedCallstack(plantationSlotIndex);

        plantationSlotIndex = -1;
        Destroy(gameObject);
    }
    public Cost GetYieldItem()
    {
        if (YieldLeft() > 0)
        {
            Cost yieldObj = yieldItems[Random.Range(0, yieldItems.Count)];
            yieldItems.Remove(yieldObj);
            return yieldObj;
        }
        else
            return null;
    }
    public int YieldLeft()
    {
        return yieldItems.Count;
    }

    public void SetYield(List<Cost> newYields)
    {
        yieldItems.Clear();
        yieldItems.AddRange(newYields);
    }
    public void SetGrowTimer(float value)
    {
        growTimer = value;

        if (growTimer < growTime)
            fullyGrown = false;
    }
    public float GetGrowTimer()
    {
        return growTimer;
    }

	//private methods
    private void ResetStats()
    {
        growTimer = 0;
        fullyGrown = false;

        transform.localScale = minScale;
    }
    private void Complete()
    {
        fullyGrown = true;
    }
}
