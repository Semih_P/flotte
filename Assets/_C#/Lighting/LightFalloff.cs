﻿using PlayWay.Water;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AzureSky_Controller))]
public class LightFalloff : MonoBehaviour 
{
    //public variables
    public static Color UnderWaterAbsorptionColor;
    public WaterProfile waterProfile;
    public WaterCamera waterCamera;

    //private variables
    private AzureSky_Controller sky;
    private PersonController personController;
    private float surfaceNormal;

    public Color originalIME;
    public Color bottomIMEColor;
    public Color currentIMEColor;
    private Gradient originalAmbientColor;
    private AnimationCurve originalSunIntensityCurve;
    private AnimationCurve originalMoonIntensityCurve;

	//Unity methods
	void Start () 
    {
        personController = GameObject.FindGameObjectWithTag("Player").GetComponent<PersonController>();
        sky = GetComponent<AzureSky_Controller>();

        //originalIME = waterProfile.UnderwaterAbsorptionColor;
        originalAmbientColor = new Gradient();
        originalAmbientColor.colorKeys = sky.AmbientColorGradient[0].colorKeys;
        originalSunIntensityCurve = new AnimationCurve(sky.SunDirLightIntensityCurve[0].keys);
        originalMoonIntensityCurve = new AnimationCurve(sky.MoonDirLightIntensityCurve[0].keys);
	}
	void Update () 
    {
        if (!personController.headAboveWater)
        {
            surfaceNormal = GetSurfaceNormal();

            //Set IME color
            currentIMEColor = UnderWaterAbsorptionColor;
            UnderWaterAbsorptionColor = new Color
            (
            bottomIMEColor.r + (originalIME.r - bottomIMEColor.r) * surfaceNormal,
            bottomIMEColor.g + (originalIME.g - bottomIMEColor.g) * surfaceNormal,
            bottomIMEColor.b + (originalIME.b - bottomIMEColor.b) * surfaceNormal,
            1f
            );

            //Set ambient gradient
            GradientColorKey[] colorKeys = new GradientColorKey[sky.AmbientColorGradient[0].colorKeys.Length];
            for (int i = 0; i < colorKeys.Length; i++)
            {
                GradientColorKey original = originalAmbientColor.colorKeys[i];
                colorKeys[i] = new GradientColorKey(original.color * surfaceNormal, original.time);
            }
            Gradient gradient = new Gradient();
            gradient.colorKeys = colorKeys;
            sky.AmbientColorGradient[0] = gradient;

            //Set sun intensity curve
            Keyframe[] sunKeys = new Keyframe[originalSunIntensityCurve.keys.Length];
            for (int i = 0; i < sunKeys.Length; i++)
            {
                Keyframe original = originalSunIntensityCurve.keys[i];
                sunKeys[i] = new Keyframe(original.time, original.value * surfaceNormal);
            }
            sky.SunDirLightIntensityCurve[0] = new AnimationCurve(sunKeys);

            //Set moon intensity curve
            Keyframe[] moonKeys = new Keyframe[originalMoonIntensityCurve.keys.Length];
            for (int i = 0; i < moonKeys.Length; i++)
            {
                Keyframe original = originalMoonIntensityCurve.keys[i];
                moonKeys[i] = new Keyframe(original.time, original.value * surfaceNormal);
            }
            sky.MoonDirLightIntensityCurve[0] = new AnimationCurve(sunKeys);
        }
	}


    //private methods
    private float GetSurfaceNormal()
    {
        float playerY = Mathf.Abs(personController.transform.position.y);
        float bottom = Mathf.Abs(GameManager.OceanBottom);

        float normal = 1 - (playerY / bottom);
        return Mathf.Clamp(normal, 0, 1f);
    }

}
