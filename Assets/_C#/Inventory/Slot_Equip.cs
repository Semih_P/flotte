﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipSlotType { None, Feet, Head}
public class Slot_Equip : Slot 
{
	//public variables

	//private variables
    private static List<Slot_Equip> allEquipSlots = new List<Slot_Equip>();
    private PlayerEquipment playerEquipment;

	//Unity methods
    protected override void Awake()
    {
        base.Awake();
        allEquipSlots.Add(this);
    }
    protected override void Start()
    {
        base.Start();
        if (playerEquipment == null)
            playerEquipment = ComponentManager<PlayerEquipment>.Get();
    }

	//public methods
    public override void SetItem(ItemInstance newInstance)
    {
        if (playerEquipment == null)
            playerEquipment = ComponentManager<PlayerEquipment>.Get();

        //Unquiped current item
        if (newInstance == null && itemInstance != null)
        {
            playerEquipment.UnEquip(itemInstance.baseItem);
        }
        //Equiped a new item
        else if (newInstance != null && itemInstance == null)
        {
            playerEquipment.Equip(newInstance.baseItem);
        }
        //Unequip and then equip new item
        else if (itemInstance != null && newInstance != null)
        {
            playerEquipment.UnEquip(itemInstance.baseItem);
            playerEquipment.Equip(newInstance.baseItem);
        }

        base.SetItem(newInstance);
    }
    public override void SetItem(Item_Base newItem, int amount)
    {
        if (playerEquipment == null)
            playerEquipment = ComponentManager<PlayerEquipment>.Get();

        //Unquiped current item
        if (newItem == null && itemInstance != null)
        {
            playerEquipment.UnEquip(itemInstance.baseItem);
        }
        //Equiped a new item
        else if (newItem != null && itemInstance == null)
        {
            playerEquipment.Equip(newItem);
        }
        //Unequip and then equip new item
        else if (itemInstance != null && newItem != null)
        {
            playerEquipment.UnEquip(itemInstance.baseItem);
            playerEquipment.Equip(newItem);
        }

        base.SetItem(newItem, amount);
    }
    public static Slot_Equip GetEquipSlotWithTag(EquipSlotType tag)
    {
        if (tag == EquipSlotType.None)
            return null;

        for (int i = 0; i < allEquipSlots.Count; i++)
        {
            Slot_Equip slot = allEquipSlots[i];
            if (slot == null || slot.IsEmpty())
                continue;

            if (slot.itemInstance.GetType() == ItemType.Equipable && slot.itemInstance.settings_equipment.GetEquipSlotType() == tag)
                return slot;
        }
        return null;
    }

	//private methods
}
