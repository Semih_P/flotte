﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour
 {
 	//public variables
    public ItemInstance itemInstance;

    public Image imageComponent;
    public Text textComponent;

    [HideInInspector]
    public RectTransform rectTransform;

 	//private variables
    protected Inventory inventory;
    private Sprite defaultSprite;

 	//Unity methods
    protected virtual void Awake()
    {
        defaultSprite = imageComponent.sprite;
        itemInstance = null;

        rectTransform = GetComponent<RectTransform>();
    }
    public void InitializeEventListeners(Inventory inventory)
    {
        EventTrigger trigger = GetComponentInParent<EventTrigger>();

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((eventData) => { inventory.MoveItem(this); });
        trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => { inventory.MoveItem(this); });
        trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback.AddListener((eventData) => { inventory.HoverEnter(this); });
        trigger.triggers.Add(entry);


        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerExit;
        entry.callback.AddListener((eventData) => { inventory.HoverExit(this); });
        trigger.triggers.Add(entry);
    }
	protected virtual void Start ()
	{
        RefreshComponents();
	}

	//public methods
    public virtual void SetItem(ItemInstance newInstance)
    {
        this.itemInstance = newInstance;
        RefreshComponents();
    }
    public virtual void SetItem(Item_Base newItem, int amount)
    {
        if (newItem != null)
            itemInstance = new ItemInstance(newItem, amount);
        else
            itemInstance = null;

        RefreshComponents();
    }
    public void AddItem(Item_Base newItem)
    {
        if (itemInstance == null)
        {
            itemInstance = new ItemInstance(newItem, 1);
        }
        else if (itemInstance.UniqueIndex == newItem.UniqueIndex)
        {
            itemInstance.amount++;
        }
        RefreshComponents();
    }
    public void RemoveItem(int amount)
    {
        if (IsEmpty())
            return;

        itemInstance.amount -= amount;

        if (itemInstance.amount <= 0)
        {
            Reset();
        }
        else
        {
            RefreshComponents();
        }
    }
    public bool IsEmpty()
    {
        return itemInstance == null;
    }
    public bool StackIsFull()
    {
        if (IsEmpty())
            return false;

        if (itemInstance.amount == itemInstance.settings_Inventory.GetStackSize())
            return true;
        else
            return false;
    }
    public void RefreshComponents()
    {
        if (imageComponent != null)
        {
            if (IsEmpty())
            {
                imageComponent.sprite = defaultSprite;
            }
            else
            {
                imageComponent.sprite = itemInstance.settings_Inventory.GetSprite();
            }

            imageComponent.enabled = (imageComponent.sprite != null);
        }
        if (!IsEmpty() && itemInstance.amount > 1)
            textComponent.text = itemInstance.amount.ToString();
        else
            textComponent.text = string.Empty;
    }

	//private methods
    private void Reset()
    {
        itemInstance = null;
        RefreshComponents();
    }
}
