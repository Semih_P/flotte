﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryPickupMenuItem : MonoBehaviour
 {
 	//public variables
    public Text amountTextComponent;
    public Text nameTextComponent;
    public Image imageComponent;
    [HideInInspector]
    public RectTransform rect;
    [HideInInspector]
    public int index;

 	//private variables
    private CanvasGroup canvasGroup;
    [SerializeField]
    private float fadeSpeed;
    private bool fade;
 	//Unity methods
	void Awake () 
	{
        rect = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
	}
	void Update () 
	{
        Vector3 pos = rect.transform.localPosition;
        pos.y = Mathf.Lerp(pos.y, index * rect.sizeDelta.y, Time.deltaTime * 4f);
        rect.transform.localPosition = pos;

        if (fade)
        {
            canvasGroup.alpha -= fadeSpeed * Time.deltaTime;
            if (canvasGroup.alpha <= 0)
            {
                gameObject.SetActive(false);
                rect.localPosition = new Vector3(0, -2 * rect.sizeDelta.y, 0);
            }
        }
	}


	//public methods
    public void SetItem(Item_Base item, int amount)
    {
        amountTextComponent.text = "+" + amount.ToString();
        nameTextComponent.text = item.settings_Inventory.GetDisplayName();
        imageComponent.sprite = item.settings_Inventory.GetSprite();

        canvasGroup.alpha = 1f;
        fade = false;

        Invoke("StartFade", 3.5f);
    }

	//private methods
    private void StartFade()
    {
        fade = true;
    }
}
