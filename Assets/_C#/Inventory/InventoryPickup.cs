﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryPickup : MonoBehaviour
 {
 	//public variables
    public InventoryPickupMenuItem menuItemPrefab;

 	//private variables
    [SerializeField]
    private int numberOfItems;
    private List<InventoryPickupMenuItem> items = new List<InventoryPickupMenuItem>();

 	//Unity methods
	void Start () 
	{
        CreateItems();
	}

	//public methods
    public void ShowItem(string uniqueItemName, int amount)
    {
        InventoryPickupMenuItem newItem = GetFirstItem();

        Item_Base item = ItemManager.GetItemByName(uniqueItemName);
        if (newItem == null || item == null)
            return;

        foreach(InventoryPickupMenuItem menuItem in items)
        {
            if (menuItem.gameObject.activeInHierarchy)
                menuItem.index++;
        }

        newItem.rect.localPosition = new Vector3(0, -2 * newItem.rect.sizeDelta.y, 0);
        newItem.gameObject.SetActive(true);
        newItem.SetItem(item, amount);
        newItem.index = 0;
    }


	//private methods
    private InventoryPickupMenuItem GetFirstItem()
    {
        for (int i = 0; i < items.Count; i++)
        {
            InventoryPickupMenuItem item = items[i];
            if (item.gameObject.activeInHierarchy)
                continue;

            return item;
        }

        if (items.Count > 0)
        {
            InventoryPickupMenuItem maxIndexItem = items[0];
            foreach (InventoryPickupMenuItem item in items)
            {
                if (item.index > maxIndexItem.index)
                {
                    maxIndexItem = item;
                }
            }

            return maxIndexItem;
        }
        else
            return null;
    }
    private void CreateItems()
    {
        for (int i = 0; i < numberOfItems + numberOfItems / 2; i++)
        {
            GameObject obj = Instantiate(menuItemPrefab.gameObject, transform) as GameObject;
            obj.transform.localScale = menuItemPrefab.transform.localScale;

            InventoryPickupMenuItem item = obj.GetComponent<InventoryPickupMenuItem>();

            item.rect.localPosition = new Vector3(0, -2 * item.rect.sizeDelta.y, 0);
            items.Add(item);

            obj.SetActive(false);
        }
    }
}
