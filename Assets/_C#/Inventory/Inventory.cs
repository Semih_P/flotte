﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Inventory : MonoBehaviour
 {
 	//public variables
    [Header("Inventory settings")]
    public int numberOfSlots;
    public int rows;
    public int slotSize;
    public float paddingLeft;
    public float paddingTop;


    [Header("Prefabs")]
    public GameObject slotPrefab;
    public PickupItem dropBarrel;

    [Header("Components")]
    public RectTransform hoverTransform;
    public RectTransform darkenedTransform;
    public Image draggingImage;
    public Transform slotParent;

    [HideInInspector]
    public List<Slot> allSlots;

    //protected variables
    protected RectTransform invRectTransform;

 	//private variables

    //static variables
    public static Transform Parent;
    private static Transform parent;
    private static Slot fromSlot;
    private static Slot toSlot;
    private static Slot hoverSlot;
    private static bool dragging;
    private static bool canQuickEquip = true;

 	//Unity methods
    protected virtual void Awake()
    {
        invRectTransform = GetComponent<RectTransform>();
        hoverTransform.sizeDelta = new Vector2(slotSize, slotSize);
        darkenedTransform.sizeDelta = new Vector2(slotSize, slotSize);
        draggingImage.rectTransform.sizeDelta = new Vector2(slotSize * 0.75f, slotSize * 0.75f);

        CreateSlots();
    }
    protected virtual void Start() 
	{
        Hide();
        CanvasHelper.Singleton.SubscribeToMenuClose(OnMenuClose);
	}
    protected virtual void Update()
    {
        dragging = (fromSlot != null && !fromSlot.IsEmpty());

        //Hover icon
        hoverTransform.gameObject.SetActive(hoverSlot != null);
        if (hoverTransform.gameObject.activeInHierarchy)
            hoverTransform.position = hoverSlot.rectTransform.position;

        //Darkened icon
        darkenedTransform.gameObject.SetActive(dragging);
        if (dragging)
        {
            if (hoverSlot == fromSlot)
            {
                hoverTransform.gameObject.SetActive(false);
            }

            darkenedTransform.position = fromSlot.rectTransform.position;
        }

        //Drag image
        draggingImage.gameObject.SetActive(dragging);
        if (dragging)
        {
            draggingImage.sprite = fromSlot.itemInstance.settings_Inventory.GetSprite();
            draggingImage.rectTransform.position = Input.mousePosition - new Vector3(draggingImage.rectTransform.sizeDelta.x / 4, -draggingImage.rectTransform.sizeDelta.y / 4);
        }

        //Quick equip items
        if (canQuickEquip && !dragging && hoverSlot != null)
        {
            int keyIndex = (int)KeyCode.Alpha1;
            for (int i = keyIndex; i < keyIndex + 8; i++)
            {
                KeyCode key = (KeyCode)keyIndex + (i - keyIndex);

                if (Input.GetKeyDown(key))
                {
                    int slotIndex = i - keyIndex;

                    Slot slot = ComponentManager<PlayerInventory>.Get().GetSlot(slotIndex);
                    fromSlot = hoverSlot;
                    toSlot = slot;

                    MoveItem(slot);
                    canQuickEquip = false;
                    Invoke("ResetQuickEquip", 0.05f);
                    break;
                }
            }
        }
    }

	//public methods
    public virtual void AddItem(string uniqueItemName, int amount)
    {
        Item_Base item = ItemManager.GetItemByName(uniqueItemName);
        if (item == null)
            return;

        Slot finalSlot = null;

        if (item.settings_Inventory.IsStackable())
        {
            int amountToAdd = amount;
            for (int i = 0; i < amount; i++ )
            {
                Slot slot = FindFirstStackableSlot(item.UniqueIndex);

                if (slot != null && !slot.StackIsFull())
                {
                    slot.AddItem(item);
                    amountToAdd--;

                    finalSlot = slot;
                }
                else
                {
                    slot = FindFirstEmptySlot();

                    if (slot != null)
                    {
                        slot.SetItem(item, amountToAdd);
                        finalSlot = slot;
                        break;
                    }
                    else
                    {
                        if (this is PlayerInventory)
                        {
                            ComponentManager<PlayerInventory>.Get().DropItem(item, amount);
                        }
                        return;
                    }
                }
            }
        }
        else
        {
            Slot slot = FindFirstEmptySlot();
            if (slot != null)
            {
                slot.SetItem(item, amount);
                finalSlot = slot;
            }
            else
            {
                if (this is PlayerInventory)
                {
                    ComponentManager<PlayerInventory>.Get().DropItem(item, amount);
                }
                return;
            }
        }

        if (ComponentManager<PlayerInventory>.Get().hotbar.IsSelectedHotSlot(finalSlot))
        {
            ComponentManager<PlayerInventory>.Get().hotbar.ReselectCurrentSlot();
        }

        if (finalSlot != null)
            ComponentManager<SoundManager>.Get().PlaySound("UIMoveItem");
    }
    public virtual void AddItem(string uniqueItemName, Slot slot, int amount)
    {
        Item_Base item = ItemManager.GetItemByName(uniqueItemName);
        if (item == null)
            return;

        Slot finalSlot = null;
        if (item.settings_Inventory.IsStackable())
        {
            for (int i = 0; i < amount; i++)
            {
                if (!slot.StackIsFull())
                {
                    slot.AddItem(item);
                    finalSlot = slot;
                }
                else
                {
                    slot = FindFirstEmptySlot();

                    if (slot != null)
                    {
                        slot.SetItem(item, amount);
                        finalSlot = slot;
                        break;
                    }
                    else
                    {
                        if (this is PlayerInventory)
                        {
                            ComponentManager<PlayerInventory>.Get().DropItem(item, amount);
                        }
                        return;
                    }
                }
            }
        }
        else
        {
            slot.SetItem(item, amount);
            finalSlot = slot;
        }

        if (ComponentManager<PlayerInventory>.Get().hotbar.IsSelectedHotSlot(finalSlot))
        {
            ComponentManager<PlayerInventory>.Get().hotbar.ReselectCurrentSlot();
        }

        if (finalSlot != null)
            ComponentManager<SoundManager>.Get().PlaySound("UIMoveItem");
    }

    public void RemoveItem(string uniqueItemName, int amount)
    {
        Item_Base item = ItemManager.GetItemByName(uniqueItemName);
        if (item == null)
            return;

        Slot finalSlot = null;
        int removedAmount = 0;
        foreach(Slot slot in allSlots)
        {
            if (slot.IsEmpty())
                continue;

            if (slot.itemInstance.UniqueIndex == item.UniqueIndex)
            {
                finalSlot = slot;

                if (slot.itemInstance.amount >= amount)
                {
                    slot.RemoveItem(amount);
                    removedAmount = amount;
                }
                else
                {
                    removedAmount += slot.itemInstance.amount;
                    slot.RemoveItem(slot.itemInstance.amount);
                }
            }

            if (removedAmount == amount)
            {
                break;
            }
        }

        if (ComponentManager<PlayerInventory>.Get().hotbar.IsSelectedHotSlot(finalSlot))
        {
            ComponentManager<PlayerInventory>.Get().hotbar.ReselectCurrentSlot();
        }
    }

    public void HoverEnter(Slot slot)
    {
        hoverSlot = slot;

        if (fromSlot != null && fromSlot != toSlot)
        {
            toSlot = slot;
        }

        ComponentManager<PlayerInventory>.Get().SetItemDescription(hoverSlot);
    }
    public void HoverExit(Slot slot)
    {
        if (fromSlot != null && toSlot == slot)
            toSlot = null;

        if (toSlot == null)
            hoverSlot = null;

        ComponentManager<PlayerInventory>.Get().SetItemDescription(null);
    }
    public void MoveItem(Slot slot)
    {
        //Grab start slot
        if (fromSlot == null)
        {
            fromSlot = slot;
            return;
        }
        //Released on nothing
        else if (toSlot == null)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                ComponentManager<PlayerInventory>.Get().DropItem(fromSlot);
                fromSlot.SetItem(null);
            }
            fromSlot = toSlot = null;
            return;
        }

        //Release on same slot as user started on
        if (fromSlot == toSlot)
        {
            fromSlot = toSlot = null;
            return;
        }
        //Switch place of items
        else if (fromSlot != null && toSlot != null)
        {
            //Abort slots are empty
            if (fromSlot.IsEmpty() || (fromSlot.IsEmpty() && toSlot.IsEmpty()))
            {
                fromSlot = toSlot = null;
                return;
            }

            //Switch to empty slot
            if (toSlot.IsEmpty())
            {
                bool toIsEquipSlot = toSlot is Slot_Equip;
                bool fromIsEquipSlot = fromSlot is Slot_Equip;

                //Switched to an empty normal slot
                if (!toIsEquipSlot || fromIsEquipSlot)
                {
                    ItemInstance tmpInstance = fromSlot.itemInstance.Clone();
                    fromSlot.SetItem(null, 0);
                    toSlot.SetItem(tmpInstance);

                    ComponentManager<SoundManager>.Get().PlaySound("UIMoveItem");
                }
                //Started on a normal slot and released on a equip slot
                else if (fromSlot.itemInstance.GetType() == ItemType.Equipable)
                {
                    Slot_Equip occupiedSlot = Slot_Equip.GetEquipSlotWithTag(fromSlot.itemInstance.settings_equipment.GetEquipSlotType());
                    if (occupiedSlot != null)
                    {
                        ItemInstance tmpItem = occupiedSlot.itemInstance.Clone();
                        occupiedSlot.SetItem(null);
                        toSlot.SetItem(fromSlot.itemInstance.Clone());
                        fromSlot.SetItem(tmpItem);
                    }
                    else
                    {
                        ItemInstance tmpItem = fromSlot.itemInstance.Clone();
                        fromSlot.SetItem(null);
                        toSlot.SetItem(tmpItem);
                    }
                    ComponentManager<SoundManager>.Get().PlaySound("UIMoveItem");
                }
                else
                {
                    fromSlot = toSlot = null;
                    return;
                }
            }
            //Change places of items
            else
            {
                //Stack on top
                if (fromSlot.itemInstance.UniqueIndex == toSlot.itemInstance.UniqueIndex && fromSlot.itemInstance.settings_Inventory.IsStackable())
                {
                    int fromAmount = fromSlot.itemInstance.amount;
                    int toAmount = toSlot.itemInstance.amount;
                    int maxAmount = fromSlot.itemInstance.settings_Inventory.GetStackSize();

                    int totalAmount = fromAmount + toAmount;
                    if (totalAmount > maxAmount)
                    {
                        toSlot.itemInstance.amount = maxAmount;
                        fromSlot.itemInstance.amount = totalAmount - maxAmount;
                    }
                    else
                    {
                        toSlot.itemInstance.amount = totalAmount;
                        fromSlot.SetItem(null);
                    }

                    toSlot.RefreshComponents();
                    fromSlot.RefreshComponents();
                    ComponentManager<SoundManager>.Get().PlaySound("UIMoveItem");
                }
                //Switch place
                else
                {
                    bool toSlotIsEquip = toSlot is Slot_Equip;
                    bool fromSlotIsEquip = fromSlot is Slot_Equip;
                    bool bothIsEquipableItems = fromSlot.itemInstance.GetType() == ItemType.Equipable && toSlot.itemInstance.GetType() == ItemType.Equipable;
                    if (!fromSlotIsEquip && !toSlotIsEquip)
                    {
                        ItemInstance tmpInstance = fromSlot.itemInstance.Clone();
                        fromSlot.SetItem(null);
                        fromSlot.SetItem(toSlot.itemInstance.Clone());
                        toSlot.SetItem(tmpInstance);

                        ComponentManager<SoundManager>.Get().PlaySound("UIMoveItem");
                    }
                    else if (bothIsEquipableItems)
                    {
                        if (!fromSlotIsEquip || !toSlotIsEquip)
                        {
                            Slot_Equip occupiedSlot = Slot_Equip.GetEquipSlotWithTag(fromSlot.itemInstance.settings_equipment.GetEquipSlotType());
                            if (occupiedSlot != null)
                            {
                                Slot emptySlot = FindFirstEmptySlot();
                                if (emptySlot != null)
                                    emptySlot.SetItem(occupiedSlot.itemInstance.Clone());
                                else
                                    AddItem(occupiedSlot.itemInstance.baseItem.UniqueName, 1);

                                occupiedSlot.SetItem(null, 0);
                            }
                        }

                        ItemInstance tmpItem1 = toSlot.itemInstance;
                        ItemInstance tmpItem2 = fromSlot.itemInstance;
                        toSlot.SetItem(null);
                        fromSlot.SetItem(null);

                        toSlot.SetItem(tmpItem2);
                        fromSlot.SetItem(tmpItem1);

                        ComponentManager<SoundManager>.Get().PlaySound("UIMoveItem");
                    }
                }
            }

            if (ComponentManager<PlayerInventory>.Get().hotbar.IsSelectedHotSlot(fromSlot) || ComponentManager<PlayerInventory>.Get().hotbar.IsSelectedHotSlot(toSlot))
            {
                ComponentManager<PlayerInventory>.Get().hotbar.ReselectCurrentSlot();
            }
            fromSlot = toSlot = null;
        }
    }

    public int GetItemCount(string uniqueItemName)
        {
        int count = 0;
        foreach(Slot slot in allSlots)
        {
            if (slot.IsEmpty())
                continue;

            if (slot.itemInstance.UniqueName == uniqueItemName)
            {
                count += slot.itemInstance.amount;
            }
        }
        return count;
    }

    public int GetSlotCount()
    {
        return allSlots.Count;
    }
    public Slot GetSlot(int index)
    {
        if (index >= 0 && index < allSlots.Count)
            return allSlots[index];
        else
            return null;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void SetLocalPosition(Vector3 position)
    {
        invRectTransform.localPosition = position;
    }

	//private methods
    protected virtual void CreateSlots()
    {
        allSlots = new List<Slot>();

        int maxSlotsPerRow = Mathf.CeilToInt((float)numberOfSlots / (float)rows);
        float invWidth = maxSlotsPerRow * slotSize + paddingLeft + paddingLeft * maxSlotsPerRow;
        float invHeight = rows * slotSize + paddingTop + paddingTop * rows;

        SetLocalPosition(new Vector3(-invWidth / 2, invRectTransform.localPosition.y));

        invRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, invWidth);
        invRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, invHeight);

        int slotsAdded = 0;
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < maxSlotsPerRow; x++)
            {
                GameObject slotObj = Instantiate(slotPrefab, slotParent) as GameObject;
                RectTransform slotRect = slotObj.GetComponent<RectTransform>();

                slotObj.name = "Slot: " + x + ", " + y;
                slotObj.transform.localScale = slotPrefab.transform.localScale;

                slotRect.localPosition = new Vector3(paddingLeft + x * slotSize + x * paddingLeft, -paddingTop + -y * slotSize - paddingTop * y);
                slotRect.sizeDelta = new Vector2(slotSize, slotSize);

                Slot newSlot = slotObj.GetComponent<Slot>();
                newSlot.InitializeEventListeners(this);
                allSlots.Add(newSlot);

                //Count slots
                slotsAdded++;
                if (slotsAdded == numberOfSlots)
                    break;
            }
            if (slotsAdded == numberOfSlots)
                break;
        }
    }
    protected virtual Slot FindFirstEmptySlot()
    {
        foreach(Slot slot in allSlots)
        {
            if (slot.IsEmpty())
                return slot;
        }
        return null;
    }
    private Slot FindFirstStackableSlot(int uniqueItemIndex)
    {
        foreach (Slot slot in allSlots)
        {
            if (!slot.IsEmpty() && slot.itemInstance.UniqueIndex == uniqueItemIndex && !slot.StackIsFull())
                return slot;
        }
        return null;
    }
    private void ResetQuickEquip()
    {
        canQuickEquip = true;
    }

    private void OnMenuClose()
    {
        toSlot = null;
        fromSlot = null;
        hoverSlot = null;
    }
}
