﻿using UnityEngine;
using System.Collections;

public class Hotbar : MonoBehaviour
 {
 	//public variables
    public RectTransform hotbarSelectionTransform;
    public RectTransform hotslotBackground;
    public RectTransform hotslotParent;

 	//private variables
    private PlayerItemManager playerItemManager;
    [HideInInspector] public Network_Player playerNetwork;
    private PlayerInventory playerInventory;
    private int slotIndex = 0;

 	//Unity methods
    public void Initialize(PlayerItemManager playerItemManager, Network_Player playerNetwork)
    {
        this.playerInventory = ComponentManager<PlayerInventory>.Get();
        this.playerItemManager = playerItemManager;
        this.playerNetwork = playerNetwork;
        float size = ComponentManager<PlayerInventory>.Get().slotSize;
        hotbarSelectionTransform.sizeDelta = new Vector2(size, size);
    }
	void Update () 
	{
        if (!playerNetwork.IsLocalPlayer)
            return;

        HandleHotbarSelection();
	}

	//public methods
    public void SelectHotslot(Slot slot)
    {
        if (!playerNetwork.IsLocalPlayer)
            return;

        if (slot != null)
            hotbarSelectionTransform.localPosition = slot.rectTransform.localPosition;

        if (slot == null || slot.IsEmpty())
        {
            SelectItem(null);
            return;
        }


        SelectItem(slot.itemInstance.baseItem);
    }
    public void ReselectCurrentSlot()
    {
        if (!playerNetwork.IsLocalPlayer)
            return;
        Slot slot = playerInventory.GetSlot(slotIndex);

        SelectHotslot(slot);
    }
    public int GetSelectedSlotIndex()
    {
        return slotIndex;
    }
    public void SetSelectedSlotIndex(int index)
    {
        if (!playerNetwork.IsLocalPlayer)
            return;

        slotIndex = index;
    }
    public bool IsSelectedHotSlot(Slot slot)
    {
        if (slot == null)
            return false;

        return playerInventory.GetSlot(slotIndex) == slot;
    }

	//private methods
    private void HandleHotbarSelection()
    {
        if (GameManager.IsInMenu || playerItemManager == null || Player.IsDead)
            return;

        if (!playerItemManager.CanSwitch())
            return;

        int keyIndex = (int)KeyCode.Alpha1;

        for (int i = keyIndex; i < keyIndex + 8; i++)
        {
            KeyCode key = (KeyCode)keyIndex + (i - keyIndex);

            if (Input.GetKeyDown(key))
            {
                slotIndex = i - keyIndex;

                Slot slot = playerInventory.GetSlot(slotIndex);
                SelectHotslot(slot);
            }
        }

        float wheel = Input.GetAxis("Mouse ScrollWheel");
        if (wheel > 0)
            slotIndex--;
        else if (wheel < 0)
            slotIndex++;

        if (wheel > 0 || wheel < 0)
        {
            int slotCount = playerInventory.hotslotCount;

            if (slotIndex > slotCount - 1)
                slotIndex = 0;
            else if (slotIndex < 0)
                slotIndex = slotCount - 1;

            Slot newSlot = playerInventory.GetSlot(slotIndex);
            SelectHotslot(newSlot);
        }
    }
    private void SelectItem(Item_Base item)
    {
        if (playerItemManager == null)
            return;

        if (item == null || !item.settings_usable.IsUsable())
        {
            playerItemManager.SelectUsable(null);
        }
        else
        {
            playerItemManager.SelectUsable(item);
        }
    }
}
