﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : Inventory
{
    //properties

    //public variables
    [Header("Hotbar settings")]
    [HideInInspector] public Hotbar hotbar;
    public int hotslotCount;
    public float hotbarDistanceFromBottom;

    [Header("Equipment settings")]
    [SerializeField] private int equipslotCount = 0;
    [SerializeField] private Slot_Equip equipslotPrefab = null;
    [SerializeField] private Transform equipslotParent = null;


    [Header("Item description")]
    public Text itemNameText;
    public Text itemDescriptionText;
    public Image itemImage;

    //private variables
    private InventoryPickup inventoryPickup;

    //network

    //Unity methods
    protected override void Awake()
    {
        inventoryPickup = GameObject.FindObjectOfType<InventoryPickup>();
        invRectTransform = GetComponent<RectTransform>();
        hoverTransform.sizeDelta = new Vector2(slotSize, slotSize);
        darkenedTransform.sizeDelta = new Vector2(slotSize, slotSize);
        draggingImage.rectTransform.sizeDelta = new Vector2(slotSize * 0.75f, slotSize * 0.75f);

        itemNameText.text = itemDescriptionText.text = string.Empty;
        itemImage.enabled = false;
    }
    public void Initialize(Hotbar hotbar)
    {
        this.hotbar = hotbar;
        CreateSlots();
    }
    protected override void Start()
    {
        base.Start();
        if (GameManager.IsInNewGame)
        {
            AddItem("Hook_Plastic", 1);
            //AddItem("Hammer", 1);
            //AddItem("Spear_Scrap", 1);
            //AddItem("Axe", 1);
            AddItem("Placeable_Brick_Wet", 10);
            //AddItem("Plank", 500);
            //AddItem("Plastic", 500);
            //AddItem("Nail", 500);
            //AddItem("Thatch", 500);
            //AddItem("Rope", 500);
        }
    }

    //Override methods
    protected override void CreateSlots()
    {
        base.CreateSlots();

        //Create hotslots
        float totalWidth = hotslotCount * slotSize + paddingLeft * hotslotCount + paddingLeft * 2;
        float totalHeight = slotSize + paddingTop * 2;
        hotbar.hotslotBackground.sizeDelta = new Vector2(totalWidth, totalHeight);
        hotbar.hotslotBackground.localPosition = new Vector3(0, totalHeight / 2 - hotbarDistanceFromBottom);

        List<Slot> hotSlots = new List<Slot>();
        for (int i = 0; i < hotslotCount; i++)
        {
            GameObject slotObj = Instantiate(slotPrefab, hotbar.hotslotParent) as GameObject;
            RectTransform slotRect = slotObj.GetComponent<RectTransform>();

            slotObj.name = "HotSlot: " + i;
            slotObj.transform.localScale = slotPrefab.transform.localScale;

            float startX = -totalWidth / 2;
            float y = hotbar.hotslotParent.position.y + slotSize + hotbarDistanceFromBottom + paddingTop;
            slotRect.localPosition = new Vector3(startX + i * slotSize + paddingLeft * i + paddingLeft, y);
            slotRect.sizeDelta = new Vector2(slotSize, slotSize);

            Slot hotSlot = slotObj.GetComponent<Slot>();
            hotSlot.InitializeEventListeners(this);
            hotSlots.Add(hotSlot);
        }
        for (int i = hotSlots.Count - 1; i >= 0; i--)
        {
            allSlots.Insert(0, hotSlots[i]);
        }
        Slot_Equip[] equipSlots = equipslotParent.GetComponentsInChildren<Slot_Equip>();
        for (int i = 0; i < equipSlots.Length; i++)
        {
            Slot_Equip equipSlot = equipSlots[i];

            equipSlot.InitializeEventListeners(this);
            equipSlot.name = "EquipSlot: " + i;
        }
    }
    public override void AddItem(string uniqueItemName, int amount)
    {
        base.AddItem(uniqueItemName, amount);
        inventoryPickup.ShowItem(uniqueItemName, amount);
    }
    public override void AddItem(string uniqueItemName, Slot slot, int amount)
    {
        base.AddItem(uniqueItemName, slot, amount);
        inventoryPickup.ShowItem(uniqueItemName, amount);
    }
    public void AddItem(ItemInstance itemInstance)
    {
        if (itemInstance == null)
            return;

        Slot slot = FindFirstEmptySlot();
        if (slot != null)
        {
            slot.SetItem(itemInstance.Clone());
        }
        else
        {
            if (this is PlayerInventory)
            {
                DropItem(itemInstance.Clone());
            }
        }

        if (slot != null && hotbar.IsSelectedHotSlot(slot))
        {
            hotbar.ReselectCurrentSlot();
        }

        if (slot != null)
            ComponentManager<SoundManager>.Get().PlaySound("UIMoveItem");

        inventoryPickup.ShowItem(itemInstance.UniqueName, itemInstance.amount);
    }
    protected override Slot FindFirstEmptySlot()
    {
        for (int i = 0; i < allSlots.Count; i++)
        {
            if (allSlots[i].IsEmpty())
                return allSlots[i];
        }
        return null;
    } 

    //public methods
    public Slot GetSelectedHotbarSlot()
    {
        return GetSlot(hotbar.GetSelectedSlotIndex());
    }
    public ItemInstance GetSelectedHotbarItem()
    {
        return GetSlot(hotbar.GetSelectedSlotIndex()).itemInstance;
    }
    public void SetItemDescription(Slot slot)
    {
        if (slot != null && !slot.IsEmpty())
        {
            ItemInstance item = slot.itemInstance;

            itemNameText.text = item.settings_Inventory.GetDisplayName();
            itemDescriptionText.text = item.settings_Inventory.GetDescription();
            itemImage.enabled = true;
            itemImage.sprite = item.settings_Inventory.GetSprite();
        }
        else if (slot == null)
        {
            itemNameText.text = itemDescriptionText.text = string.Empty;
            itemImage.enabled = false;
        }
    }
    public void RemoveSelectedHotSlotItem(int amount)
    {
        Slot selectedSlot = GetSlot(hotbar.GetSelectedSlotIndex());
        if (selectedSlot == null || selectedSlot.IsEmpty())
        {
            return;
        }

        if (selectedSlot.itemInstance.amount >= amount)
        {
            selectedSlot.RemoveItem(amount);
        }
        else
        {
            selectedSlot.RemoveItem(selectedSlot.itemInstance.amount);
        }

        hotbar.ReselectCurrentSlot();
        selectedSlot.RefreshComponents();
    }
    public void DropItem(Slot slot)
    {
        if (slot == null || slot.IsEmpty())
            return;


        Helper.DropItem(slot.itemInstance, hotbar.playerNetwork.transform.position, hotbar.playerNetwork.CameraTransform.forward);

        slot.SetItem(null, 0);
        if (hotbar.IsSelectedHotSlot(slot))
        {
            hotbar.ReselectCurrentSlot();
        }
    }
    public void DropItem(Item_Base item, int amount)
    {
        Helper.DropItem(new ItemInstance(item, amount), hotbar.playerNetwork.transform.position, hotbar.playerNetwork.CameraTransform.forward);
    }
    public void DropItem(ItemInstance instance)
    {
        Helper.DropItem(instance, hotbar.playerNetwork.transform.position, hotbar.playerNetwork.CameraTransform.forward);
    }

    //private methods
}
