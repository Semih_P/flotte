﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;

public class PrefabImportLog : AssetPostprocessor
{
    public static void OnPostprocessAllAssets( string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths )
    {
        var importedPrefabs = new StringBuilder();

        foreach( string path in importedAssets )
        {
            if( !path.EndsWith( ".prefab", System.StringComparison.OrdinalIgnoreCase ) )
                continue;

            importedPrefabs.Append( "Imported: " );
            importedPrefabs.AppendLine( path );
        }

        foreach( string path in deletedAssets )
        {
            if( !path.EndsWith( ".prefab", System.StringComparison.OrdinalIgnoreCase ) )
                continue;

            importedPrefabs.Append( "Deleted: " );
            importedPrefabs.AppendLine( path );
        }

        foreach( string path in movedAssets )
        {
            if( !path.EndsWith( ".prefab", System.StringComparison.OrdinalIgnoreCase ) )
                continue;

            importedPrefabs.Append( "Moved: " );
            importedPrefabs.AppendLine( path );
        }

        string rawr = importedPrefabs.ToString();
        if (!string.IsNullOrEmpty(rawr))
            Debug.Log( importedPrefabs.ToString() );
    }
}
#endif
