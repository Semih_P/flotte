﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNetworkManager : MonoBehaviour_Network 
{
	//public variables

    //private variables
    #region Brick
    public static List<Brick_Wet> allBricks = new List<Brick_Wet>();
    #endregion
    //network variables
    private Network_Player playerNetwork;
    private PlayerInventory playerInventory;
    private PlayerAnimator playerAnimator;

	//Unity methods
	void Start () 
	{
        playerInventory = ComponentManager<PlayerInventory>.Get();
        playerNetwork = GetComponentInParent<Network_Player>();
        playerAnimator = playerNetwork.Animator;
	}

	//public methods
#region Brick
    public void PickupWetBrick(Brick_Wet brick, bool useNetwork)
    {
        if (useNetwork)
            playerNetwork.Network.RPC(new Message_Brick_Pickup(Messages.Brick_Pickup, this, brick.ObjectIndex), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

        playerAnimator.SetAnimation(PlayerAnimation.Trigger_GrabItem);

        if (playerNetwork.IsLocalPlayer)
            playerInventory.AddItem("Brick_Dry", 1);

        allBricks.Remove(brick);
        Destroy(brick.gameObject);
    }
    public Brick_Wet GetBrickByObjectIndex(uint objectIndex)
    {
        return allBricks.Find(b => b.ObjectIndex == objectIndex);
    }
#endregion
	//private methods

    //network methods
    public override void Serialize_Create()
    {
        AddMessage(new Message_PlayerNetworkManager_Create(Messages.PlayerNetworkManager_Create, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.PlayerNetworkManager_Create:
                Message_PlayerNetworkManager_Create msgCreate = msg as Message_PlayerNetworkManager_Create;
                msgCreate.Restore(this, msgCreate);
                break;
            case Messages.Brick_Pickup:
                Message_Brick_Pickup msgBrick = msg as Message_Brick_Pickup;
                PickupWetBrick(GetBrickByObjectIndex(msgBrick.brickObjectIndex), playerNetwork.Network.IsHost);
                break;
        }
    }
}

[System.Serializable]
public class Message_PlayerNetworkManager_Create : Message_NetworkBehaviour
{
    public Message_PlayerNetworkManager_Create(Messages type, MonoBehaviour_Network behaviour) : base(type, behaviour)
    {
        AddBricks();
    }
    public void Restore(PlayerNetworkManager manager, Message_PlayerNetworkManager_Create msgCreate)
    {
        RestoreBricks(manager, msgCreate);
    }

    #region Brick
    public RGD_Brick[] rgdBricks;
    public void AddBricks()
    {
        rgdBricks = new RGD_Brick[PlayerNetworkManager.allBricks.Count];
        for(int i = 0; i < PlayerNetworkManager.allBricks.Count; i++)
        {
            rgdBricks[i] = new RGD_Brick(PlayerNetworkManager.allBricks[i]);
        }
    }
    public void RestoreBricks(PlayerNetworkManager manager, Message_PlayerNetworkManager_Create msg)
    {
        foreach(RGD_Brick rgdBrick in rgdBricks)
        {
            rgdBrick.Restore(manager.GetBrickByObjectIndex(rgdBrick.brickObjectIndex));
        }
    }
    #endregion

}
[System.Serializable]
public class Message_Brick_Pickup : Message_NetworkBehaviour
{
    public uint brickObjectIndex;
    public Message_Brick_Pickup(Messages type, MonoBehaviour_Network behaviour, uint brickObjectIndex)
        :base(type, behaviour)
    {
        this.brickObjectIndex = brickObjectIndex;
    }
}
