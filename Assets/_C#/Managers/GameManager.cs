﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;

public class GameManager : SingletonGeneric<GameManager>
{
    
 	//public variables
    public static float OceanBottom = -40f;
    public static string VersionNumber = "Version 1.05";
    public static bool IsInNewGame;
    public static bool IsInMenu;
    public static bool IsInBuildMenu;
    public static string CurrentGameFileName;

    public AzureSky_Controller skyController;
    public Texture2D cursorTexture;
    public Transform globalBuoyancyParent;
    [SerializeField] private DeathMenu deathMenu = null;

 	//private variables
    private bool gameOver;

 	//Unity methods
    void Awake()
    {
        GameManager.IsInNewGame = true;
        GameManager.CurrentGameFileName = "ForcedEditorNewGame.rgd";

        Application.targetFrameRate = 60;
        ItemManager.Initialize();
        Inventory.Parent = GameObject.FindWithTag("InventoryParent").transform;

        skyController = GameObject.FindObjectOfType<AzureSky_Controller>();
        IsInMenu = false;
    }
    void Start()
    {
        Helper.Start();

        //Set for how long screen is black
        float loadingScreenTime = 2.5f;
        #if UNITY_EDITOR
        loadingScreenTime = 0.2f;
        #endif

        Invoke("DelayedStart", loadingScreenTime);
    }
    void DelayedStart()
    {
        if (!IsInNewGame)
        {
            SaveAndLoad.Singleton.Load();
        }
    }
    void Update()
    {
        Helper.Update();
        Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
    }

	//public methods
    public void GameOver()
    {
        if (gameOver)
            return;
        gameOver = true;

        deathMenu.Show();
    }
    public void Respawn()
    {
        
    }
	//private methods
}
