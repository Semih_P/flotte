﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
 {
 	//public variables

 	//private variables
    private Dictionary<string, AudioSource> audioConnections = new Dictionary<string, AudioSource>();
    private static bool initialized;

 	//Unity methods
    void Awake()
    {
        if (initialized)
        {
            Destroy(gameObject);
            return;
        }
        initialized = true;
        DontDestroyOnLoad(gameObject);
        ComponentManager<SoundManager>.Set(this);

        if (audioConnections.Count == 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                audioConnections.Add(child.name, child.GetComponent<AudioSource>());
            }
        }
    }

	//public methods
    public AudioSource PlaySound(string nameOfSound, float minPitch = 0.8f, float maxPitch = 1.2f)
    {
        if (!audioConnections.ContainsKey(nameOfSound))
            return null;

        AudioSource source = audioConnections[nameOfSound];
        source.transform.position = Vector3.zero;
        source.pitch = Random.Range(minPitch, maxPitch);
        source.Play();

        return source;
    }
    public AudioSource PlaySound(string nameOfSound, Vector3 position, float minPitch = 0.8f, float maxPitch = 1.2f)
    {
        if (!audioConnections.ContainsKey(nameOfSound))
            return null;

        AudioSource source = audioConnections[nameOfSound];
        source.transform.position = position;
        source.pitch = Random.Range(minPitch, maxPitch);
        source.Play();

        return source;
    }
    public AudioSource PlaySoundCopy(string nameOfSound, Vector3 position, bool destroyWhenComplete, float minPitch = 0.8f, float maxPitch = 1.2f)
    {
        if (!audioConnections.ContainsKey(nameOfSound))
            return null;

        AudioSource source = audioConnections[nameOfSound];
        GameObject objSound = new GameObject("SoundCopy - " + nameOfSound);
        AudioSource newSource = objSound.AddComponent<AudioSource>();

        CopyAudioSource(source, newSource);

        objSound.transform.position = position;
        newSource.pitch = Random.Range(minPitch, maxPitch);
        newSource.Play();

        if (destroyWhenComplete)
            Destroy(objSound, newSource.clip.length);

        return newSource;
    }

	//private methods
    private void CopyAudioSource(AudioSource original, AudioSource target)
    {
        target.clip = original.clip;
        target.volume = original.volume;
        target.loop = original.loop;
        target.spatialBlend = original.spatialBlend;
        target.outputAudioMixerGroup = original.outputAudioMixerGroup;
    }
}
