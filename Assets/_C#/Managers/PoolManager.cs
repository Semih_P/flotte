﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour
{
    //public variables
    public static Transform Transform;
    public List<ObjectPool> objectPools = new List<ObjectPool>();

    //private variables

    //Unity methods
    void Awake()
    {
        ComponentManager<PoolManager>.Set(this);

        Transform = this.transform;
        foreach(ObjectPool obj in objectPools)
        {
            obj.Initialize();
        }
    }

    //public methods
    public GameObject GetObject(string objectName, bool returnObjectActive = true)
    {
        List<ObjectPool> availablePools = new List<ObjectPool>();
        for (int i = 0; i < objectPools.Count; i++)
        {
            ObjectPool pool = objectPools[i];
            if (pool.name == objectName)
            {
                availablePools.Add(pool);
            }
        }
        if (availablePools.Count > 0)
        {
            return availablePools[Random.Range(0, availablePools.Count)].GetObject(returnObjectActive);
        }
        else
        {
            Debug.Log("Tried to access pool with name [" + objectName + "] from pool manager but there was no pool with that name");
            return null;
        }
    }
    public static void ReturnObject(GameObject obj)
    {
        if (obj.transform.parent != Transform)
            obj.transform.SetParent(Transform);

        obj.SetActive(false);
    }

    //private methods
}
