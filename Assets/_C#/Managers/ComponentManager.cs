﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentManager<T> : MonoBehaviour 
{
    //private variables
    private static T component;

    public static T Get()
    {
        return component;
    }
    public static void Set(T comp)
    {
        component = comp;
    }
}
