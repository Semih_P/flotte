﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour 
{
	//public variables

	//private variables
    private static PoolManager poolManager;

	//Unity methods
    void Start()
    {
        poolManager = ComponentManager<PoolManager>.Get();
    }

	//public methods
    public static GameObject PlaySystem(string poolName, Vector3 position, bool activateSystem = true)
    {
        GameObject system = poolManager.GetObject(poolName, false);

        //Set position of system
        system.transform.position = position;

        //Play system by activating the gameobject
        if (activateSystem)
            system.SetActive(true);

        return system;
    }
    public static GameObject PlaySystem(string poolName, Vector3 position, Quaternion rotation)
    {
        GameObject system = PlaySystem(poolName, position, false);

        //Set rotation of system
        system.transform.rotation = rotation;

        //Play system by activating the gameobject
        system.SetActive(true);

        return system;

    }

	//private methods

}
