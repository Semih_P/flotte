﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CostCollection : MonoBehaviour 
{
	//public variables

	//private variables
    [SerializeField] private bool useInventoryAsAmount = true;
    [SerializeField] private BuildingUI_CostBox costBoxPrefab;
    [SerializeField] private float prefabScaleMultiplier = 1f;
    [SerializeField] private List<BuildingUI_CostBox> costBoxes = new List<BuildingUI_CostBox>();
    private PlayerInventory inventory;

	//Unity methods
    void Start()
    {
        inventory = ComponentManager<PlayerInventory>.Get();
    }

	//public methods
    public bool MeetsRequirements()
    {
        foreach(BuildingUI_CostBox box in costBoxes)
        {
            if (!box.MeetsRequirements())
                return false;
        }
        return true;
    }
    public bool RequiresItem(Item_Base item)
    {
        if (item == null)
            return false;

        foreach (BuildingUI_CostBox box in costBoxes)
        {
            if (box.RequiresItem(item))
            {
                return !box.MeetsRequirements();
            }
        }
        return false;
    }
    public void ShowCost(List<Cost> cost)
    {
        if (cost.Count > costBoxes.Count)
            SetNewLength(cost.Count);

        for (int i = 0; i < costBoxes.Count; i++)
        {
            if (i < cost.Count)
            {
                BuildingUI_CostBox box = costBoxes[i];
                box.gameObject.SetActive(true);
                box.SetRequiredItem(cost[i].item);
                box.SetRequiredAmount(cost[i].amount);

                if (useInventoryAsAmount)
                {
                    if (inventory == null)
                        inventory = ComponentManager<PlayerInventory>.Get();
                    box.SetAmountInInventory(inventory);
                }
            }
            else
            {
                costBoxes[i].gameObject.SetActive(false);
            }
        }
    }
    public void IncrementItem(string uniqueName, int amount)
    {
        foreach(BuildingUI_CostBox box in costBoxes)
        {
            if (box.RequiresItem(uniqueName))
            {
                box.IncrementAmount(amount);
            }
        }
    }
    public void SetAmount(string uniqueName, int amount)
    {
        foreach (BuildingUI_CostBox box in costBoxes)
        {
            if (box.RequiresItem(uniqueName))
            {
                box.SetAmount(amount);
            }
        }
    }
    public void SetRequiredAmount(string uniqueName, int requiredAmount)
    {
        foreach(BuildingUI_CostBox box in costBoxes)
        {
            if (box.RequiresItem(uniqueName))
            {
                box.SetRequiredAmount(requiredAmount);
            }
        }
    }
    public void SetRadial(string uniqueName, float normal)
    {
        foreach (BuildingUI_CostBox box in costBoxes)
        {
            if (box.RequiresItem(uniqueName))
            {
                box.SetRadial(normal);
            }
        }
    }
    public void SetRadial(string uniqueName, bool state)
    {
        foreach (BuildingUI_CostBox box in costBoxes)
        {
            if (box.RequiresItem(uniqueName))
            {
                box.SetRadial(state);
            }
        }
    }
	//private methods
    private void SetNewLength(int newLength)
    {
        while(costBoxes.Count != newLength)
        {
            BuildingUI_CostBox box = Instantiate(costBoxPrefab, transform) as BuildingUI_CostBox;
            box.transform.localScale = costBoxPrefab.transform.localScale * prefabScaleMultiplier;
            costBoxes.Add(box);
        }
    }
}
