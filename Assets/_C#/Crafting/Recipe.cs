﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class Cost
{
    public Item_Base item;
    public int amount;

    public Cost()
    {

    }
    public Cost(Item_Base item, int amount)
    {
        this.item = item;
        this.amount = amount;
    }
}
[CreateAssetMenu(fileName = "Recipe", menuName = "Crafting/Recipe", order = 1)]
public class Recipe : ScriptableObject
 {
 	//public variables
    public CraftingCategory craftingCategory;
    public List<Cost> cost = new List<Cost>();
    public Cost itemToCraft;

 	//private variables


 	//Unity methods
#if UNITY_EDITOR
    void OnValidate()
    {
        if (itemToCraft.amount <= 0)
            itemToCraft.amount = 1;
    }
#endif


	//public methods


	//private methods
}
