﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemInstance_Cookable
{
    [SerializeField] private float cookTime = 1f;
    [SerializeField] private Item_Base cookingResult = null;

    public ItemInstance_Cookable(float cookTime, Item_Base cookingResult)
    {
        this.cookTime = cookTime;
        this.cookingResult = cookingResult;
    }
    public ItemInstance_Cookable Clone()
    {
        return (ItemInstance_Cookable)MemberwiseClone();
    }
    public float GetCookTime()
    {
        return cookTime;
    }
    public Item_Base GetCookingResult()
    {
        return cookingResult;
    }
}

public enum Fluid { None, SaltWater, Water }
[System.Serializable]
public class ItemInstance_Consumeable
{
    public Fluid FluidType { get { return fluid; } }
    [SerializeField] private Fluid fluid = Fluid.None;
    [SerializeField] private int uses = 1;
    [SerializeField] private int maxUses = 0;
    [SerializeField] private float hungerYield = 0f;
    [SerializeField] private float thirstYield = 0f;
    [SerializeField] private bool isRaw = true;
    [SerializeField] private Cost itemAfterUse = null;

    public ItemInstance_Consumeable(int uses, float hungerYield, float thirstYield, bool isRaw, Cost itemAfterUse)
    {
        this.uses = uses;
        this.maxUses = uses;
        this.hungerYield = hungerYield;
        this.thirstYield = thirstYield;
        this.isRaw = isRaw;
        this.itemAfterUse = itemAfterUse;
    }
    public ItemInstance_Consumeable Clone()
    {
        return (ItemInstance_Consumeable)MemberwiseClone();
    }
    public Cost GetItemAfterUse()
    {
        return itemAfterUse;
    }
    public float GetHungerYield()
    {
        return hungerYield;
    }
    public float GetThirstYield()
    {
        return thirstYield;
    }
    public bool IsRaw()
    {
        return isRaw;
    }
    public int GetUses()
    {
        return uses;
    }
    public int GetMaxUses()
    {
        return maxUses;
    }
    public bool HasMaxUses()
    {
        return uses == maxUses;
    }
    public void SetUses(int newValue)
    {
        uses = newValue;
    }
    public void IncrementUses(int amount)
    {
        uses += amount;
    }
}

[System.Serializable]
public class ItemInstance_Usable
{
    [SerializeField] private bool isUsable = false;
    [SerializeField] private bool allowHoldButton = false;
    [SerializeField] private string useButtonName = "LeftClick";
    [SerializeField] private float useButtonCooldown = 0.2f;

    public ItemInstance_Usable(string useButtonName, float useButtonCooldown, bool isUsable, bool allowHoldButton)
    {
        this.useButtonName = useButtonName;
        this.useButtonCooldown = useButtonCooldown;
        this.isUsable = isUsable;
        this.allowHoldButton = allowHoldButton;
    }
    public ItemInstance_Usable Clone()
    {
        return (ItemInstance_Usable)MemberwiseClone();
    }
    public string GetUseButtonName()
    {
        return useButtonName;
    }
    public float GetUseButtonCooldown()
    {
        return useButtonCooldown;
    }
    public bool IsUsable()
    {
        return isUsable;
    }
    public bool AllowHoldButton()
    {
        return allowHoldButton;
    }
}

[System.Serializable]
public class ItemInstance_Inventory
{
    [SerializeField] private Sprite sprite = null;
    [SerializeField] private string displayName = "An item";
    [SerializeField] private string description = "A description of an item";
    [SerializeField] private int stackSize = 1;
    [SerializeField] private bool gripAnimation = false;

    public ItemInstance_Inventory(Sprite sprite, string displayName, string description, int stackSize, bool gripAnimation)
    {
        this.sprite = sprite;
        this.displayName = displayName;
        this.description = description;
        this.stackSize = stackSize;
        this.gripAnimation = gripAnimation;
    }
    public ItemInstance_Inventory Clone()
    {
        return (ItemInstance_Inventory)MemberwiseClone();
    }
    public Sprite GetSprite()
    {
        return sprite;
    }
    public bool IsStackable()
    {
        return stackSize > 1;
    }
    public int GetStackSize()
    {
        return stackSize;
    }
    public string GetDisplayName()
    {
        return displayName;
    }
    public string GetDescription()
    {
        return description;
    }
    public bool SetAnimationToGrip()
    {
        return gripAnimation;
    }
}

[System.Serializable]
public class ItemInstance_Buildable
{
    [SerializeField] private Block blockPrefab = null;

    public int RequiredStabilityCount { get { return requiredStabilityCount; } private set { requiredStabilityCount = value; } }
    [SerializeField] private int requiredStabilityCount = 0;

    public ItemInstance_Buildable(Block blockPrefab, int requriedStabilityCount)
    {
        this.blockPrefab = blockPrefab;
        this.RequiredStabilityCount = requriedStabilityCount;
    }
    public ItemInstance_Buildable Clone()
    {
        return (ItemInstance_Buildable)MemberwiseClone();
    }
    public Block GetBlockPrefab()
    {
        return blockPrefab;
    }
}

 [System.Serializable]
public class ItemInstance_Equipment
{
     [SerializeField] private EquipSlotType slotType = EquipSlotType.None;

     public ItemInstance_Equipment(EquipSlotType slotType)
     {
         this.slotType = slotType;
     }
     public ItemInstance_Equipment Clone()
     {
         return (ItemInstance_Equipment)MemberwiseClone();
     }
     public EquipSlotType GetEquipSlotType()
     {
         return slotType;
     }
}

[System.Serializable]
public class ItemInstance 
{
    public int amount;
    public Item_Base baseItem;

    //ITEM_BASE
    private ItemType type;

    public string UniqueName { get { return uniqueName; } private set { uniqueName = value; } }
    private string uniqueName;

    public int UniqueIndex { get { return uniqueIndex; } private set { uniqueIndex = value; } }
    private int uniqueIndex;

    //Settings
    public ItemInstance_Cookable settings_cookable;
    public ItemInstance_Consumeable settings_consumeable;
    public ItemInstance_Usable settings_usable;
    public ItemInstance_Inventory settings_Inventory;
    public ItemInstance_Buildable settings_buildable;
    public ItemInstance_Equipment settings_equipment;

	//CTOR
    public ItemInstance()
    {

    }
    public ItemInstance(Item_Base itemBase, int amount)
    {
        this.amount = amount;
        this.baseItem = itemBase;

        //ITEM_BASE
        this.type = itemBase.GetType();
        this.uniqueIndex = itemBase.UniqueIndex;
        this.uniqueName = itemBase.UniqueName;

        settings_cookable = itemBase.settings_cookable.Clone();
        settings_consumeable = itemBase.settings_consumeable.Clone();
        settings_usable = itemBase.settings_usable.Clone();
        settings_Inventory = itemBase.settings_Inventory.Clone();
        settings_buildable = itemBase.settings_buildable.Clone();
        settings_equipment = itemBase.settings_equipment.Clone();
    }
    public ItemInstance Clone()
    {
        ItemInstance i = new ItemInstance();
        i.amount = this.amount;
        i.baseItem = this.baseItem;
        i.type = this.type;
        i.uniqueIndex = this.uniqueIndex;
        i.uniqueName = this.uniqueName;

        i.settings_cookable = settings_cookable.Clone();
        i.settings_consumeable = settings_consumeable.Clone();
        i.settings_usable = settings_usable.Clone();
        i.settings_Inventory = settings_Inventory.Clone();
        i.settings_buildable = settings_buildable.Clone();
        i.settings_equipment = settings_equipment.Clone();

        return i;
    }

    //ITEM_BASE
    public new ItemType GetType()
    {
        return type;
    }
    public bool IsValid()
    {
        if (baseItem == null || uniqueName == null)
            return false;

        return true;
    }
}
