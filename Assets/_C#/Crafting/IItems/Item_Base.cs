﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType { None, Inventory, Buildable, Equipable, Drinkable }
[CreateAssetMenu(fileName = "Item_Base", menuName = "Semih/Item_Base", order = 1)]
public class Item_Base : ScriptableObject, IComparable<Item_Base>
{
	//public variables
    [SerializeField] private ItemType type;

    public string UniqueName { get { return uniqueName; } private set { uniqueName = value; } }
    [SerializeField] private string uniqueName;

    public int UniqueIndex { get { return uniqueIndex; } private set { uniqueIndex = value; } }
    [SerializeField] private int uniqueIndex;
    private bool hasBeenInitialized;

	//private variables
    
    //Settings
    public ItemInstance_Cookable settings_cookable;
    public ItemInstance_Consumeable settings_consumeable;
    public ItemInstance_Usable settings_usable;
    public ItemInstance_Inventory settings_Inventory;
    public ItemInstance_Buildable settings_buildable;
    public ItemInstance_Equipment settings_equipment;

    //Ctor
    public void Initialize(int uniqueIndex, string uniqueName, ItemType type)
    {
        if (hasBeenInitialized)
            return;
        hasBeenInitialized = true;

        this.uniqueIndex = uniqueIndex;
        this.uniqueName = uniqueName;
        this.type = type;
    }

    //IComparable
    public int CompareTo(Item_Base other)
    {
        return uniqueIndex.CompareTo(other.uniqueIndex);
    }

	//ITEM_BASE
    public new ItemType GetType()
    {
        return type;
    }

	//private methods
}
