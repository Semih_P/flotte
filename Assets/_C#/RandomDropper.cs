﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RandomDropItem
{
    [Range(0, 100)]
    public float spawnChance;
    public Item_Base item;
}
public class RandomDropper : MonoBehaviour
{
 	//public variables
    public RandomDropItem[] items;

 	//private variables


 	//Unity methods

	//public methods
    public Item_Base GetItemToSpawn()
    {
        float totalRange = 0;
        foreach (RandomDropItem item in items)
            totalRange += item.spawnChance;

        float randomValue = Random.Range(0, totalRange);
        float accumulative = 0;

        foreach (RandomDropItem item in items)
        {
            accumulative += item.spawnChance;

            if (randomValue < accumulative)
                return item.item;
        }

        return null;
    }

	//private methods
}
