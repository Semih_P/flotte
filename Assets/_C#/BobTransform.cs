﻿using UnityEngine;
using System.Collections;

public class BobTransform : MonoBehaviour
 {
 	//public variables
    public bool randomizeTimeOffset;

    [Space(10)]
    [Header("Up and down settings")]
    public float frequency = 20f;
    public float magnitude = 0.5f;
    public Vector3 waterOffset;


    [Header("Rotate settings")]
    public float frequencyRotation = 20f;
    public float magnitudeRotation = 0.5f;
    public Vector3 rotationOffset;

    public Transform rotatePoint;
    public float angle;
 	//private variables
    private Vector3 startPos;
    public float timeOffset = 0;
    public Vector3 targetEuler;

    public float sin;
 	//Unity methods
	void Start () 
	{
        startPos = transform.position;
        targetEuler = transform.eulerAngles;

        if (randomizeTimeOffset)
            timeOffset = Random.Range(-10000, 10000);
	}
	void Update () 
	{
        startPos.x = transform.position.x;
        startPos.z = transform.position.z;
        transform.position = startPos + waterOffset + Vector3.up * Mathf.Sin((Time.time + timeOffset)* frequency) * magnitude;

        //targetEuler.x = rotationOffset.x + (Vector3.right * Mathf.Sin((Time.time + timeOffset) * frequencyRotation) * magnitudeRotation).x;
        //targetEuler.z = (Vector3.forward * Mathf.Sin((timeOffset - Time.time) * frequencyRotation) * magnitudeRotation).z;

        //if (rotatePoint != null)
        //{
        //    sin = Mathf.Sin(Time.time * frequencyRotation) * magnitudeRotation;
        //    targetEuler = new Vector3(sin, 0, sin);

        //    transform.RotateAround(rotatePoint.position, Vector3.forward, targetEuler.z);
        //    transform.RotateAround(rotatePoint.position, Vector3.right, targetEuler.x);
        //}
	}


	//public methods


	//private methods
}
