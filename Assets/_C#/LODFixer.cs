﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LODFixer_LOD
{
    [HideInInspector]
    public string name;
    public Renderer[] renderers;
    [Range(0f, 1f)]
    public float screenHeight;
}
public class LODFixer : MonoBehaviour 
{
	//public variables
    public LODFixer_LOD[] lods;

	//private variables
    private LODGroup lodGroup;
    private bool isDirty = false;
	//Unity methods
	void Awake () 
	{
        isDirty = true;
        if (lodGroup == null)
            lodGroup = GetComponent<LODGroup>();

        foreach(LODFixer_LOD lod in lods)
        {
            for (int j = 0; j < lod.renderers.Length; j++)
            {
                lod.renderers[j] = transform.FindChild(lod.renderers[j].transform.name).GetComponent<Renderer>();
            }
        }
        OnValidate();
        Destroy(this);
	}	
    void OnValidate()
    {
        if (!isDirty)
            return;
        isDirty = false;
        if (lodGroup == null)
            lodGroup = GetComponent<LODGroup>();

        LOD[] newLods = new LOD[lods.Length];
        for (int i = 0; i < newLods.Length; i++)
        {
            if (lods[i].renderers != null)
            {
                newLods[i].renderers = lods[i].renderers;
            }

            newLods[i].screenRelativeTransitionHeight = lods[i].screenHeight;
            if (i != 0)
            {
                if (newLods[i].screenRelativeTransitionHeight > newLods[i - 1].screenRelativeTransitionHeight - 0.05f)
                {
                    newLods[i].screenRelativeTransitionHeight = newLods[i - 1].screenRelativeTransitionHeight - 0.05f;
                    lods[i].screenHeight = newLods[i].screenRelativeTransitionHeight;
                }
            }
        }
        lodGroup.SetLODs(newLods);
    }

	//public methods

	//private methods
}
