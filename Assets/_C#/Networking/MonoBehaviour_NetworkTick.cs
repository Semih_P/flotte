﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviour_NetworkTick : MonoBehaviour_Network 
{
    //public variables
    [Header("Network settings")]
    [Range(0, 50)]
    public int ticksPerSecond = 10;

    //privat variables
    private float tickTimer;

    //public methods
    public void UpdateNetworkTick()
    {
        tickTimer += Time.deltaTime;
        if (tickTimer >= 1f / ticksPerSecond)
        {
            tickTimer -= 1f / ticksPerSecond;

            Tick();
        }
    }

    public virtual void Tick()
    {
        if (IsFresh)
            return;

        SetDirty();
    }
}
