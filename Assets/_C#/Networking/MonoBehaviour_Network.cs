﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviour_Network : MonoBehaviour_ID 
{
    //properties
    public List<Message> MessagesStack { get { return messages; } }
    public bool IsFresh 
    {
        get
        {
            return fresh;
        }
        set
        {
            fresh = value;
        }
    }
    public bool IsDirty { get { return currentSendIndex != lastSentIndex; } }
    public bool IsKilled { get { return dead; } }
    public uint BehaviourIndex { get { return behaviourIndex; } set { behaviourIndex = value; } }
    private uint behaviourIndex;

    //private variables
    private List<Message> messages = new List<Message>();
    private uint currentSendIndex = 0;
    private uint lastSentIndex = 0;
    private bool fresh = true;
    private bool dead = false;

    //Unity methods

    //public methods
    public void SetDirty()
    {
        currentSendIndex = lastSentIndex + 1;
    }
    public void Clean()
    {
        lastSentIndex = currentSendIndex;
        MessagesStack.Clear();
    }
    public void Kill()
    {
        dead = true;
    }
    public void Revive()
    {
        dead = false;
        fresh = true;
        Clean();
    }

    //protected methods
    protected void AddMessage(Message_NetworkBehaviour msg)
    {
        if (msg != null)
            MessagesStack.Add(msg);
    }

    //Network methods
    public virtual void Serialize_Create()
    {
    }
    public virtual void Serialize_Update()
    {
    }
    public virtual void Deserialize(Message_NetworkBehaviour msg)
    {
    }
}
