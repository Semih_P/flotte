﻿using Steamworks;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public enum PacketType
{
    Single,
    Multiple
}
public enum Messages
{
    NOTHING,
    InitiateSession,
    InitiateResult,
    Ping,
    Pong,
    RequestWorld,
    CreateWorld,
    PlayerJoined,
    PlayerDisconnected,
    Create,
    Update,
    UpdateWater,
    UpdateRaft,
    CreateFloatingObject,
    CreateHostNetwork,
    CreatePlayer,
    CreateShark,
    UpdateShark,
    CreateBlock,
    RotateBlock,
    SelectBlock,
    SelectItem,
    RemoveNetworkBehaviour,
    DamageEntity,
    ItemCollectorCollect,
    ItemCollectorClear,
    ItemCollectorCreate,
    RemoveItem,
    PlantSeed,
    HarvestPlant,
    PlantManagerCreate,
    AnchorChange,
    ConnectThrowableAnchorStand,
    DisconnectThrowableAnchorStand,
    ThrowableThrow,
    ThrowableStandCreate,
    ThrowableStandCreateInAir,
    StationaryAnchorUse,
    StationaryAnchorCreate,
    Door_Open,
    Door_Close,
    CreateNetworkPickupManager,
    CookingStand_Insert,
    CookingStand_AddFuel,
    CookingStand_Collect,
    CookingStand_Create,
    StorageManager_Create,
    Storage_Open,
    Storage_Close,
    DropItem,
    Brick_Pickup,
    PlayerNetworkManager_Create
}

//Other messages
[System.Serializable]
public class Packet
{
    public PacketType packetType;
}
[System.Serializable]
public class Packet_Single : Packet
{
    public Message message;
    public EP2PSend sendType;
    public Packet_Single(EP2PSend sendType)
    {
        this.sendType = sendType;
        this.packetType = PacketType.Single;
    }
}
[System.Serializable]
public class Packet_Multiple : Packet
{
    public Message[] messages;
    public EP2PSend sendType;

    public Packet_Multiple(EP2PSend sendType)
    {
        this.sendType = sendType;
        this.packetType = PacketType.Multiple;
    }
}
//[System.Serializable]
//public class Packet
//{
//    public List<Message> messages;
//    public EP2PSend sendType;

//    public Packet(EP2PSend sendType)
//    {
//        this.sendType = sendType;
//    }
//    public Packet(EP2PSend sendType, List<Message> messages)
//    {
//        this.sendType = sendType;
//        this.messages = messages;
//    }
//}
[System.Serializable]
public class Message
{
    public Messages type;
    public Message()
    {

    }
    public Message(Messages type)
    {
        this.type = type;
    }
}
[System.Serializable]
public class Message_PingPong : Message
{
    public float timeSent;
    public Message_PingPong(Messages type, float timeSent = 0)
    {
        this.type = type;
        this.timeSent = timeSent;
    }
}
[System.Serializable]
public class Message_SteamID : Message
{
    public ulong id;
    public Message_SteamID(Messages type, CSteamID id)
    {
        this.type = type;
        this.id = id.m_SteamID;
    }

    public CSteamID GetCSteamID()
    {
        return (CSteamID)id;
    }
}
[System.Serializable]
public class Message_Result : Message
{
    public bool result;
    public Message_Result(Messages type, bool result)
    {
        this.type = type;
        this.result = result;
    }
}

//NetworkBehaviour messages
[System.Serializable]
public class Message_NetworkBehaviour : Message
{
    public uint objectIndex;
    public uint behaviourIndex;

    public Message_NetworkBehaviour(Messages type, MonoBehaviour_Network behaviour) : base(type)
    {
        this.objectIndex = behaviour.ObjectIndex;
        this.behaviourIndex = behaviour.BehaviourIndex;
    }
}
[System.Serializable]
public class Message_NetworkBehaviour_SteamID : Message_NetworkBehaviour
{
    public ulong steamID;

    public Message_NetworkBehaviour_SteamID(Messages type, MonoBehaviour_Network behaviour, CSteamID steamID)
        :base(type, behaviour)
	{
        this.steamID = steamID.m_SteamID;
	}

    public CSteamID GetID()
    {
        return (CSteamID)steamID;
    }
}
