﻿using Steamworks;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public enum Target
{
    All,
    Other
}
public enum NetworkChannel
{
    Channel_Game = 0,
    Channel_Session = 1
}
public class Semih_Network : MonoBehaviour 
{
    //Delegates
    public delegate void PlayerAction(Network_Player playerNetwork);
    public static event PlayerAction PlayerInitialized;

    //properties
    public bool IsHost { get { return isHost; } }
    public bool IsConnectedToHost { get { return HostID.IsValid() && connectedToHost; } }
    public float Ping { get { return (ping * 1000f); } }
    public float BytesSentPerSecond { get { return bytesSentPerSecond; } }
    public float TotalBytesSent { get { return totalBytesSent; } }
    public float MaxBytePackage { get { return maxBytePackage; } }
    public CSteamID HostID { get { return hostID; } }
    public CSteamID LocalSteamID { get { return localSteamID; } }
    public string PersonaName { get { return SteamFriends.GetPersonaName(); } }

    //public variables
    public string gameSceneName;
    public string lobbySceneName;
    public Network_Player playerPrefab;
    [HideInInspector]
    public Dictionary<CSteamID, Network_Player> remoteUsers = new Dictionary<CSteamID,Network_Player>();
    [HideInInspector]
    public Network_Host hostNetwork;

    //private variables
    private CSteamID localSteamID;
    private CSteamID lobbyID;
    private CSteamID hostID;
    private List<CSteamID> connectingUsers = new List<CSteamID>();

    //private variables
    private static bool initialized;
    private bool connectedToHost;
    private bool isHost;
    private float ping;
    private float totalBytesSent, lastSecondBytes, bytesSentPerSecond, maxBytePackage;
    private Scene startScene;
    private const int MaxUnreliableSize = 1200 - sizeof(EP2PSend);

    //Callbacks
    private CallResult<LobbyCreated_t> OnLobbyCreatedCallresult;
    private CallResult<LobbyEnter_t> OnLobbyEnterCallresult;
    private Callback<P2PSessionRequest_t> OnP2PSessionRequestCallback;
    private Callback<P2PSessionConnectFail_t> OnP2PSessionConnectFailCallback;

    //Unity methods
    void OnEnable()
    {
        if (initialized)
        {
            Destroy(gameObject);
            return;
        }
        initialized = true;
        ComponentManager<Semih_Network>.Set(this);


        SetupCallbacks();
        InvokeRepeating("SendPing", 2f, 2f);
        InvokeRepeating("CalculateBytes", 1f, 1f);

        DontDestroyOnLoad(gameObject);
        localSteamID = SteamUser.GetSteamID();
        SteamNetworking.AllowP2PPacketRelay(true);
        SceneManager.sceneLoaded += OnSceneLoaded;

        if (string.IsNullOrEmpty(startScene.name))
            startScene = SceneManager.GetActiveScene();
        if (startScene.name == gameSceneName)
        {
            HostGame();
        }
    }
    void OnApplicationQuit()
    {
        foreach(KeyValuePair<CSteamID, Network_Player> pair in remoteUsers)
        {
            P2PSessionState_t state;
            if (SteamNetworking.GetP2PSessionState(pair.Key, out state))
            {
                SteamNetworking.CloseP2PSessionWithUser(pair.Key);
            }
        }
    }
	void Update () 
    {
        ReadP2P_Channel_Connecting();
        ReadP2P_Channel_Game();
	}
    private void SendPing()
    {
        if (IsConnectedToHost)
        {
            if (IsHost)
            {
                ping = 0;
                return;
            }
            else
            {
                SendP2P(HostID, new Message_PingPong(Messages.Ping, Time.time), EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
            }
        }
    }
    private void CalculateBytes()
    {
        bytesSentPerSecond = lastSecondBytes / 1f;
        lastSecondBytes = 0f;
    }

    //public methods
    public void SendP2P(CSteamID steamID, Packet_Single packetSingle, NetworkChannel channel)
    {
        //Initialize packet
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        MemoryStream memoryStream = new MemoryStream();

        //Serialize the packet into bytes
        binaryFormatter.Serialize(memoryStream, packetSingle);
        byte[] bytes = memoryStream.ToArray();

        //Check if it is to big
        if (packetSingle.sendType == EP2PSend.k_EP2PSendUnreliable && bytes.Length > MaxUnreliableSize)
        {
            Debug.LogWarning("Sending a packet single of type unreliable with to much bytes: " + bytes.Length + "/" + MaxUnreliableSize);
        }
        else
        {
            SteamNetworking.SendP2PPacket(steamID, bytes, (uint)bytes.Length, packetSingle.sendType, (int)channel);
            //Store data about sending network stuff
            totalBytesSent += bytes.Length;
            lastSecondBytes += bytes.Length;
            if (bytes.Length > maxBytePackage)
                maxBytePackage = bytes.Length;
        }
    }
    public void SendP2P(CSteamID steamID, Packet_Multiple packet, NetworkChannel channel)
    {
        //Initialize packet
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        MemoryStream memoryStream = new MemoryStream();

        //Serialize the packet into bytes
        binaryFormatter.Serialize(memoryStream, packet);
        byte[] bytes = memoryStream.ToArray();

        //Check if it is to big
        if (packet.sendType == EP2PSend.k_EP2PSendUnreliable && bytes.Length > MaxUnreliableSize)
        {
            Packet_Multiple subPacket = new Packet_Multiple(packet.sendType);
            int subPacketSize = 0;
            List<Message> subMsgs = new List<Message>();

            for (int i = 0; i < packet.messages.Length; i++)
            {
                Message message = packet.messages[i];

                memoryStream.SetLength(0);
                binaryFormatter.Serialize(memoryStream, message);
                int messageSize = memoryStream.ToArray().Length;

                if (subPacketSize + messageSize < MaxUnreliableSize)
                {
                    subMsgs.Add(message);
                    subPacket.messages = subMsgs.ToArray();

                    memoryStream.SetLength(0);
                    binaryFormatter.Serialize(memoryStream, subPacket);
                    subPacketSize = memoryStream.ToArray().Length;
                }
                else
                {
                    if (subPacket.messages.Length > 1)
                    {
                        SendP2P(steamID, subPacket, channel);
                    }
                    else if (subPacket.messages.Length == 1)
                    {
                        Packet_Single packSingle = new Packet_Single(packet.sendType);
                        packSingle.message = subPacket.messages[0];
                        SendP2P(steamID, packSingle, channel);
                    }
                    subPacket = new Packet_Multiple(packet.sendType);
                    subMsgs.Clear();

                    subMsgs.Add(message);
                    subPacket.messages = subMsgs.ToArray();

                    memoryStream.SetLength(0);
                    binaryFormatter.Serialize(memoryStream, subPacket);
                    subPacketSize = memoryStream.ToArray().Length;
                }
            }

            if (subPacket.messages.Length > 1)
            {
                SendP2P(steamID, subPacket, channel);
            }
            else if (subPacket.messages.Length == 1)
            {
                Packet_Single packSingle = new Packet_Single(packet.sendType);
                packSingle.message = subPacket.messages[0];
                SendP2P(steamID, packSingle, channel);
            }
            return;
        }
        //If it is not to big, lets send it
        else
        {
            SteamNetworking.SendP2PPacket(steamID, bytes, (uint)bytes.Length, packet.sendType, (int)channel);
        }


        //Store data about sending network stuff
        totalBytesSent += bytes.Length;
        lastSecondBytes += bytes.Length;
        if (bytes.Length > maxBytePackage)
            maxBytePackage = bytes.Length;
    }
    public void SendP2P(CSteamID steamID, Message message, EP2PSend sendType, NetworkChannel channel)
    {
        Packet_Single packetSingle = new Packet_Single(sendType);
        packetSingle.message = message;
        SendP2P(steamID, packetSingle, channel);
    }
    public void SendP2P(CSteamID steamID, List<Message> messages, EP2PSend sendType, NetworkChannel channel)
    {
        Packet_Multiple packetMulti = new Packet_Multiple(sendType);
        packetMulti.messages = messages.ToArray();
        SendP2P(steamID, packetMulti, channel);
    }
    public void RPC(Message message, Target target, EP2PSend sendType, NetworkChannel channel)
    {
        List<Message> messages = new List<Message>();
        messages.Add(message);
        RPC(messages, target, sendType, channel);
    }
    public void RPC(List<Message> messages, Target target, EP2PSend sendType, NetworkChannel channel)
    {
        foreach (KeyValuePair<CSteamID, Network_Player> pair in remoteUsers)
        {
            if (target == Target.All)
            {
                SendP2P(pair.Key, messages, sendType, channel);
            }
            else if (target == Target.Other)
            {
                if (pair.Key != LocalSteamID)
                {
                    SendP2P(pair.Key, messages, sendType, channel);
                }
            }
        }
    }
    public void RPCExclude(Packet_Multiple packetMulti, Target target, CSteamID excludeID, NetworkChannel channel)
    {
        foreach(KeyValuePair<CSteamID, Network_Player> pair in remoteUsers)
        {
            if (target == Target.All && pair.Key != excludeID)
            {
                SendP2P(pair.Key, packetMulti, channel);
            }
            else if (target == Target.Other && pair.Key != excludeID)
            {
                if (pair.Key != LocalSteamID)
                {
                    SendP2P(pair.Key, packetMulti, channel);
                }
            }
        }
    }

    private void ReadP2P_Channel_Connecting()
    {
        uint msgSize;
        if (SteamNetworking.IsP2PPacketAvailable(out msgSize, (int)NetworkChannel.Channel_Session))
        {
            byte[] bytes = new byte[msgSize];
            uint newMsgSize;
            CSteamID remoteID;

            if (SteamNetworking.ReadP2PPacket(bytes, msgSize, out  newMsgSize, out remoteID, (int)NetworkChannel.Channel_Session))
            {
                MemoryStream stream = new MemoryStream(bytes);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                Packet packet = binaryFormatter.Deserialize(stream) as Packet;
                Packet_Single packetSingle = null;
                Packet_Multiple packetMulti = null;
                if (packet.packetType == PacketType.Single)
                {
                    packetSingle = packet as Packet_Single;
                    packetMulti = new Packet_Multiple(packetSingle.sendType);
                    packetMulti.messages = new Message[] { packetSingle.message };
                }
                else
                    packetMulti = packet as Packet_Multiple;

                foreach (Message message in packetMulti.messages)
                {
                    switch (message.type)
                    {
                        case Messages.InitiateSession:
                            OnSessionRequest(remoteID);
                            break;
                        case Messages.InitiateResult:
                            Message_Result msgInitiateResult = message as Message_Result;
                            NetworkUI.Singleton.Console(SteamFriends.GetFriendPersonaName(remoteID) + (msgInitiateResult.result ? " allowed you to join" : " did not allow you to join"));

                            if (!IsConnectedToHost && msgInitiateResult.result)
                            {
                                connectedToHost = true;
                                hostID = remoteID;
                                bool acceptSession = SteamNetworking.AcceptP2PSessionWithUser(remoteID);
                                SceneManager.LoadScene(gameSceneName, LoadSceneMode.Single);
                            }
                            break;
                        case Messages.RequestWorld:
                            if (IsHost)
                            {
                                List<Message> worldMessages = GetWorld();
                                SendP2P(remoteID, worldMessages, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                            }
                            break;
                        case Messages.PlayerJoined:
                            if (IsHost)
                            {
                                if (SceneManager.GetActiveScene().name == gameSceneName)
                                    OnIncommingConnection(remoteID);
                            }
                            break;
                        case Messages.PlayerDisconnected:
                            Message_SteamID msgPlayerDisconnect = message as Message_SteamID;
                            OnDisconnect(msgPlayerDisconnect.GetCSteamID());
                            break;
                    }
                }
            }
        }
    }
    private void ReadP2P_Channel_Game()
    {
        uint msgSize;
        while(SteamNetworking.IsP2PPacketAvailable(out msgSize, (int)NetworkChannel.Channel_Game))
        {
            byte[] bytes = new byte[msgSize];
            uint newMsgSize;
            CSteamID remoteID;

            if (SteamNetworking.ReadP2PPacket(bytes, msgSize, out  newMsgSize, out remoteID, (int)NetworkChannel.Channel_Game))
            {
                MemoryStream stream = new MemoryStream(bytes);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                Packet packet = binaryFormatter.Deserialize(stream) as Packet;
                Packet_Single packetSingle = null;
                Packet_Multiple packetMulti = null;
                if (packet.packetType == PacketType.Single)
                {
                    packetSingle = packet as Packet_Single;
                    packetMulti = new Packet_Multiple(packetSingle.sendType);
                    packetMulti.messages = new Message[] { packetSingle.message };
                }
                else
                    packetMulti = packet as Packet_Multiple;

                List<Message> packetMessages = packetMulti.messages.ToList();
                for (int i = 0; i < packetMessages.Count; i++)
                {
                    Message message = packetMessages[i];
                    if (message == null)
                        continue;

                    switch (message.type)
                    {
                        case Messages.Ping:
                            if (IsHost)
                            {
                                Message_PingPong msgPing = message as Message_PingPong;
                                msgPing.type = Messages.Pong;
                                SendP2P(remoteID, msgPing, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                            }
                            break;
                        case Messages.Pong:
                            Message_PingPong msgPong = message as Message_PingPong;
                            ping = Time.time - msgPong.timeSent;
                            break;

                        case Messages.CreatePlayer:
                            if (!IsHost)
                            {
                                Message_Player_Create msgPlayerCreate = message as Message_Player_Create;
                                AddPlayer(msgPlayerCreate);
                                packetMessages.Remove(message); i--;
                            }
                            continue;

                        case Messages.CreateBlock:
                            Message_BuildBlock msgBuildBlock = message as Message_BuildBlock;
                            BlockCreator creator = remoteUsers[remoteID].BlockCreator;
                            creator.CreateBlock(msgBuildBlock);
                            packetMessages.Remove(message); i--;
                            continue;

                        case Messages.CreateHostNetwork:
                            Message_NetworkBehaviour msgHostNetwork = message as Message_NetworkBehaviour;
                            Network_Host host = GameObject.FindObjectOfType<Network_Host>();
                            host.BehaviourIndex = msgHostNetwork.behaviourIndex;
                            host.ObjectIndex = msgHostNetwork.objectIndex;
                            packetMessages.Remove(message); i--;
                            continue;

                        case Messages.CreateFloatingObject:
                            if (hostNetwork == null)
                                continue;

                            Message_FloatingObject_Create msgFloatingCreate = message as Message_FloatingObject_Create;
                            hostNetwork.spawnerManager.spawners[(int)msgFloatingCreate.spawnerIndex].SpawnNewItemsClient(msgFloatingCreate);
                            packetMessages.Remove(message); i--;
                            continue;

                        case Messages.CreateShark:
                            Network_Host_Entities hostEntities = GameObject.FindObjectOfType<Network_Host_Entities>();
                            hostEntities.CreateSharkClient(message as Message_Shark);
                            packetMessages.Remove(message); i--;
                            continue;

                        case Messages.CreateNetworkPickupManager:
                            Message_PickupManager_Create msgCreatePickupManager = message as Message_PickupManager_Create;
                            PickupObjectManager pickupManager = GameObject.FindObjectOfType<PickupObjectManager>();
                            pickupManager.BehaviourIndex = msgCreatePickupManager.behaviourIndex;
                            pickupManager.ObjectIndex = msgCreatePickupManager.objectIndex;
                            break;
                    }
                }

                packetMulti.messages = packetMessages.ToArray();
                NetworkUpdateManager.Deserialize(packetMulti, remoteID);
            }
        }
    }

    public void CreateLobby()
    {
        SteamAPICall_t handle = SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypeFriendsOnly, 4);
        OnLobbyCreatedCallresult.Set(handle);

        NetworkUI.Singleton.Console("Creating lobby | " + ELobbyType.k_ELobbyTypeFriendsOnly + " | 4");
    }
    public void JoinLobby(CSteamID hostID)
    {
        FriendGameInfo_t gameInfo;
        SteamFriends.GetFriendGamePlayed(hostID, out gameInfo);

        if (gameInfo.m_steamIDLobby.IsValid())
        {
            SteamAPICall_t handle = SteamMatchmaking.JoinLobby(gameInfo.m_steamIDLobby);
            OnLobbyEnterCallresult.Set(handle);

            NetworkUI.Singleton.Console("Joining lobby | " + SteamFriends.GetFriendPersonaName(hostID) + " | " + gameInfo.m_steamIDLobby.m_SteamID);
        }
    }
    public void HostGame()
    {
        if (IsHost || IsConnectedToHost)
        {
            NetworkUI.Singleton.Console("Can't host a game, you are already a host");
            return;
        }

        //Mark self as host
        isHost = true;
        connectedToHost = true;
        hostID = LocalSteamID;

        //Change scene
        if (startScene.name != gameSceneName)
            SceneManager.LoadScene(gameSceneName, LoadSceneMode.Single);
        else
        {
            OnIncommingConnection(LocalSteamID);
        }
    }
    public void TryToJoinGame(CSteamID hostID)
    {
        if (IsConnectedToHost)
        {
            NetworkUI.Singleton.Console("Can't join game, you are already connected to a host");
            return;
        }

        this.hostID = hostID;

        NetworkUI.Singleton.Console("Sending request to join " + SteamFriends.GetFriendPersonaName(hostID) + "'s game");
        SendP2P(hostID, new Message(Messages.InitiateSession), EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Session);
    }
    public void DisconnectFromHost()
    {
        if (!IsConnectedToHost)
        {
            NetworkUI.Singleton.Console("Can't disconnect, you are already disconnected");
            return;
        }

        Disconnect();
    }
    public CSteamID GetIDFromPlayer(Network_Player player)
    {
        foreach(KeyValuePair<CSteamID, Network_Player> pair in remoteUsers)
        {
            if (pair.Value == player)
                return pair.Key;
        }
        return new CSteamID(0);
    }
    public Network_Player GetRandomPlayer()
    {
        if (remoteUsers == null || remoteUsers.Count == 0)
        {
            return null;
        }

        int playerIndex = Random.Range(0, remoteUsers.Count);
        KeyValuePair<CSteamID, Network_Player> user = remoteUsers.ElementAt(playerIndex);
        return user.Value;
    }
    public Network_Player GetLocalPlayer()
    {
        if (remoteUsers == null || remoteUsers.Count == 0)
        {
            return null;
        }

        foreach (KeyValuePair<CSteamID, Network_Player> pair in remoteUsers)
        {
            if (pair.Value.IsLocalPlayer)
                return pair.Value;
        }
        return null;
    }

    //private methods
    private bool CanUserJoinMe(CSteamID remoteID)
    {
        if (!IsHost)
            return false;

        if (remoteUsers.ContainsKey(remoteID))
            return false;

        if (connectingUsers.Contains(remoteID))
            return false;

        return true;
    }
    private void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        if (IsHost)
        {
            //Join yourself
            OnIncommingConnection(LocalSteamID);
        }
        else
        {
            Packet_Multiple packet = new Packet_Multiple(EP2PSend.k_EP2PSendReliable);
            packet.messages = new Message[2];
            packet.messages[0] = new Message(Messages.PlayerJoined);
            packet.messages[1] = new Message(Messages.RequestWorld);
            SendP2P(hostID, packet, NetworkChannel.Channel_Session);
        }
    }
    private List<Message> GetWorld()
    {
        List<Message> msgs = new List<Message>();
        List<Message> createPlayerMsgs = new List<Message>();
        List<Message> createFloatingMsgs = new List<Message>();

        foreach (KeyValuePair<uint, MonoBehaviour_Network> pair in NetworkUpdateManager.NetworkedBehaviours)
        {
            MonoBehaviour_Network behaviour = pair.Value;

            behaviour.Clean();
            behaviour.Serialize_Create();
            if (behaviour.MessagesStack.Count == 0)
                continue;

            for (int i = 0; i < behaviour.MessagesStack.Count; i++)
            {
                Message msg = behaviour.MessagesStack[i];

                //Prioritize player
                if (msg.type == Messages.CreatePlayer)
                    createPlayerMsgs.Insert(0, msg);
                else if (msg.type == Messages.CreateFloatingObject)
                    createFloatingMsgs.Insert(0, msg);
                else
                    msgs.Add(msg);
            }
            behaviour.Clean();
        }

        msgs.InsertRange(0, createFloatingMsgs);
        msgs.InsertRange(0, createPlayerMsgs);
        return msgs;
    }

    //Callbacks
    private void SetupCallbacks()
    {
        OnP2PSessionRequestCallback = Callback<P2PSessionRequest_t>.Create(OnP2PSessionRequest);
        OnP2PSessionConnectFailCallback = Callback<P2PSessionConnectFail_t>.Create(OnP2PSessionConnectFail);
        OnLobbyCreatedCallresult = CallResult<LobbyCreated_t>.Create(OnLobbyCreated);
        OnLobbyEnterCallresult = CallResult<LobbyEnter_t>.Create(OnLobbyEnter);
    }
    private void OnLobbyCreated(LobbyCreated_t callback, bool bIOFailure)
    {
        lobbyID = (CSteamID)callback.m_ulSteamIDLobby;

        NetworkUI.Singleton.Console("Lobby created | " + callback.m_ulSteamIDLobby + " | " + callback.m_eResult);
    }
    private void OnLobbyEnter(LobbyEnter_t callback, bool bIOFailure)
    {
        lobbyID = (CSteamID)callback.m_ulSteamIDLobby;
        NetworkUI.Singleton.Console("Entered lobby | " + lobbyID.m_SteamID);
    }

    private void OnP2PSessionRequest(P2PSessionRequest_t callback)
    {
        OnSessionRequest(callback.m_steamIDRemote);
    }
    private void OnSessionRequest(CSteamID remoteID)
    {
        bool userCanJoin = CanUserJoinMe(remoteID);
        if (userCanJoin)
        {
            connectingUsers.Add(remoteID);
            StartCoroutine(RemoveConnectingUser(remoteID, 5f));

            NetworkUI.Singleton.Console(SteamFriends.GetFriendPersonaName(remoteID) + " requested to join, result=" + userCanJoin);
            SendP2P(remoteID, new Message_Result(Messages.InitiateResult, userCanJoin), EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Session);
        }
    }
    private void OnIncommingConnection(CSteamID remoteID)
    {
        if (IsHost)
        {
            //Add host player, i.e yourself
            if (remoteID == HostID)
            {
                AddPlayer(remoteID);
            }
            //Add a remote client
            else
            {
                if (connectingUsers.Contains(remoteID))
                {
                    connectingUsers.Remove(remoteID);
                    bool acceptSession = SteamNetworking.AcceptP2PSessionWithUser(remoteID);

                    if (acceptSession)
                        AddPlayer(remoteID);
                }
            }
        }
    }
    private void AddPlayer(CSteamID id)
    {
        if (remoteUsers.ContainsKey(id))
        {
            NetworkUI.Singleton.Console("Could not add " + SteamFriends.GetFriendPersonaName(id) + ", already exists in dictionary");
            return;
        }
        
        NetworkUI.Singleton.Console("Added " + SteamFriends.GetFriendPersonaName(id) + " to your game");

        GameObject spawnPoint = GameObject.FindWithTag("NetworkSpawnPoint");
        Network_Player player = Instantiate(playerPrefab, spawnPoint == null ? Vector3.zero : spawnPoint.transform.position, spawnPoint == null ? Quaternion.identity : spawnPoint.transform.rotation) as Network_Player;
        remoteUsers.Add(id, player);
        player.OnPlayerCreated(this, id == localSteamID, id, SteamFriends.GetFriendPersonaName(id));
        
        if (IsHost)
            player.SetDirty();

        //Call event for when player has entered the game
        if (PlayerInitialized != null)
        {
            PlayerInitialized(player);
        }
    }
    private void AddPlayer(Message_Player_Create msg)
    {
        CSteamID remoteID = (CSteamID)msg.steamID;
        if (remoteUsers.ContainsKey(remoteID))
        {
            return;
        }

        NetworkUI.Singleton.Console("Added player " + msg.personaName);

        GameObject spawnPoint = GameObject.FindWithTag("NetworkSpawnPoint");
        Network_Player player = Instantiate(playerPrefab, spawnPoint == null ? Vector3.zero : spawnPoint.transform.position, spawnPoint == null ? Quaternion.identity : spawnPoint.transform.rotation) as Network_Player;
        remoteUsers.Add(remoteID, player);
        player.OnPlayerCreated(this, remoteID == localSteamID, remoteID, msg.personaName);
        player.SetIndex(msg.objectIndex, msg.behaviourIndex);
        player.SetDirty();

        //Call event for when player has entered the game
        if (PlayerInitialized != null)
        {
            PlayerInitialized(player);
        }
    }

    private void OnP2PSessionConnectFail(P2PSessionConnectFail_t callback)
    {
        EP2PSessionError error = (EP2PSessionError)callback.m_eP2PSessionError;
        NetworkUI.Singleton.Console("P2PSessionFail Error | " + error);

        switch(error)
        {
            case EP2PSessionError.k_EP2PSessionErrorTimeout:
                OnDisconnect(callback.m_steamIDRemote);
                break;
        }
    }
    private void OnDisconnect(CSteamID remoteID)
    {
        //If the host is disconnecting
        if (remoteID == hostID)
        {
            NetworkUI.Singleton.Console("Host has disconnected, disconnecting yourself...");
            Disconnect();
            return;
        }
        //You disconnected yourself
        else if (remoteID == LocalSteamID)
        {
            NetworkUI.Singleton.Console("You have disconnected yourself");
            Disconnect();
            return;
        }
        else if (remoteUsers.ContainsKey(remoteID))
        {
            if (IsHost)
            {
                //Message other player about the player disconnecting
                RPC(new Message_SteamID(Messages.PlayerDisconnected, remoteID), Target.Other, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Session);
            }

            //Remove the player
            RemovePlayer(remoteID);
            NetworkUI.Singleton.Console(SteamFriends.GetFriendPersonaName(remoteID) + " has disconnected");

            //Close session with user
            SteamNetworking.CloseP2PSessionWithUser(remoteID);
        }
    }
    private void Disconnect()
    {
        //Remove all remote users
        foreach (KeyValuePair<CSteamID, Network_Player> pair in remoteUsers)
        {
            SteamNetworking.CloseP2PSessionWithUser(pair.Key);
            Destroy(pair.Value.gameObject);
        }
        remoteUsers.Clear();

        //Reset flags
        this.isHost = false;
        this.connectedToHost = false;
        this.hostID = new CSteamID(0);

        //Change to lobby scene
        SceneManager.LoadScene(lobbySceneName, LoadSceneMode.Single);
        Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
    }
    private void RemovePlayer(CSteamID id)
    {
        if (remoteUsers.ContainsKey(id))
        {
            NetworkUI.Singleton.Console(SteamFriends.GetFriendPersonaName(id) + " has disconnected");
            Network_Player player = remoteUsers[id];
            Destroy(player.gameObject);
            remoteUsers.Remove(id);
        }
    }
    private IEnumerator RemoveConnectingUser(CSteamID id, float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);

        if (connectingUsers.Contains(id))
            connectingUsers.Remove(id);
    }
}
