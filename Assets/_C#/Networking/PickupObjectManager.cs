﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObjectManager : MonoBehaviour_Network 
{
	//public variables
    public static List<PickupItem> trackedPickupItems = new List<PickupItem>();
    public TextMesh textmesh;

	//private variables
    private Semih_Network network;
    private PoolManager poolManager;

	//Unity methods
    void Awake()
    {
        NetworkUpdateManager.SetIndexForBehaviour(this);
        NetworkUpdateManager.AddBehaviour(this);

        ComponentManager<PickupObjectManager>.Set(this);
    }
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
        poolManager = ComponentManager<PoolManager>.Get();
    }   
    void Update()
    {
        textmesh.text = "Total: " + trackedPickupItems.Count;
        for (int i = 0; i < trackedPickupItems.Count; i++)
        {
            textmesh.text += "\n" + i + "_" + trackedPickupItems[i].itemInstance.UniqueName + ","+trackedPickupItems[i].itemInstance.amount;
        }
    }

	//public methods
    public void StartTrackingPickup(PickupItem item)
    {
        item.trackedByPickupManager = true;
        trackedPickupItems.Add(item);
    }
    public void RemovePickupItem(PickupItem item, bool useNetwork)
    {
        //Get the index of tracked item
        int index = trackedPickupItems.IndexOf(item);

        //Stop tracking the item
        trackedPickupItems.RemoveAt(index);

        //Remove the item
        if (item.isDropped)
            PoolManager.ReturnObject(item.gameObject);
        else
            Destroy(item.gameObject);

        NetworkUI.Singleton.Console("Remove pickupitem: " + index);
        if (useNetwork)
            network.RPC(new Message_RemoveItem(Messages.RemoveItem, this, index), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
    public void DropItem(ItemInstance item, Vector3 position, Vector3 dropDirection, bool useNetwork)
    {
        if (item == null)
            return;

        GameObject dropItemGameObject = poolManager.GetObject("DropBarrel");
        ComponentManager<SoundManager>.Get().PlaySound("UIDropItem");

        if (dropItemGameObject != null)
        {
            PickupItem pickup = dropItemGameObject.GetComponent<PickupItem>();
            if (pickup != null)
            {
                //Set values for dropped item
                pickup.transform.SetParent(GameManager.Singleton.globalBuoyancyParent);
                pickup.transform.position = position;
                pickup.transform.rotation = Quaternion.Euler(new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)));
                pickup.itemInstance = item;
                pickup.isDropped = true;
                StartTrackingPickup(pickup);

                //Apply a force to the dropped item
                Rigidbody body = pickup.GetComponent<Rigidbody>();
                if (body != null)
                {
                    body.velocity = Vector3.zero;
                    body.AddForce(dropDirection * 100 + Vector3.up * 200);
                }

                //Reset the destroy timer
                DropItemDestroyTimer dropItem = pickup.transform.GetComponent<DropItemDestroyTimer>();
                if (dropItem != null)
                {
                    dropItem.ResetDestroyTimer();
                }
            }
        }

        if (useNetwork)
            network.RPC(new Message_DropItem(Messages.DropItem, this, item, position, dropDirection), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }

	//private methods
    private void SpawnBarrel(ItemInstance itemInstance, Vector3 position, Vector3 rotation, int amount)
    {
        GameObject dropItemGameObject = poolManager.GetObject("DropBarrel");
        PickupItem pickup = dropItemGameObject.GetComponent<PickupItem>();

        pickup.transform.SetParent(GameManager.Singleton.globalBuoyancyParent);
        pickup.transform.position = position;
        pickup.transform.eulerAngles = rotation;
        pickup.amount = amount;
        pickup.itemInstance = itemInstance;
        pickup.requiresHands = true;
        pickup.isDropped = true;
        StartTrackingPickup(pickup);
    }
    //network methods 
    public override void Serialize_Create()
    {
        AddMessage(new Message_PickupManager_Create(Messages.CreateNetworkPickupManager, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.CreateNetworkPickupManager:
                Message_PickupManager_Create msgCreate = msg as Message_PickupManager_Create;
                foreach(RGD_PickupItem rgdItem in msgCreate.pickupItems)
                {
                    SpawnBarrel(rgdItem.rgdInstance.GetRealInstance(), rgdItem.DropPosition, rgdItem.DropRotation, rgdItem.amount);
                }
                break;
            case Messages.RemoveItem:
                Message_RemoveItem msgRemove = msg as Message_RemoveItem;   
                RemovePickupItem(trackedPickupItems[msgRemove.itemIndex], network.IsHost);
                break;
            case Messages.DropItem:
                Message_DropItem msgDropItem = msg as Message_DropItem;
                DropItem(msgDropItem.rgdInstance.GetRealInstance(), msgDropItem.DropPosition, msgDropItem.DropDirection, network.IsHost);
                break;
        }
    }
}

[System.Serializable]
public class Message_PickupManager_Create : Message_NetworkBehaviour
{
    public List<RGD_PickupItem> pickupItems = new List<RGD_PickupItem>();

    public Message_PickupManager_Create(Messages type, PickupObjectManager pickupObjectManager)
        : base(type, pickupObjectManager)
    {
        foreach(PickupItem item in PickupObjectManager.trackedPickupItems)
        {
            RGD_PickupItem rgdItem = new RGD_PickupItem(item);
            pickupItems.Add(rgdItem);
        }
    }
}
[System.Serializable]
public class Message_DropItem : Message_NetworkBehaviour
{
    public Vector3 DropPosition { get { return new Vector3(xPos, yPos, zPos); } set { xPos = value.x; yPos = value.y; zPos = value.z; } }
    private float xPos, yPos, zPos;
    public Vector3 DropDirection { get { return new Vector3(xDir, yDir, zDir); } set { xDir = value.x; yDir = value.y; zDir = value.z; } }
    private float xDir, yDir, zDir;
    public RGD_ItemInstance rgdInstance;

    public Message_DropItem(Messages type, MonoBehaviour_Network behaviour, ItemInstance droppedItem, Vector3 dropPosition, Vector3 dropDirection)
        : base(type, behaviour)
    {
        rgdInstance = new RGD_ItemInstance(droppedItem);
        DropPosition = dropPosition;
        DropDirection = dropDirection;
    }
}
