﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NetworkUpdateManager : MonoBehaviour 
{
    //public variables

    //private variables
    public static SortedDictionary<uint, MonoBehaviour_Network> NetworkedBehaviours = new SortedDictionary<uint, MonoBehaviour_Network>();
    private static Semih_Network network;
    private static uint globalBehaviourIndex;

    //Unity methods
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
    }
    void LateUpdate()
    {
        NetworkUI.Singleton.updateManagerText.text = string.Empty;

        //Fix dirty behaviours
        List<Message> reliableMessages = new List<Message>();
        List<Message> unreliableMessages = new List<Message>();
        for (int i = 0; i < NetworkedBehaviours.Count; i++)
        {
            KeyValuePair<uint, MonoBehaviour_Network> pair = NetworkedBehaviours.ElementAt(i);
            MonoBehaviour_Network networkBehaviour = pair.Value;

            #region UI Stuff
            if (pair.Key != pair.Value.BehaviourIndex)
            {
                NetworkUI.Singleton.Console("Indexes dont match");
            }

            if (NetworkUI.Singleton.canvas.gameObject.activeInHierarchy)
            {
                if (pair.Value != null && pair.Value.transform != null)
                {
                    if (pair.Value.transform.parent != null)
                        if (pair.Value.transform.parent.name.Contains("Reef"))
                            continue;
                    string name = pair.Value.transform.name;
                    string playerName = name.Contains(",") ? name.Split(',').Last() : string.Empty;
                    name = playerName == string.Empty ? pair.Value.transform.name : pair.Value.GetType() + "-" + playerName;
                    NetworkUI.Singleton.updateManagerText.text += "\nOI=" + pair.Value.ObjectIndex + ",BI=" + pair.Value.BehaviourIndex + "," + name;
                }
            }
            #endregion

            //Remove any dead behaviour
            if (networkBehaviour.IsKilled && network.IsHost)
            {
                //Save index of behaviour to be removed
                uint behaveIndex = networkBehaviour.BehaviourIndex;

                //Revive the dead behaviour, maybe it will be used again :P
                networkBehaviour.Revive();

                //Create and add a destroy message
                Message_NetworkBehaviour destroyMessage = new Message_NetworkBehaviour(Messages.RemoveNetworkBehaviour, networkBehaviour);
                reliableMessages.Add(destroyMessage);

                //Deserialize with destroy message
                networkBehaviour.Deserialize(destroyMessage);

                //Remove the behaviour with saved index, the behaviour may have been destroy and that is why we are using the saved index
                NetworkedBehaviours.Remove(behaveIndex);
                i--;
                continue;
            }

            //Send out any dirty behaviours
            else if (networkBehaviour.IsDirty)
            {
                //Is it the first time this behaviour is synced?
                if (networkBehaviour.IsFresh)
                {
                    networkBehaviour.IsFresh = false;
                    networkBehaviour.Serialize_Create();
                    reliableMessages.AddRange(networkBehaviour.MessagesStack);
                }
                else
                {
                    networkBehaviour.Serialize_Update();
                    unreliableMessages.AddRange(networkBehaviour.MessagesStack);
                }

                //Clean the behaviour so we know it is not dirty next update loop
                networkBehaviour.Clean();
            }
        }


        //Send out reliable messages
        if (reliableMessages.Count > 0)
        {
            if (network.IsHost)
                network.RPC(reliableMessages, Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
            else
                network.SendP2P(network.HostID, reliableMessages, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }

        //Send out unreliable messages
        if (unreliableMessages.Count > 0)
        {
            if (network.IsHost)
                network.RPC(unreliableMessages, Target.Other, Steamworks.EP2PSend.k_EP2PSendUnreliable, NetworkChannel.Channel_Game);
            else
                network.SendP2P(network.HostID, unreliableMessages, Steamworks.EP2PSend.k_EP2PSendUnreliable, NetworkChannel.Channel_Game);
        }
    }

    //public methods
    public static void AddBehaviour(MonoBehaviour_Network behaviour)
    {
        if (NetworkedBehaviours.ContainsKey(behaviour.BehaviourIndex))
            Debug.LogWarning("Key already preseent: " + behaviour.transform.name, behaviour);
        else
            NetworkedBehaviours.Add(behaviour.BehaviourIndex, behaviour);
    }
    public static void SetBehaviourDead(MonoBehaviour_Network behaviour)
    {
        if (network.IsHost)
        {
            if (NetworkedBehaviours.ContainsKey(behaviour.BehaviourIndex))
            {
                NetworkedBehaviours[behaviour.BehaviourIndex].Kill();
            }
        }
        else
        {
            network.SendP2P(network.HostID, new Message_NetworkBehaviour(Messages.RemoveNetworkBehaviour, behaviour), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }
    public static void SetIndexForBehaviour(MonoBehaviour_Network behaviour)
    {
        behaviour.ObjectIndex = SaveAndLoad.GetUniqueObjectIndex();
        behaviour.BehaviourIndex = GetUniqueBehaviourIndex();
    }
    public static void Deserialize(Packet_Multiple packet, Steamworks.CSteamID remoteID)
    {
        List<Message> packetMessages = packet.messages.ToList();

        for (int i = 0; i < packetMessages.Count; i++)
        {
            Message msg = packetMessages[i];
            if (msg is Message_NetworkBehaviour)
            {
                Message_NetworkBehaviour msgNetwork = msg as Message_NetworkBehaviour;
                if (NetworkedBehaviours.ContainsKey(msgNetwork.behaviourIndex))
                {
                    MonoBehaviour_Network networkBehaviour = NetworkedBehaviours[msgNetwork.behaviourIndex];
                    if (msgNetwork.type == Messages.RemoveNetworkBehaviour)
                    {
                        if (network.IsHost)
                        {
                            SetBehaviourDead(networkBehaviour);
                            packetMessages.RemoveAt(i);
                            i--;
                        }
                        else
                        {
                            networkBehaviour.Revive();
                            networkBehaviour.Deserialize(msgNetwork);
                            NetworkedBehaviours.Remove(networkBehaviour.BehaviourIndex);

                        }
                    }
                    else
                    {
                        networkBehaviour.Deserialize(msgNetwork);
                    }

                }
            }
        }

        if (network != null && network.IsHost && packetMessages.Count > 0)
        {
            packet.messages = packetMessages.ToArray();
            network.RPCExclude(packet, Target.Other, remoteID, NetworkChannel.Channel_Game);
        }
    }
    public static MonoBehaviour_Network GetBehaviourFromIndex(uint behaviourIndex)
    {
        if (NetworkedBehaviours.ContainsKey(behaviourIndex))
            return NetworkedBehaviours[behaviourIndex];
        else
            return null;
    }
    //private methods
    public static uint GetUniqueBehaviourIndex()
    {
        return ++globalBehaviourIndex;
    }
}
