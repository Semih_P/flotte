﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Network_Host : MonoBehaviour_NetworkTick
{
    //public variables
    [Header("Affected by tick")]
    public Network_Water water;
    public Raft raft;

    [Space(20)]
    public ObjectSpawnerManager spawnerManager;

    //private variables
    private Semih_Network network;

    //unity methods
    void Awake()
    {
        NetworkUpdateManager.SetIndexForBehaviour(this);
        NetworkUpdateManager.AddBehaviour(this);
        ComponentManager<Network_Host>.Set(this);
    }
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
        network.hostNetwork = this;

        if (network.IsHost)
        {
            SetDirty();
        }
    }
    void Update()
    {
        if (network.IsHost)
        {
            UpdateNetworkTick();
        }
    }

    //Network methods
    public override void Serialize_Create()
    {
        AddMessage(new Message_NetworkBehaviour(Messages.CreateHostNetwork, this));
    }
    public override void Serialize_Update()
    {
        AddMessage(new Message_Water(Messages.UpdateWater, this, water));
        AddMessage(new Message_Raft(Messages.UpdateRaft, this, raft));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.UpdateWater:
                Message_Water msgWater = msg as Message_Water;
                water.SetNetworkWaterTime(msgWater.waterTime);
                break;
            case Messages.UpdateRaft:
                Message_Raft msgRaft = msg as Message_Raft;
                raft.SetNetworkProperties(msgRaft);
                break;
            case Messages.AnchorChange:
                Message_AnchorChange msgAnchor = msg as Message_AnchorChange;
                if (msgAnchor.anchorChange > 0)
                    raft.AddAnchor();
                else
                    raft.RemoveAnchor();
                break;
        }
    }
}

[System.Serializable]
public class Message_Water : Message_NetworkBehaviour
{
    public float waterTime;

    public Message_Water(Messages type, MonoBehaviour_Network behaviour, Network_Water water)
        : base(type, behaviour)
    {
        this.waterTime = water.WaterTime;
    }
}
[System.Serializable]
public class Message_Raft : Message_NetworkBehaviour
{
    public float xPos, zPos;

    public Message_Raft(Messages type, MonoBehaviour_Network behaviour, Raft raft)
        :base (type, behaviour)
    {
        this.xPos = raft.transform.position.x;
        this.zPos = raft.transform.position.z;
    }
}
