﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Network_Host_Entities : MonoBehaviour 
{
	//public variables
    [SerializeField] private Shark sharkPrefab;
    [SerializeField] private bool spawnSharkBeginning = true;

	//private variables
    private Semih_Network network;

	//Unity methods
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();

        if (spawnSharkBeginning)
        {
            if (network.IsHost)
            {
                CreateSharkHost();
            }
        }
    }

	//public methods
    private void CreateSharkHost()
    {
        if (!network.IsHost)
            return;

        Vector3 spawnPos = GameManager.Singleton.globalBuoyancyParent.position + Vector3.down * 10 + new Vector3(Random.insideUnitCircle.x, 0, Random.insideUnitCircle.y) * 10;
        Shark shark = Instantiate(sharkPrefab, spawnPos, Quaternion.identity) as Shark;

        NetworkUpdateManager.SetIndexForBehaviour(shark);
        NetworkUpdateManager.AddBehaviour(shark);
        shark.SetDirty();

    }
    public void CreateSharkClient(Message_Shark msg)
    {
        if (network.IsHost)
            return;

        Vector3 spawnPos = new Vector3(msg.posX, msg.posY, msg.posZ);
        Shark shark = Instantiate(sharkPrefab, spawnPos, Quaternion.Euler(msg.rotX, msg.rotY, msg.rotZ)) as Shark;

        shark.ObjectIndex = msg.objectIndex;
        shark.BehaviourIndex = msg.behaviourIndex;
        shark.IsFresh = false;
        NetworkUpdateManager.AddBehaviour(shark);
    }

	//private methods
}