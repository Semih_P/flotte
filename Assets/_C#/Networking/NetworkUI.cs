﻿using Steamworks;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NetworkUI : MonoBehaviour 
{
    //public variables
    public static NetworkUI Singleton;
    public Semih_Network network;
    public Text console;
    public Text infoText;
    public Text updateManagerText;

    //private variables
    private int textColorCount = 0;
    [HideInInspector]public GameObject canvas;

    //Unity methods
    void Awake()
    {
        if (Singleton != null)
        {
            Destroy(gameObject);
            return;
        }
        Singleton = this;

        canvas = transform.FindChild("Canvas").gameObject;
        canvas.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ClearConsole();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            canvas.SetActive(!canvas.gameObject.activeInHierarchy);
        }

        if (canvas.gameObject.activeInHierarchy)
            GUI();
    }

    //public methods
    public void Console(string text)
    {
        if (!canvas.gameObject.activeInHierarchy)
            return;
        if (console.text.Length > 1000)
            ClearConsole();

        textColorCount++;
        string color = (textColorCount % 2 == 0) ? "<color=#FFFFFFFF>" : "<color=#B4B4B4FF>";
        console.text += "\n" + color + text + "</color>";

    }
    public void ClearConsole()
    {
        console.text = string.Empty;
    }

    //UI connections
    public void CreateLobby()
    {
        network.CreateLobby();
    }
    public void JoinLobby(CSteamID id)
    {
        network.JoinLobby(id);
    }
    public void HostGame()
    {
        network.HostGame();
    }
    public void JoinGame(CSteamID id)
    {
        network.TryToJoinGame(id);
    }
    public void DisconnectFromGame()
    {
        network.DisconnectFromHost();
    }
    public void ClearDebug()
    {
        ClearConsole();
    }

    //private methods
    private void GUI()
    {
        if (!SteamManager.Initialized)
            return;

        infoText.text = string.Empty;
        infoText.text += "LocalUser = " + network.PersonaName + "_" + network.LocalSteamID.m_SteamID;
        
        if (network.HostID.m_SteamID != 0)
        {
            infoText.text += "\nHostUser = " + SteamFriends.GetFriendPersonaName(network.HostID) + "_" + network.HostID;
            infoText.text += "\nIs host = " + network.IsHost;
            //foreach(KeyValuePair<CSteamID, Network_Player> pair in network.remoteUsers)
            //{
            //    P2PSessionState_t sessionState;
            //    bool connected = SteamNetworking.GetP2PSessionState(pair.Key, out sessionState);
            //    infoText.text +=
            //        "\nName = " + SteamFriends.GetFriendPersonaName(pair.Key) +
            //        ", Connected = " + connected +
            //        ", Relay = " + sessionState.m_bUsingRelay;
            //}

            if (network.hostNetwork != null)
            {
                infoText.text += "\nWaterTime " + network.hostNetwork.water.WaterTime;
            }
        }
    }
}
