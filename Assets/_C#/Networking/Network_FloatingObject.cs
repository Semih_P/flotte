﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Network_FloatingObject : MonoBehaviour_Network 
{
	//public variables
    public ObjectSpawnType spawnType;
    [HideInInspector] public uint spawnerIndex;
    [HideInInspector] public uint floatingObjectIndex;
    [HideInInspector] public ObjectSpawner spawner;

	//private variables

	//Unity methods

	//public methods

	//private methods

    //Networking methods
    public override void Serialize_Create()
    {
        Serialize_Update();
    }
    public override void Serialize_Update()
    {
        AddMessage(new Message_FloatingObject_Create(Messages.CreateFloatingObject, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        if (msg.type == Messages.RemoveNetworkBehaviour)
        {
            spawner.spawnedObjects.Remove(this);
            PoolManager.ReturnObject(gameObject);
        }
    }
}

[System.Serializable]
public class Message_FloatingObject_Create : Message_NetworkBehaviour
{
    public uint spawnerIndex;
    public uint floatingObjectIndex;
    public float xPos, yPos, zPos;
    public float xRot, yRot, zRot;

    public Message_FloatingObject_Create(Messages type, Network_FloatingObject floatingObject)
        : base(type, floatingObject)
    {
        this.spawnerIndex = floatingObject.spawnerIndex;
        this.floatingObjectIndex = floatingObject.floatingObjectIndex;
        this.xPos = floatingObject.transform.position.x;
        this.yPos = floatingObject.transform.position.y;
        this.zPos = floatingObject.transform.position.z;
        this.xRot = floatingObject.transform.eulerAngles.x;
        this.yRot = floatingObject.transform.eulerAngles.y;
        this.zRot = floatingObject.transform.eulerAngles.z;
    }
}
[System.Serializable]
public class Message_FloatingObjectReef_Create : Message_FloatingObject_Create
{
    public bool[] activeReefIndexes;
    public Message_FloatingObjectReef_Create(Messages type, Network_Reef networkReef)
        : base(type, networkReef)
    {
        activeReefIndexes = new bool[networkReef.reef.pickups.Count];
        for (int i = 0; i < activeReefIndexes.Length; i++)
        {
            activeReefIndexes[i] = networkReef.reef.pickups[i].gameObject.activeSelf;
        }
    }
}
