﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Network_Water : MonoBehaviour 
{
	//public variables
    public float WaterTime { get { return playwayWater.Time; } }

    [Range(0f, 100f)]
    [SerializeField]
    private float lerpSpeed = 20f;

	//private variables
    private Semih_Network network;
    private PlayWay.Water.Water playwayWater;
    private float networkTime;

	//Unity methods
    void Start()
    {
        playwayWater = GetComponent<PlayWay.Water.Water>();
        network = ComponentManager<Semih_Network>.Get();
    }
	void Update ()
 	{
        if (!network.IsHost)
        {
            playwayWater.Time = Mathf.Lerp(playwayWater.Time, networkTime, Time.deltaTime * lerpSpeed);
        }
	}

	//public methods
    public void SetNetworkWaterTime(float newWaterTime)
    {
        networkTime = newWaterTime;
    }
	//private methods
}
