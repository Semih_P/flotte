﻿using Steamworks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Network_Player : MonoBehaviour_NetworkTick
{
    //properties
    public bool IsLocalPlayer { get { return isLocalPlayer; } }
    private bool isLocalPlayer;

    public Semih_Network Network 
    { 
        get { return network; }
        set { network = value; } 
    }
    private Semih_Network network;

    //public variables
    [HideInInspector] public CSteamID steamID;
    [HideInInspector] public string playerName;

    [Header("Components")]
    public SkinnedMeshRenderer fullBodyMesh;
    public SkinnedMeshRenderer armMesh;
    public AnimatorOverrideController firstPersonController;
    public TextMesh playerNameTextMesh;
    public Transform CameraTransform;
    public Transform playerPivot;

    public Camera Camera { get { return camera; } private set { camera = value; } }
    private new Camera camera;

    public PersonController Controller { get { return controller; } private set { controller = value; } }
    private PersonController controller;

    public PlayerAnimator Animator { get { return animator; } private set { animator = value; } }
    private PlayerAnimator animator;

    public PlayerItemManager ItemManager { get { return itemManager; } private set { itemManager = value; } }
    private PlayerItemManager itemManager;

    public BlockCreator BlockCreator { get { return blockCreator; } private set { blockCreator = value; } }
    private BlockCreator blockCreator;

    public PlayerNetworkManager PlayerNetworkManager { get { return playerNetworkManager; } private set { playerNetworkManager = value; } }
    private PlayerNetworkManager playerNetworkManager;

    //private variables

    //Unity methods
    void Awake()
    {
        this.Controller = GetComponent<PersonController>();
        this.Animator = GetComponentInChildren<PlayerAnimator>(true);
        this.ItemManager = GetComponentInChildren<PlayerItemManager>(true);
        this.Camera = GetComponentInChildren<Camera>(true);
        this.BlockCreator = GetComponentInChildren<BlockCreator>(true);
        this.PlayerNetworkManager = GetComponentInChildren<PlayerNetworkManager>(true);
    }
    void Update()
    {
        if (IsLocalPlayer)
        {
            UpdateNetworkTick();
        }
    }

    //public methods
    /// <summary>
    /// Called on all players
    /// </summary>
    public void OnPlayerCreated(Semih_Network network, bool isLocalPlayer, CSteamID steamID, string playerName)
    {
        this.network = network;
        this.isLocalPlayer = isLocalPlayer;
        this.steamID = steamID;
        this.playerName = playerName;

        //Setup components
        this.playerNameTextMesh.text = transform.name = playerName;
        this.transform.name = playerName;

        //Remove non local behaviours
        if (!IsLocalPlayer)
        {
            Destroy(GetComponent<MouseLook>());

            //Clear camera scripts
            Destroy(playerPivot.GetComponent<MouseLook>());
            Destroy(Camera.GetComponent<AudioListener>());
            Destroy(Camera.GetComponent<GUILayer>());
            Destroy(Camera.GetComponent<AzureFog>());
            Destroy(Camera.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>());
            Destroy(Camera.GetComponent<PlayWay.Water.WaterDropsIME>());
            Destroy(Camera.GetComponent<PlayWay.Water.UnderwaterIME>());
            Destroy(Camera.GetComponent<PlayWay.Water.WaterCamera>());
            Destroy(Camera.GetComponent<AudioReverbFilter>());
            Destroy(Camera.GetComponent<Camera>());
            Camera = null;
        }
        else
        {
            //Turn of fullbody mesh
            this.fullBodyMesh.gameObject.SetActive(false);
            this.armMesh.gameObject.SetActive(true);
            this.Animator.anim.runtimeAnimatorController = firstPersonController;

            ComponentManager<Player>.Set(GetComponent<Player>());
            ComponentManager<Network_Player>.Set(this);
        }

        if (Network.IsHost)
        {
            //Assign index to all child networks
            List<MonoBehaviour_Network> networkUpdateBehaviours = GetComponentsInChildren<MonoBehaviour_Network>(true).ToList();
            networkUpdateBehaviours.Remove(this);
            networkUpdateBehaviours.Insert(0, this);
            foreach (MonoBehaviour_Network b in networkUpdateBehaviours)
            {
                b.transform.name = b.GetType() + ", " + playerName;
                NetworkUpdateManager.SetIndexForBehaviour(b);
                NetworkUpdateManager.AddBehaviour(b);
            }

            if (IsLocalPlayer)
            {
                //Create game layout
                BlockCreator.CreateNewGameLayout();
            }
        }
    }
    /// <summary>
    /// Called when a player is created as a remote player (by a create player message)
    /// </summary>
    public void SetIndex(uint objectIndex, uint behaviourIndex)
    {
        ObjectIndex = objectIndex;
        BehaviourIndex = behaviourIndex;
        NetworkUpdateManager.AddBehaviour(this);

        MonoBehaviour_Network[] networkUpdateBehaviours = GetComponentsInChildren<MonoBehaviour_Network>(true);
        uint newOIndex = objectIndex + 1;
        uint newBIndex = behaviourIndex + 1;
        foreach (MonoBehaviour_Network b in networkUpdateBehaviours)
        {
            b.transform.name = b.GetType() + ", " + playerName;
            if (b != this)
            {
                b.ObjectIndex = newOIndex++;
                b.BehaviourIndex = newBIndex++;
                NetworkUpdateManager.AddBehaviour(b);
            }
        }
    }
    public void SendP2P(Message message, EP2PSend sendType, NetworkChannel channel)
    {
        Network.SendP2P(Network.HostID, message, sendType, channel);
    }

    //private methods

    //Serialize methods
    public override void Serialize_Create()
    {
        AddMessage(new Message_Player_Create(Messages.CreatePlayer, this, steamID, playerName));
    }
    public override void Serialize_Update()
    {
        AddMessage(new Message_Player_Update(Messages.Update, this, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.Update:
                Message_Player_Update msgUpdate = msg as Message_Player_Update;
                Controller.SetNetworkProperties(msgUpdate);
                Animator.SetNetworkProperties(msgUpdate);
                break;
        }
    }
}

[System.Serializable]
public class Message_Player_Create : Message_NetworkBehaviour
{
    public ulong steamID;
    public string personaName;

    public Message_Player_Create(Messages type, MonoBehaviour_Network behaviour, CSteamID steamID, string personaName) : base (type, behaviour)
    {
        this.steamID = steamID.m_SteamID;
        this.personaName = personaName;
    }
}
[System.Serializable]
public class Message_Player_Update : Message_NetworkBehaviour
{
    public ControllerType controllerType;
    public float posX, posY, posZ;
    public float rotX, rotY;
    public float animVelocityX, animVelocityY, animVelocityZ, animVelocityW;
    public bool animHammerAxeHit, animHookThrow;
    public string[] anim_triggers;

    public Message_Player_Update(Messages type, MonoBehaviour_Network behaviour, Network_Player playerNetwork) : base (type, behaviour)
    {
        //Controller variables
        PersonController controller = playerNetwork.Controller;
        this.controllerType = controller.controllerType;
        if (controller.controllerType == ControllerType.Ground)
        {
            this.posX = controller.transform.localPosition.x;
            this.posY = controller.transform.localPosition.y;
            this.posZ = controller.transform.localPosition.z;
        }
        else if (controllerType == ControllerType.Water)
        {
            this.posX = controller.transform.position.x;
            this.posY = controller.transform.position.y;
            this.posZ = controller.transform.position.z;
        }

        this.rotY = controller.transform.eulerAngles.y;
        this.rotX = playerNetwork.playerPivot.eulerAngles.x;

        //Animation variables
        Animator anim = playerNetwork.Animator.anim;
        this.anim_triggers = playerNetwork.Animator.networkAnimTriggers.ToArray();
        playerNetwork.Animator.networkAnimTriggers.Clear();
        this.animVelocityX = anim.GetFloat("VelocityX");
        this.animVelocityY = anim.GetFloat("VelocityY");
        this.animVelocityZ = anim.GetFloat("VelocityZ");
        this.animVelocityW = anim.GetFloat("VelocityW");
        this.animHammerAxeHit = anim.GetBool("HammerAxeHit");
        this.animHookThrow = anim.GetBool("HookThrow");
    }
}
