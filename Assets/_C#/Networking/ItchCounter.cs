﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class ItchDonator : IComparable <ItchDonator>
{
    public string mail;
    public float amount;

    public ItchDonator(float amount, string mail)
    {
        this.mail = mail;
        this.amount = amount;
    }

    public int CompareTo(ItchDonator other)
    {
        return amount.CompareTo(other.amount);
    }
}
public class ItchCounter : MonoBehaviour 
{
	//public variables

	//private variables
    public List<ItchDonator> amounts = new List<ItchDonator>();

	//Unity methods
	void Start () 
	{
        StreamReader stream = new StreamReader("purchasesNew.txt");

        string line = string.Empty;
        while ((line = stream.ReadLine()) != null)
        {
            string[] parts = line.Split(',');

            if (parts.Length >= 3)
            {
                amounts.Add(new ItchDonator(float.Parse(parts[2]), parts[5]));
            }
        }
        stream.Close();

        amounts.Sort((a, b) => a.amount.CompareTo(b.amount));
        amounts.Reverse();
        amounts.RemoveRange(30, amounts.Count - 30);
	}
	void Update ()
 	{
	}

	//public methods

	//private methods
}
