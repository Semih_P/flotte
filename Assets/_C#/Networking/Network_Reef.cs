﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Network_Reef : Network_FloatingObject 
{
	//public variables
    [HideInInspector]public Reef reef;
    
	//private variables
    private Semih_Network network;

	//Unity methods
	void Awake () 
	{
        reef = GetComponent<Reef>();
	}
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
    }

	//public methods
    public void RemoveItem(PickupItem pickup, bool useNetwork)
    {
        int index = reef.pickups.IndexOf(pickup);
        reef.pickups[index].gameObject.SetActive(false);
        if (useNetwork)
            network.RPC(new Message_RemoveItem(Messages.RemoveItem, this, index), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }

	//private methods

    //Networking methods
    public override void Serialize_Update()
    {
        AddMessage(new Message_FloatingObjectReef_Create(Messages.CreateFloatingObject, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        base.Deserialize(msg);
        if (msg.type == Messages.RemoveItem)
        {
            Message_RemoveItem msgRemove = msg as Message_RemoveItem;
            RemoveItem(reef.pickups[msgRemove.itemIndex], network.IsHost);
        }
    }
}

[System.Serializable]
public class Message_RemoveItem : Message_NetworkBehaviour
{
    public int itemIndex;
    public Message_RemoveItem(Messages type, MonoBehaviour_Network behaviour, int itemIndex)
        :base(type, behaviour)
    {
        this.itemIndex = itemIndex;
    }
}
