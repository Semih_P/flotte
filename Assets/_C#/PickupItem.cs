﻿using UnityEngine;
using System.Collections;

public class PickupItem : MonoBehaviour
 {
 	//public variables
    public Item_Base item;
    public ItemInstance itemInstance;
    public MonoBehaviour_Network networkBehaviour;
    public int amount;
    public bool canBePickedUp = true;
    public bool requiresHands = false;
    public bool onReef = false;
    public bool trackedByPickupManager;

    [Header("Use random dropper?")]
    public RandomDropper dropper;

    [Header("Drop settings")]
    public bool isDropped;

 	//private variables

 	//Unity methods
    void OnMouseExit()
    {
        CanvasHelper.Singleton.SetDisplayText(false);
    }

	//public methods
    public Item_Base GetItem()
    {
        if (dropper == null)
        {
            return item;
        }
        else
        {
            return dropper.GetItemToSpawn();
        }
    }

	//private methods
}
