﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookingStandManager : MonoBehaviour_Network 
{
    public static List<CookingStand> allCookingStands = new List<CookingStand>();

	//public variables

	//private variables
    private Network_Player playerNetwork;

	//Unity methods
	void Start()
	{
        playerNetwork = GetComponentInParent<Network_Player>();
	}

	//public methods
    public void InsertItem(Item_Base itemToInsert, CookingSlot slot, CookingStand stand, bool useNetwork)
    {
        stand.InsertItem(itemToInsert, slot, playerNetwork.IsLocalPlayer);
        playerNetwork.Animator.SetAnimation(PlayerAnimation.Trigger_Plant, true);

        if (useNetwork)
        {
            playerNetwork.Network.RPC(new Message_CookingStand_Insert(Messages.CookingStand_Insert, this, itemToInsert, stand), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }
    public void AddFuel(CookingStand stand, bool useNetwork)
    {
        stand.AddFuel(playerNetwork.IsLocalPlayer);
        playerNetwork.Animator.SetAnimation(PlayerAnimation.Trigger_Plant, true);

        if (useNetwork)
            playerNetwork.Network.RPC(new Message_CookingStand(Messages.CookingStand_AddFuel, this, stand), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
    public void CollectItem(CookingStand stand, CookingSlot slotToCollect, bool useNetwork)
    {
        stand.CollectItem(slotToCollect, playerNetwork.IsLocalPlayer);
        playerNetwork.Animator.SetAnimation(PlayerAnimation.Trigger_GrabItem);
        if (useNetwork)
            playerNetwork.Network.RPC(new Message_CookingStand_Collect(Messages.CookingStand_Collect, this, stand, slotToCollect), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
    public CookingStand GetCookingStandByObjectIndex(uint objectIndex)
    {
        return allCookingStands.Find(s => s.ObjectIndex == objectIndex);
    }

	//private methods

    //network methods
    public override void Serialize_Create()
    {
        AddMessage(new Message_CookingStand_Create(Messages.CookingStand_Create, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch (msg.type)
        {
            case Messages.CookingStand_Create:
                Message_CookingStand_Create msgCreate = msg as Message_CookingStand_Create;
                msgCreate.RestoreCookingStands(msgCreate, this);
                break;

            case Messages.CookingStand_Insert:
                Message_CookingStand_Insert msgInsert = msg as Message_CookingStand_Insert;
                Item_Base itemToInsert = ItemManager.GetItemByIndex(msgInsert.itemToInsertIndex);
                CookingStand stand = GetCookingStandByObjectIndex(msgInsert.cookingstandObjectIndex);

                CookingSlot emptySlot = stand.CanInsertItem(itemToInsert);
                if (emptySlot != null)
                    InsertItem(itemToInsert, emptySlot, stand, playerNetwork.Network.IsHost);
                break;

            case Messages.CookingStand_AddFuel:
                Message_CookingStand msgAddfuel = msg as Message_CookingStand;
                AddFuel(GetCookingStandByObjectIndex(msgAddfuel.cookingstandObjectIndex), playerNetwork.Network.IsHost);
                break;

            case Messages.CookingStand_Collect:
                Message_CookingStand_Collect msgCollect = msg as Message_CookingStand_Collect;
                CookingStand standCollect = GetCookingStandByObjectIndex(msgCollect.cookingstandObjectIndex);
                CookingSlot slotToCollect = standCollect.cookingSlots[msgCollect.slotToCollectIndex];
                CollectItem(standCollect, slotToCollect, playerNetwork.Network.IsHost);
                break;
        }
    }
}

[System.Serializable]
public class Message_CookingStand_Create : Message_NetworkBehaviour
{
    public List<RGD_CookingStand> rgdCookingStands = new List<RGD_CookingStand>();
    public Message_CookingStand_Create(Messages type, MonoBehaviour_Network behaviour)
        : base(type, behaviour)
    {
        for (int i = 0; i < CookingStandManager.allCookingStands.Count; i++)
        {
            CookingStand stand = CookingStandManager.allCookingStands[i];
            RGD_CookingStand rgdStand = new RGD_CookingStand(stand);
            rgdCookingStands.Add(rgdStand);
        }
    }

    public void RestoreCookingStands(Message_CookingStand_Create msgCreate, CookingStandManager cookingStandManager)
    {
        for (int i = 0; i < rgdCookingStands.Count; i++)
        {
            RGD_CookingStand rgdStand = rgdCookingStands[i];
            CookingStand stand = cookingStandManager.GetCookingStandByObjectIndex(rgdStand.cookingStandObjectIndex);

            rgdStand.RestoreStand(stand);
        }
    }
}
[System.Serializable]
public class Message_CookingStand : Message_NetworkBehaviour
{
    public uint cookingstandObjectIndex;

    public Message_CookingStand(Messages type, MonoBehaviour_Network behaviour, CookingStand stand)
        : base(type, behaviour)
    {
        cookingstandObjectIndex = stand.ObjectIndex;
    }
}
[System.Serializable]
public class Message_CookingStand_Insert : Message_CookingStand
{
    public int itemToInsertIndex;

    public Message_CookingStand_Insert(Messages type, MonoBehaviour_Network behaviour, Item_Base itemToInsert, CookingStand stand)
        : base(type, behaviour, stand)
    {
        itemToInsertIndex = itemToInsert.UniqueIndex;
    }
}
[System.Serializable]
public class Message_CookingStand_Collect : Message_CookingStand
{
    public int slotToCollectIndex;

    public Message_CookingStand_Collect(Messages type, MonoBehaviour_Network behaviour, CookingStand stand, CookingSlot slotToCollect)
        : base(type, behaviour, stand)
    {
        slotToCollectIndex = stand.GetIndexOfCookingSlot(slotToCollect);
    }
}
