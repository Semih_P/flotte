﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anchor_Throwable_Stand : MonoBehaviour_NetworkTick 
{
	//public variables

	//private variables
    [SerializeField] private GameObject pickupModel = null;
    [SerializeField] private Item_Base usableItemOnPickup = null;
    [SerializeField] private Rope rope = null;
    [SerializeField] private Transform ropeConnectionTransform = null;
    private static Raft raft;
    private Anchor_Throwable anchor_Throwable;
    private Anchor thrownAnchor;
    private bool fullyAnchored;
    private Item_Base previousItemInHand;
    private Semih_Network network;
    private Network_Player playerNetwork;

	//Unity methods
	void Start () 
	{
        raft = ComponentManager<Raft>.Get();
        network = ComponentManager<Semih_Network>.Get();
	}
    public void OnBlockPlaced()
    {
        if (network == null)
            network = ComponentManager<Semih_Network>.Get();

        if (network.IsHost)
        {
            NetworkUpdateManager.SetIndexForBehaviour(this);
            NetworkUpdateManager.AddBehaviour(this);
        }
        else
        {
            Block block = GetComponent<Block>();
            ObjectIndex = block.ObjectIndex + 1;
            BehaviourIndex = block.BehaviourIndex + 1;
            NetworkUpdateManager.AddBehaviour(this);
        }
    }
	void Update () 
	{
        if (IsFullyAnchored())
        {
            if (!rope.gameObject.activeInHierarchy)
                rope.gameObject.SetActive(true);
            return;
        }

        if (IsConnected())
        {
            //Set rope position
            rope.SetPosition(0, anchor_Throwable.anchorParent);
            rope.SetPosition(1, ropeConnectionTransform.position);

            //Disconnect with right click
            if (Input.GetButtonDown("LeftClick"))
            {
                playerNetwork.Animator.anim.SetBool("HookThrow", true);
            }
            else if (Input.GetButtonDown("RightClick") && !Input.GetButton("LeftClick"))
            {
                if (network.IsHost)
                {
                    DisconnectThrowableWithStand(true);
                }
                else
                {
                    network.SendP2P(network.HostID, new Message_NetworkBehaviour(Messages.DisconnectThrowableAnchorStand, this), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                }
            }
        }
        else if (thrownAnchor != null)
        {
            //Set rope position
            rope.SetPosition(0, thrownAnchor.transform);
            rope.SetPosition(1, ropeConnectionTransform.position);

            if (!IsFullyAnchored())
            {
                //Detect bottom
                if (thrownAnchor.transform.position.y < GameManager.OceanBottom)
                {
                    OnAnchorHit();
                }
            }
        }
        else
        {
            rope.ResetParent();
            rope.SetPosition(0, ropeConnectionTransform.position);
            rope.SetPosition(1, pickupModel.transform.position);
        }
	}

	//public methods
    public bool HasThrownAnchor()
    {
        return thrownAnchor != null;
    }
    public bool IsBusy()
    {
        return anchor_Throwable != null;
    }
    public bool IsFullyAnchored()
    {
        return fullyAnchored;
    }
    public void OnThrowableThrow(Anchor thrownAnchor)
    {
        //Save reference to throwable object
        this.thrownAnchor = thrownAnchor;
        this.thrownAnchor.SetReciever(transform);
        this.thrownAnchor.transform.SetParent(null);

        //Deselect hand item
        playerNetwork.Animator.anim.SetBool("HookThrow", false);
        playerNetwork.ItemManager.SelectUsable(null);
        if (playerNetwork.IsLocalPlayer)
        {
            PlayerItemManager.IsBusy = false;
        }
    }
    public void OnAnchorHit()
    {
        if (!network.IsHost)
            return;

        if (!fullyAnchored)
        {
            this.thrownAnchor.GetComponent<Rigidbody>().isKinematic = true;
            fullyAnchored = true;

            raft.AddAnchor();

            network.RPC(new Message_ThrowableStandCreate(Messages.ThrowableStandCreate, this, thrownAnchor.transform), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }
    public void DestroyStand()
    {
        raft.RemoveAnchor();
        NetworkUpdateManager.SetBehaviourDead(GetComponentInParent<Block>());
    }
    public void ConnectStandWithThrowable(bool useNetwork, Network_Player player)
    {
        if (playerNetwork == player)
            return;

        //Connect stand with throwable
        pickupModel.SetActive(false);
        anchor_Throwable = player.GetComponentInChildren<Anchor_Throwable>();
        anchor_Throwable.anchor_stand = this;
        playerNetwork = player;

        playerNetwork.Animator.SetAnimation(PlayerAnimation.Index_2_Hook);

        //Select item and set hands to busy
        if (player.IsLocalPlayer)
        {
            previousItemInHand = playerNetwork.ItemManager.useItemController.GetCurrentItemInHand();
            playerNetwork.ItemManager.SelectUsable(usableItemOnPickup);
            PlayerItemManager.IsBusy = true;
        }

        if (useNetwork)
        {
            network.RPC(new Message_NetworkBehaviour_SteamID(Messages.ConnectThrowableAnchorStand, this, network.GetIDFromPlayer(player)), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }
    public void DisconnectThrowableWithStand(bool useNetwork)
    {
        //Disconnect stand
        anchor_Throwable.anchor_stand = null;
        pickupModel.SetActive(true);

        //Deselect hand item
        if (playerNetwork.IsLocalPlayer)
        {
            playerNetwork.ItemManager.SelectUsable(previousItemInHand);
            PlayerItemManager.IsBusy = false;
        }

        //Hide rope
        rope.gameObject.SetActive(false);


        if (useNetwork)
        {
            network.RPC(new Message_NetworkBehaviour(Messages.DisconnectThrowableAnchorStand, this), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }

        playerNetwork = null;
        anchor_Throwable = null;
    }

	//private methods
    private bool IsConnected()
    {
        if (anchor_Throwable == null)
            return false;
        return anchor_Throwable.anchor_stand == this;
    }

    //network methods
    public override void Serialize_Create()
    {
        if (HasThrownAnchor())
        {
            if (IsFullyAnchored())
            {
                AddMessage(new Message_ThrowableStandCreate(Messages.ThrowableStandCreate, this, thrownAnchor.transform));
            }
        }
        if (IsConnected())
        {
            AddMessage(new Message_NetworkBehaviour_SteamID(Messages.ConnectThrowableAnchorStand, this, network.GetIDFromPlayer(playerNetwork)));
        }
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.ThrowableStandCreateInAir:
                pickupModel.SetActive(false);
                rope.gameObject.SetActive(false);
                break;
            case Messages.ThrowableStandCreate:
                if (thrownAnchor != null)
                    Destroy(thrownAnchor);
                Message_ThrowableStandCreate msgCreate = msg as Message_ThrowableStandCreate;
                pickupModel.SetActive(true);
                pickupModel.transform.SetParent(null);
                pickupModel.transform.position = msgCreate.Position;
                pickupModel.transform.eulerAngles = msgCreate.Rotation;

                fullyAnchored = true;
                
                rope.SetPosition(0, ropeConnectionTransform);
                rope.SetPosition(1, pickupModel.transform);
                break;
            case Messages.ConnectThrowableAnchorStand:
                Message_NetworkBehaviour_SteamID msgID = msg as Message_NetworkBehaviour_SteamID;
                ConnectStandWithThrowable(network.IsHost, network.remoteUsers[msgID.GetID()]);
                break;
            case Messages.DisconnectThrowableAnchorStand:
                DisconnectThrowableWithStand(network.IsHost);
                break;
        }
    }
}

[System.Serializable]
public class Message_ThrowableStandCreate : Message_NetworkBehaviour
{
    public Vector3 Position { get { return new Vector3(xPos, yPos, zPos); } set { xPos = value.x; yPos = value.y; zPos = value.z; } }
    public Vector3 Rotation { get { return new Vector3(xRot, yRot, zRot); } set { xRot = value.x; yRot = value.y; zRot = value.z; } }
    private float xPos, yPos, zPos;
    private float xRot, yRot, zRot;

    public Message_ThrowableStandCreate(Messages type, MonoBehaviour_Network behaviour, Transform anchor)
        :base (type, behaviour)
    {
        Position = anchor.position;
        Rotation = anchor.eulerAngles;
    }
}