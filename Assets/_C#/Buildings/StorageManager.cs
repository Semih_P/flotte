﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageManager : MonoBehaviour_Network 
{
	//public variables
    public static List<Storage_Small> allStorages = new List<Storage_Small>();

	//private variables
    private Network_Player playerNetwork;
    private Storage_Small currentLocalStorage;

	//Unity methods
    void Start()
    {
        playerNetwork = GetComponentInParent<Network_Player>();
    }
    void Update()
    {
        if (!playerNetwork.IsLocalPlayer)
            return;

        if (currentLocalStorage != null)
        {
            if (currentLocalStorage.IsOpen && currentLocalStorage.CanCloseWithUseButton)
            {
                if (Input.GetButtonDown("UseButton") || Input.GetButtonDown("Inventory") || Input.GetButtonDown("Cancel"))
                {
                    if (playerNetwork.Network.IsHost)
                        CloseStorage(currentLocalStorage, true);
                    else
                        playerNetwork.SendP2P(new Message_Storage_Close(Messages.Storage_Close, this, currentLocalStorage), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

                    currentLocalStorage = null;
                }
            }
        }
    }

	//public methods
    public void OpenStorage(Storage_Small storage, bool useNetwork)
    {
        if (storage.IsOpen)
            return;

        if (playerNetwork.IsLocalPlayer)
            currentLocalStorage = storage;

        storage.Open(playerNetwork.IsLocalPlayer);
        if (useNetwork)
            playerNetwork.Network.RPC(new Message_Storage(Messages.Storage_Open, this, storage), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
    public void CloseStorage(Storage_Small storage, bool useNetwork)
    {
        if (!storage.IsOpen)
            return;

        storage.Close(playerNetwork.IsLocalPlayer);

        if (useNetwork)
            playerNetwork.Network.RPC(new Message_Storage_Close(Messages.Storage_Close, this, storage), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
    public Storage_Small GetStorageByObjectIndex(uint index)
    {
        return allStorages.Find(s => s.ObjectIndex == index);
    }

	//private methods

    //network methods
    public override void Serialize_Create()
    {
        AddMessage(new Message_Storage_Create(Messages.StorageManager_Create, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.Storage_Open:
                Message_Storage msgOpen = msg as Message_Storage;
                OpenStorage(GetStorageByObjectIndex(msgOpen.storageObjectIndex), playerNetwork.Network.IsHost);
                break;
            case Messages.Storage_Close:
                Message_Storage_Close msgClose = msg as Message_Storage_Close;
                Storage_Small storageClose = GetStorageByObjectIndex(msgClose.storageObjectIndex);
                msgClose.rgdInventory.RestoreInventory(storageClose.GetInventoryReference());

                CloseStorage(storageClose, playerNetwork.Network.IsHost);
                break;
            case Messages.StorageManager_Create:
                Message_Storage_Create msgCreate = msg as Message_Storage_Create;
                msgCreate.RestoreStorages(msgCreate, this);
                break;
        }
    }
}

[System.Serializable]
public class Message_Storage_Create : Message_NetworkBehaviour
{
    public List<RGD_Inventory> rgdInventories = new List<RGD_Inventory>();
    public Message_Storage_Create(Messages type, MonoBehaviour_Network behaviour)
        : base(type, behaviour)
    {
        for (int i = 0; i < StorageManager.allStorages.Count; i++)
        {
            Storage_Small storage = StorageManager.allStorages[i];
            Inventory inventory = storage.GetInventoryReference();
            RGD_Inventory rgdInventory = new RGD_Inventory(storage.ObjectIndex, inventory);
            rgdInventories.Add(rgdInventory);
        }
    }

    public void RestoreStorages(Message_Storage_Create msgCreate, StorageManager storageManager)
    {
        for (int i = 0; i < msgCreate.rgdInventories.Count; i++)
        {
            RGD_Inventory rgdInventory = msgCreate.rgdInventories[i];
            Inventory inventory = storageManager.GetStorageByObjectIndex(rgdInventory.storageObjectIndex).GetInventoryReference();

            rgdInventory.RestoreInventory(inventory);
        }
    }
}
[System.Serializable]
public class Message_Storage : Message_NetworkBehaviour
{
    public uint storageObjectIndex;
    public Message_Storage(Messages type, MonoBehaviour_Network behaviour, Storage_Small storage)
        : base(type, behaviour)
    {
        storageObjectIndex = storage.ObjectIndex;
    }
}
[System.Serializable]
public class Message_Storage_Close : Message_Storage
{
    public RGD_Inventory rgdInventory;

    public Message_Storage_Close(Messages type, MonoBehaviour_Network behaviour, Storage_Small storage)
        :base (type, behaviour, storage)
    {
        rgdInventory = new RGD_Inventory(storage.ObjectIndex, storage.GetInventoryReference());
    }
}
