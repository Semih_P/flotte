﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterExtractor : MonoBehaviour 
{
	//public variables

    //private variables
    private static PlayerInventory inventory;

    [Header("Item to extract from")]
    [Header("Finds extract items by child named: ExtractItemParent")]
    [SerializeField] private float extractionTime = 5f;
    [SerializeField] private BuildingUI buildingUI;
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject cup;
    private GameObject[] extractItems;
    private CanvasHelper canvas;
    private float extractionTimer;

	//Unity methods
	protected virtual void Awake () 
	{
        InitializeExtractItems();
        HideAllExtractItems();
	}
    protected virtual void Start()
    {
        canvas = CanvasHelper.Singleton;
        if (inventory == null)
            inventory = ComponentManager<PlayerInventory>.Get();

        buildingUI.Hide();
    }
    void OnMouseOver()
    {
        if (!buildingUI.IsVisible)
            buildingUI.Show();
        else
        {
            //If it is full, extract by holding use button
            if (buildingUI.cost.MeetsRequirements())
            {
                if (Input.GetButton("UseButton"))
                {
                    extractionTimer += Time.deltaTime;
                    if (extractionTimer >= extractionTime)
                    {
                        ExtractionComplete();
                    }
                    canvas.SetLoadCircle(extractionTimer / extractionTime);
                }
                else
                {
                    extractionTimer = 0;
                    canvas.SetLoadCircle(false);
                }
            }
            //Otherwise, insert more items
            else
            {
                if (Input.GetButtonDown("UseButton"))
                {
                    ItemInstance selectedHotbarItem = inventory.GetSelectedHotbarItem();
                    if (buildingUI.cost.RequiresItem(ItemManager.GetItemByIndex(selectedHotbarItem.UniqueIndex)))
                    {
                        inventory.RemoveSelectedHotSlotItem(1);
                        if (selectedHotbarItem.UniqueName != "PlasticCup_Empty")
                            InsertItem();
                        else
                        {
                            buildingUI.cost.IncrementItem("PlasticCup_Empty", 1);
                            cup.gameObject.SetActive(true);
                        }
                    }
                }
            }

            anim.SetBool("Pressing", extractionTimer != 0);
        }
    }
    void OnMouseExit()
    {
        if (buildingUI.IsVisible)
            buildingUI.Hide();

        if (extractionTimer > 0)
        {
            extractionTimer = 0;
            canvas.SetLoadCircle(false);
            anim.SetBool("Pressing", false);
        }
    }

	//public methods

	//private methods
    private void InsertItem()
    {
        for(int i = 0; i < extractItems.Length; i++)
        {
            if (!extractItems[i].gameObject.activeInHierarchy)
            {
                extractItems[i].gameObject.SetActive(true);
                break;
            }
        }
        buildingUI.cost.IncrementItem("Thatch", 1);
    }
    private void InitializeExtractItems()
    {
        Transform extractItemParent = transform.FindChild("ExtractItemParent");
        extractItems = new GameObject[extractItemParent.childCount];

        for (int i = 0; i < extractItems.Length; i++)
        {
            extractItems[i] = extractItemParent.GetChild(i).gameObject;
        }
    }
    private void HideAllExtractItems()
    {
        for(int i = 0; i < extractItems.Length; i++)
        {
            extractItems[i].gameObject.SetActive(false);
        }
        buildingUI.cost.SetAmount("Thatch", 0);
        cup.gameObject.SetActive(false);
    }
    private void ExtractionComplete()
    {
        extractionTimer = 0f;
        HideAllExtractItems();

        buildingUI.cost.SetAmount("PlasticCup_Empty", 0);

        inventory.AddItem("PlasticCup_Water", 1);
    }
}
