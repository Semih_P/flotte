﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick_Wet : MonoBehaviour_ID 
{
	//public variables
    [SerializeField]
    private TextMesh textmesh;
    [SerializeField] private float dryTimeSeconds = 20f;
    [SerializeField] private MeshFilter filter;
    [SerializeField] private Mesh dryMesh;

	//private variables
    public bool Complete { get { return timer >= dryTimeSeconds; } }
    public float Timer { get { return timer; } set { timer = value; } }
    private float timer = 0f;
    
    //network
    private PlayerNetworkManager playerNetworkManager;
    private Semih_Network network;
    private CanvasHelper canvas;

	//Unity methods
    void Awake()
    {
        this.enabled = false;
    }
    public void OnBlockPlaced()
    {
        this.enabled = true;
        this.timer = 0f;
        this.playerNetworkManager = ComponentManager<Semih_Network>.Get().GetLocalPlayer().PlayerNetworkManager;
        this.network = ComponentManager<Semih_Network>.Get();
        this.canvas = CanvasHelper.Singleton;
        ObjectIndex = SaveAndLoad.GetUniqueObjectIndex();
        ObjectIndex = GetComponent<Block>().ObjectIndex + 1;
        PlayerNetworkManager.allBricks.Add(this);
    }
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= dryTimeSeconds)
        {
            OnFinishedDrying();
        }
        textmesh.text = timer.ToString("F1");
    }
    void OnMouseOver()
    {
        if (Complete && Helper.LocalPlayerIsWithinDistance(transform.position, Player.UseDistance))
        {
            canvas.SetDisplayText("Pickup", true);
            if (Input.GetButtonDown("UseButton"))
            {
                if (network.IsHost)
                    playerNetworkManager.PickupWetBrick(this, true);
                else
                    network.SendP2P(network.HostID, new Message_Brick_Pickup(Messages.Brick_Pickup, playerNetworkManager, ObjectIndex), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
            }
        }
        else
            canvas.SetDisplayText(false);
    }
    void OnMouseExit()
    {
        canvas.SetDisplayText(false);
    }

	//public methods

	//private methods
    private void OnFinishedDrying()
    {
        filter.mesh = dryMesh;
        this.enabled = false;
    }
}
