﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingUI_CostBox : MonoBehaviour 
{
	//public variables
	//private variables
    [SerializeField] private Image radialImage = null;
    [SerializeField] private Image image = null;
    [SerializeField] private Text label = null;
    [SerializeField] private Text displayName = null;
    [SerializeField] private List<Item_Base> items = null;
    [SerializeField] private int requiredAmount = 1;
    private int amount = 0;


	//Unity methods
    void Awake()
    {
        SetRadial(false);
    }
    void OnValidate()
    {
        Refresh();
    }

	//public methods
    public void Refresh()
    {
        if (items == null || image == null || label == null)
            return;

        if (items != null && items.Count == 1 && items[0] != null)
        {
            image.sprite = items[0].settings_Inventory.GetSprite();
            if (displayName != null)
                displayName.text = items[0].settings_Inventory.GetDisplayName();
        }
        label.text = amount + "/" + requiredAmount;
    }


    public bool RequiresItem(Item_Base item)
    {
        foreach (Item_Base i in items)
        {
            if (i.UniqueIndex == item.UniqueIndex)
                return true;
        }
        return false;
    }
    public bool RequiresItem(string uniqueName)
    {
        foreach (Item_Base i in items)
        {
            if (i.UniqueName == uniqueName)
                return true;
        }
        return false;
    }
    public bool MeetsRequirements()
    {
        return amount == requiredAmount;
    }
    public void SetAmount(int newAmount)
    {
        this.amount = newAmount;
        Refresh();
    }
    public void IncrementAmount(int amountToIncrement)
    {
        this.amount += amountToIncrement;
        this.amount = Mathf.Clamp(this.amount, 0, requiredAmount);
        Refresh();
    }
    public void SetAmountInInventory(PlayerInventory inventory)
    {
        int totalAmount = 0;
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] == null)
                continue;
            totalAmount += inventory.GetItemCount(items[i].UniqueName);
        }
        SetAmount(totalAmount);
    }
    public void SetRequiredItem(Item_Base item)
    {
        this.items[0] = item;
        Refresh();
    }
    public void SetRequiredItem(List<Item_Base> items)
    {
        this.items = items;
        Refresh();
    }
    public void SetRequiredAmount(int newRequiredAmount)
    {
        this.requiredAmount = newRequiredAmount;
        Refresh();
    }
    public int GetAmount()
    {
        return this.amount;
    }
    public void HideText()
    {
        label.gameObject.SetActive(false);
    }
    public void ShowText()
    {
        label.gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void SetRadial(float normal)
    {
        if (radialImage == null)
            return;

        if (!radialImage.gameObject.activeSelf)
            SetRadial(true);

        radialImage.fillAmount = normal;
    }
    public void SetRadial(bool visible)
    {
        if (radialImage == null)
            return;

        radialImage.gameObject.SetActive(visible);
    }
	//private methods
}
