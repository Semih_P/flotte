﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingUI : MonoBehaviour 
{
	//public variables
    public CostCollection cost;
    [SerializeField] private bool bilboard = true;

	//private variables
    private static Transform player;
    public bool IsVisible { get { return isVisible; } private set { isVisible = value; } }
    private bool isVisible;

	//Unity methods
    protected virtual void Start() 
	{
        if (player == null)
            player = GameObject.FindObjectOfType<Player>().transform;
	}
	protected virtual void Update () 
	{
        if (!IsVisible)
            return;
        if (bilboard)
            transform.rotation = Quaternion.LookRotation(transform.position - player.position);
	}

	//public methods
    public virtual void Show()
    {
        if (GameManager.IsInMenu)
            return;

        gameObject.SetActive(true);
        IsVisible = true;
    }
    public void Hide()
    {
        gameObject.SetActive(false);
        IsVisible = false;
    }
    

	//private methods
}
