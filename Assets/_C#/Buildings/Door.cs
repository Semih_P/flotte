﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour_Network 
{
	//public variables
    public bool open = false;
    [SerializeField] private Animator animator;

	//private variables
    private Semih_Network network;
    private CanvasHelper canvas;

	//Unity methods
    public void OnBlockPlaced()
    {
        if (network == null)
            network = ComponentManager<Semih_Network>.Get();

        if (network.IsHost)
        {
            NetworkUpdateManager.SetIndexForBehaviour(this);
            NetworkUpdateManager.AddBehaviour(this);
        }
        else
        {
            Block block = GetComponent<Block>();
            ObjectIndex = block.ObjectIndex + 1;
            BehaviourIndex = block.BehaviourIndex + 1;
            NetworkUpdateManager.AddBehaviour(this);
        }

        network = ComponentManager<Semih_Network>.Get();
        canvas = CanvasHelper.Singleton;
    }

    void OnMouseOver()
    {
        if (canvas == null)
            return;

        if (Helper.LocalPlayerIsWithinDistance(transform.position, Player.UseDistance * 2))
        {
            canvas.SetDisplayText(open ? "Close" : "Open", true);
            if (Input.GetButtonDown("UseButton"))
            {
                if (network.IsHost)
                {
                    if (open)
                        Close(true);
                    else
                        Open(true);
                }
                else
                    network.SendP2P(network.HostID, new Message_NetworkBehaviour(open ? Messages.Door_Close : Messages.Door_Open, this), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
            }
        }
    }
    void OnMouseExit()
    {
        if (canvas == null)
            return;

        canvas.SetDisplayText(false);
    }

	//public methods

	//private methods
    private void Open(bool useNetwork)
    {
        open = true;
        animator.SetBool("Open", open);

        if (useNetwork)
            network.RPC(new Message_NetworkBehaviour(Messages.Door_Open, this), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
    private void Close(bool useNetwork)
    {
        open = false;
        animator.SetBool("Open", open);

        if (useNetwork)
            network.RPC(new Message_NetworkBehaviour(Messages.Door_Close, this), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }

    //network methods
    public override void Serialize_Create()
    {
        if (open)
            AddMessage(new Message_NetworkBehaviour(Messages.Door_Open, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.Door_Open:
                Open(network.IsHost);
                break;
            case Messages.Door_Close:
                Close(network.IsHost);
                break;
        }
    }
}
