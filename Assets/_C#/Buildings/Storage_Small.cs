﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage_Small : MonoBehaviour_ID 
{
    //public variables
    [Header("Create inventory settings")]
    public Inventory inventoryPrefab;

    [Header("Chest settings")]
    public Animator anim;
    
    //private variables
    private Inventory inventoryReference;
    private CanvasHelper canvas;
    public bool IsOpen { get { return isOpen; } private set { isOpen = value; } }
    private bool isOpen;
    public bool CanCloseWithUseButton { get { return canCloseWithUsebutton; } private set { canCloseWithUsebutton = value; } }
    private bool canCloseWithUsebutton;

    //network
    private StorageManager storageManager;
    private Semih_Network network;


    //Unity methods
    void Start()
    {
        canvas = CanvasHelper.Singleton;
        canvas.SubscribeToMenuClose(OnMenuClose);
        storageManager = ComponentManager<Semih_Network>.Get().GetLocalPlayer().GetComponentInChildren<StorageManager>();
        network = ComponentManager<Semih_Network>.Get();
    }
    public void OnBlockPlaced()
    {
        ObjectIndex = SaveAndLoad.GetUniqueObjectIndex();
        ObjectIndex = GetComponent<Block>().ObjectIndex + 1;
        StorageManager.allStorages.Add(this);

        inventoryReference = Instantiate(inventoryPrefab, Inventory.Parent) as Inventory;
        inventoryReference.transform.localScale = inventoryPrefab.transform.localScale;
        inventoryReference.transform.localPosition = inventoryPrefab.transform.localPosition;
    }
    void OnMouseOver()
    {
        if (GameManager.IsInMenu)
        {
            return;
        }


        //Open storage
        if (!IsOpen && !PlayerItemManager.IsBusy && Helper.LocalPlayerIsWithinDistance(transform.position, Player.UseDistance + 0.5f))
        {
            canvas.SetDisplayText("Open", true);
            if (Input.GetButtonDown("UseButton"))
            {
                if (network.IsHost)
                    storageManager.OpenStorage(this, true);
                else
                    network.SendP2P(network.HostID, new Message_Storage(Messages.Storage_Open, storageManager, this), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
            }
        }
        else
        {
            canvas.SetDisplayText(false);
        }
    }
    void OnMouseExit()
    {
        canvas.SetDisplayText(false);
    }
    void OnDestroy()
    {
        if (inventoryReference != null)
            Destroy(inventoryReference.gameObject);
    }

    //public methods
    public Inventory GetInventoryReference()
    {
        return inventoryReference;
    }
    public void OnMenuClose()
    {
        if (this == null || gameObject == null || gameObject.activeInHierarchy == false)
            return;

        //if (IsOpen)
        //{
        //    Close(true);
        //}
    }
    public void Open(bool isLocalPlayer)
    {
        IsOpen = true;
        ComponentManager<SoundManager>.Get().PlaySoundCopy("ChestOpen", transform.position, true);
        anim.SetBool("IsOpen", IsOpen);

        if (isLocalPlayer)
        {
            PlayerItemManager.IsBusy = true;
            inventoryReference.Show();
            canvas.OpenMenus();
            Invoke("AllowCloseWithUsebutton", 0.1f);
        }
    }
    public void Close(bool isLocalPlayer)
    {
        isOpen = false;
        ComponentManager<SoundManager>.Get().PlaySoundCopy("ChestClose", transform.position, true);
        anim.SetBool("IsOpen", isOpen);
        canCloseWithUsebutton = false;

        if (isLocalPlayer)
        {
            PlayerItemManager.IsBusy = false;
            inventoryReference.Hide();
            CanvasHelper.Singleton.CloseMenus();
        }
    }

    //private methods
    private void AllowCloseWithUsebutton()
    {
        canCloseWithUsebutton = true;
    }
}
