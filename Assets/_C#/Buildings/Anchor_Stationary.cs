﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Anchor_Stationary : MonoBehaviour_Network 
{
	//public variables
    [SerializeField] private Rigidbody anchor = null;
    [SerializeField] private SpringJoint joint = null;
    [SerializeField] private float dropSpeed = 1f;
    [SerializeField] private float weighSpeed = 1f;
    [SerializeField] private Rope rope = null;
    [SerializeField] private Animator anim = null;
    [SerializeField] private Transform ropeConnectionPoint = null;
    [SerializeField] private Transform usePoint = null;

	//private variables
    private CanvasHelper canvas;
    private Raft raft;
    private bool canUse = true;
    private bool atBottom = false;
    private bool droppingAnchor;
    private bool weighingAnchor;
    private Semih_Network network;

	//Unity methods
	void Start () 
	{
        canvas = CanvasHelper.Singleton;
        raft = ComponentManager<Raft>.Get();
	}	
    public void OnBlockPlaced()
    {
        if (network == null)
            network = ComponentManager<Semih_Network>.Get();

        if (network.IsHost)
        {
            NetworkUpdateManager.SetIndexForBehaviour(this);
            NetworkUpdateManager.AddBehaviour(this);
        }
        else
        {
            Block block = GetComponent<Block>();
            ObjectIndex = block.ObjectIndex + 1;
            BehaviourIndex = block.BehaviourIndex + 1;
            NetworkUpdateManager.AddBehaviour(this);
        }

        anchor.transform.SetParent(transform.parent);
    }

	// Update is called once per frame
	void Update () 
	{
        if (droppingAnchor)
            DroppingUpdate();
        if (weighingAnchor)
            WeighUpdate();


        rope.SetPosition(0, anchor.transform);
        rope.SetPosition(1, ropeConnectionPoint);
	}
    void OnMouseOver()
    {
        if (canUse && Helper.LocalPlayerIsWithinDistance(usePoint.position, Player.UseDistance))
        {
            string text = atBottom ? "Weigh anchor" : "Drop anchor";
            canvas.SetDisplayText(text, true);
            if (Input.GetButtonDown("UseButton"))
            {
                if (network.IsHost)
                {
                    Use(true);
                }
                else
                {
                    network.SendP2P(network.HostID, new Message_NetworkBehaviour(Messages.StationaryAnchorUse, this), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                }
            }
        }
        else
            canvas.SetDisplayText(false);
    }
    void OnMouseExit()
    {
        canvas.SetDisplayText(false);
    }

    //public methods

	//private methods
    private void Use(bool useNetwork)
    {
        canUse = false;

        if (atBottom)
            WeighAnchor();
        else
            DropAnchor();

        if (useNetwork)
            network.RPC(new Message_NetworkBehaviour(Messages.StationaryAnchorUse, this), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
    private void DroppingUpdate()
    {
        float distanceToBottom = Vector3.Distance(ropeConnectionPoint.position, anchor.position);
        float animSpeed = Mathf.Clamp(distanceToBottom * 0.1f, 0f, 2f);
        anim.SetFloat("Speed", animSpeed);

        joint.minDistance += dropSpeed * Time.deltaTime;

        if (anchor.position.y < GameManager.OceanBottom)
        {
            OnAnchorHit();
            return;
        }
    }
    private void WeighUpdate()
    {
        if (joint.minDistance > 0)
        {
            joint.minDistance -= weighSpeed * Time.deltaTime;

            if (joint.minDistance <= 0)
            {
                weighingAnchor = false;
                canUse = true;
                atBottom = false;
                anim.SetFloat("Speed", 0f);
                return;
            }
        }
    }
    private void DropAnchor()
    {
        if (network.IsHost)
            raft.AddAnchor();

        droppingAnchor = true;
        anchor.transform.SetParent(null);
        anim.SetFloat("Speed", 0f);
    }
    private void WeighAnchor()
    {
        if (network.IsHost)
            raft.RemoveAnchor();

        weighingAnchor = true;
        atBottom = false;
        anchor.isKinematic = false;
        anchor.transform.SetParent(transform.parent);
        
        anim.SetFloat("Speed", -0.5f);
    }

    //Event
    public void OnAnchorHit()
    {
        if (!atBottom && droppingAnchor)
        {
            anim.SetFloat("Speed", 0f);
            droppingAnchor = false;
            anchor.isKinematic = true;
            canUse = true;
            atBottom = true;
        }

        if (network.IsHost)
            network.RPC(new Message_StationaryAnchorCreate(Messages.StationaryAnchorCreate, this, anchor.transform, atBottom, joint.minDistance), Target.Other, Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }

    //network methods
    public override void Serialize_Create()
    {
        if (atBottom)
        {
            AddMessage(new Message_StationaryAnchorCreate(Messages.StationaryAnchorCreate, this, anchor.transform, true, joint.minDistance));
        }
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.StationaryAnchorCreate:
                Message_StationaryAnchorCreate msgCreate = msg as Message_StationaryAnchorCreate;
                if (msgCreate.atBottom)
                {
                    anim.SetFloat("Speed", 0f);
                    atBottom = true;
                    canUse = true;
                    droppingAnchor = false;

                    anchor.isKinematic = true;
                    anchor.transform.SetParent(null);
                    anchor.transform.position = msgCreate.Position;
                    anchor.transform.eulerAngles = msgCreate.Rotation;

                    rope.SetPosition(0, anchor.transform);
                    rope.SetPosition(1, ropeConnectionPoint);

                    joint.minDistance = msgCreate.jointDistance;
                }
                break;
            case Messages.StationaryAnchorUse:
                if (network.IsHost && !canUse)
                    return;
                Use(network.IsHost);
                break;
        }
    }
}
[System.Serializable]
public class Message_StationaryAnchorCreate : Message_NetworkBehaviour
{
    public bool atBottom;
    public float jointDistance;
    public Vector3 Position { get { return new Vector3(xPos, yPos, zPos); } set { xPos = value.x; yPos = value.y; zPos = value.z; } }
    public Vector3 Rotation { get { return new Vector3(xRot, yRot, zRot); } set { xRot = value.x; yRot = value.y; zRot = value.z; } }

    private float xPos, yPos, zPos;
    private float xRot, yRot, zRot;

    public Message_StationaryAnchorCreate(Messages type, MonoBehaviour_Network behaviour, Transform anchor, bool atBottom, float jointDistance)
        :base(type, behaviour)
    {
        Position = anchor.position;
        Rotation = anchor.eulerAngles;
        this.atBottom = atBottom;
        this.jointDistance = jointDistance;
    }
}
