﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anchor : MonoBehaviour 
{
	//public variables
    [SerializeField] private Transform reciever;

	//private variables

	//Unity methods
    void OnTriggerEnter(Collider other)
    {
        reciever.SendMessage("OnAnchorHit", SendMessageOptions.DontRequireReceiver);
    }

	//public methods
    public void SetReciever(Transform reciever)
    {
        this.reciever = reciever;
    }

	//private methods
}
