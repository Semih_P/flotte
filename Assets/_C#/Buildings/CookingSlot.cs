﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CookingSlot : MonoBehaviour 
{
    public bool IsComplete { get { return complete; } }
    public bool IsBusy { get { return cookingItem != null; } }
    public float CookTimer { get { return cookTimer; } }

	//public variables
    [SerializeField] private List<CookItemConnection> itemConnections = new List<CookItemConnection>();

	//private variables
    private Item_Base cookingItem = null;
    private bool complete = false;
    private float cookTimer = 0f;
    private new Collider collider;

	//Unity methods
    void Awake()
    {
        collider = GetComponent<Collider>();
        collider.enabled = false;
        DisableItems();
    }
    void OnValidate()
    {
        foreach (CookItemConnection con in itemConnections)
        {
            if (con.cookableItem != null)
                con.name = con.cookableItem.UniqueName;
            else
                con.name = "No cookable attached";
        }
    }
 
	//public methods
    public void UpdateSlot()
    {
        if (IsBusy && ! complete)
        {
            cookTimer += Time.deltaTime;
            if (cookTimer >= cookingItem.settings_cookable.GetCookTime())
            {
                complete = true;
                collider.enabled = true;
                EnableCookedItem(cookingItem);
            }
        }
    }
    public Item_Base GetCookingItem()
    {
        return cookingItem;
    }
    public bool CanCookItem(Item_Base itemToCheck)
    {
        foreach (CookItemConnection item in itemConnections)
        {
            if (item.cookableItem.UniqueIndex == itemToCheck.UniqueIndex)
                return true;
        }
        return false;
    }
    public void StartCooking(Item_Base cookableItem)
    {
        cookingItem = cookableItem;
        EnableRawItem(cookableItem);
    }
    public void ResetSlot()
    {
        complete = false;
        cookingItem = null;
        cookTimer = 0f;
        collider.enabled = false;

        DisableItems();
    }
    public void ForceSetTimer(float newTimerValue)
    {
        cookTimer = newTimerValue;
    }

	//private methods
    private void EnableRawItem(Item_Base cookableItem)
    {
        foreach (CookItemConnection item in itemConnections)
        {
            if (item.cookableItem == cookableItem)
            {
                item.SetRawState(true);
                item.SetCookedState(false);
                break;
            }
        }
    }
    private void EnableCookedItem(Item_Base cookableItem)
    {
        foreach (CookItemConnection item in itemConnections)
        {
            if (item.cookableItem == cookableItem)
            {
                item.SetRawState(false);
                item.SetCookedState(true);
                break;
            }
        }
    }
    private void DisableItems()
    {
        foreach (CookItemConnection item in itemConnections)
        {
            item.SetRawState(false);
            item.SetCookedState(false);
        }
    }

}
