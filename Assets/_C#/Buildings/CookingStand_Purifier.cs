﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PurifyConnectection
{
    public Item_Base emptyContainer;
    public Item_Base fullContainer;
}
public class CookingStand_Purifier : CookingStand 
{
	//public variables

	//private variables
    [SerializeField] private FluidComponent freshWater;
    [SerializeField] private FluidComponent saltWater;

	//Unity methods

	//public methods
    protected override void Update()
    {
        base.Update();

        //Handle saltwater
        int activeSlots = GetActiveSlotCount();
        if (activeSlots == 0)
            saltWater.HideFluid();
        else
            saltWater.SetStep(activeSlots);

        //Handle freshwater
        int completeSlots = GetCompleteSlotCount();
        if (completeSlots == 0)
            freshWater.HideFluid();
        else
            freshWater.SetStep(completeSlots);
    }

    //protected methods
    public override void CollectItem(CookingSlot slot, bool localPlayer)
    {
        ItemInstance handItem = playerInventory.GetSelectedHotbarItem();

        if (handItem.GetType() != ItemType.Drinkable)
            return;

        if (!localPlayer)
        {
            slot.ResetSlot();
            return;
        }


        //Pick up with empty bottle
        if (handItem.settings_consumeable.FluidType == Fluid.None)
        {
            Slot hotSlot = playerInventory.GetSelectedHotbarSlot();
            hotSlot.SetItem(handItem.settings_cookable.GetCookingResult(), 1);
            hotSlot.itemInstance.settings_consumeable.SetUses(1);
            slot.ResetSlot();
        }
        //Pick up with a bottle that already contains a fluid
        else
        {
            Item_Base completeItem = slot.GetCookingItem().settings_cookable.GetCookingResult();
            if (handItem.settings_consumeable.FluidType == completeItem.settings_consumeable.FluidType)
            {
                if (!handItem.settings_consumeable.HasMaxUses())
                {
                    handItem.settings_consumeable.IncrementUses(1);
                    slot.ResetSlot();
                }
            }
        }
    }
    protected override void CheckForCollecting(CookingSlot slot)
    {
        ItemInstance handItem = playerInventory.GetSelectedHotbarItem();
        if (handItem.GetType() == ItemType.Drinkable)
        {
            Item_Base completeItem = slot.GetCookingItem().settings_cookable.GetCookingResult();
            if (handItem.settings_consumeable.FluidType == Fluid.None)
            {
                canvas.SetDisplayText("Fill " + handItem.settings_Inventory.GetDisplayName() + " with water", true);

                if (Input.GetButtonDown("UseButton"))
                {
                    if (network.IsHost)
                        cookingstandManager.CollectItem(this, slot, true);
                    else
                        network.SendP2P(network.HostID, new Message_CookingStand_Collect(Messages.CookingStand_Collect, cookingstandManager, this, slot), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                }
            }
            else if (handItem.settings_consumeable.FluidType == completeItem.settings_consumeable.FluidType)
            {
                if (!handItem.settings_consumeable.HasMaxUses())
                {
                    canvas.SetDisplayText("Fill " + handItem.settings_Inventory.GetDisplayName() + " with water", true);
                    if (Input.GetButtonDown("UseButton"))
                    {
                        if (network.IsHost)
                            cookingstandManager.CollectItem(this, slot, true);
                        else
                            network.SendP2P(network.HostID, new Message_CookingStand_Collect(Messages.CookingStand_Collect, cookingstandManager, this, slot), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                    }
                }
            }
        }
    }

	//private methods
    private int GetActiveSlotCount()
    {
        int count = 0;
        for (int i = 0; i < cookingSlots.Length; i++)
        {
            if (cookingSlots[i].IsBusy && !cookingSlots[i].IsComplete && cookingSlots[i].GetCookingItem().settings_consumeable.FluidType != Fluid.None)
            {
                count++;
            }
        }
        return count;
    }
    private int GetCompleteSlotCount()
    {
        int count = 0;
        for (int i = 0; i < cookingSlots.Length; i++)
        {
            if (cookingSlots[i].IsComplete)
            {
                count++;
            }
        }
        return count;
    }
}
