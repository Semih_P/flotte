﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fuel : MonoBehaviour 
{
    public int MaxFuel { get { return maxFuel; } }
	//public variables

	//private variables
    [SerializeField] private float burnTimePerFuel = 10f;
    [SerializeField] private int maxFuel = 1;
    [SerializeField] private List<GameObject> fuelModels;
    private int fuelCount = 0;
    private bool burningFuel = false;
    private float burnTimer = 0f;

	//Unity methods
    void Awake()
    {
        if (maxFuel < 0)
            fuelCount = maxFuel;
        RefreshFuelModels();
    }
    void Update()
    {
        if (burningFuel)
        {
            burnTimer += Time.deltaTime;
            if (burnTimer >= burnTimePerFuel)
            {
                burnTimer = 0f;
                fuelCount--;
                if (fuelCount <= 0)
                    burningFuel = false;

                RefreshFuelModels();
            }
        }
    }
    void OnValidate()
    {
        if (fuelModels != null && fuelModels.Count > 0)
        {
            while(fuelModels.Count > maxFuel)
            {
                fuelModels.RemoveAt(fuelModels.Count - 1);
            }
        }
    }

	//public methods
    public void ForceSetTimer(float value)
    {
        burnTimer = value;
    }
    public float GetTimer()
    {
        return burnTimer;
    }
    public float GetBurningNormal()
    {
            return burnTimer / burnTimePerFuel;
    }
    public int GetFuelCount()
    {
        return fuelCount;
    }
    public void AddFuel(int amount)
    {
        fuelCount = Mathf.Clamp(fuelCount + amount, 0, maxFuel);
        RefreshFuelModels();
    }
    public bool HasMaxFuel()
    {
        if (fuelCount < 0)
            return true;
        else
            return fuelCount == maxFuel;
    }
    public bool IsBurning()
    {
        return burningFuel;
    }
    public void StartBurning()
    {
        if (burningFuel || fuelCount == 0)
            return;

        burningFuel = true;
    }
    public void StopBurning()
    {
        burningFuel = false;
    }

	//private methods
    private void RefreshFuelModels()
    {
        if (fuelModels == null || fuelModels.Count == 0)
            return;

        for (int i = 0; i < fuelModels.Count; i++)
        {
            if (i < fuelCount)
                fuelModels[i].SetActive(true);
            else
                fuelModels[i].SetActive(false);
        }
    }
}
