﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CookItemConnection
{
    [HideInInspector]
    public string name;
    public Item_Base cookableItem;
    public GameObject rawItem;
    public GameObject cookedItem;

    public void SetRawState(bool state)
    {
        if (rawItem != null)
            rawItem.SetActive(state);
    }
    public void SetCookedState(bool state)
    {
        if (cookedItem != null)
            cookedItem.SetActive(state);
    }
}
public class CookingStand : MonoBehaviour_ID
 {
 	//public variables
    public Fuel fuel;
    [SerializeField] protected BuildingUI buildingUI;

    public GameObject particleController;
    public AudioSource audioSource;
    [HideInInspector] public CookingSlot[] cookingSlots;

 	//private variables
    protected PlayerInventory playerInventory;
    protected CanvasHelper canvas;
    protected CookingStandManager cookingstandManager;
    protected Semih_Network network;

 	//Unity methods
    void Awake()
    {
        if (particleController != null)
            particleController.SetActive(false);

        if (audioSource != null)
            audioSource.Stop();

        cookingSlots = GetComponentsInChildren<CookingSlot>();

        if (buildingUI != null)
        {
            buildingUI.cost.SetRequiredAmount("Plank", fuel.MaxFuel);
            buildingUI.cost.SetRequiredAmount("FoodCollection", cookingSlots.Length);
        }
    }
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
        cookingstandManager = ComponentManager<Semih_Network>.Get().GetLocalPlayer().GetComponentInChildren<CookingStandManager>();
        playerInventory = ComponentManager<PlayerInventory>.Get();
        canvas = CanvasHelper.Singleton;
    }
    public void OnBlockPlaced()
    {
        ObjectIndex = SaveAndLoad.GetUniqueObjectIndex();
        ObjectIndex = GetComponent<Block>().ObjectIndex + 1;
        CookingStandManager.allCookingStands.Add(this);
    }
    protected virtual void Update()
    {
        //Pickup cooked item
        RaycastHit hit = Helper.HitAtCursor(Player.UseDistance, LayerMasks.MASK_item);
        if (hit.transform != null)
        {
            CookingSlot slot = hit.transform.GetComponent<CookingSlot>();
            if (slot != null)
            {
                if (slot.IsBusy && slot.IsComplete)
                {
                    CheckForCollecting(slot);
                }
            }
        }

        //Start grill when there is fuel and raw itom on grill
        if (RawItemIsOnSlot())
        {
            if (!fuel.IsBurning() && fuel.GetFuelCount() != 0)
            {
                fuel.StartBurning();

                if (particleController != null)
                    particleController.SetActive(true);

                if (audioSource != null)
                    audioSource.Play();
            }
        }

        //Set fuel radial image
        if (buildingUI != null)
            buildingUI.cost.SetRadial("Plank", 1 - fuel.GetBurningNormal());
        
        //If the stand is burning
        if (fuel.IsBurning())
        {
            if (!RawItemIsOnSlot())
            {
                fuel.StopBurning();
                
                if (particleController != null)
                    particleController.SetActive(false);

                if (audioSource != null)
                    audioSource.Stop();
            }
            else
            {
                //Update each slot timer
                foreach (CookingSlot slot in cookingSlots)
                {
                    if (slot.IsBusy && !slot.IsComplete)
                        slot.UpdateSlot();
                }
            }
        }
        else if (particleController != null && particleController.activeInHierarchy)
        {
            fuel.StopBurning();
            particleController.SetActive(false);
            audioSource.Stop();
        }
    }
    void OnMouseOver()
    {
        if (GameManager.IsInMenu || !Helper.LocalPlayerIsWithinDistance(transform.position, Player.UseDistance))
        {
            canvas.SetDisplayText(false);
            if (buildingUI != null)
            {
                if (buildingUI.IsVisible)
                    buildingUI.Hide();
            }
            return;
        }

        //Insert items
        if (buildingUI != null && !buildingUI.IsVisible)
            buildingUI.Show();
        else
        {
            if (buildingUI != null)
                buildingUI.cost.SetAmount("Plank", fuel.GetFuelCount());

            //Aquire item in hand
            ItemInstance selectedHotbarItem = playerInventory.GetSelectedHotbarItem();

            if (selectedHotbarItem == null || selectedHotbarItem.baseItem == null)
            {
                canvas.SetDisplayText(false);
                return;
            }

            //Find an empty slot
            CookingSlot emptySlot = CanInsertItem(selectedHotbarItem.baseItem);
            if (emptySlot != null)
            {
                canvas.SetDisplayText("Place " + selectedHotbarItem.baseItem.settings_Inventory.GetDisplayName(), true);

                //Did we press the USE BUTTON?
                if (Input.GetButtonDown("UseButton"))
                {
                    if (network.IsHost)
                        cookingstandManager.InsertItem(selectedHotbarItem.baseItem, emptySlot, this, true);
                    else
                        network.SendP2P(network.HostID, new Message_CookingStand_Insert(Messages.CookingStand_Insert, cookingstandManager, selectedHotbarItem.baseItem, this), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

                }
            }
            else if (selectedHotbarItem.UniqueName == "Plank" && !fuel.HasMaxFuel())
            {
                canvas.SetDisplayText("Place plank", true);

                //Did we press the USE BUTTON?
                if (Input.GetButtonDown("UseButton"))
                {
                    if (network.IsHost)
                        cookingstandManager.AddFuel(this, true);
                    else
                        network.SendP2P(network.HostID, new Message_CookingStand(Messages.CookingStand_AddFuel, cookingstandManager, this), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                }
            }
            else
            {
                canvas.SetDisplayText(false);
            }
        }
    }
    void OnMouseExit()
    {
        if (buildingUI != null && buildingUI.IsVisible)
            buildingUI.Hide();
        canvas.SetDisplayText(false);
    }

	//public methods
    public void InsertItem(Item_Base itemToInsert, CookingSlot emptySlot, bool localPlayer)
    {
        emptySlot.StartCooking(itemToInsert);
        if (buildingUI != null)
            buildingUI.cost.IncrementItem("FoodCollection", 1);

        if (localPlayer)
        {
            Slot inventorySlot = playerInventory.GetSelectedHotbarSlot();
            inventorySlot.itemInstance.settings_consumeable.IncrementUses(-1);
            if (inventorySlot.itemInstance.settings_consumeable.GetUses() <= 0)
            {
                if (inventorySlot.itemInstance != null && inventorySlot.itemInstance.amount > 1)
                    inventorySlot.RemoveItem(1);
                else
                {
                    Cost itemAfterUse = inventorySlot.itemInstance.settings_consumeable.GetItemAfterUse();
                    inventorySlot.SetItem(itemAfterUse.item, itemAfterUse.amount);
                    playerInventory.hotbar.ReselectCurrentSlot();
                }
            }
        }
    }
    public void AddFuel(bool localPlayer)
    {
        fuel.AddFuel(1);

        if (localPlayer)
            playerInventory.RemoveSelectedHotSlotItem(1);
    }
    public virtual void CollectItem(CookingSlot slot, bool localPlayer)
    {
        Item_Base completeItem = slot.GetCookingItem().settings_cookable.GetCookingResult();

        if (localPlayer)
            playerInventory.AddItem(completeItem.UniqueName, 1);

        if (buildingUI != null)
            buildingUI.cost.IncrementItem("FoodCollection", -1);
        slot.ResetSlot();
    }
    public CookingSlot CanInsertItem(Item_Base itemToInsert)
    {
        CookingSlot emptySlot = GetEmptyCookingSlot();
        if (emptySlot != null && !emptySlot.IsBusy && emptySlot.CanCookItem(itemToInsert))
            return emptySlot;
        else
            return null;
    }
    public int GetIndexOfCookingSlot(CookingSlot slot)
    {
        for (int i = 0; i < cookingSlots.Length; i++)
        {
            if (slot == cookingSlots[i])
                return i;
        }
        return -1;
    }

    //protected methods
    protected virtual void CheckForCollecting(CookingSlot slot)
    {
        Item_Base completeItem = slot.GetCookingItem().settings_cookable.GetCookingResult();
        canvas.SetDisplayText("Pickup " + completeItem.settings_Inventory.GetDisplayName(), true);

        if (Input.GetButtonDown("UseButton"))
        {
            if (network.IsHost)
                cookingstandManager.CollectItem(this, slot, true);
            else
                network.SendP2P(network.HostID, new Message_CookingStand_Collect(Messages.CookingStand_Collect, cookingstandManager, this, slot), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
    }
	//private methods
    private CookingSlot GetEmptyCookingSlot()
    {
        foreach (CookingSlot slot in cookingSlots)
            if (!slot.IsBusy)
                return slot;

        return null;
    }
    private bool RawItemIsOnSlot()
    {
        foreach(CookingSlot slot in cookingSlots)
        {
            if (slot.IsBusy && !slot.IsComplete)
            {
                return true;
            }
        }
        return false;
    }
}
