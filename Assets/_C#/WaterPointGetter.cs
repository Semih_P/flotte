﻿using PlayWay.Water;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPointGetter : MonoBehaviour 
{
    public float offset;
    private WaterSample sample;

	void Start () 
    {
        sample = new WaterSample(FindObjectOfType<Water>(), WaterSample.DisplacementMode.Height, 1f);
	}
	
    public float GetWaterPoint(Vector3 origin)
    {
        if (sample == null)
            sample = new WaterSample(FindObjectOfType<Water>(), WaterSample.DisplacementMode.Height, 1f);

        if (sample == null)
            return 0f;

        float y = sample.GetAndReset(origin, WaterSample.ComputationsMode.ForceCompletion).y + offset;

        if (float.IsNaN(y))
            return 0f;
        else
            return y;
    }
}
