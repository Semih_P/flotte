﻿using UnityEngine;
using System.Collections;

public class LockRotation : MonoBehaviour
 {
 	//public variables
    public bool lockX;
    public bool lockY;
    public bool lockZ;

    [Space(10)]
    public Vector3 lockrotation;

 	//private variables


 	//Unity methods
	void Start () 
	{
	
	}
	void Update () 
	{
        if (!lockX && !lockY && !lockZ)
        {
            this.enabled = false;
            return;
        }

        Vector3 euler = transform.eulerAngles;

        //Lock x rotation
	    if (lockX)
        {
            euler.x = lockrotation.x;
        }

        //Lock y rotation
        if (lockY)
        {
            euler.y = lockrotation.y;
        }

        //Lock z rotation
        if (lockZ)
        {
            euler.z = lockrotation.z;
        }

        transform.eulerAngles = euler;
	}


	//public methods


	//private methods
}
