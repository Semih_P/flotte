using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FPSCounter : MonoBehaviour
{
    const float fpsMeasurePeriod = 0.5f;
    private int m_FpsAccumulator = 0;
    private float m_FpsNextPeriod = 0;
    private int m_CurrentFps;
    private Text m_Text;
    private Semih_Network network;

    private void Start()
    {
        m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
        m_Text = GetComponent<Text>();
        m_Text.enabled = false;
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            m_Text.enabled = !m_Text.enabled;
        }

        // measure average frames per second
        m_FpsAccumulator++;
        if (Time.realtimeSinceStartup > m_FpsNextPeriod)
        {
            m_CurrentFps = (int)(m_FpsAccumulator / fpsMeasurePeriod);
            m_FpsAccumulator = 0;
            m_FpsNextPeriod += fpsMeasurePeriod;
            m_Text.text = "FPS: " + m_CurrentFps;
            if (network != null)
            {
                m_Text.text += "\nPing: " + network.Ping.ToString("F1");
                m_Text.text += "\nTotal sent: " + (network.TotalBytesSent / 1024f).ToString("F2") + " (KB)";
                m_Text.text += "\nMaxPackageSize: " + (network.MaxBytePackage / 1024f).ToString("F2") + " (KB)";
                m_Text.text += "\nKB/s: " + (network.BytesSentPerSecond / 1024f).ToString("F2");
            }
            else
                network = ComponentManager<Semih_Network>.Get();
        }
    }
}

