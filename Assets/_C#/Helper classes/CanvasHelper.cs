﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CanvasHelper : SingletonGeneric<CanvasHelper>
 {
    public delegate void OnMenuClose();
    private OnMenuClose menuCloseCallstack;

 	//public variables
    [Header("Close/open on INVENTORY button")]
    public List<GameObject> inventoryMenus = new List<GameObject>();

    [Header("Stats")]
    public Slider healthSlider;
    public Slider hungerSlider;
    public Slider hungerTargetSlider;
    public Slider thirstSlider;
    public Slider thirstTargetSlider;
    public Slider wellBeingSlider;

    [Header("Oxygen")]
    public GameObject fatigueParent;
    public Image fatigueRadial;
    public Material fatigueShader;

    [Header("Remove Block Slider")]
    public Image removeBlockRadialImage;

    [Header("Blockplacer")]
    public ItemCostBox[] blockPlacerCostBoxes;

    [Header("Others")]
    public GameObject EButton;
    public Text displayText;
    public GameObject gameOver;
    public GameObject aimCursor;

 	//private variables


 	//Unity methods
    void Awake()
    {
        Helper.aimCursor = aimCursor;
        SetLoadCircle(false);
        SetEButton(false);
        SetDisplayText(false);
        SetFatigueSlider(false);
        Invoke("DelayedStart", 0.1f);
    }
    void DelayedStart()
    {
        CloseMenus();
    }
    void Update()
    {
        if (!PlayerItemManager.IsBusy && !GameManager.IsInBuildMenu && !PauseMenu.IsOpen && !Player.IsDead)
        {
            bool openKey = Input.GetButtonDown("Inventory");
            bool closeKey = Input.GetButtonDown("Inventory") || Input.GetKeyDown(KeyCode.Escape);

            if (GameManager.IsInMenu && closeKey)
            {
                CloseMenus();
            }
            else if (openKey)
            {
                OpenMenus();
            }
        }
    }

    public void SubscribeToMenuClose(OnMenuClose closeMethod)
    {
        menuCloseCallstack += closeMethod;
    }

    //Menus
    public void CloseMenus()
    {
        GameManager.IsInMenu = false;

        foreach (GameObject obj in inventoryMenus)
        {
            obj.SetActive(false);
        }

        Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);

        if (menuCloseCallstack != null)
            menuCloseCallstack();
    }
    public void OpenMenus()
    {
        GameManager.IsInMenu = true;

        foreach (GameObject obj in inventoryMenus)
        {
            obj.SetActive(true);
        }

        Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
    }

    //Fatigue
    public void SetFatigueSlider(float value)
    {
        fatigueRadial.fillAmount = value;
        fatigueShader.SetFloat("_FatigueValue", value);
    }
    public void SetFatigueSlider(bool state)
    {
        fatigueParent.gameObject.SetActive(state);
    }

    //Remove block 
    public void SetLoadCircle(bool state)
    {
        if (removeBlockRadialImage != null)
            removeBlockRadialImage.gameObject.SetActive(state);
    }
    public void SetLoadCircle(float value)
    {
        if (value > 0)
            SetLoadCircle(true);
        if (removeBlockRadialImage != null)
            removeBlockRadialImage.fillAmount = value;
    }

    //Blockplacer
    public ItemCostBox[] GetBlockPlacerCostBoxes()
    {
        return blockPlacerCostBoxes;
    }
    public void HideBlockPlacerCostBoxes()
    {
        foreach(ItemCostBox box in blockPlacerCostBoxes)
        {
            box.gameObject.SetActive(false);
        }
    }
    public void SetBlockPlacerCostBox(int index, Cost cost)
    {
        if (index >= 0 && index < blockPlacerCostBoxes.Length)
        {
            ItemCostBox box = blockPlacerCostBoxes[index];
            box.gameObject.SetActive(true);
            box.SetCost(cost);
        }
    }

    //Others
    public void SetEButton(bool state)
    {
        EButton.SetActive(state);
    }
    public void SetDisplayText(bool state)
    {
        if (displayText != null)
            displayText.gameObject.SetActive(state);

        if (EButton)
            EButton.SetActive(state);
    }
    public void SetDisplayText(string message, bool showEButton = false)
    {
        if (displayText != null)
        {
            displayText.gameObject.SetActive(true);
            displayText.text = message;
        }

        if (EButton != null)
        {
            this.EButton.SetActive(showEButton);
        }
    }
    public void SetGameOver(bool state)
    {
        gameOver.SetActive(state);
    }
}
