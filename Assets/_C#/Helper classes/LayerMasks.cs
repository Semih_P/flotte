﻿using UnityEngine;
using System.Collections;

public class LayerMasks : MonoBehaviour
 {
 	//public variables
    public LayerMask ignoreMask;

    public static LayerMask MASK_water;
    public static LayerMask MASK_item;
    public static LayerMask MASK_ignorePlayer;
    public static LayerMask MASK_block;
    public static LayerMask MASK_buildQuad;
    public static LayerMask MASK_Obstruction;

    public const string TAG_Plant = "Plant";

 	//private variables


 	//Unity methods
    void Awake()
    {
        MASK_ignorePlayer = ignoreMask;
        MASK_water = 1 << LayerMask.NameToLayer("Water");
        MASK_item = 1 << LayerMask.NameToLayer("Item");
        MASK_block = 1 << LayerMask.NameToLayer("Block");
        MASK_buildQuad = 1 << LayerMask.NameToLayer("BuildQuad");
        MASK_Obstruction = 1 << LayerMask.NameToLayer("Obstruction");
    }
	void Start () 
	{
	
	}
	void Update () 
	{
	
	}


	//public methods


	//private methods
}
