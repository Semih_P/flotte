﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemHider : MonoBehaviour 
{
    [Header("THIS NEEDS TO BE PLACED NEXT TO A PARTICLESYSTEM")]

    //public variables
    public bool hideAfterLifeTime;

	//private variables
    private ParticleSystem particleSystem;

	//Unity methods
    void Awake()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }
    void OnEnable()
    {
        if (hideAfterLifeTime)
            StartCoroutine(HideSystem(particleSystem.main.startLifetime.constant * 1.1f));
    }

	//public methods

	//private methods
    private IEnumerator HideSystem(float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);

        gameObject.SetActive(false);
    }
}
