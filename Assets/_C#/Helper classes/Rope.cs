﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour 
{
	//public variables

	//private variables
    [SerializeField] private Transform[] ropeParts;
    [SerializeField] private Renderer ropeMaterial;


	//Unity methods
    void Start()
    {
        gameObject.SetActive(false);
    }

	//public methods
    public void SetPosition(int index, Vector3 position)
    {
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        if (index < 0 || index > ropeParts.Length - 1)
            return;

        ropeParts[index].position = position;

        Refresh();
    }
    public void SetPosition(int index, Transform transformToFollow)
    {
        if (index < 0 || index > ropeParts.Length - 1)
            return;

        ropeParts[index].SetParent(transformToFollow);

        SetPosition(index, transformToFollow.position);
    }
    public void ResetParent()
    {
        for (int i = 0; i < ropeParts.Length; i++)
        {
            if (ropeParts[i].parent != transform)
                ropeParts[i].SetParent(transform);
        }
    }

	//private methods
    private void Refresh()
    {
        //Rotation
        Quaternion lastRotation = Quaternion.identity;
        for (int i = 0; i < ropeParts.Length; i++)
        {
            if (i == ropeParts.Length - 1)
            {
                ropeParts[i].rotation = lastRotation;
            }
            else
            {
                ropeParts[i].transform.LookAt(ropeParts[i + 1].transform);
                lastRotation = ropeParts[i].rotation;
            }
        }

        //Tiling
        if (ropeMaterial != null)
        {
            float distance = Vector3.Distance(ropeParts[0].position, ropeParts[ropeParts.Length - 1].position);
            ropeMaterial.material.SetTextureScale("_BumpMap", new Vector2(1, 6 * distance));
            ropeMaterial.material.SetTextureScale("_MainTex", new Vector2(6, 100 * distance));
        }
    }
}
