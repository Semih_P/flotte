﻿using UnityEngine;
using System.Collections;

public class Helper
 {
 	//public variables
    public static GameObject aimCursor;

 	//private variables
    private static CursorLockMode lockMode;
    private static Player localPlayer;

    //network
    private static Semih_Network network;
    private static PickupObjectManager pickupObjectManager;

	//public methods
    public static void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
        pickupObjectManager = ComponentManager<PickupObjectManager>.Get();
        localPlayer = ComponentManager<Player>.Get();
    }
    public static void Update()
    {
        Cursor.lockState = lockMode;
    }
    public static RaycastHit HitAtCursor(float rayDistance)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
        RaycastHit hit;

        Physics.Raycast(ray, out hit, rayDistance, LayerMasks.MASK_ignorePlayer);
        return hit;
    }
    public static RaycastHit HitAtCursor(float rayDistance, LayerMask mask)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
        RaycastHit hit;

        Physics.Raycast(ray, out hit, rayDistance, mask);
        return hit;
    }
    public static RaycastHit HitAtCursorCompareLayers(float rayDistance, LayerMask layerToCompareWith)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rayDistance, LayerMasks.MASK_ignorePlayer))
        {
            if (((1 << hit.transform.gameObject.layer) & layerToCompareWith) != 0)
            {
                return hit;
            }
        }

        return hit;
    }
    public static void SetCursorVisible(bool state)
    {
        Cursor.visible = state;
        if (aimCursor != null)
            aimCursor.SetActive(!state);
    }
    public static void SetCursorLockState(CursorLockMode mode)
    {
        Cursor.lockState = mode;
        lockMode = mode;
    }
    public static void SetCursorVisibleAndLockState(bool state, CursorLockMode mode)
    {
        SetCursorVisible(state);
        SetCursorLockState(mode);
    }
    public static Vector3 GetColliderExtents(Collider col)
    {
        if (col == null)
            return Vector3.zero;

        if (col.enabled)
        {
            if (!col.gameObject.activeInHierarchy)
            {
                col.gameObject.SetActive(true);
                Vector3 extents = col.bounds.extents;
                col.gameObject.SetActive(false);
                return extents;
            }
            else
                return col.bounds.extents;
        }
        else
        {
            col.enabled = true;
            Vector3 extents = col.bounds.extents;
            col.enabled = false;

            return extents;
        }
    }
    public static Vector3 GetColliderCenter(Collider col)
    {
        if (col == null)
            return Vector3.zero;

        if (col.enabled)
        {
            if (!col.gameObject.activeInHierarchy)
            {
                col.gameObject.SetActive(true);
                Vector3 center = col.bounds.center;
                col.gameObject.SetActive(false);

                return center;
            }
            else
                return col.bounds.center;
        }
        else
        {
            col.enabled = true;
            Vector3 center = col.bounds.center;
            col.enabled = false;

            return center;
        }
    }
    public static Collider[] GetCollisions(BoxCollider col, LayerMask mask, float extentsMultiplier = 1f)
    {
        return Physics.OverlapBox(GetColliderCenter(col), (col.size * 0.5f) * extentsMultiplier, col.transform.rotation, mask);
    }
    public static bool IsColliding(BoxCollider col, LayerMask mask, float extentsMultiplier = 1f)
    {
        Collider[] collisions = Physics.OverlapBox(GetColliderCenter(col), (col.size * 0.5f) * extentsMultiplier, col.transform.rotation, mask);
        foreach (Collider r in collisions)
        {
            if (r.transform == col.transform)
                continue;
            else
            {
                return true;
            }
        }

        return false;
    }
    public static bool IsColliding(BoxCollider col, LayerMask mask, BoxCollider[] excludeColliders, float extentsMultiplier = 1f)
    {
        Collider[] collisions = Physics.OverlapBox(GetColliderCenter(col), (col.size * 0.5f) * extentsMultiplier, col.transform.rotation, mask);
        foreach (Collider hitCol in collisions)
        {
            if (hitCol.transform == col.transform)
                continue;
            else
            {
                bool isInExcludeList = false;
                foreach(BoxCollider exCol in excludeColliders)
                {
                    if (hitCol == exCol)
                    {
                        isInExcludeList = true;
                        break;
                    }
                }
                if (isInExcludeList)
                    continue;
                else
                    return true;
            }
        }

        return false;
    }
    public static void DropItem(ItemInstance item, Vector3 position, Vector3 direction)
    {
        if (network.IsHost)
            pickupObjectManager.DropItem(item, position, direction, true);
        else
            network.SendP2P(network.HostID, new Message_DropItem(Messages.DropItem, pickupObjectManager, item, position, direction), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
    public static Vector3 GetRandomPointAroundOrigin(Vector3 origin, float minRadius, float maxRadius)
    {
        Vector3 point = origin + Random.insideUnitSphere.normalized * Random.Range(minRadius, maxRadius);
        return point;
    }
    public static bool HasClearVision(Vector3 origin, Vector3 target, LayerMask mask, float rayLength = float.PositiveInfinity)
    {
        Vector3 dirToPoint = target - origin;
        if (target == origin || dirToPoint.magnitude < 0.5f)
            return true;

        RaycastHit hit;
        if (Physics.Raycast(origin, dirToPoint.normalized, out hit, rayLength, mask))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public static bool HasClearVisionAndWithinRange(Vector3 origin, Vector3 target, LayerMask mask, float visionRange, float rayLength = float.PositiveInfinity)
    {
        Vector3 dirToPoint = target - origin;
        if (target == origin || dirToPoint.magnitude < 0.5f)
            return true;

        RaycastHit hit;
        if (Physics.Raycast(origin, dirToPoint.normalized, out hit, rayLength, mask))
        {
            return false;
        }
        else
        {
            float dist = Vector3.Distance(origin, target);
            if (dist <= visionRange)
                return true;
            else
                return false;
        }
    }
    public static bool LocalPlayerIsWithinDistance(Vector3 position, float requiredDistance)
    {
        if (localPlayer == null)
            localPlayer = ComponentManager<Player>.Get();

        float distance = Vector3.Distance(localPlayer.transform.position, position);

        return distance <= requiredDistance;
    }
    public static float MapValue(float min, float max, float value, float newMin, float newMax)
    {
        return newMin + (value - min) * (newMax - newMin) / (max - min);
    }
    public static void SetLayerOfTransformAndChildren(Transform trans, string layerName)
    {
        for (int i = 0; i < trans.childCount; i++)
        {
            trans.GetChild(i).gameObject.layer = LayerMask.NameToLayer(layerName);
            SetLayerOfTransformAndChildren(trans.GetChild(i), layerName);
        }
    }

	//private methods
}
