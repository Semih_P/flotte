﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Reef : MonoBehaviour 
{
	//public variables
    [HideInInspector]
    public List<PickupItem> pickups;

	//private variables

	//Unity methods

	//public methods
    public void ResetPickups()
    {
        foreach (PickupItem pickup in pickups)
        {
            pickup.gameObject.SetActive(true);
        }
    }

	//private methods
}
