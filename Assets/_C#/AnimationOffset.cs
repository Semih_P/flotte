﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationOffset : MonoBehaviour 
{
	//private variables
    private Animator anim;

	//Unity methods
	void Start () 
	{
        anim = GetComponent<Animator>();

        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        anim.Play(info.fullPathHash, -1, Random.Range(0f, 1f));
	}	

	//public methods

	//private methods
}
