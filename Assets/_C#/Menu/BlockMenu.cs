﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class BlockMenu : MonoBehaviour
 {
 	//public variables
    public PieMenu piemenu;

    [Header("UI components")]
    public Text UIName;
    public Text UIDescription;
    public Image UIImage;
    public List<ItemCostBox> costboxes = new List<ItemCostBox>();

    [Header("Items to build with")]
    public List<Block> blockItems = new List<Block>();

    public bool IsActive { get { return gameObject.activeInHierarchy; } }
    [HideInInspector]
    public BlockCreator blockCreator;

 	//private variables


 	//Unity methods
    void Awake()
    {
        blockCreator = GameObject.FindObjectOfType<BlockCreator>();

        piemenu.Create(blockItems.Count);

        for (int i = 0; i < blockItems.Count; i++)
        {
            PieSlice slice = piemenu.GetSlice(i);
            slice.centerImage.sprite = blockItems[i].buildableItem.settings_Inventory.GetSprite();
        }
    }
    void Update()
    {
        int index = piemenu.GetPressButtonIndex();
        Block block = blockItems[index];
        SelectBlock(block);

        if (Input.GetButtonUp("RightClick"))
            SetState(false);
    }

	//public methods
    public void SetState(bool state)
    {
        GameManager.IsInMenu = state;
        GameManager.IsInBuildMenu = state;
        gameObject.SetActive(state);

        if (state)
        {
            Helper.SetCursorVisibleAndLockState(false, CursorLockMode.None);
        }
        else
        {
            Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);
        }
    }
    public void SelectBlock(Block block)
    {
        if (blockCreator != null && block != null)
        {
            blockCreator.SetBlockTypeToBuild(block.buildableItem.UniqueName);

            UIImage.sprite = block.buildableItem.settings_Inventory.GetSprite();
            UIName.text = block.buildableItem.settings_Inventory.GetDisplayName();
            UIDescription.text = block.buildableItem.settings_Inventory.GetDescription();

            DisableCostBoxes();
            for (int i = 0; i < block.buildCost.Count; i++)
            {
                Cost cost = block.buildCost[i];
                ItemCostBox box = costboxes[i];

                box.gameObject.SetActive(true);
                box.SetCost(cost);
            }
        }
    }
    

	//private methods
    private void DisableCostBoxes()
    {
        foreach(ItemCostBox box in costboxes)
        {
            box.gameObject.SetActive(false);
        }
    }
}
