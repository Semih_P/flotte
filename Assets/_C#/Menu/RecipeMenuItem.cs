﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RecipeMenuItem : MonoBehaviour
 {
 	//public variables
    public Image recipeImage;
    public Text displayName;

 	//private variables
    private Recipe recipe;
    private CraftingMenu menu;


 	//Unity methods
	void Start () 
	{
        menu = GameObject.FindObjectOfType<CraftingMenu>();
	}

	//public methods
    public void AttachRecipe(Recipe recipe)
    {
        this.recipe = recipe;
        recipeImage.sprite = recipe.itemToCraft.item.settings_Inventory.GetSprite();
        displayName.text = recipe.itemToCraft.item.settings_Inventory.GetDisplayName();
    }
    public void OnButtonClick()
    {
        menu.SelectRecipe(recipe);
    }
	//private methods
}
