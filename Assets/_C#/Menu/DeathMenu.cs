﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathMenu : SingletonGeneric<DeathMenu> 
{
	//public variables
    public FadePanel fadePanel;
    public GameObject deathMenuParent;

 	//private variables


 	//Unity methods
    void Awake()
    {
        deathMenuParent.SetActive(false);
    }
    void Update()
    {
        if (deathMenuParent.activeInHierarchy)
            Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
    }
	//public methods
    public void Show()
    {
        deathMenuParent.SetActive(true);
        fadePanel.SetAlpha(0f);
        StartCoroutine(fadePanel.FadeToAlpha(1f, 0.5f));
    }


	//private methods

}
