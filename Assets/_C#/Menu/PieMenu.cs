﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PieMenu : MonoBehaviour
{
    //Delegate
 	//public variables
    [Header("Pie settings")]
    public Image piePrefab;

    [Header("ColorSettings")]
    public Color normalColor = Color.white;
    public Color highlightColor = Color.gray;

 	//private variables
    private List<PieSlice> slices = new List<PieSlice>();
    private float anglePerButton;
    private Vector2 fakeBoxPos;
    private Vector2 prevMousepos;

    private int selectedIndex;
    private int previousIndex;

 	//Unity methods
	void Update () 
	{
        GetItemInMenu();
	}

	//public methods
    public void Create(int pieCount)
    {
        anglePerButton = 360f / pieCount;

        for (int i = 0; i < pieCount; i++)
        {
            GameObject obj = Instantiate(piePrefab.gameObject, transform) as GameObject;
            obj.transform.localScale = piePrefab.transform.localScale;
            
            PieSlice slice = obj.GetComponent<PieSlice>();

            Image image = obj.GetComponent<Image>();
            image.type = Image.Type.Filled;
            image.fillMethod = Image.FillMethod.Radial360;
            image.fillOrigin = 1;
            image.fillAmount = anglePerButton / 360f;
            image.fillClockwise = false;

            RectTransform rect = image.GetComponent<RectTransform>();
            rect.localPosition = Vector2.zero;
            rect.localEulerAngles = new Vector3(0, 0, -anglePerButton * i);
            rect.offsetMin = rect.offsetMax = Vector2.zero;

            slices.Add(slice);
            slice.Initialize();
        }

        selectedIndex = previousIndex = 0;
    }
    public PieSlice GetSlice(int index)
    {
        if (index >= 0 && index < slices.Count)
            return slices[index];
        else
            return null;
    }

	//private methods
    private void GetItemInMenu()
    {
        Vector2 screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);
        Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 delta = mousePosition - prevMousepos; delta.Normalize();

        fakeBoxPos += delta * 20;

        float distanceToCenter = Vector2.Distance(fakeBoxPos, screenCenter);
        if (distanceToCenter > 100)
        {
            fakeBoxPos = screenCenter + (fakeBoxPos - screenCenter).normalized * 100;
        }
        else if (distanceToCenter < 20)
        {
            fakeBoxPos = screenCenter + (fakeBoxPos - screenCenter).normalized * 20;
        }
        

        Vector2 dir = fakeBoxPos - screenCenter; dir.Normalize();
        float angle = 360 - (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) - 270;
        if (angle < 0)
            angle += 360;

        int index = (int)(angle / anglePerButton);

        if (index != selectedIndex && index >= 0 && index < slices.Count)
        {
            previousIndex = selectedIndex;
            selectedIndex = index;

            Image newImage = slices[index].radialImage;
            Image prevImage = slices[previousIndex].radialImage;

            prevImage.color = normalColor;
            newImage.color = highlightColor;
        }
        prevMousepos = mousePosition;
    }
    public int GetPressButtonIndex()
    {
        return selectedIndex;
    }
}
