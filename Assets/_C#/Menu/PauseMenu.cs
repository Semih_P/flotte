﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
 {
    public static bool IsOpen;
    private delegate void ExitMethod(bool forceExit);

 	//public variables
    public GameObject pauseMenuParent;
    public GameObject yesNoSaveLayout;
    public Button saveButton;
    public Text saveButtonText;

 	//private variables
    private bool hasSavedGame = false;
    private ExitMethod exitMethod;
    //private Settings settings;

 	//Unity methods
    void Start()
    {
        //settings = ComponentManager<Settings>.Get();
        Time.timeScale = 1;
        CloseMenu();
    }
    void Update()
    {
        if (IsOpen && !GameManager.IsInMenu)
            GameManager.IsInMenu = true;

        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    if (!IsOpen)
        //        OpenPauseMenu();
        //}

        saveButton.interactable = !hasSavedGame;
        saveButtonText.text = saveButton.interactable ? "Save game" : "Game is saved";
    }

	//public methods
    public void ContinuePlaying()
    {
        ComponentManager<SoundManager>.Get().PlaySound("UIClick");
        CloseMenu();
    }
    public void SaveGame()
    {
        if (!hasSavedGame)
        {
            hasSavedGame = true;
            SaveAndLoad.Singleton.Save();
        }
    }
    public void ExitToMainMenu(bool forceExit = false)
    {
        if (!hasSavedGame && !forceExit)
        {
            SetYesNoSaveLayout(true);
            exitMethod = ExitToMainMenu;
        }
        else
        {
            ComponentManager<SoundManager>.Get().PlaySound("UIClick");;
            SceneManager.LoadScene("MainMenuScene");
            Time.timeScale = 1;
        }
    }
    public void ExitApplication(bool forceExit = false)
    {
        if (!hasSavedGame && !forceExit)
        {
            SetYesNoSaveLayout(true);
            exitMethod = ExitApplication;
        }
        else
        {
            ComponentManager<SoundManager>.Get().PlaySound("UIClick");;
            Application.Quit();
        }
    }
    public void ExitWithSetMethod(bool forceExit)
    {
        if (exitMethod != null)
            exitMethod(forceExit);
    }

    public void SetYesNoSaveLayout(bool state)
    {
        yesNoSaveLayout.gameObject.SetActive(state);

        if (state == false)
            exitMethod = null;
    }
    public void OpenPauseMenu()
    {
        if (GameManager.IsInMenu)
            return;

        IsOpen = true;
        Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
        Time.timeScale = 0;
        
        pauseMenuParent.SetActive(true);
    }
    public void CloseMenu()
    {
        Time.timeScale = 1;
        IsOpen = false;
        GameManager.IsInMenu = false;
        Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);

        exitMethod = null;
        pauseMenuParent.SetActive(false);
        SetYesNoSaveLayout(false);
        hasSavedGame = false;
    }

    public void OpenOptionMenu()
    {
        pauseMenuParent.SetActive(false);
    }

	//private methods
}
