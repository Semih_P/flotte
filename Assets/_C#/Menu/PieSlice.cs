﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PieSlice : MonoBehaviour
 {
 	//public variables
    public Image radialImage;
    public Image centerImage;
    public Vector2 centerImageSize;
    [Range(0, 1)]
    public float imageOffsetFromCenter;

 	//private variables

 	//Unity methods
    void OnValidate()
    {
        SetImageAtCenterOfSlice();
    }

	//public methods
    public void Initialize()
    {
        SetImageAtCenterOfSlice();
    }

	//private methods
    private void SetImageAtCenterOfSlice()
    {
        float zRotation = radialImage.transform.eulerAngles.z;
        float radialAngle = (radialImage.fillAmount * 360f);
        float angle = (radialAngle / 2f) + zRotation;
        float rad = angle * Mathf.Deg2Rad;

        Vector3 dir = new Vector3(Mathf.Cos(rad), Mathf.Sin(rad)); dir.Normalize();

        if (centerImage != null)
        {
            RectTransform radialRect = radialImage.GetComponent<RectTransform>();
            RectTransform centerRect = centerImage.GetComponent<RectTransform>();
            
            Vector3[] corners = new Vector3[4];
            radialRect.GetWorldCorners(corners);

            Vector3 distanceToCorner = corners[2] - radialRect.position;
            centerRect.position = radialRect.position + distanceToCorner * imageOffsetFromCenter;

            float distance = Vector3.Distance(centerRect.position, radialRect.position);
            centerRect.position = radialRect.position + dir * distance;

            centerRect.eulerAngles = Vector3.zero;
            centerRect.sizeDelta = centerImageSize;
        }
    }
}
