﻿//using PlayWay.Water;
//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class Settings : MonoBehaviour
{
//    //Properties
    public float MouseSensitivty
    {
        get
        {
            return mouseSensitivity;
        }
        private set
        {
            mouseSensitivity = value;
        }
    }
    private float mouseSensitivity;

    public bool InvertYAxis
    {
        get
        {
            return invertYAxis;
        }
        private set
        {
            invertYAxis = value;
        }
    }
    private bool invertYAxis;

//    public float MasterVolume 
//    { 
//        get
//        {
//            return masterVolume;
//        }
//        private set
//        {
//            masterVolume = value;
//        }
//    }
//    private float masterVolume;

//    public float AmbienceVolume
//    {
//        get
//        {
//            return ambienceVolume;
//        }
//        private set
//        {
//            ambienceVolume = value;
//        }
//    }
//    private float ambienceVolume;

//    public float SFXVolume
//    {
//        get
//        {
//            return sfxVolume;
//        }
//        private set
//        {
//            sfxVolume = value;
//        }
//    }
//    private float sfxVolume;

//    public float UIVolume
//    {
//        get
//        {
//            return uiVolume;
//        }
//        private set
//        {
//            uiVolume = value;
//        }
//    }
//    private float uiVolume;

//    public float MusicVolume
//    {
//        get
//        {
//            return musicVolume;
//        }
//        private set
//        {
//            musicVolume = value;
//        }
//    }
//    private float musicVolume;

//    public int WaterQuality
//    { 
//        get
//        {
//            return waterQuality;
//        }
//        private set
//        {
//            waterQuality = value;
//        }
//    }
//    private int waterQuality;

//    public bool AmbientOcclusion
//    { 
//        get
//        {
//            return ambientOcclusion;
//        }
//        private set
//        {
//            ambientOcclusion = value;
//        }
//    }
//    private bool ambientOcclusion;

//    public bool SunShafts
//    {
//        get
//        {
//            return sunShafts;
//        }
//        private set
//        {
//            sunShafts = value;
//        }
//    }
//    private bool sunShafts;

//    public bool CameraBob
//    {
//        get
//        {
//            return cameraBob;
//        }
//        private set
//        {
//            cameraBob = value;
//        }
//    }
//    private bool cameraBob;

//    public bool Antialiasing
//    {
//        get
//        {
//            return antialiasing;
//        }
//        private set
//        {
//            antialiasing = value;
//        }
//    }
//    private bool antialiasing;

//    public bool AzureSkyFog
//    { 
//        get
//        {
//            return azureSkyFog;
//        }
//        set
//        {
//            azureSkyFog = value;
//            SetBehaviourOnCamera<AzureFog>(azureSkyFog);
//        }
//    }
//    private bool azureSkyFog;

//    //public variables
//    private Camera localCamera;
//    private WaterCamera waterCamera;

//    //private varibles
    private static bool initialized;

//    //Unity methods
    void Awake()
    {
        if (initialized)
        {
            Destroy(gameObject);
            return;
        }
        initialized = true;
        DontDestroyOnLoad(gameObject);

        ComponentManager<Settings>.Set(this);
    }

//    //public methods

//    //Controls
//    public void SetMouseSensitivty(Slider slider)
//    {
//        MouseSensitivty = slider.value;
//    }
//    public void SetInvertYAxis(Toggle toggle)
//    {
//        InvertYAxis = toggle.isOn;
//    }

//    //Audio
//    public void SetMasterVolume(Slider slider)
//    {
//        MasterVolume = slider.value;
//        //OptionsMenu.mixer.SetFloat("VolumeMaster", MasterVolume);
//    }
//    public void SetAmbienceVolume(Slider slider)
//    {
//        AmbienceVolume = slider.value;
//        //OptionsMenu.mixer.SetFloat("VolumeAmbience", AmbienceVolume);
//    }
//    public void SetSFXVolume(Slider slider)
//    {
//        SFXVolume = slider.value;
//        //OptionsMenu.mixer.SetFloat("VolumeSFX", SFXVolume);
//    }
//    public void SetMusicVolume(Slider slider)
//    {
//        MusicVolume = slider.value;
//        //OptionsMenu.mixer.SetFloat("VolumeMusic", MusicVolume);
//    }
//    public void SetUIVolume(Slider slider)
//    {
//        UIVolume = slider.value;
//        //OptionsMenu.mixer.SetFloat("VolumeUI", UIVolume);
//    }

//    //Video
//    public void SetWaterQuality(Slider slider)
//    {
//        WaterQuality = (int)slider.value;
//        WaterQualitySettings.Instance.SetQualityLevel(WaterQuality);
//    }
//    public void SetAmbientOcclusion(Toggle toggle)
//    {
//        AmbientOcclusion = toggle.isOn;
//        SetBehaviourOnCamera<ScreenSpaceAmbientOcclusion>(toggle.isOn);
//    }
//    public void SetSunshaft(Toggle toggle)
//    {
//        SunShafts = toggle.isOn;
//        SetBehaviourOnCamera<SunShafts>(toggle.isOn);
//    }
//    public void SetCameraBob(Toggle toggle)
//    {
//        CameraBob = toggle.isOn;
//        SetBehaviourOnCamera<CameraBob>(toggle.isOn);
//    }
//    public void SetAntialiasing(Toggle toggle)
//    {
//        Antialiasing = toggle.isOn;
//        SetBehaviourOnCamera<Antialiasing>(toggle.isOn);
//    }

//    //Save and load
//    public void SaveSettings()
//    {
//        //Save controls
//        PlayerPrefs.SetFloat("MouseSensitivity", MouseSensitivty);
//        PlayerPrefs.SetInt("InvertYAxis", InvertYAxis ? 1 : 0);

//        //Save audio
//        PlayerPrefs.SetFloat("MasterVolume", MasterVolume);
//        PlayerPrefs.SetFloat("AmbienceVolume", AmbienceVolume);
//        PlayerPrefs.SetFloat("SFXVolume", SFXVolume);
//        PlayerPrefs.SetFloat("MusicVolume", MusicVolume);
//        PlayerPrefs.SetFloat("UIVolume", UIVolume);

//        //Save video
//        PlayerPrefs.SetInt("WaterQuality", WaterQuality);
//        PlayerPrefs.SetInt("AmbientOcclusion", AmbientOcclusion ? 1 : 0);
//        PlayerPrefs.SetInt("SunShaft", SunShafts ? 1 : 0);
//        PlayerPrefs.SetInt("CameraBob", CameraBob ? 1 : 0);
//        PlayerPrefs.SetInt("Antialiasing", Antialiasing ? 1 : 0);
//    }
    public void LoadSettings(Camera camera)
    {
        //this.localCamera = camera;
        //this.waterCamera = camera.GetComponent<WaterCamera>();

        //Load controls
        MouseSensitivty = PlayerPrefs.GetFloat("MouseSensitivity", 2.25f);
        InvertYAxis = PlayerPrefs.GetInt("InvertYAxis", 0) == 1 ? true : false;

        //OptionsMenu.MouseSensitivitySlider.value = MouseSensitivty;
        //OptionsMenu.InvertYAxisToggle.isOn = InvertYAxis;

        //Load audio
        //MasterVolume = PlayerPrefs.GetFloat("MasterVolume", 0);
        //AmbienceVolume = PlayerPrefs.GetFloat("AmbienceVolume", 0);
        //SFXVolume = PlayerPrefs.GetFloat("SFXVolume", 0);
        //MusicVolume = PlayerPrefs.GetFloat("MusicVolume", 0);
        //UIVolume = PlayerPrefs.GetFloat("UIVolume", 0);

        //OptionsMenu.masterVolumeSlider.value = MasterVolume;
        //OptionsMenu.ambienceVolumeSlider.value = AmbienceVolume;
        //OptionsMenu.sfxVolumeSlider.value = SFXVolume;
        //OptionsMenu.musicVolumeSlider.value = MusicVolume;
        //OptionsMenu.uiVolumeSlider.value = UIVolume;

        //Invoke("SetMixerParameters", 0.001f);

        //Load video
        //WaterQuality = PlayerPrefs.GetInt("WaterQuality", 5);
        //WaterQualitySettings.Instance.SetQualityLevel(WaterQuality);
        //OptionsMenu.waterQualitySlider.value = WaterQuality;

        //AmbientOcclusion = PlayerPrefs.GetInt("AmbientOcclusion", 1) == 1 ? true : false;
        //SunShafts = PlayerPrefs.GetInt("SunShaft", 1) == 1 ? true : false;
        //CameraBob = PlayerPrefs.GetInt("CameraBob", 1) == 1 ? true : false;
        //Antialiasing = PlayerPrefs.GetInt("Antialiasing", 1) == 1 ? true : false;

        //SetBehaviourOnCamera<ScreenSpaceAmbientOcclusion>(AmbientOcclusion);
        //SetBehaviourOnCamera<SunShafts>(SunShafts);
        //SetBehaviourOnCamera<CameraBob>(CameraBob);
        //SetBehaviourOnCamera<Antialiasing>(Antialiasing);

        //OptionsMenu.ambientOcclusionToggle.isOn = AmbientOcclusion;
        //OptionsMenu.sunshaftToggle.isOn = SunShafts;
        //OptionsMenu.cameraBobToggle.isOn = CameraBob;
        //OptionsMenu.antialisingToggle.isOn = Antialiasing;
    }
//    public void SubmersionChanged()
//    {
//        switch (waterCamera.SubmersionState)
//        {
//            case SubmersionState.Full:
//                AzureSkyFog = false;
//                break;
//            case SubmersionState.None:
//                AzureSkyFog = true;
//                break;
//            case SubmersionState.Partial:
//                AzureSkyFog = true;
//                break;
//        }
//    }

//    //private methods
//    private void SetMixerParameters()
//    {
//        //OptionsMenu.mixer.SetFloat("VolumeMaster", MasterVolume);
//        //OptionsMenu.mixer.SetFloat("VolumeAmbience", AmbienceVolume);
//        //OptionsMenu.mixer.SetFloat("VolumeSFX", SFXVolume);
//        //OptionsMenu.mixer.SetFloat("VolumeMusic", MusicVolume);
//        //OptionsMenu.mixer.SetFloat("VolumeUI", UIVolume);
//    }
//    private void SetBehaviourOnCamera<T>(bool enabled) where T : Behaviour
//    {
//        return;
//        T behaviour = localCamera.GetComponent<T>();

//        if (behaviour != null && behaviour.enabled != enabled)
//            behaviour.enabled = enabled;
//    }
}
