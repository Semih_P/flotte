﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemCostBox : MonoBehaviour
 {
 	//public variables
    public Text costLabel;
    public Text displayName;
    public Image itemImage;
    public Cost cost;

 	//private variables
    private PlayerInventory playerInventory;

 	//Unity methods
    void Start()
    {
        playerInventory = ComponentManager<PlayerInventory>.Get();
    }
    void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            Refresh();
        }
    }
	//public methods
    public void Refresh()
    {
        if (cost == null)
            return;

        SetCost(cost);
    }
    public void SetCost(Cost cost)
    {
        if (cost == null)
            return;

        if (playerInventory == null)
        {
            playerInventory = ComponentManager<PlayerInventory>.Get();
        }


        this.cost = cost;
        itemImage.sprite = cost.item.settings_Inventory.GetSprite();
        if (displayName != null)
            displayName.text = cost.item.settings_Inventory.GetDisplayName();

        if (playerInventory != null)
            costLabel.text = playerInventory.GetItemCount(cost.item.UniqueName).ToString() + "/" + cost.amount;
    }

	//private methods
}
