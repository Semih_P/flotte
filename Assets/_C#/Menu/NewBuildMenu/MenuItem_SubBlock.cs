﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuItem_SubBlock : MonoBehaviour 
{
	//public variables

	//private variables
    [SerializeField] private Image icon;
    [SerializeField]
    private Item_Base buildableItem;
    private MenuItem_MainBlock menuItemMainBlock;
    private BuildMenu buildMenu;

	//Unity methods
    void Awake()
    {
        icon.sprite = buildableItem.settings_Inventory.GetSprite();
        menuItemMainBlock = GetComponentInParent<MenuItem_MainBlock>();
    }
    void Start()
    {
        buildMenu = ComponentManager<BuildMenu>.Get();
    }
    void OnValidate()
    {
        if (icon != null && buildableItem != null)
            icon.sprite = buildableItem.settings_Inventory.GetSprite();
    }
	//public methods
    public void OnPointerEnter()
    {
        buildMenu.OnPointerEnterSubBlock(buildableItem);
        menuItemMainBlock.ViewBlock(buildableItem);
    }
    public Item_Base GetBuildable()
    {
        return buildableItem;
    }

	//private methods
}
