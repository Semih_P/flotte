﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildMenu : MonoBehaviour
{
	//public variables

	//private variables
    [Header("References")]
    [SerializeField] private GameObject buildPanel;
    [SerializeField] private Image selectedBlockImage;
    [SerializeField] private Text selectedBlockDescription;
    [SerializeField] private CostCollection costColletionPanel;
    [SerializeField] private CostCollection costColletionCursor;
    [SerializeField] private RectTransform categoryBackground;
    [SerializeField] private Vector3 categoryBackgroundOffset;

    private GameObject currentSubParent;
    private BlockCreator blockCreator;
    private Player player;
    private Item_Base currentBuildable;

	//Unity methods
    void Awake()
    {
        ComponentManager<BuildMenu>.Set(this);
    }
	void Start () 
	{
        buildPanel.SetActive(false);
	}
    public void Initialize(BlockCreator blockCreator, Player player)
    {
        this.blockCreator = blockCreator;
        this.player = player;
    }
	void Update () 
	{
        if (blockCreator == null || blockCreator.GetPlayerNetwork() == null || !blockCreator.GetPlayerNetwork().IsLocalPlayer)
            return;

        if (!blockCreator.gameObject.activeInHierarchy && buildPanel.activeInHierarchy)
            DisableMenu();

        if (!blockCreator.gameObject.activeInHierarchy)
        {
            costColletionCursor.gameObject.SetActive(false);
            return;
        }
        if (blockCreator.selectedBlock != null && blockCreator.selectedBlock.buildCost.Count == 0)
            return;

        if (Input.GetButtonDown("RightClick") && !buildPanel.activeInHierarchy)
        {
            EnableMenu();
        }
        else if (Input.GetButtonUp("RightClick") && buildPanel.activeInHierarchy)
        {
            DisableMenu();
        }

        //Update cost at cursor
        if (currentBuildable != null && !buildPanel.activeInHierarchy)
        {
            if (!costColletionCursor.gameObject.activeInHierarchy)
                costColletionCursor.gameObject.SetActive(true);

            costColletionCursor.ShowCost(currentBuildable.settings_buildable.GetBlockPrefab().buildCost);
        }
        if (GameManager.IsInMenu && !GameManager.IsInBuildMenu && costColletionCursor.gameObject.activeInHierarchy)
            costColletionCursor.gameObject.SetActive(false);

        //Update cost in panel
        if (buildPanel.activeInHierarchy && currentBuildable != null)
        {
            costColletionPanel.ShowCost(currentBuildable.settings_buildable.GetBlockPrefab().buildCost);
        }
	}

	//public methods
    public void OnPointerEnterMainBlock(GameObject subParent, Item_Base buildableItem)
    {
        //Turn off any previous subparent
        if (currentSubParent != null && currentSubParent != subParent)
        {
            currentSubParent.SetActive(false);
        }

        //Activate new subparent
        currentSubParent = subParent;
        currentSubParent.SetActive(true);
        categoryBackground.gameObject.SetActive(true);
        categoryBackground.position = currentSubParent.transform.position + categoryBackgroundOffset;
        categoryBackground.sizeDelta = new Vector2(110 + (subParent.transform.childCount - 1)* 77 , categoryBackground.sizeDelta.y);

        SelectBlock(buildableItem);
    }
    public void OnPointerEnterSubBlock(Item_Base buildableSubItem)
    {
        SelectBlock(buildableSubItem);
    }
	
    //private methods
    private void SelectBlock(Item_Base buildableItem)
    {
        if (buildableItem == null)
            return;

        selectedBlockImage.sprite = buildableItem.settings_Inventory.GetSprite();
        selectedBlockDescription.text = buildableItem.settings_Inventory.GetDisplayName();

        currentBuildable = buildableItem;
        blockCreator.SetBlockTypeToBuild(currentBuildable.UniqueName);
    }

    private void EnableMenu()
    {
        GameManager.IsInMenu = true;
        GameManager.IsInBuildMenu = true;
        buildPanel.SetActive(true);
        player.SetMouseLookScripts(false);
        Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
    }
    private void DisableMenu()
    {
        GameManager.IsInMenu = false;
        GameManager.IsInBuildMenu = false;
        buildPanel.SetActive(false);
        player.SetMouseLookScripts(true);
        Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);

        if (currentSubParent != null)
        {
            currentSubParent.SetActive(false);
            currentSubParent = null;
            categoryBackground.gameObject.SetActive(false);
        }
    }
}
