﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class MenuItem_MainBlock : MonoBehaviour 
{
	//public variables

	//private variables
    [SerializeField] private Image icon;
    [SerializeField] private GameObject subParent;
    private Item_Base currentBuildable;
    private BuildMenu buildMenu;
	//Unity methods
	void Start () 
	{
        buildMenu = ComponentManager<BuildMenu>.Get();

        MenuItem_SubBlock[] buildableItems = subParent.transform.GetComponentsInChildren<MenuItem_SubBlock>();
        if (buildableItems.Length > 0)
            ViewBlock(buildableItems[0].GetBuildable());

        subParent.SetActive(false);
	}

	//public methods
    public void OnPointerEnter()
    {
        buildMenu.OnPointerEnterMainBlock(subParent, currentBuildable);
    }
    public void ViewBlock(Item_Base buildableItem)
    {
        if (buildableItem != null)
        {
            currentBuildable = buildableItem;
            icon.sprite = buildableItem.settings_Inventory.GetSprite();
        }
    }

	//private methods
}
