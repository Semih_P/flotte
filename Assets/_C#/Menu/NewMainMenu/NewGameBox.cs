﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewGameBox : MenuBox 
{
	//public variables
    [Header("Components")]
    [SerializeField] private InputField inputfield_GameName;
    [SerializeField] private Toggle toggle_AllowFriends;
    [SerializeField] private Text text_ModeDescription;

    [Space(10)]
    [Header("Other")]
    [TextArea(1, 3)]
    [SerializeField]
    private string easyDescription = "Easy";

    [TextArea(1, 3)]
    [SerializeField]
    private string normalDescription = "Normal";

    [TextArea(1, 3)]
    [SerializeField]
    private string hardDescription = "Hard";

	//private variables
    private Semih_Network network;

	//Unity methods
    void OnEnable()
    {
        Button_Easy();
    }
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
    }
	//public methods

	//private methods

    //Button methods
    public void Button_Close()
    {
        Hide();
    }
    public void Button_Easy()
    {
        text_ModeDescription.text = easyDescription;
    }
    public void Button_Normal()
    {
        text_ModeDescription.text = normalDescription;
    }
    public void Button_Hard()
    {
        text_ModeDescription.text = hardDescription;
    }
    public void Button_CreateNewGame()
    {
        network.HostGame();
    }
}
