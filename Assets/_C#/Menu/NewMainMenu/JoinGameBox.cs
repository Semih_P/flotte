﻿using Steamworks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JoinGameBox : MenuBox 
{
	//public variables
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private JoinGame_Selection gameSelectionPrefab;

	//private variables
    private Semih_Network network;
    private CSteamID selectedSteamID;

	//Unity methods
    void Start()
    {
        network = ComponentManager<Semih_Network>.Get();
    }
	//public methods
    public override void OnBoxEnabled()
    {
        RefreshGames();
    }
	//private methods
    private void RefreshGames()
    {
        int childCount = scrollRect.content.childCount;
        for (int i = 0; i < childCount; i++)
            DestroyImmediate(scrollRect.content.GetChild(0).gameObject);

        int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagAll);
        for (int i = 0; i < friendCount; i++)
        {
            CSteamID friendID = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagAll);
            string friendName = SteamFriends.GetFriendPersonaName(friendID);
            if (friendName.Contains("Trassan") || friendName.Contains("Levlette") || friendName.Contains("SVEN BENGT") || friendName.Contains("Laptop"))
            {
                JoinGame_Selection gameSelection = Instantiate(gameSelectionPrefab, scrollRect.content) as JoinGame_Selection;
                gameSelection.Set(friendID);
            }
        }
    }

    //Button methods
    public void Button_SelectGame(JoinGame_Selection gameSelection)
    {
        selectedSteamID = gameSelection.steamID;
    }
    public void Button_Close()
    {
        Hide();
    }
    public void Button_Join()
    {
        if (selectedSteamID.IsValid())
            network.TryToJoinGame(selectedSteamID);
    }
    public void Button_Refresh()
    {
        RefreshGames();
    }
}
