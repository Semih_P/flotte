﻿using Steamworks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JoinGame_Selection : MonoBehaviour 
{
	//public variables
    [HideInInspector] public CSteamID steamID;
    [SerializeField] private Text text_GameName;
    [SerializeField] private Image image_Avatar;

	//private variables

	//Unity methods
    void Awake()
    {
        JoinGameBox joinGameBox = GetComponentInParent<JoinGameBox>();
        EventTrigger trigger = GetComponent<EventTrigger>();

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((eventData) => { joinGameBox.Button_SelectGame(this); });
        trigger.triggers.Add(entry);
    }

	//public methods
    public void Set(CSteamID steamID)
    {
        this.steamID = steamID;
        this.text_GameName.text = Steamworks.SteamFriends.GetFriendPersonaName(steamID);
        StartCoroutine(SetAvatar(this.image_Avatar, steamID));
    }

	//private methods
    private IEnumerator SetAvatar(Image target, CSteamID steamid)
    {
        int FriendAvatar = SteamFriends.GetLargeFriendAvatar(steamid);

        while (FriendAvatar == -1)
            yield return null;

        if (FriendAvatar > 0)
        {
            uint ImageWidth;
            uint ImageHeight;
            bool ret = SteamUtils.GetImageSize(FriendAvatar, out ImageWidth, out ImageHeight);

            if (ret && ImageWidth > 0 && ImageHeight > 0)
            {
                byte[] Image = new byte[ImageWidth * ImageHeight * 4];

                ret = SteamUtils.GetImageRGBA(FriendAvatar, Image, (int)(ImageWidth * ImageHeight * 4));
                Texture2D m_MediumAvatar = new Texture2D((int)ImageWidth, (int)ImageHeight, TextureFormat.RGBA32, false, true);
                m_MediumAvatar.LoadRawTextureData(Image);
                m_MediumAvatar.Apply();

                target.sprite = Sprite.Create(m_MediumAvatar, new Rect(0, 0, ImageWidth, ImageHeight), new Vector2(0.5f, 0.5f));
            }
        }
    }
}
