﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGameBox : MenuBox 
{
	//public variables

	//private variables
    private StartMenuScreen menuScreen;

	//Unity methods
    void Awake()
    {
        menuScreen = GetComponentInParent<StartMenuScreen>();
    }
	//public methods

	//private methods

    //Button methods
    public void Button_ExitYes()
    {
        menuScreen.ExitApplication();
    }
    public void Button_ExitNo()
    {
        Hide();
    }
}
