﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenuScreen : MonoBehaviour
{
    //public variables

    //private variables
    private MenuBox[] menuBoxes;

    //Unity methods
    void Start()
    {
        Time.timeScale = 1;
        menuBoxes = GetComponentsInChildren<MenuBox>(true);
        HideAllBoxes();
    }

    //public variables
    public void ExitApplication()
    {
        Application.Quit();
    }
    public void ShowBox(MenuBox box)
    {
        if (!box.gameObject.activeInHierarchy)
            HideAllBoxes();
        box.gameObject.SetActive(!box.gameObject.activeInHierarchy);

        if (box.gameObject.activeInHierarchy)
            box.OnBoxEnabled();
        else
            box.OnBoxDisabled();
    }
    
    //private variables
    private void LoadInSavedGames()
    {
        DirectoryInfo info = new DirectoryInfo(SaveAndLoad.Singleton.Path);
        FileInfo[] files = info.GetFiles();

        SaveAndLoad.Singleton.SavedGameNames = new string[files.Length];

        //var children = new List<GameObject>();
        //foreach (Transform child in buttonParent) children.Add(child.gameObject);
        //children.ForEach(child => Destroy(child));

        //for (int i = 0; i < files.Length; i++)
        //{
        //    SaveAndLoad.Singleton.SavedGameNames[i] = files[i].Name;
        //    SavedGameButton button = Instantiate(savedGameButtonPrefab, buttonParent) as SavedGameButton;
        //    button.transform.localScale = savedGameButtonPrefab.transform.localScale;
        //    string displayName = files[i].Name.Split('.').First();
        //    button.SetText(displayName);
        //    button.savedGameName = files[i].Name;

        //    if (button.savedGameName.Contains("DEAD-") || button.savedGameName.Contains("OldVersion-"))
        //    {
        //        button.SetDead();
        //    }
        //}

        //int overflow = files.Length  > 4 ? files.Length - 4 : 0;
        //scrollViewContent.sizeDelta = new Vector2(scrollViewContent.sizeDelta.x, 170 + 30 * overflow);
    }
    private void HideAllBoxes()
    {
        foreach (MenuBox box in menuBoxes)
            box.gameObject.SetActive(false);
    }

    //Button methods
}
