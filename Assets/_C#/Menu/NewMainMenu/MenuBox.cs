﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBox : MonoBehaviour 
{
	//public variables

	//private variables

	//Unity methods

	//public methods
    public virtual void OnBoxEnabled()
    {

    }
    public virtual void OnBoxDisabled()
    {

    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }

	//private methods
}
