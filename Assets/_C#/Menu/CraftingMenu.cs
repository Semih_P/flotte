﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public enum CraftingCategory { Tools, Placeables, Basics, Decorations, Equipment, Nothing}
public class CraftingMenu : MonoBehaviour
 {
 	//public variables
    public GameObject recipeMenuItem;
    public RectTransform recipeMenuItemParent;
    public SelectedRecipeBox selectedRecipeBox;
    public Image selectedCategoryImage;
    public Text selectedCategoryText;

 	//private variables
    private CraftingCategory selectedCategory;
    private Dictionary<CraftingCategory, List<Recipe>> allRecipes = new Dictionary<CraftingCategory,List<Recipe>>();

    private List<RecipeMenuItem> recipeMenuItems = new List<RecipeMenuItem>();

    private float defaultHeight;
    private float spacingHeight;
    private float recipeHeight;

 	//Unity methods
    void Awake()
    {
        Recipe[] recipes = Resources.LoadAll<Recipe>("Recipes");
        for (int i = 0; i < recipes.Length; i++)
        {
            Recipe r = recipes[i];

            if (allRecipes.ContainsKey(r.craftingCategory))
            {
                allRecipes[r.craftingCategory].Add(r);
            }
            else
            {
                List<Recipe> list = new List<Recipe>();
                list.Add(r);

                allRecipes.Add(r.craftingCategory, list);
            }
        }

        int maxCount = 0;
        foreach(KeyValuePair<CraftingCategory, List<Recipe>> pair in allRecipes)
        {
            if (pair.Value.Count > maxCount)
                maxCount = pair.Value.Count;
        }

        for (int i = 0; i < maxCount; i++)
        {
            GameObject recipeObj = Instantiate(recipeMenuItem, recipeMenuItemParent) as GameObject;
            RecipeMenuItem item = recipeObj.GetComponent<RecipeMenuItem>();
            if (item != null)
                recipeMenuItems.Add(item);
        }
        SelectCraftingCategory(selectedCategory);
    }
	void Start () 
	{
        selectedRecipeBox.gameObject.SetActive(false);
        CanvasHelper.Singleton.SubscribeToMenuClose(OnMenuClose);

        defaultHeight = 85f;
        spacingHeight = recipeMenuItemParent.GetComponent<VerticalLayoutGroup>().spacing;
        recipeHeight = recipeMenuItem.GetComponent<RectTransform>().sizeDelta.y;

        SelectCraftingCategory(CraftingCategory.Nothing);
	}


	//public methods
    public void SelectCategoryTools(Image image)
    {
        ComponentManager<SoundManager>.Get().PlaySound("UIClick");
        SelectCraftingCategory(CraftingCategory.Tools);
        selectedCategoryImage.sprite = image.sprite;
    }
    public void SelectCategoryPlaceables(Image image)
    {
        ComponentManager<SoundManager>.Get().PlaySound("UIClick");
        SelectCraftingCategory(CraftingCategory.Placeables);
        selectedCategoryImage.sprite = image.sprite;
    }
    public void SelectCategoryBasics(Image image)
    {
        ComponentManager<SoundManager>.Get().PlaySound("UIClick");
        SelectCraftingCategory(CraftingCategory.Basics);
        selectedCategoryImage.sprite = image.sprite;
    }
    public void SelectCategoryDecorations(Image image)
    {
        ComponentManager<SoundManager>.Get().PlaySound("UIClick");
        SelectCraftingCategory(CraftingCategory.Decorations);

    }
    public void SelectCategoryEquipment(Image image)
    {
        ComponentManager<SoundManager>.Get().PlaySound("UIClick");
        SelectCraftingCategory(CraftingCategory.Equipment);        
        selectedCategoryImage.sprite = image.sprite;
    }
    public void SelectCraftingCategory(CraftingCategory category)
    {
        if (category == CraftingCategory.Nothing)
        {
            recipeMenuItemParent.gameObject.SetActive(false);
            return;
        }

        if (!allRecipes.ContainsKey(category))
            return;
        recipeMenuItemParent.gameObject.SetActive(true);
        selectedCategoryText.text = category.ToString();

        List<Recipe> recipes = allRecipes[category];
        selectedCategory = category;

        foreach (RecipeMenuItem recipeItem in recipeMenuItems)
            recipeItem.gameObject.SetActive(false);

        for (int i = 0; i < recipes.Count; i++)
        {
            recipeMenuItems[i].gameObject.SetActive(true);
            recipeMenuItems[i].AttachRecipe(recipes[i]);
        }

        Vector2 size = recipeMenuItemParent.sizeDelta;
        size.y = defaultHeight + (recipeHeight * recipes.Count) + (spacingHeight * recipes.Count);
        recipeMenuItemParent.sizeDelta = size;
    }
    public void SelectRecipe(Recipe recipe)
    {
        if (recipe == null)
            return;

        ComponentManager<SoundManager>.Get().PlaySound("UIClick");;
        Recipe selected = selectedRecipeBox.selectedRecipe;
        if (selected == null || selected != recipe)
        {
            selectedRecipeBox.DisplayRecipe(recipe);
        }
        else if (selected == recipe)
        {
            selectedRecipeBox.DisplayRecipe(null);
        }
    }
    public void CraftItem()
    {
        if (selectedRecipeBox.selectedRecipe == null)
            return;

        Recipe recipeToCraft = selectedRecipeBox.selectedRecipe;

        PlayerInventory playerInventory = ComponentManager<PlayerInventory>.Get();
        foreach(Cost cost in recipeToCraft.cost)
        {
            playerInventory.RemoveItem(cost.item.UniqueName, cost.amount);
        }

        playerInventory.AddItem(recipeToCraft.itemToCraft.item.UniqueName, recipeToCraft.itemToCraft.amount);
    }

	//private methods
    private void OnMenuClose()
    {
        if (GameManager.IsInMenu)
        {
            selectedRecipeBox.DisplayRecipe(null);
        }
    }
}
