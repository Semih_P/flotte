﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SelectedRecipeBox : MonoBehaviour
 {
 	//public variables
    public Text recipeLabel;
    public Text recipeDescription;
    public Image recipeImage;
    public Button craftButton;

    public List<ItemCostBox> costBoxes = new List<ItemCostBox>();

    [HideInInspector]
    public Recipe selectedRecipe;

 	//private variables
    private PlayerInventory playerInventory;

 	//Unity methods
	void Start () 
	{
        playerInventory = ComponentManager<PlayerInventory>.Get();
	}
	void Update () 
	{
        if (selectedRecipe == null)
            return;

        bool hasEnoughResource = true;
        foreach(Cost cost in selectedRecipe.cost)
        {
            int invCount = playerInventory.GetItemCount(cost.item.UniqueName);

            if (invCount < cost.amount)
            {
                hasEnoughResource = false;
                break;
            }
        }

        craftButton.interactable = hasEnoughResource;
	}


	//public methods
    public void DisplayRecipe(Recipe recipe)
    {
        selectedRecipe = recipe;
        if (selectedRecipe == null)
        {
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.SetActive(true);
        }

        recipeLabel.text = recipe.itemToCraft.item.settings_Inventory.GetDisplayName();
        recipeDescription.text = recipe.itemToCraft.item.settings_Inventory.GetDescription();
        recipeImage.sprite = recipe.itemToCraft.item.settings_Inventory.GetSprite();

        foreach(ItemCostBox box in costBoxes)
        {
            box.gameObject.SetActive(false);
        }

        for (int i = 0; i < recipe.cost.Count; i++)
        {
            costBoxes[i].gameObject.SetActive(true);
            costBoxes[i].SetCost(recipe.cost[i]);
        }
    }

	//private methods
}
