﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pickup : MonoBehaviour
 {
 	//public variables
    public LayerMask pickupMask;

 	//private variables
    private PlayerInventory playerInventory;
    private PlayerAnimator playerAnimator;
    private CanvasHelper canvasHelper;
    private GameObject prevItem;
    private float playerUseDistance;
    private Network_Player playerNetwork;
    private PickupObjectManager network_PickupObject;

 	//Unity methods
    void Awake()
    {
        playerUseDistance = Player.UseDistance;
    }
    void Start()
    {
        playerNetwork = GetComponent<Network_Player>();
        playerAnimator = playerNetwork.Animator;
        canvasHelper = CanvasHelper.Singleton;
        playerInventory = ComponentManager<PlayerInventory>.Get();
        network_PickupObject = ComponentManager<PickupObjectManager>.Get();
    }
	void Update () 
	{
        if (!playerNetwork.IsLocalPlayer)
            return;

        //Aquire transform at cursor
        RaycastHit hit = Helper.HitAtCursor(playerUseDistance, pickupMask);
        PickupItem pickedUpItem = null;

        if (hit.transform != null)
        {
            //Check to see if the transform hit can be picked up
            pickedUpItem = hit.transform.GetComponent<PickupItem>();
            if (pickedUpItem != null && pickedUpItem.canBePickedUp)
            {
                if (!(pickedUpItem is ItemNet))
                {
                    string displayText = !pickedUpItem.itemInstance.IsValid() ? pickedUpItem.item.settings_Inventory.GetDisplayName() : pickedUpItem.itemInstance.settings_Inventory.GetDisplayName();
                    canvasHelper.SetDisplayText("Pickup\n" + "'" + displayText + "'", true);
                }
            }

            if (prevItem != hit.transform.gameObject)
                prevItem = hit.transform.gameObject;
        }
        else
        {
            //Set display text to nothing
            if (prevItem != null)
            {
                prevItem = null;
                canvasHelper.SetDisplayText(false);
            }
        }

        //Pickup key pressed
        if (Input.GetButtonDown("UseButton") && hit.transform != null)
        {
            if (pickedUpItem != null)
            {
                if (pickedUpItem is ItemNet)
                    PickupItemNet(pickedUpItem as ItemNet);
                else
                    PickupItem(pickedUpItem);
            }
        }
	}


	//public methods


	//private methods
    private void PickupItem(PickupItem pickup)
    {
        if (!pickup.canBePickedUp)
            return;
        playerAnimator.SetAnimation(PlayerAnimation.Trigger_GrabItem);

        if (pickup.dropper != null)
        {
            //Add item separatly because of random dropper chance
            for (int i = 0; i < pickup.amount; i++)
            {
                Item_Base itemToPickup = pickup.GetItem();
                playerInventory.AddItem(itemToPickup.UniqueName, 1);
            }
        }
        else
        {
            if (pickup.itemInstance.IsValid())
                playerInventory.AddItem(pickup.itemInstance);
            else
                playerInventory.AddItem(pickup.item.UniqueName, pickup.amount);
        }

        if (pickup.networkBehaviour != null)
            NetworkUpdateManager.SetBehaviourDead(pickup.networkBehaviour);
        else if (pickup.onReef)
        {
            Network_Reef netReef = pickup.GetComponentInParent<Network_Reef>();
            if (playerNetwork.Network.IsHost)
                netReef.RemoveItem(pickup, true);
            else
                playerNetwork.SendP2P(new Message_RemoveItem(Messages.RemoveItem, netReef, netReef.reef.pickups.IndexOf(pickup)), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        }
        else if (pickup.trackedByPickupManager)
        {
            if (playerNetwork.Network.IsHost)
                network_PickupObject.RemovePickupItem(pickup, true);
            else
                playerNetwork.SendP2P(new Message_RemoveItem(Messages.RemoveItem, network_PickupObject, PickupObjectManager.trackedPickupItems.IndexOf(pickup)), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

            canvasHelper.SetDisplayText(false);
        }
        else
            PoolManager.ReturnObject(pickup.gameObject);

    }
    private void PickupItemNet(ItemNet itemNet)
    {
        itemNet.itemCollector.AddCollectedItemsToInventory(playerInventory);
    }
}
