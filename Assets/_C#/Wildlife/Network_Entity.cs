﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Network_Entity : MonoBehaviour_NetworkTick 
{
	//public variables
    [Header("Network entity settings")]
    [SerializeField] protected Stat_Health stat_health;
    public bool IsDead { get { return stat_health.Value <= 0; } }
	//private variables

	//Unity methods

	//public methods
    public virtual void Damage(float damage)
    {
        stat_health.Value -= damage;
    }
	//private methods
}

[System.Serializable]
public class Message_DamageEntity : Message_NetworkBehaviour
{
    public int damage;

    public Message_DamageEntity(Messages type, MonoBehaviour_Network behaviour, int damage)
        : base(type, behaviour)
    {
        this.damage = damage;
    }
}
