﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EelHome : MonoBehaviour 
{
	//public variables

	//private variables
    private Eel eel;

	//Unity methods
	void Start () 
	{
        eel = GetComponentInChildren<Eel>();
        eel.homePoint = transform.position;
	}
    void OnTriggerEnter(Collider col)
    {
        if (col.transform.tag == "Player")
        {
            PlayerStats player = col.transform.GetComponent<PlayerStats>();
            eel.OnPlayerEnter(player);
        }
    }

	//public methods

	//private methods
}
