﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Eel : Entity 
{
	//public variables
    [HideInInspector]
    public Vector3 homePoint;

    [Header("Animator")]
    [SerializeField] private Animator animator = null;

    [Header("Movement settings")]
    [SerializeField] private float savePositionInterval = 1.5f;
    [SerializeField] private float rotationSpeed = 90f;
    [SerializeField] private float movementSpeed = 4;
    [SerializeField] private float maxDistanceFromHome = 15f;

    [Header("Attack settings")]
    [SerializeField] private float attackDistance = 1f;
    [SerializeField] private float attackInterval = 1;
    [SerializeField] private int attackDamage = 5;
    [SerializeField] private ParticleController hitParticles = null;

    [Header("Gizmo settings")]
    public float wayPointSize;

	//private variables
    private List<Vector3> previousPositions;
    private PersonController personController;
    private PlayerStats targetPlayer;
    private bool isChasing;
    private float positionTimer;
    private bool isReturning;
    private bool hasBitten = false;
    private bool canBite = true;
    private Vector3 returnPoint;
    private Quaternion originalRotation;
    private Vector3 originalPosition;

	//Unity methods
    protected override void Start()
    {
        base.Start();
        originalRotation = transform.rotation;
        originalPosition = transform.position;
        isReturning = false;
        isChasing = false;
        targetPlayer = null;
        previousPositions = new List<Vector3>();
    }
	void Update() 
	{
        if (IsDead())
            return;

        if (isChasing)
        {
            Chase();
        }
        else if (isReturning)
        {
            Return();
        }
	}
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        if (previousPositions != null && previousPositions.Count > 0)
        {
            for (int i = 0; i < previousPositions.Count; i++)
            {
                Gizmos.DrawSphere(previousPositions[i], wayPointSize);

                if (i < previousPositions.Count - 1)
                    Gizmos.DrawLine(previousPositions[i], previousPositions[i + 1]);
            }
        }

        if (homePoint == Vector3.zero)
            Gizmos.DrawWireSphere(transform.position, maxDistanceFromHome);
        else
            Gizmos.DrawWireSphere(homePoint, maxDistanceFromHome);
    }

	//public methods
    public void OnPlayerEnter(PlayerStats player)
    {
        if (isChasing)
            return;

        previousPositions.Clear();
        previousPositions.Add(homePoint);
        
        isChasing = true;
        isReturning = false;

        positionTimer = 0;

        //Set swim animation
        animator.SetBool("Swimming", true);

        //Save player ref
        personController = player.transform.GetComponent<PersonController>();
        targetPlayer = player;
    }

	//private methods
    private void Chase()
    {
        bool hasVisionToPlayer = Helper.HasClearVision(transform.position, targetPlayer.transform.position, LayerMasks.MASK_Obstruction, Vector3.Distance(transform.position, targetPlayer.transform.position));
        //Return home if....
        if (!isReturning && (
            targetPlayer == null || 
            targetPlayer.IsDead || 
            DistanceFromHome() >= maxDistanceFromHome ||
            personController.controllerType == ControllerType.Ground ||
            (!hasVisionToPlayer && previousPositions.Count > 3)))
        {
            ReturnHome();
            return;
        }

        //Save previous positions so we can backtrack
        SavePositions();

        //Start attack animation if eel is within distance
        if (DistanceToPlayer() < attackDistance)
        {
            if (canBite)
            {
                canBite = false;
                animator.SetInteger("Variation", Random.Range(1, 3));
                animator.SetTrigger("Attack");
            }
        }

        if (!hasBitten)
        {
            //Move towards attack range
            MoveTowards(targetPlayer.transform.position + Vector3.up * 0.7f);
        }
        else
        {
            MoveTowards(returnPoint);
            if (Vector3.Distance(transform.position, returnPoint) <= 0.5f)
            {
                hasBitten = false;
                canBite = true;
            }
        }
    }
    private void Return()
    {
        //Find new point to follow
        if (returnPoint == Vector3.zero)
        {
            returnPoint = GetReturnPoint();
        }
        else
        {
            //Check if we have reached our desired point
            float distanceToPoint = Vector3.Distance(returnPoint, transform.position);
            if (distanceToPoint <= 0.3f)
            {
                //Is the point our home?
                if (returnPoint == homePoint)
                {
                    ResetToHome();
                }
                //It was not our home, find a new point
                else
                {
                    returnPoint = Vector3.zero;
                }
            }
        }

        //Move to point
        MoveTowards(returnPoint);
    }

    private void MoveTowards(Vector3 position)
    {
        //Find direction towards player
        Vector3 directionToPlayer = position - transform.position;

        //Rotate towards target direction
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(directionToPlayer), rotationSpeed * Time.deltaTime);

        //Move forward
        transform.position += transform.forward * movementSpeed * Time.deltaTime;
    }
    private void SavePositions()
    {
        //Save positions at set intervals
        positionTimer += Time.deltaTime;
        if (positionTimer >= savePositionInterval)
        {
            positionTimer -= savePositionInterval;
            previousPositions.Add(transform.position);
        }
    }
    private float DistanceToPlayer()
    {
        return Vector3.Distance(transform.position, targetPlayer.transform.position);
    }
    private float DistanceFromHome()
    {
        return Vector3.Distance(homePoint, transform.position);
    }

    //Called from animation
    public void Attack()
    {
        if (targetPlayer == null)
        {
            ReturnHome();
            return;
        }

        targetPlayer.Damage(attackDamage);

        //Find a random point to move towards before next bite
        Vector3 point = Vector3.zero;
        do
        {
            //Find a new point
            point = targetPlayer.transform.position + Random.insideUnitSphere * 10;

            //Make sure point is under water
            if (point.y > -1)
                point.y = -1;
        }
        //Check to see if there is vision to the point
        while (!Helper.HasClearVision(transform.position, point, LayerMasks.MASK_Obstruction));
        returnPoint = point;

        hasBitten = true;
    }

    private void ReturnHome()
    {
        isReturning = true;
        isChasing = false;
        targetPlayer = null;
        returnPoint = Vector3.zero;
    }
    private void ResetToHome()
    {
        //Set swim animation
        animator.SetBool("Swimming", false);

        transform.position = originalPosition;
        transform.rotation = originalRotation;

        isChasing = false;
        isReturning = false;
        targetPlayer = null;
        positionTimer = 0;
    }
    private Vector3 GetReturnPoint()
    {
        //Find closest point to home which is not obstructed
        for (int i = 0; i < previousPositions.Count; i++)
        {
            Vector3 point = previousPositions[i];
            Vector3 directionToPoint = point - transform.position;
            //Physics.Raycast(transform.position, directionToPoint, Vector3.Distance(point, transform.position), LayerMasks.MASK_Obstruction))
            if (Helper.HasClearVision(transform.position, point, LayerMasks.MASK_Obstruction, Vector3.Distance(point, transform.position)))
            {
                return point;
            }
        }

        //If the above failed... Return the last known position
        if (previousPositions.Count > 0)
        {
            return previousPositions.Last();
        }

        //If the above failed.... Failsafe to homepoint directly
        return homePoint;
    }

    protected override void Die()
    {
        base.Die();
        animator.SetBool("Dead", true);
    }
    public override void Damage(float damage)
    {
        base.Damage(damage);

        animator.SetInteger("Variation", Random.Range(1, 3));
        animator.SetTrigger("Hit");

        if (hitParticles != null)
        {
            hitParticles.PlayParticles();
        }
    }
}
