﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EelPart : MonoBehaviour 
{
	//public variables
    public Transform abovePart;

	//private variables
    private float forwardOffset;


	//Unity methods
	void Start () 
	{
        forwardOffset = Vector3.Distance(abovePart.transform.position, transform.position);
        transform.parent = transform.parent.parent;
	}	
	// Update is called once per frame
	void Update () 
	{
        Vector3 targetPos = abovePart.position - abovePart.forward * forwardOffset;
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * 20);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(abovePart.position - transform.position, abovePart.up), Time.deltaTime * 5);
	}

	//public methods

	//private methods
}
