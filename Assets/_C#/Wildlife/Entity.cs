﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour_ID
 {
    [Header("Entity settings")]
 	//public variables

 	//private variables
    [SerializeField] protected Stat_Health stat_health;
    private bool isDead;


 	//Unity methods
	protected virtual void Start () 
	{
	}

    //protected methods
    protected virtual void Die()
    {
        isDead = true;
    }
    public bool IsDead()
    {
        return isDead;
    }

	//public methods
    public virtual void Damage(float damage)
    {
        if (isDead)
            return;

        stat_health.Value -= damage;
    }

	//private methods
}
