﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SharkState { PassiveSurface, PassiveWater, AttackPlayer, Bait, AttackRaft, Dead, Dive, None, DeadSink}
[RequireComponent(typeof(WaterPointGetter))]
public class Shark : Network_Entity
{
 	//public variables
    [Header("Shark settings")]
    [SerializeField] private float newPointMinRadius = 10;
    [SerializeField] private float newPointMaxRadius = 15;
    [SerializeField] private float swimSpeed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private List<Item_Base> yield = new List<Item_Base>();
    [SerializeField] private SharkState state = SharkState.None;
    [SerializeField] private AnimationCurve swimSpeedCurve;
    [SerializeField] private float animationCurveTime = 30f;
    private Vector3 targetPoint;
    private float originalSwimSpeed;

    [Header("Attack raft settings")]
    [SerializeField] private float biteRaftRange = 2f;
    [SerializeField] private int biteRaftDamage = 5;
    [SerializeField] private float biteRaftInterval = 1f;
    [SerializeField] private float searchBlockInterval = 10f;
    [SerializeField] private Vector3 biteRaftOffset = Vector3.zero;
    [SerializeField] private float damageTakenUntilDive = 15;
    private float hitByPlayerDamage = 0;
    private float biteRaftTimer = 0;
    private float searchBlockTimer = 0f;
    private bool bitingRaft = false;
    private Block targetBlock;

    [Header("Attack player settings")]
    [SerializeField] private float playerVisionRange = 10;
    [SerializeField] private float attackPlayerRange = 2;
    [SerializeField] private float driveByDistance = 5f;
    [SerializeField] private int attackPlayerDamage = 10;
    [SerializeField] private float attackSwimSpeedMultiplier = 2f;
    private Vector3 lastKnownPlayerPosition;
    private bool canAttack = true;
    private bool hasBitten = false;

    [Header("Bait settings")]
    public static List<SharkBait> sharkBaitInWorld = new List<SharkBait>();
    [SerializeField] private float baitVisionRange = 20f;
    [SerializeField] private int biteBaitDamage = 5;
    [SerializeField] private float biteBaitCooldown = 30;
    private float biteBaitTimer;
    private bool damagedDuringBait;


    [Space(10)]
    [Header("Components")]
    public Animator animator;
    [SerializeField] private LayerMask obstructionMask = 0;
    [SerializeField] private ParticleController woodParticles;
    [SerializeField] private ParticleController waterSplash;

    [Space(10)]
    [Header("Network settings")]
    [SerializeField] private float networkMovementLerpSpeed = 10f;
    [SerializeField] private float networkRotationLerpSpeed = 5f;
    private Vector3 networkPosition;
    private Vector3 networkRotation;
    [HideInInspector]public List<string> networkAnimTriggers = new List<string>();
    private float targetTurnUpdown, targetTurnRightLeft;

 	//References
    private PersonController targetToAttack;
    private WaterPointGetter waterPoint;
    private Transform centerOfRaftTransform;
    private Semih_Network network;
    private ParticleSystemHider watersplashParticles;

    //private variables
    private float swimcurveTimer;

 	//Unity methods
    void Start()
    {
        originalSwimSpeed = swimSpeed;

        waterPoint = GetComponent<WaterPointGetter>();
        centerOfRaftTransform = GameManager.Singleton.globalBuoyancyParent;

        network = ComponentManager<Semih_Network>.Get();
        ChangeState(SharkState.PassiveWater);
    }
	void Update () 
	{
        if (state == SharkState.None)
            return;

        if (network.IsHost)
            UpdateNetworkTick();

        //Keep shark away from flying above "water level"
        if (state != SharkState.AttackRaft)
        {
            if (transform.position.y > 0)
            {
                transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            }
        }


        if (!network.IsHost)
        {
            transform.position = Vector3.Lerp(transform.position, networkPosition, networkMovementLerpSpeed * Time.deltaTime);
            if (networkRotation != Vector3.zero)
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(networkRotation), networkRotationLerpSpeed * Time.deltaTime);
        }
        else
        {
            swimcurveTimer += Time.deltaTime;
            if (swimcurveTimer > animationCurveTime)
                swimcurveTimer = 0f;
            float curveX = swimcurveTimer / animationCurveTime;
            float curveY = swimSpeedCurve.Evaluate(curveX);
            animator.SetFloat("SwimSpeed", curveY);

            if (swimSpeed > originalSwimSpeed)
                swimSpeed = Mathf.Lerp(swimSpeed, originalSwimSpeed, Time.deltaTime);

            //Find a new target to attack
            SetTargetToAttack();
            if (targetToAttack == null)
                return;

            //Update the correct state
            switch (state)
            {
                case SharkState.PassiveSurface:
                    PassiveSurfaceUpdate();
                    break;
                case SharkState.PassiveWater:
                    PassiveWaterUpdate();
                    break;
                case SharkState.AttackPlayer:
                    AttackPlayerUpdate();
                    break;
                case SharkState.AttackRaft:
                    AttackRaftUpdate();
                    break;
                case SharkState.Dive:
                    DiveUpdate();
                    break;
                case SharkState.DeadSink:
                    DeadSinkUpdate();
                    break;
            }
            BaitUpdate();

        }
	}

    //Entity methods
    public override void Damage(float damage)
    {
        //Change health
        base.Damage(damage);
        if (stat_health.IsDead())
            ChangeState(SharkState.Dead);

        if (state != SharkState.Dead)
        {
            //Check if the shark was damaged during an attack
            if (!canAttack & !hasBitten)
            {
                TriggerAnimation("Damaged");
                canAttack = false;
                hasBitten = true;
                

                //Find random point in water
                targetPoint = GetRandomWaterPointInVision(driveByDistance, driveByDistance, targetToAttack.transform);
            }

            //If the shark is attacking the raft
            if (state == SharkState.AttackRaft)
            {
                if (targetBlock is SharkBait)
                {
                    damagedDuringBait = true;
                    biteBaitTimer = biteBaitCooldown;
                    lastKnownPlayerPosition = targetToAttack.transform.position;
                    ChangeState(SharkState.Dive);
                }
                else
                {
                    hitByPlayerDamage += damage;
                    if (hitByPlayerDamage >= damageTakenUntilDive)
                    {
                        ChangeState(SharkState.Dive);
                    }
                }
            }
        }
        else
        {
            //The shark was hit when it was dead, give the player loot
            if (yield.Count > 0)
            {
                ComponentManager<PlayerInventory>.Get().AddItem(yield[0].UniqueName, 1);
                yield.RemoveAt(0);

                if (yield.Count == 0)
                    ChangeState(SharkState.DeadSink);
            }
        }
    }

	//public methods

	//private methods
    private void PassiveSurfaceUpdate()
    {
        Debug.DrawLine(transform.position, targetPoint, Color.magenta);
        if (targetToAttack.controllerType == ControllerType.Ground)
        {
            //Circulate around aimlessly
            if (MoveTowards(targetPoint))
            {
                targetPoint = GetRandomSurfacePointInVision(newPointMinRadius, newPointMaxRadius);
            }

            //Look for bait in range
            if (CanSearchForBait())
            {
                SharkBait bait = GetBaitInRange();
                if (bait != null)
                {
                    targetBlock = bait;
                    ChangeState(SharkState.AttackRaft);
                    return;
                }
            }

            //Accumulate timer
            searchBlockTimer += Time.deltaTime;
            if (searchBlockTimer >= searchBlockInterval)
            {
                //Search for a block to attack
                if (targetBlock == null)
                {
                    targetBlock = FindBlockToAttack();

                    //We have found a block
                    if (targetBlock != null)
                    {
                        ChangeState(SharkState.AttackRaft);
                        return;
                    }
                }
            }
        }
        else if (targetToAttack.controllerType == ControllerType.Water)
        {
            ChangeState(SharkState.PassiveWater);
            return;
        }
    }
    private void PassiveWaterUpdate()
    {
        Debug.DrawLine(transform.position, targetPoint, Color.magenta);

        //Is the player in the water?
        if (targetToAttack.controllerType == ControllerType.Water)
        {
            //Circulate in water
            if (MoveTowards(targetPoint))
            {
                targetPoint = GetRandomWaterPointInVision(newPointMinRadius, newPointMaxRadius, centerOfRaftTransform);
            }

            //Does the shark have clear vision to the player
            if (Helper.HasClearVisionAndWithinRange(transform.position, targetToAttack.transform.position, obstructionMask, playerVisionRange, playerVisionRange))
            {
                ChangeState(SharkState.AttackPlayer);
                return;
            }

            //Look for bait in range
            if (CanSearchForBait())
            {
                SharkBait bait = GetBaitInRange();
                if (bait != null)
                {
                    targetBlock = bait;
                    ChangeState(SharkState.AttackRaft);
                    return;
                }
            }
        }
        else if (targetToAttack.controllerType == ControllerType.Ground)
        {
            ChangeState(SharkState.PassiveSurface);
            return;
        }
    }
    private void AttackPlayerUpdate()
    {
        Debug.DrawLine(transform.position, targetToAttack.transform.position + targetToAttack.transform.up, Color.green);
        Debug.DrawLine(transform.position, lastKnownPlayerPosition, Color.yellow);
        Debug.DrawLine(transform.position, targetPoint, Color.magenta);

        //Is the player behind an object, i.e. the shark can't see the player
        bool hasVisionToPlayer = Helper.HasClearVisionAndWithinRange(transform.position, targetToAttack.transform.position, obstructionMask, playerVisionRange, playerVisionRange);
        if (!hasVisionToPlayer && !hasBitten)
        {
            //Move towards last known position
            if (MoveTowards(lastKnownPlayerPosition, attackPlayerRange))
            {
                hasVisionToPlayer = Helper.HasClearVisionAndWithinRange(transform.position, targetToAttack.transform.position, obstructionMask, playerVisionRange, playerVisionRange);
                if (!hasVisionToPlayer)
                {
                    ChangeState(SharkState.PassiveWater);
                }
            }

            //If player is on the raft, start circulating surface
            if (targetToAttack.controllerType == ControllerType.Ground)
            {
                ChangeState(SharkState.PassiveSurface);
            }

            return;
        }

        //Is the player in the water?
        if (targetToAttack.controllerType == ControllerType.Water)
        {
            if (canAttack && !hasBitten)
            {
                //Set target point
                Vector3 playerPos = (targetToAttack.transform.position + targetToAttack.transform.up);
                Vector3 dirToPlayer = playerPos - transform.position;
                Debug.DrawRay(transform.position, dirToPlayer, Color.white);

                targetPoint = playerPos + dirToPlayer * 1;

                if (targetPoint.y > 0)
                    targetPoint.y = -1;
                lastKnownPlayerPosition = playerPos;

                if (MoveTowards(targetPoint, attackPlayerRange + 7.5f))
                {
                    swimSpeed *= attackSwimSpeedMultiplier;
                    canAttack = false;
                    TriggerAnimation("Attack");
                }
            }
            else
            {
                if (MoveTowards(targetPoint))
                {
                    hasBitten = false;
                    canAttack = true;
                }
            }
        }
        else if (targetToAttack.controllerType == ControllerType.Ground)
        {
            ChangeState(SharkState.PassiveSurface);
            return;
        }
    }
    private void AttackRaftUpdate()
    {
        Debug.DrawLine(transform.position, targetPoint, Color.red);

        //If the block is null or destroyed, return to the water
        if (targetBlock == null || targetBlock.health <= 0 || (targetBlock is SharkBait && targetBlock.health <= 0))
        {
            bitingRaft = false;
            animator.SetBool("BitingRaft", bitingRaft);
            if (waterSplash != null)
                waterSplash.StopParticles();
            ChangeState(SharkState.Dive);
            return;
        }

        //If the shark is not biting the raft, move closer to the targeted block
        if (!bitingRaft)
        {
            //Set destination to the blocks position
            targetPoint = targetBlock.transform.position;

            MoveTowards(targetPoint);

            //Check bite range
            if (Vector3.Distance(transform.position, targetPoint) < biteRaftRange)
            {
                //Start biting
                if (targetBlock is SharkBait)
                    transform.SetParent(targetBlock.transform);
                else
                    transform.SetParent(GameManager.Singleton.globalBuoyancyParent);

                bitingRaft = true;
            }
        }
        else
        {
            //Set new position so that the shark bites the foundation correctly
            Vector3 blockPos = targetBlock.transform.position;
            Vector3 sharkPos = transform.position;
            sharkPos.y = blockPos.y;

            targetPoint = targetBlock.transform.position;
            targetPoint.y += biteRaftOffset.y;

            Vector3 dir = blockPos - sharkPos;
            targetPoint = targetPoint - dir.normalized * biteRaftOffset.z;

            if (Vector3.Distance(transform.position, targetPoint) > 0.05f)
            {
                //Move to correct position on block
                transform.position = Vector3.Lerp(transform.position, targetPoint, Time.deltaTime * swimSpeed);
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, transform.eulerAngles.y, 0), Time.deltaTime * swimSpeed);
            }
            else
            {
                if (waterSplash != null && !waterSplash.IsPlaying)
                    waterSplash.PlayParticles();
            }

            //Damage block
            biteRaftTimer += Time.deltaTime;
            if (biteRaftTimer >= biteRaftInterval)
            {
                biteRaftTimer -= biteRaftInterval;
                if (woodParticles != null)
                    woodParticles.PlayParticles();

                //Remove block as parent if the block is about to be destroyed
                if (targetBlock.health - biteRaftDamage <= 0)
                {
                    transform.SetParent(null);
                }

                //Damage the block
                if (targetBlock is SharkBait)
                {
                    targetBlock.health -= biteBaitDamage;
                }
                else
                    targetBlock.Damage(biteRaftDamage);
            }
        }

        //Set the animator
        animator.SetBool("BitingRaft", bitingRaft);
    }
    private void BaitUpdate()
    {
        if (biteBaitTimer > 0)
            biteBaitTimer -= Time.deltaTime;
    }
    private void DiveUpdate()
    {
        if (MoveTowards(targetPoint, 1.5f))
        {
            if (damagedDuringBait)
            {
                damagedDuringBait = false;
                ChangeState(SharkState.AttackPlayer);
                return;
            }

            if (targetToAttack.controllerType == ControllerType.Water)
            {
                ChangeState(SharkState.PassiveWater);
            }
            else
            {
                ChangeState(SharkState.PassiveSurface);
            }
        }
    }
    private void DeadSinkUpdate()
    {
        transform.position += Vector3.down * Time.deltaTime * 5f;
        if (transform.position.y < GameManager.OceanBottom)
        {
            ChangeState(SharkState.None);
            NetworkUpdateManager.SetBehaviourDead(this);
        }
    }

    private void ChangeState(SharkState newState)
    {
        if (newState == state)
            return;

        switch(newState)
        {
            case SharkState.PassiveSurface:
                targetPoint = GetRandomSurfacePointInVision(newPointMinRadius, newPointMaxRadius);
                searchBlockTimer = 0;
                break;

            case SharkState.PassiveWater:
                targetPoint = GetRandomWaterPointInVision(newPointMinRadius, newPointMaxRadius, centerOfRaftTransform);
                break;

            case SharkState.AttackPlayer:
                canAttack = true;
                hasBitten = false;
                break;

            case SharkState.AttackRaft:
                biteRaftTimer = 0f;
                bitingRaft = false;
                animator.SetBool("BitingRaft", bitingRaft);
                break;

            case SharkState.Dead:
                TriggerAnimation("Dead");

                if (waterSplash != null)
                    waterSplash.StopParticles();
                if (woodParticles != null)
                    woodParticles.StopParticles();
                break;

            case SharkState.Dive:
                targetBlock = null;
                transform.SetParent(null);
                biteRaftTimer = 0f;
                hitByPlayerDamage = 0f;
                bitingRaft = false;
                animator.SetBool("BitingRaft", bitingRaft);

                RaycastHit hit;
                Vector3 dir = (transform.position + Vector3.down * 10 + transform.forward * 5);
                if (Physics.Raycast(transform.position, dir, out hit, float.PositiveInfinity, obstructionMask))
                {
                    targetPoint = hit.point;
                }
                else
                {
                    targetPoint = transform.position + dir;
                }
                break;
        }
        state = newState;
    }
    private bool MoveTowards(Vector3 point, float reachedPointDistance = 3f)
    {
        float distance = Vector3.Distance(point, transform.position);
        if (distance <= reachedPointDistance)
            return true;

        Vector3 direction = point - transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(direction);

        float lastYAngle = transform.eulerAngles.y;
        float lastXAngle = transform.eulerAngles.x;
        transform.position += transform.forward * swimSpeed * Time.deltaTime;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);

        if (network.IsHost)
        {
            //Set animation Y
            int rotDir = 0;
            float angleDiff = Mathf.Abs(transform.eulerAngles.y - lastYAngle);
            if (angleDiff < 0.15f)
                rotDir = 0;
            else
            {
                if (transform.eulerAngles.y > lastYAngle)
                    rotDir = 1;
                else
                    rotDir = -1;

                if (angleDiff > 180)
                    rotDir *= -1;
            }
            animator.SetFloat("TurnRightLeft", Mathf.Lerp(animator.GetFloat("TurnRightLeft"), rotDir, Time.deltaTime));

            //Set animation X
            angleDiff = Mathf.Abs(transform.eulerAngles.x - lastXAngle);
            if (angleDiff < 0.25f)
                rotDir = 0;
            else
            {
                if (transform.eulerAngles.y > lastYAngle)
                    rotDir = 1;
                else
                    rotDir = -1;

                if (angleDiff > 180)
                    rotDir *= -1;
            }
            animator.SetFloat("TurnUpDown", Mathf.Lerp(animator.GetFloat("TurnUpDown"), rotDir, Time.deltaTime));
        }
        else
        {
            animator.SetFloat("TurnUpDown", Mathf.Lerp(animator.GetFloat("TurnUpDown"), targetTurnUpdown, Time.deltaTime));
            animator.SetFloat("TurnRightLeft", Mathf.Lerp(animator.GetFloat("TurnRightLeft"), targetTurnRightLeft, Time.deltaTime));
        }
        return false;
    }
    private Block FindBlockToAttack()
    {
        Vector3 center = centerOfRaftTransform.position;
        Vector3 direction = center - transform.position;
        RaycastHit hit;

        if (Physics.Raycast(transform.position, direction.normalized, out hit, float.PositiveInfinity, LayerMasks.MASK_block))
        {
            Block block = hit.transform.GetComponentInParent<Block>();
            return block;
        }
        return null;
    }
    private Vector3 GetRandomSurfacePointInVision(float minRadius, float maxRadius)
    {
        float waterLevel = waterPoint.GetWaterPoint(transform.position);
        Vector3 point = GetRandomPointInVisionAroundTarget(centerOfRaftTransform.position, minRadius, maxRadius);

        //Make sure point is not above surface
        if (point.y > waterLevel)
            point.y = waterLevel - 1;
        //Make sure point is not to deep, we want to circulate around the surface
        else if (point.y < waterLevel - 2)
            point.y = waterLevel - 2;

        return point;
    }
    private Vector3 GetRandomWaterPointInVision(float minRadius, float maxRadius, Transform originTarget)
    {
        float waterLevel = waterPoint.GetWaterPoint(transform.position);
        Vector3 point = GetRandomPointInVisionAroundTarget(originTarget.position, minRadius, maxRadius);

        //Make sure point is not above surface
        if (point.y > waterLevel)
            point.y = waterLevel - 1;
        //Make sure point is not below bottom
        else if (point.y < GameManager.OceanBottom + 10)
            point.y = GameManager.OceanBottom + 10;

        return point;
    }
    private Vector3 GetRandomPointInVisionAroundTarget(Vector3 target, float minRadius, float maxRadius)
    {
        Vector3 point = Vector3.zero;
        do
        {
            //Find a new point
            point = Helper.GetRandomPointAroundOrigin(target, minRadius, maxRadius);
        }
        //Check to see if there is vision to the point
        while (!Helper.HasClearVision(transform.position, point, obstructionMask));
        return point;
    }
    private SharkBait GetBaitInRange()
    {
        for (int i = 0; i < sharkBaitInWorld.Count; i++)
        {
            SharkBait bait = sharkBaitInWorld[i];
            if (bait == null)
            {
                sharkBaitInWorld.RemoveAt(i);
                i--;
                continue;
            }

            bool inRangeAndVision = Helper.HasClearVisionAndWithinRange(transform.position, bait.transform.position, obstructionMask, baitVisionRange, baitVisionRange);
            if (inRangeAndVision)
            {
                return bait;
            }
        }
        return null;
    }
    private bool CanSearchForBait()
    {
        return biteBaitTimer <= 0;
    }
    private void SetTargetToAttack()
    {
        foreach(KeyValuePair<Steamworks.CSteamID, Network_Player> pair in network.remoteUsers)
        {
            PersonController controller = pair.Value.Controller;

            //If we do not have a target, grab the first one
            if (targetToAttack == null)
            {
                targetToAttack = controller;
                continue;
            }

            if (controller.controllerType == ControllerType.Water)
            {
                //Prioritize targets in water above ground
                if (targetToAttack.controllerType == ControllerType.Ground)
                {
                    targetToAttack = controller;
                }
                //If current and compared target are in water, pick the closest one
                else if (targetToAttack.controllerType == ControllerType.Water)
                {

                }
            }
        }
    }
    private void TriggerAnimation(string name)
    {
        animator.SetTrigger(name);
        if (!networkAnimTriggers.Contains(name))
            networkAnimTriggers.Add(name);
    }

    //Animation methods
    public void Attack()
    {
        if (!network.IsHost)
            return;

        hasBitten = true;
        canAttack = false;

        //Damage player here
        NetworkUI.Singleton.Console("Sending damage to " + targetToAttack.transform.name);
        network.SendP2P(network.HostID, new Message_DamageEntity(Messages.DamageEntity, targetToAttack.GetComponent<Network_Entity>(), attackPlayerDamage), Steamworks.EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

        //Find random point in water
        targetPoint = GetRandomWaterPointInVision(driveByDistance, driveByDistance, targetToAttack.transform);
    }

    //Network methods
    public override void Serialize_Create()
    {
        AddMessage(new Message_Shark(Messages.CreateShark, this, this));
    }
    public override void Serialize_Update()
    {
        AddMessage(new Message_Shark(Messages.UpdateShark, this, this));
    }
    public override void Deserialize(Message_NetworkBehaviour msg)
    {
        switch(msg.type)
        {
            case Messages.UpdateShark:
                Message_Shark msgShark = msg as Message_Shark;
                networkPosition = new Vector3(msgShark.posX, msgShark.posY, msgShark.posZ);
                networkRotation = new Vector3(msgShark.rotX, msgShark.rotY, msgShark.rotZ);

                animator.SetBool("BitingRaft", msgShark.anim_BitingRaft);
                foreach (string trigger in msgShark.anim_triggers)
                {
                    animator.SetTrigger(trigger);
                }
                targetTurnRightLeft = msgShark.anim_turnrightleft;
                targetTurnUpdown = msgShark.anim_turnupdown;
                break;

            case Messages.DamageEntity:
                Message_DamageEntity msgDmgEntity = msg as Message_DamageEntity;
                Damage(msgDmgEntity.damage);
                break;

            case Messages.RemoveNetworkBehaviour:
                Destroy(gameObject);
                break;
        }
    }
}

[System.Serializable]
public class Message_Shark : Message_NetworkBehaviour
{
    public float posX, posY, posZ;
    public float rotX, rotY, rotZ;
    public float anim_turnupdown, anim_turnrightleft;
    public bool anim_BitingRaft;
    public string[] anim_triggers;

    public Message_Shark(Messages type, MonoBehaviour_Network behaviour, Shark shark)
        : base(type, behaviour)
    {
        posX = shark.transform.position.x;
        posY = shark.transform.position.y;
        posZ = shark.transform.position.z;

        rotX = shark.transform.forward.x;
        rotY = shark.transform.forward.y;
        rotZ = shark.transform.forward.z;

        anim_turnupdown = shark.animator.GetFloat("TurnUpDown");
        anim_turnrightleft = shark.animator.GetFloat("TurnRightLeft");
        anim_BitingRaft = shark.animator.GetBool("BitingRaft");
        anim_triggers = shark.networkAnimTriggers.ToArray();
        shark.networkAnimTriggers.Clear();
    }
}
